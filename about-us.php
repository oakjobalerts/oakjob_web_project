<?php ob_start();
require_once('include/configuration.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="At <?=TITLE?>, we help people find a new job and employers hire the right candidates through our strict job search algorithm. ">
    <meta name="author" content="">

    <title>About Us | <?=TITLE?></title>

    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/oakstyle.css" rel="stylesheet">
    <link rel="shortcut icon" href="<?=FAVICONPATH?>favicon.ico" type="image/x-icon">
    <!-- Custom Fonts -->
    <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- UserVoice JavaScript SDK (only needed once on a page) -->
<script>(function(){var uv=document.createElement('script');uv.type='text/javascript';uv.async=true;uv.src='//widget.uservoice.com/RUVxfxpYSZzV74j6P7cENw.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(uv,s)})()</script>

<!-- A link to launch the Classic Widget -->
<style>
    p
    {
        font-size: 17px !important;
    }
    
</style>

<?php
if(DOMAIN == '2ndchancejobalerts.com')
{ 
    ?>
        	<!-- Google Tag Manager -->
			
			<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','GTM-5TGHSS');</script>
			<!-- End Google Tag Manager -->
<?php
}
?>

</head>

<body id="page-top" class="index">
	
<?php
if(DOMAIN == '2ndchancejobalerts.com')
{
	?>
		  <!-- Google Tag Manager -->
            <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5TGHSS"
            height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<?php
}

?>
<?php
    include('include/common.php');
?>
    <!-- Navigation -->
    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo $baseurl; ?>"><img src="<?=IMAGE_BASE_URL?>/logo1.png"><?=HEADER_TITLE?></a>
               
                
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                
                
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                   <!-- <li class="page-scroll">
                        <a href="#portfolio">Portfolio</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#about">About</a>
                    </li>-->
                    <li class="page-scroll">
                       
                    </li>
                </ul>

                
                
                
            </div>
            <!-- /.navbar-collapse -->
        </div>
        
        
        <!-- /.container-fluid -->
    </nav>

    <!-- section -->
    <section>
        <h3 style="text-align:center; margin-bottom: 0px !important;">About Us</h3>
        <div class="container" style="text-align: left;">
          
            <div class="row">
                
                <p style="text-align:justify">At <?=TITLE?>, we help people find a new job and employers hire the right candidates through our strict job search algorithm.  We are both a job aggregator and a job search engine but we like to think of ourselves as a facilitator in the recruitment industry.
</p>
                
                <p style="text-align:justify">We match the two million jobs that we receive from our partners with the ten million users who utilize our services.  Therefore, our results will closely align with what our users are looking for.  We pledge that we will neither distract our users with unwanted advertising nor will we include unwelcome signup forms, lead generation or other ancillary interstitial products that distract you from viewing and applying to jobs.
</p>
               
                <p style="text-align:justify">We build trust with our users and drive quality traffic to our advertisers.  We pledge to our partners that we will not develop a competitive sales team and will not use the data we generate to their detriment.  Our vision is to help our partners achieve their goals by fulfilling a need in the marketplace.  We want to be a trusted provider who will not become their competitor.
</p>
              
                <p style="text-align:justify">The founders at <?=TITLE?> each have over a decade of experience in the recruitment industry.  One of their biggest joys in life is helping people find jobs.
 
</p>
              
            </div>
    </section>

   

    <?php include("include/footer.php"); ?>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top page-scroll visible-xs visble-sm">
        <a class="btn btn-primary" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>

   <!-- jQuery Version 1.11.0 -->
    <script src="js/jquery-1.11.0.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    

    <!-- Custom Theme JavaScript -->
    <script src="js/oak.js"></script> 
    
    
    
    <script type="text/javascript">
        function validateForm(){
            if($('#what').val() == ''){
                $('#where').css('border-color','#ccc');
                $('#where').css('border-bottom-color','#aaa');
                $('#where').css('border-right-color','#aaa');
                $('#what').css('border-color','red');
                return false;
            } else if($('#where').val() == ''){
                $('#what').css('border-color','#ccc');
                $('#what').css('border-bottom-color','#aaa');
                $('#what').css('border-right-color','#aaa');
                $('#where').css('border-color','red');
                return false;
            }
        }

        $('#where').keyup(function(event) {
            if(event.keyCode == 37 || event.keyCode == 39) {
            } else {
                var tags = $(this).val().split(',');
                $(this).val(function(index, value) {
                    //alert(value);
                    if (tags.length > 2) {
                        value = value.replace(/,\s*$/, ''); // remove commas from existing input
                        return value; // add commas back in
                    } else {
                        return value; // add commas back in
                    }
                });
            }
       });
    </script>
    

</body>

</html>

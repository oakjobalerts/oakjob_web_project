<?php
ob_start();
require_once('include/DBManager.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title><?=TITLE?></title>

<!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
<link href="css/bootstrap.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="css/oakstyle.css" rel="stylesheet">

<link rel="shortcut icon" href="<?=FAVICONPATH?>favicon.ico" type="image/x-icon">
<link rel="icon" href="<?=FAVICONPATH?>favicon.ico" type="image/x-icon">

<!-- Custom Fonts -->
<link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
<meta name="google-signin-clientid" content="984802659276-pg7bs72pgq5hjm7pgemnrfv5ttg03qns.apps.googleusercontent.com" />
<meta name="google-signin-scope" content="https://www.googleapis.com/auth/plus.login" />
<meta name="google-signin-requestvisibleactions" content="http://schema.org/AddAction" />
<meta name="google-signin-cookiepolicy" content="single_host_origin" />
<!-- jQuery Version 1.11.0 --> 
<script src="js/jquery-1.11.0.js"></script> 

<!-- Bootstrap Core JavaScript --> 
<script src="js/bootstrap.min.js"></script> 

<!-- Plugin JavaScript --> 
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script> 
<script src="js/classie.js"></script> 
<script src="js/cbpAnimatedHeader.js"></script> 

<!-- Contact Form JavaScript --> 
<script src="js/jqBootstrapValidation.js"></script> 

<!-- Custom Theme JavaScript --> 
<script src="js/oak.js"></script>

<!-- UserVoice JavaScript SDK (only needed once on a page) -->
<script>(function(){var uv=document.createElement('script');uv.type='text/javascript';uv.async=true;uv.src='//widget.uservoice.com/RUVxfxpYSZzV74j6P7cENw.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(uv,s)})()</script>

<!-- A link to launch the Classic Widget -->
<?php
if(DOMAIN == '2ndchancejobalerts.com')
{ 
    ?>
       	<!-- Google Tag Manager -->
			
			<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','GTM-5TGHSS');</script>
			<!-- End Google Tag Manager -->
<?php
}
?>

<!-- impelemting arbor pixel-->
<?php
     if(isset($_COOKIE['login_type'])) {
         $pixel = '17028';
         $md5_email = md5($_COOKIE['email']);
         $sha1_email = hash('sha1', $_COOKIE['email']);
         $sha256_email =  hash('sha256', $_COOKIE['email']);
         ?>
            
            <script src="//pippio.com/api/sync?pid=<?=ARBOR_PIXEL?>&it=4&iv=<?php echo  $md5_email; ?>&it=4&iv=<?php echo  $sha1_email; ?>&it=4&iv=<?php echo  $sha256_email; ?>" async></script>
            
         <?php
     }
    ?>



</head>

<body id="page-top" class="listing">
	
<?php
if(DOMAIN == '2ndchancejobalerts.com')
{
	?>
		  <!-- Google Tag Manager -->
            <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5TGHSS"
            height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<?php
}

?>
<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

$objDBManager = new DBManager();//initialize db connection
$objDBManager->createConnection();

$firstname 			= '';
$lastname 			= '';
$email 				= '';
$zip_code 			= '';
$phone 				= '';
$other_fields 		= '';
$education_level	= '';
$education_year		= '';
$citizenship        = '';
$check_age			= '';
$password 			= '';
$alert_check 		= '';
$unique_value 		= '';
$login_type			= '';
$message = '';



if(!empty($_POST)) {

	$firstname 		= $_POST['firstname'];
	$lastname 		= $_POST['lastname'];
  	$zip_code   	        = $_POST['zipcode'];
  	$email 			= $_POST['email'];
	$phone 			= $_POST['phonenumber'];
	// $password 		= md5($_POST['password']);
  if($_POST['password'] != '')
  {
    $password     = md5($_POST['password']);
  }
	$alert_check 	= 'Y';
	$unique_value   = $_POST['unique_value'] == '' ? $_POST['email'] : $_POST['unique_value'];
	$login_type	= $_POST['unique_value'] == '' ? 'normal' : $_POST['login_type'];

	if(isset($_POST['emailalert'])){
		$alert_check = ($_POST['emailalert'])      ?   '1'   :   '0';
	}

                
                $fieldArray = array();
		$fieldArray['firstname']		 = addslashes($firstname);
		$fieldArray['lastname']                  = addslashes($lastname);
                $fieldArray['email'] 			 = addslashes($email);
		$fieldArray['zipcode'] 			 = addslashes($zip_code);
		$fieldArray['phonenumber']	         = addslashes($phone);
		if($password != '')
                $fieldArray['password']		          = $password;
		
		$fieldArray['email_alert']  = $alert_check;
                $where = 'id='.$_POST['profile_id'].''; 
		$objDBManager->updateRecord('tbl_signup_users',$fieldArray,$where,false);
		$message = '<div style="width:38%; text-align:left; color:#7cc243" >Profile has been updated.</div>';
	
        
}

if(isset($_GET['id']) && !empty($_GET['id'])){
	
	$whereFields = array();
        $whereFields['id'] =  base64_decode($_GET['id']);
        $checkUser = $objDBManager->getRecord('tbl_signup_users',$whereFields,false,'');
	$firstname = $checkUser[0]['firstname'];
	$lastname = $checkUser[0]['lastname'];
	$email = $checkUser[0]['email'];
	$zip_code = $checkUser[0]['zipcode'];
	$phone = $checkUser[0]['phonenumber'];
	$email_alert = $checkUser[0]['email_alert'];
}else{
	
	header('Location:index.php');
}



?>
<!-- Navigation -->
<nav class="navbar navbar-default">
  <div class="container"> 
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header page-scroll">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand" href="index.php"><img src="<?=IMAGE_BASE_URL?>/logo1.png" title="<?= TITLE ?> Logo"> <?=HEADER_TITLE?></a></div>
    
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
                 
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <ul class="mobile">
                    <li><a  href="#">Search Jobs</a></li>
                     <?php if(!isset($_COOKIE['login_type'])) {?>
                     <li><a href="sign_in.php">Manage Alerts</a></li>
                     <li><a href="sign_in.php">Saved Jobs</a></li>
                     <?php } else { ?>
                      <li><a href="alert_list.php">Manage Alerts</a></li>
                      <li><a href="savedjobs.php">Saved Jobs</a></li>
                      <li><a class="active" href="profile.php?id=<?=base64_encode($_COOKIE['id']);?>">Manage Profile</a></li>
                      <?php } ?>
                    <li><a href="https://jobalerts.freshdesk.com/support/tickets/new" alt="Help/FAQ">Help/FAQ</a></li>
                    </ul>
                   <!-- <li class="page-scroll">
                        <a href="#portfolio">Portfolio</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#about">About</a>
                    </li>-->
                    <li class="page-scroll">
                        <?php if(!isset($_COOKIE['login_type'])) {?>
                        <a href="sign_in.php" style="color: #fff;text-decoration: none;"><button type="submit" class="btn btn-signin">Sign in</button></a>
                        <?php } else {?>
                        <a href="sign_in.php?logout=true" style="color: #fff;text-decoration: none;"><button type="submit" class="btn btn-signup">Sign out</button></a>
                        <?php }?>
                    </li>
                </ul>
    </div>
    <!-- /.navbar-collapse --> 
  </div>
  <!-- /.container-fluid -->
  
	<div class="secondnav web">
		<ul class="container">
		<li><a  href="index.php">Search Jobs</a></li>
		<?php if(!isset($_COOKIE['login_type'])) {?>
		<li><a href="sign_in.php">Manage Alerts</a></li>
		<li><a href="sign_in.php">Saved Jobs</a></li>
		<?php } else { ?>
		<li><a  href="alert_list.php">Manage Alerts</a></li>
		<li><a href="savedjobs.php">Saved Jobs</a></li>
		<li><a class="active" href="profile.php?id=<?=base64_encode($_COOKIE['id']);?>">Manage Profile</a></li>
		<?php } ?>
		<li><a href="https://jobalerts.freshdesk.com/support/tickets/new" alt="Help/FAQ">Help/FAQ</a></li>
		</ul>
        </div>
  
</nav>

<!-- section -->
<section>
  <div class="container text-left">
    <h4 class="heading">Manage Profile</h4>
    <?=$message?>
   <?php if(isset($_GET['already']) && !empty($_GET['already'])) {?>
    <label class="has-error" style="color: red;">This email address is already registered.</label>
    <?php } ?>
    <div class="row">
      <div class="col-lg-8 sign_left">
        <form role="form" action="" method="post" onsubmit="return formValidation();">
          <div class="row">
            <div class="col-xs-6">
              <label>First Name</label>
              <input type="text" name="firstname" id="firstname" onfocus="$(this).css('border-color','#dce4ec');$(this).attr('placeholder','');" value="<?php echo $firstname;?>" class="form-control input-lg" placeholder="">
            </div>
            <div class="col-xs-6">
              <label>Last Name</label>
              <input type="text" name="lastname" id="lastname" onfocus="$(this).css('border-color','#dce4ec');$(this).attr('placeholder','');"  value="<?php echo $lastname;?>" class="form-control input-lg" placeholder="">
            </div>
          </div>
          <div class="row">
            <div class="col-xs-6">
              <label>Email Address</label>
              <input type="text" id="email" readonly name="email" onfocus="$(this).css('border-color','#dce4ec');$(this).attr('placeholder','');"  value="<?php echo $email;?>" class="form-control input-lg" placeholder="">
            </div>
            <div class="col-xs-6">
              <label>Zip Code</label>
              <input type="text" name="zipcode" id="zipcode" onfocus="$(this).css('border-color','#dce4ec');$(this).attr('placeholder','');" value="<?php echo $zip_code;?>" class="form-control input-lg" placeholder="">
            </div>
          </div>
          
          <div class="row">
            <div class="col-xs-6">
              <label>Phone Number</label>
              <input type="text" name="phonenumber" id="phonenumber" onfocus="$(this).css('border-color','#dce4ec');$(this).attr('placeholder','');"  value="<?=$phone?>" class="error form-control input-lg" placeholder="">
            </div>
          </div>
          <div class="row">
            <div class="col-xs-6">
              <label>Password</label>
              <input type="password" name="password" id="password" onfocus="$(this).css('border-color','#dce4ec');$(this).attr('placeholder','');"  class="form-control input-lg" placeholder="">
            </div>
          </div>
          <div class="row">
            <div class="col-xs-6">
              <label>Confirm password</label>
              <input type="password" name="repassword" id="repassword" onfocus="$(this).css('border-color','#dce4ec');$(this).attr('placeholder','');"  class="form-control input-lg" placeholder="">
            </div>
            </div>
            <div class="checkbox">
                <label>
                  <input type="checkbox"  name="emailalert" <?php if($email_alert) { ?> checked="checked" <?php } ?> value="yes">I would like to receive job alerts from <?=DOMAINNAME?>
                </label>
                
                
 			 </div>
            <input type="hidden" class="form-control" name="unique_value" id="unique_value" value="<?php echo $unique_value; ?>"/>
           <input type="hidden" class="form-control" name="login_type" id="login_type" value="<?php echo $login_type; ?>"/>
           <input type="hidden" class="form-control" name="profile_id" id="profile_id" value="<?php echo base64_decode($_GET['id']); ?>"/>
          <button type="submit" class="btn btn-default">Update</button>
        </form>
      </div>
      
    </div>
  </div>
</section>

<?php include("include/footer.php"); ?>

<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
<div class="scroll-top page-scroll visible-xs visble-sm"> <a class="btn btn-primary" href="#page-top"> <i class="fa fa-chevron-up"></i> </a> </div>


<script>
 $( document ).ready(function() {
    $("#terms").click(function() {
    // this function will get executed every time the #home element is clicked (or tab-spacebar changed)
    if($(this).is(":checked")) // "this" refers to the element that fired the event
    {
        $('#termstxt').css('color','#333');
    }
});
});       

        
        
        function formValidation(){
           
            var firstname       = $('#firstname').val();
            var lastname        = $('#lastname').val();
            var email           = $('#email').val();
            var zipcode         = $('#zipcode').val();
            var phonenumber     = $('#phonenumber').val();
            var password        = $('#password').val();
            var repassword      = $('#repassword').val();
            var emailalert      =$('#emailalert').val();
            
         
            //alert($(this).prop("checked"));
            if(firstname == ''){
                $('#firstname').css('border-color','red');
                $('#firstname').attr('placeholder','Firstname is mandatory.');
                return false;
                
            }else if(lastname == ''){
                
                $('#lastname').css('border-color','red');
                $('#lastname').attr('placeholder','Lastname is mandatory.');
                return false;
                
            } else if(email == ''){
                
                $('#email').css('border-color','red');
                $('#email').attr('placeholder','Email is mandatory.');
                return false;
                
            } else if(zipcode == ''){
                $('#zipcode').css('border-color','red');
                $('#zipcode').attr('placeholder','Zipcode is mandatory.');
                return false;
                
            } else if(phonenumber == ''){
                $('#phonenumber').css('border-color','red');
                $('#phonenumber').attr('placeholder','Phonenumber is mandatory.');
                return false;
                
            } else if(password !='' || repassword !=''){
		if(password ==''){
			$('#password').css('border-color','red');
			$('#password').attr('placeholder','Password is mandatory.');
			return false;
		}
		if(repassword != password){
		  $('#repassword').css('border-color','red');
		  $('#repassword').val('');
		  $('#repassword').attr('placeholder','Password and repeat password not matched.');
		  return false;
		  
		 }
                
            }  
                
                
}
  function login(){
           
           FB.login(function(response) {
                statusChangeCallback(response);
          }, {scope: 'email'});
        
  }
  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      testAPI();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into Facebook.';
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '609959692445734',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.1' // use version 2.1
  });

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
                
              $('#firstname').val(response.first_name);
              $('#lastname').val(response.last_name);
              $('#email').val(response.email);
              $('#unique_value').val(response.id);
              $('#login_type').val('facebook');
              
    });
  }
</script>

</body>
</html>

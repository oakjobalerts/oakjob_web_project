<?php
//error_reporting(E_ALL); ini_set('display_errors', 1);
ob_start();
session_start();
include('include/common.php');
require_once('include/DBManager.php');
require_once('include/sources_config.php');

$objDBManager = new DBManager(); //initialize db connection
$objDBManager->createConnection();

$logosArray = array("logo1.png", "logo2.png", "logo3.png", "logo4.png", "logo5.png");
$logoCounter = 0;

$memcache_obj_elastic = memcache_connect("oakelasticcache.25s6pq.0001.use1.cache.amazonaws.com", 11211);
$userAgent='web';
$mobileAdStyle='';



if ((strpos($_SERVER['HTTP_USER_AGENT'], 'Mobile') !== false) || (strpos($_SERVER['HTTP_USER_AGENT'], 'Nexus') !== false)) {
$userAgent="mobile";
$mobileAdStyle="style='width: 100%;'";
}
// if($_SERVER['REMOTE_ADDR'] == '112.196.1.221' || $_SERVER['REMOTE_ADDR'] == '111.93.206.202') {
// echo $_SERVER['HTTP_USER_AGENT'];
// echo "<br>";
// echo $userAgent;

// }

// SMS Job Advertisement 1
function getSMSJobAd1($SJA_Keyword, $SJA_Location, $SJA_Domain, $mobileAdStyle) {
	$redirectRecords = "";
	$SJA_name = "smsjob_banner_ad.jpg";
	$Smsjobinput = $SJA_Keyword.'|#|'.$SJA_Location .'|#||#||#|'.$SJA_Domain.'_1'.'|#|smsjob|#||#||#||#||#|';
	$redirectRecords = base64_encode($Smsjobinput);
	$url = "http://smsjob.us?token=" . $redirectRecords;

	$queryStr = "url=".$url."&sourcename=smsjob_web_banner&source=smsjob_web_banner&city=".$SJA_Location."&state=".$SJA_Location;
	$compressed = urlencode(base64_encode($queryStr)) ;
	$redirecturl = "http://" . $SJA_Domain . "/RedirectWEB.php?q=" . $compressed;
	$resHTML ="<div class=''><div id='div-gpt-ad-1' class='ad1 text-center add-space'>
	<a href='". $redirecturl . "' target='_blank'>
	<img style='height:70px; margin-bottom: 10px;' class='img-responsive' ".$mobileAdStyle." alt='' src='http://". $SJA_Domain."/topresume_add_img/".$SJA_name . "' />
	</a></div></div>";
	return $resHTML;
}

// SMS Job Advertisement 2
function getSMSJobAd2($SJA_Keyword, $SJA_Location, $SJA_Domain, $mobileAdStyle){
	$redirectRecords = "";
	$SJA_name = "smsjob_banner_ad_new.jpg";
	$Smsjobinput = $SJA_Keyword.'|#|'.$SJA_Location .'|#||#||#|'.$SJA_Domain.'_2'.'|#|smsjob|#||#||#||#||#|';
	$redirectRecords = base64_encode($Smsjobinput);
	$url = "http://smsjob.us?token=" . $redirectRecords;
	$queryStr = "url=".$url."&sourcename=smsjob_web_banner_1&source=smsjob_web_banner_1&city=".$SJA_Location."&state=".$SJA_Location;
	$compressed = urlencode(base64_encode($queryStr)) ;
	$redirecturl = "http://" . $SJA_Domain . "/RedirectWEB.php?q=" . $compressed;
	$resHTML ="<div class=''><div id='div-gpt-ad-1' class='ad2 text-center add-space' style='text-align:center;'>
	<a href='". $redirecturl . "' target='_blank'>".
	"<img  style='height:70px; margin-bottom: 10px;' class='img-responsive' ".$mobileAdStyle." alt='' src='http://". $SJA_Domain."/topresume_add_img/".$SJA_name . "' /></a></div></div>";
	return $resHTML;
}

function getLogoFromClearbitAndStore($company_name){

global $objDBManager;
global $memcache_obj_elastic;

$url="https://autocomplete.clearbit.com/v1/companies/suggest?query=".$company_name;
$result = file_get_contents($url);
$vars = json_decode($result, true);

foreach ($vars as &$innerArray) {
	$logo =  $innerArray['logo'];
	$name = $innerArray['name'];
	if (ctype_alnum($name) && ctype_alnum($company_name)){
		$name = str_replace(" ","_",$name);
		$name = str_replace(".","_",$name);
		$name = str_replace("&","_",$name);
		$name = str_replace(",","_",$name);
		$name = str_replace("'","_",$name);

		$content = file_get_contents($logo);
		file_put_contents('/var/www/vhosts/oakjob/public_html/company_logos/'.$name.'.png', $content);
		memcache_set($memcache_obj_elastic, $name, $name.'.png', false, time()+86400);

		$insertRecords = array();
		$insertRecords['company_name'] = $company_name;
		$insertRecords['company_key'] = $name;
		$insertRecords['image_name'] = $name.'.png';

		$objDBManager->insertRecord('companies_logos',$insertRecords);
	}
}

$key = str_replace(" ","_",$company_name);
$key = str_replace(".","_",$key);
$key = str_replace("&","_",$key);
$key = str_replace(",","_",$key);

$value  = memcache_get($memcache_obj_elastic, $key);
if(empty($value)){
memcache_set($memcache_obj_elastic, $key, 'logo not found', false, time()+86400);
$insertRecords = array();
$insertRecords['company_name'] = $company_name;
$insertRecords['company_key'] = $key;
$insertRecords['image_name'] = 'logo not found';
$objDBManager->insertRecord('companies_logos',$insertRecords);
}
}

function getLogoImageUrl($name) {
global $memcache_obj_elastic;
global $logoCounter;
global $logosArray;
$key = str_replace(array(" ",".","&",",","'"),"_",$name);
$value  = memcache_get($memcache_obj_elastic, $key);
if(empty($value)){
getLogoFromClearbitAndStore($name);
}
$value  = memcache_get($memcache_obj_elastic, $key);
if(empty($value) || $value == 'logo not found') {
$logoUrl = '';
} else {
$logoUrl = $value;
}
if($logoUrl != '') {
$logoUrl = 'company_logos/'.$logoUrl;
$data = file_get_contents($logoUrl);
if(empty($data)) {
$logoUrl = 'company_logos_genric/'.$logosArray[$logoCounter];
}
} else {
$logoUrl = 'company_logos_genric/'.$logosArray[$logoCounter];
if($logoCounter>=4)
$logoCounter = 0;
else
$logoCounter++;
}
return $logoUrl;
}




// if(DOMAIN == 'purejobalerts.com' || DOMAIN =='2ndchancejobalerts.com'){

// for saving and getting location in case of no location
if(!isset($_GET['l']) || $_GET['l'] == '')
{
$user_ip_address = $_SERVER['REMOTE_ADDR'] ;
$whereFields['ip'] = $user_ip_address;

$zipcode_res = $objDBManager->getRecord('tbl_ip_zip_match', $whereFields, false, '');

if(count($zipcode_res) > 0)
{
$_GET['l'] = $zipcode_res[0]['zipcode'];
}
else
{
$info_from_ipinfodb = file_get_contents('http://api.ipinfodb.com/v3/ip-city/?key=f82cdd68d64ff37394ea8c0909ae21413aac4098ec1a8e23a22ab0c6d7706721&ip='.$user_ip_address.'&format=json');
$info_from_ipinfodb = json_decode($info_from_ipinfodb,true);

$insert_clickAlerts['ip'] = $info_from_ipinfodb['ipAddress'];
$insert_clickAlerts['abbrevation'] = $info_from_ipinfodb['countryCode'];
$insert_clickAlerts['state'] = $info_from_ipinfodb['regionName'];
$insert_clickAlerts['city'] = $info_from_ipinfodb['cityName'];
$insert_clickAlerts['zipcode'] = $info_from_ipinfodb['zipCode'];
$countryCode = $info_from_ipinfodb['countryCode'];

if($countryCode == 'US')
{
$zip_to_insert = $info_from_ipinfodb['zipCode'];
if($zip_to_insert == '' || $zip_to_insert == '-')
{
$_GET['l'] = '';
}
else
{
$objDBManager->insertRecord('tbl_ip_zip_match', $insert_clickAlerts);
$_GET['l'] = $zip_to_insert;
}
}else{
$_GET['l'] = 'US';
}


}

}

// }

if(isset($_GET['isFailure'])) //in case of failure job
{

	$insertArr['domain'] = DOMAIN ;
	$insertArr['keyword'] = $_GET['q'];
	$insertArr['location'] = $_GET['l'];

	if($_GET['isFailure'] == 'j2c')
		$objDBManager->insertRecord('j2cBoolean_expire_landing_user', $insertArr);
	else if($_GET['isFailure'] == 'appcast')
		$objDBManager->insertRecord('appcast_expire_landing_user', $insertArr);

	$_GET['isFailure'] = "";
}

//if(DOMAIN == 'cylcon.com')
//{
//  header('Location: jobscylcon.php?q='.$_GET['q'].'&l='.$_GET['l'].'');
//   die();
//}
?>
<!DOCTYPE html>
<html lang="en">
<head>

<?php 
if(isset($_GET['q']) && trim($_GET['q']) != "" && isset($_GET['l']) && trim($_GET['l']) != "" ) {
$title = "Browse available "._urldecode($_GET['q'])." jobs in "._urldecode($_GET['l'])." | ".TITLE; 
$meta_description = "Search & apply for ".$_GET['q']." jobs in ".$_GET['l'].". Apply online and get hired today!";
} else if(isset($_GET['q']) && trim($_GET['q']) != "") {
$title = "Browse available "._urldecode($_GET['q'])." jobs"." | ".TITLE; 
$meta_description = "Search & apply for "._urldecode($_GET['q'])." jobs. Apply online and get hired today!";
} else if(isset($_GET['l']) && trim($_GET['l']) != "") {
$title = "Browse available jobs in "._urldecode($_GET['l'])." | ".TITLE; 
$meta_description = "Search & apply for jobs in "._urldecode($_GET['l']).". Apply online and get hired today!";
} else {
$title = "Browse available jobs"." | ".TITLE;
$meta_description = "Search & apply for jobs. Apply online and get hired today!";
}




//new code for the media net ads by pawan
$getq = trim($_GET['q']);
$getl = trim($_GET['l']);
$isFor_MediaADS=true;

if($_SERVER['HTTP_HOST']=='purejobalerts.com'
||$_SERVER['HTTP_HOST']=='vierjobs.com'
||$_SERVER['HTTP_HOST']=='paperrosejobs.com'
||$_SERVER['HTTP_HOST']=='gozjobs.com'
||$_SERVER['HTTP_HOST']=='heartbeatjobs.com'




	){

$isFor_MediaADS=false;
}

$medianet_desktop='<script id="mNCC" language="javascript">
var keyword="'.$getq.'";
if(!keyword.includes("jobs")){
keyword=keyword+\' jobs\';
}              
medianet_width = "827";
medianet_height = "350";
medianet_crid = "'.MEDIA_NET_DESKTOP_TAG.'";
medianet_versionId = "111299";
medianet_chnm="'.DOMAIN.'"; // Used to specify the channel name
medianet_chnm2=" "; // Used to specify the channel 2 name
medianet_chnm3=" "; // Used to specify the channel 3 name
medianet_misc = {};
medianet_misc.zip = "'.$getl.'";
medianet_misc.gender = "m";
medianet_misc.age = "23";
medianet_misc.query = keyword;


(function() {
var isSSL = \'https:\' == document.location.protocol;
var mnSrc = (isSSL ? \'https:\' : \'http:\') + \'//contextual.media.net/nmedianet.js?cid=8CUI30N5I\' + (isSSL ? \'&https=1\' : "");
document.write(\'<script type="text/javascript" id="mNSC" src="\' + mnSrc + \'"></scr\' + \'ipt>\');
})();

</script>'; 

$media_mobile_ads='<script id="mNCC" language="javascript">
var keyword="'.$getq.'";

if(!keyword.includes("jobs")){
keyword=keyword+\' jobs\';
} 

medianet_width = "300";
medianet_height = "430";
medianet_crid = "'.MEDIA_NET_MOBILE_TAG.'";
medianet_versionId = "111299"; 
medianet_chnm="'.DOMAIN.'"; // Used to specify the channel name
medianet_chnm2=" "; // Used to specify the channel 2 name
medianet_chnm3=" "; // Used to specify the channel 3 name

medianet_misc = {};
medianet_misc.zip = "'.$getl.'";
medianet_misc.gender = "m";
medianet_misc.age = "23";
medianet_misc.query = keyword;


(function() {
var isSSL = \'https:\' == document.location.protocol;
var mnSrc = (isSSL ? \'https:\' : \'http:\') + \'//contextual.media.net/nmedianet.js?cid=8CUI30N5I\' + (isSSL ? \'&https=1\' : "");
document.write(\'<scr\' + \'ipt type="text/javascript" id="mNSC" src="\' + mnSrc + \'"></scr\' + \'ipt>\');
})();

</script>';

?>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="<?php echo $meta_description; ?>">
<meta name="author" content="">
<meta name="google-site-verification" content="<?=GMS?>" />

<title><?= $title ?></title>

<!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
<link href="css/bootstrap.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="css/oakstyle.css?v=2" rel="stylesheet">

<link rel="shortcut icon" href="<?= FAVICONPATH ?>favicon.ico" type="image/x-icon">
<link rel="icon" href="<?= FAVICONPATH ?>favicon.ico" type="image/x-icon">

<!-- Custom Fonts -->
<link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- UserVoice JavaScript SDK (only needed once on a page) -->
<script>(function(){var uv=document.createElement('script');uv.type='text/javascript';uv.async=true;uv.src='//widget.uservoice.com/RUVxfxpYSZzV74j6P7cENw.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(uv,s)})()</script>


<!--script for cylcon ads showing-->
<script src="//tags-cdn.deployads.com/a/cylcon.com.js" async ></script>


<!--pushnnami script for senza by pawan-->
<?php
if($_SERVER['HTTP_HOST'] == 'senzajobs.com')
{
?>
<script src="https://api.pushnami.com/scripts/v1/push/59c3e8b9e3f49f552259a6d5"></script>
<link rel="manifest" href="manifest.json">
<?php
}
?>
<?php
if($_SERVER['HTTP_HOST'] == 'senzadaily.com')
{
?>
<script src="https://api.pushnami.com/scripts/v1/push/59c3eaa38089573fd96a484f"></script>
<link rel="manifest" href="/manifest.json">
<?php
}
?>
<?php
if($_SERVER['HTTP_HOST'] == 'senzajobalerts.com')
{
?>
<script src="https://api.pushnami.com/scripts/v1/push/59c3eacefe835554ee9553dc"></script>
<link rel="manifest" href="/manifest.json">
<?php
}
?>


<?php
if(DOMAIN == '2ndchancejobalerts.com')
{ 
?>
<!-- Google Tag Manager -->

<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5TGHSS');</script>
<!-- End Google Tag Manager -->
<?php
}
?>
<!-- A link to launch the Classic Widget -->

<!-- impelemting arbor pixel-->
<?php
if(isset($_COOKIE['login_type'])) {
$pixel = '17028';
$md5_email = md5($_COOKIE['email']);
$sha1_email = hash('sha1', $_COOKIE['email']);
$sha256_email =  hash('sha256', $_COOKIE['email']);
$md5_security_hash = md5(strtoupper($md5_email) .strtoupper("S0126"));

?>

<script src="//pippio.com/api/sync?pid=<?=ARBOR_PIXEL?>&it=4&iv=<?php echo  $md5_email; ?>&it=4&iv=<?php echo  $sha1_email; ?>&it=4&iv=<?php echo  $sha256_email; ?>" async></script>


<!--Traverse integration-->


<img alt="" style='border:none;' width='1' height='1' src="http://traverse.<?php echo DOMAIN?>/v1/d6b07d75-d064-44ea-a437-0851513729fd/0.gif?emailMd5Lower=<?php echo  $md5_email; ?>"/>
<img alt="" style='border:none;' width='1' height='1' src="http://traverse.<?php echo DOMAIN?>/v1/d6b07d75-d064-44ea-a437-0851513729fd/1.gif?emailMd5Lower=<?php echo  $md5_email; ?>"/>
<img alt="" style='border:none;' width='1' height='1' src="http://traverse.<?php echo DOMAIN?>/v1/d6b07d75-d064-44ea-a437-0851513729fd/2.gif?emailMd5Lower=<?php echo  $md5_email; ?>"/>
<img alt="" style='border:none;' width='1' height='1' src="http://traverse.<?php echo DOMAIN?>/v1/d6b07d75-d064-44ea-a437-0851513729fd/3.gif?emailMd5Lower=<?php echo  $md5_email; ?>"/>
<img alt="" style='border:none;' width='1' height='1' src="http://traverse.<?php echo DOMAIN?>/v1/d6b07d75-d064-44ea-a437-0851513729fd/4.gif?emailMd5Lower=<?php echo  $md5_email; ?>"/>


<img alt="" style='border:none;' width='1' height='1' src="http://traverse.<?php echo DOMAIN?>/v1/d6b07d75-d064-44ea-a437-0851513729fd/0.gif?emailSha1Lower=<?php echo  $sha1_email; ?>"/>
<img alt="" style='border:none;' width='1' height='1' src="http://traverse.<?php echo DOMAIN?>/v1/d6b07d75-d064-44ea-a437-0851513729fd/1.gif?emailSha1Lower=<?php echo  $sha1_email; ?>"/>
<img alt="" style='border:none;' width='1' height='1' src="http://traverse.<?php echo DOMAIN?>/v1/d6b07d75-d064-44ea-a437-0851513729fd/2.gif?emailSha1Lower=<?php echo  $sha1_email; ?>"/>
<img alt="" style='border:none;' width='1' height='1' src="http://traverse.<?php echo DOMAIN?>/v1/d6b07d75-d064-44ea-a437-0851513729fd/3.gif?emailSha1Lower=<?php echo  $sha1_email; ?>"/>
<img alt="" style='border:none;' width='1' height='1' src="http://traverse.<?php echo DOMAIN?>/v1/d6b07d75-d064-44ea-a437-0851513729fd/4.gif?emailSha1Lower=<?php echo  $sha1_email; ?>"/>
<!--bdex pixel -->

<img alt="" style='border:none;' width='1' height='1' src="http://bdex.<?php echo DOMAIN?>//bdex/bdexSeller.jsp?bdexPartnerAccountId=140&sellerCampaignId=434&industryId=609&customerUserId=&datasetClass=IDENTITY&emailMd5Identity=<?php echo  $md5_email; ?>&emailMD5=<?php echo  $md5_email; ?>&activeEmailAccount=1&emailMD5SecurityHash=<?php echo  $md5_security_hash; ?>&servePixel"/>

<?php
}
?>

<!-- jQuery Version 1.11.0 -->
<script src="js/jquery-1.11.0.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script type="text/javascript" src="http://www.indeed.com/ads/apiresults.js"></script>
<script src="js/classie.js"></script>
<script src="js/cbpAnimatedHeader.js"></script>

<!-- Contact Form JavaScript -->
<script src="js/jqBootstrapValidation.js"></script>


<!-- Custom Theme JavaScript -->
<script src="js/oak.js"></script>
<script src="js/bootbox.js"></script>
<style>
#account-overview-container.affix {
    position: fixed;
    top: 10px
}
.affix-top {
    position: static;
}

.affix-bottom {
    position:absolute;
    bottom:auto !important;
}
</style>
<?PHP
echo HEADER_ADS_SCRIPTS;
?> 

</head>

<body id="page-top" class="listing">

<?php
if(DOMAIN == 'purejobalerts.com')
{
?>
<!-- Google Code for Add to Cart Conversion Page
In your html page, add the snippet and call goog_report_conversion
when someone clicks on the chosen link or button. -->
<script type="text/javascript">
/* <![CDATA[ */
goog_snippet_vars = function() {
var w = window;
w.google_conversion_id = 868380543;
w.google_conversion_label = "Et-ACOvXs2wQ_96JngM";
w.google_conversion_language = "en";
w.google_conversion_format = "3";
w.google_conversion_color = "ffffff";
w.google_remarketing_only = false;

// w.google_conversion_value = 13.00;
// w.google_conversion_currency = "USD";
}
// DO NOT CHANGE THE CODE BELOW.
goog_report_conversion = function(url) {
goog_snippet_vars();
window.google_conversion_format = "3";
var opt = new Object();
opt.onload_callback = function() {
if (typeof(url) != 'undefined') {
// window.location = url;
}
}
var conv_handler = window['google_trackConversion'];
if (typeof(conv_handler) == 'function') {
conv_handler(opt);
}
}
/* ]]> */
</script>


<script type="text/javascript"
src="//www.googleadservices.com/pagead/conversion_async.js">
</script>

<!-- Google Code for Add to Cart Conversion Page
In your html page, add the snippet and call goog_report_conversion
when someone clicks on the chosen link or button. -->
<script type="text/javascript">
/* <![CDATA[ */
createAlert_goog_snippet_vars = function() {
var createAlert_w = window;
createAlert_w.google_conversion_id = 868380543;
createAlert_w.google_conversion_label = "OUIwCL_Vs2wQ_96JngM";
createAlert_w.google_conversion_language = "en";
createAlert_w.google_conversion_format = "3";
createAlert_w.google_conversion_color = "ffffff";
createAlert_w.google_remarketing_only = false;

//createAlert_w.google_conversion_value = 13.00;
//createAlert_w.google_conversion_currency = "USD";
}
// DO NOT CHANGE THE CODE BELOW.
createAlert_goog_report_conversion = function(url) {
createAlert_goog_snippet_vars();
window.google_conversion_format = "3";
var opt = new Object();
opt.onload_callback = function() {
if (typeof(url) != 'undefined') {
//window.location = url;
}
}
var conv_handler = window['google_trackConversion'];
if (typeof(conv_handler) == 'function') {
conv_handler(opt);
}
}
/* ]]> */
</script>
<script type="text/javascript"
src="//www.googleadservices.com/pagead/conversion_async.js">
</script>








<!-- Google Code for Job Posting Conversion (Click &#39;Apply&#39; or Job Posting Title url) Conversion Page -->
<!--
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 868380543;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "Et-ACOvXs2wQ_96JngM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/868380543/?label=Et-ACOvXs2wQ_96JngM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>*/

-->

<?php
}

?>

<?php

if(DOMAIN == '2ndchancejobalerts.com')
{
?>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5TGHSS"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<?php
}

?>

<?php





function highlightkeyword($str, $search) {

//if(DOMAIN =='purejobalerts.com')
//$str = preg_replace('/'.str_replace(' ', '|', trim($search)).'/i', '<span style="color: '.$highlightcolor.'; background-color:yellow;font-weight:bold;">$0</span>', $str);

return $str;

}


//error_reporting(E_ALL);
//ini_set('display_errors', 1);
$objDBManager = new DBManager(); //initialize db connection
$objDBManager->createConnection();

$ZIPCODE_TABLE = 'tbl_zipcodes_test';
$ZIPCODE_FILE = '/var/nfs-93/zipcode.txt';

if (isset($_GET['source']) && !empty($_GET['source'])) {

$insert_clickAlerts               = array();
$insert_clickAlerts['event_type'] = 'View all jobs';
$insert_clickAlerts['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
$insert_clickAlerts['ip_address'] = $_SERVER['REMOTE_ADDR'];
$insert_clickAlerts['source']     = base64_decode($_GET['source']);
$objDBManager->insertRecord('tbl_other_clicks', $insert_clickAlerts);
if (SOURCE == '')
setcookie('source', base64_decode($_GET['source']), time() + 31556926);
else
setcookie('source', base64_decode(SOURCE), time() + 31556926);

}

function yourls_redirect_javascript( $location, $dontwait = true ) {
$message =  'if you are not redirected after 10 seconds, please <a href="'.$location.'">click here</a>' ;
echo <<<REDIR
<script type="text/javascript">
window.location="$location";
</script>
<small>($message)</small>
REDIR;
}

if (isset($_GET['addtoemail']) && !empty($_GET['addtoemail'])) {
$json = base64_decode($_GET['addtoemail']);
$obj = json_decode($json);

$insert_data_array               = array();
$insert_data_array['email'] = isset($obj->email) ? $obj->email : "";
$insert_data_array['original_keyword'] = isset($obj->keyword) ? $obj->keyword : "";
$insert_data_array['keyword'] = isset($_GET['q']) ? $_GET['q'] : "";
$insert_data_array['location'] = isset($_GET['l']) ? $_GET['l'] : "";
$insert_data_array['ip'] = $_SERVER['REMOTE_ADDR'];
$insert_data_array['whitelabel_name'] = WHITELABEL_NAME;
$insert_data_array['domain'] = $_SERVER['HTTP_HOST'];
$insert_data_array['provider'] = isset($obj->provider) ? $obj->provider : "";
$insert_data_array['source'] = isset($obj->source) ? $obj->source : "";
$insert_data_array['email_type'] = isset($obj->emailtype) ? $obj->emailtype : "";
$objDBManager->insertRecord('add_to_emails_stats', $insert_data_array);

if(WHITELABEL_NAME == 'handpicked'){
$location = 'http://handpickedjobalerts.com/search/?keyword='.$_GET['q'].'&location='.$_GET['l'].'';
yourls_redirect_javascript( $location );
die();
}
}


// CLICK FROM ADVERTISEMENT
if(isset($_GET['affid']) && !empty($_GET['affid'])) {
setcookie('source', "mobius-".$_GET['affid'], time() + 31556926);
$aff_query_elem = array(
'affid' => $_GET['affid'],
'c1' => (isset($_GET['c1'])) ? $_GET['c1'] : "",
'c2' => (isset($_GET['c2'])) ? $_GET['c2'] : "",
'c3' => (isset($_GET['c3'])) ? $_GET['c3'] : "",
'c4' => (isset($_GET['c4'])) ? $_GET['c4'] : "",
);
$aff_query_elem = json_encode($aff_query_elem);
setcookie('aff_query_elem', $aff_query_elem, time() + 31556926);

$insert_data_array               = array();
$insert_data_array['email'] = $_COOKIE['email'];
$insert_data_array['keyword'] = isset($_GET['q']) ? $_GET['q'] : "";
$insert_data_array['location'] = isset($_GET['l']) ? $_GET['l'] : "";
$insert_data_array['ip'] = $_SERVER['REMOTE_ADDR'];
$insert_data_array['whitelabel_name'] = WHITELABEL_NAME;
$insert_data_array['domain'] = $_SERVER['HTTP_HOST'];
$insert_data_array['provider'] = "mobius-".$_GET['affid'];
$insert_data_array['affid'] = $_GET['affid'];
$objDBManager->insertRecord('mobius_user_tracking', $insert_data_array);
}else{
if(isset($_COOKIE['source']) && (strpos(strtolower($_COOKIE['source']),'mobius-') !== false))
setcookie('source', "", time() + 31556926);
}


$keyword  = (!empty($_GET['q']) ? mysql_escape_string(trim($_GET['q'])) : "");
$location = (!empty($_GET['l']) ? mysql_escape_string(trim($_GET['l'])) : "");

// ROMMY - 20170510 - SEO FRIENDLY URLS //
$keyword  = _urldecode($keyword);

if (KEYWORDS_EXCLUSION != '' && isset($_GET['q'])) {
$keyword_ex_array = explode(',', KEYWORDS_EXCLUSION);

$whereForKeyword = "WHERE title ";
$count1          = 1;
foreach ($keyword_ex_array as $value) {
if ($count1 == 1) {
$whereForKeyword .= "NOT LIKE '%" . $value . "%'";
} else {
$whereForKeyword .= " AND title NOT LIKE '%" . $value . "%'";
}

$count1++;
}
} else {
$whereForKeyword = "";
}

if (isset($_GET['alert'])) {

$whereFields                 = array();
$whereFields['EmailAddress'] = $_COOKIE['email'];
$whereFields['Keyword']      = mysql_escape_string($_GET['q']);
$whereFields['Location']     = mysql_escape_string($_GET['l']);
if (PROVIDER != '') {
$whereFields['provider'] = PROVIDER;
}
$checkUser = $objDBManager->getRecord('tbl_jobAlerts_whitelabels', $whereFields, false, '');

if (count($checkUser) == 0) {

// Start: Get zipcode
$searchLocation = $_GET['l'];
$ZIPCODE_FILE = '/var/nfs-93/zipcode.txt';
if (is_numeric($searchLocation)) {

$searchrecords =  exec("grep $searchLocation $ZIPCODE_FILE -m 1");
$searchrecords_arr = explode('|', $searchrecords);
$zipcode = $searchrecords_arr[0];

} else {

$pos  = strpos($searchLocation, ",");
$pos1 = strpos($searchLocation, " ");

if ($pos === false && $pos1 === false) {

$searchrecords =  exec("grep -i '|".$searchLocation."|' $ZIPCODE_FILE -m 1");    
$searchrecords_arr = explode('|', $searchrecords);
$zipcode = $searchrecords_arr[0];

} else {
$pos = strpos($searchLocation, ",");

if ($pos === false)
$searchLocation = array_map("trim", explode(" ", $searchLocation, 2));
else
$searchLocation = array_map("trim", explode(",", $searchLocation, 2));

if (strlen($searchLocation[1]) > 2) {

$searchrecords =  exec("grep -i '|".trim($searchLocation[0])."|'.*'|".trim($searchLocation[1])."|' $ZIPCODE_FILE -m 1");   
$searchrecords_arr = explode('|', $searchrecords);
$zipcode = $searchrecords_arr[0];

} else {

$searchrecords =  exec("grep -i '|".trim($searchLocation[0])."'.*'|".trim($searchLocation[1])."|' $ZIPCODE_FILE -m 1");
$searchrecords_arr = explode('|', $searchrecords);
$zipcode = $searchrecords_arr[0];

}

}

}
// End: Get zipcode

$insert_jobAlerts                 = array();
$insert_jobAlerts['Keyword']      = mysql_escape_string($_GET['q']);
$insert_jobAlerts['EmailAddress'] = $_COOKIE['email'];
$insert_jobAlerts['Location']     = mysql_escape_string($_GET['l']);
$insert_jobAlerts['Zip']          = $zipcode;
if (PROVIDER != '') {
$insert_jobAlerts['provider'] = PROVIDER;
}
$objDBManager->insertRecord('tbl_jobAlerts_whitelabels', $insert_jobAlerts);
}

}

if (isset($_GET['sort']) && $_GET['sort'] != '') {
$sort = '&sort=' . $_GET['sort'];
} else {


$_GET['sort'] = (DOMAIN =='resumetargetalerts.com') ? 'date' : 'relevance';

}

if (isset($_GET['radius']) && $_GET['radius'] != '') {

$radius = '&radius=' . $_GET['radius'];

} else {

$radius         = isset($_GET['radius']) ? $_GET['radius'] : '30';
$_GET['radius'] = $radius;

}

$start = $_GET['start'];



if ($keyword == "" && $location == "") {
header('Location: ' . $baseurl);
}
$memcache_obj  = memcache_connect("localhost", 11211);
$cacheKeywords = memcache_get($memcache_obj, 'keywords');
$cacheKeywords = array_map('strtolower', $cacheKeywords);

if ($cacheKeywords[strtolower(trim($keyword))] != '') {
$keyword = trim($cacheKeywords[strtolower(trim($keyword))]);
}
//Code to create breadcrumb-------------------START------------------------------
$jobSearchHeading = "";
$breadcrumb       = explode(',', $_GET['l']);
$filterLink       = '&sort=' . $_GET['sort'] . '&radius=' . $_GET['radius'];
if (isset($_GET['q']) && $_GET['q'] != '') {
if (count($breadcrumb) == 2) {
$innerLink = '<li><a href="' . $currentUrl . '?q=&l=' . $breadcrumb['1'] . $filterLink . '">Jobs in ' . strtoupper($breadcrumb['1']) . '</a></li><li><a href="' . $currentUrl . '?q=&l=' . $breadcrumb['0'] . ',' . $breadcrumb['1'] . $filterLink . '">Jobs in ' . strtoupper($_GET['l']) . '</a></li>';
} else {
$innerLink = '<li><a href="' . $currentUrl . '?q=&l=' . $breadcrumb['0'] . $filterLink . '">Jobs in ' . strtoupper($breadcrumb['0']) . '</a></li>';
}
$breadcrumbCompleteLinks = '<ol class="breadcrumb text-left"><li><a href="' . $baseurl . '">All Jobs</a></li>' . $innerLink . '<li class="active">' . ucwords($_GET['q']) . ' Jobs in ' . strtoupper($_GET['l']) . '</li></ol>';
$jobSearchHeading        = ucwords(($_GET['q']) . ' Jobs in ' . strtoupper($_GET['l']));
} else {
if (count($breadcrumb) == 2) {
$innerLink = '<li><a href="' . $currentUrl . '?q=&l=' . $breadcrumb['1'] . $filterLink . '">Jobs in ' . strtoupper($breadcrumb['1']) . '</a></li>';
}
$breadcrumbCompleteLinks = '<ol class="breadcrumb text-left"><li><a href="' . $baseurl . '">All Jobs</a></li>' . $innerLink . '<li class="active">' . ucwords($_GET['q']) . ' Jobs in ' . strtoupper($_GET['l']) . '</li></ol>';
$jobSearchHeading        = ucwords(($_GET['q']) . ' Jobs in ' . strtoupper($_GET['l']));
}
//Code to create breadcrumb--------------------END-------------------------------

if (empty($start))
$start = 0;

$searchLocation = mysql_escape_string(trim($location));
$statePostFix   = '';
if (is_numeric($searchLocation)) {

$searchrecords =  exec("grep $searchLocation $ZIPCODE_FILE -m 1");
$searchrecords_arr = explode('|', $searchrecords);
$zipcode = $searchrecords_arr[0];
$lat = $searchrecords_arr[3];
$lng = $searchrecords_arr[4];

} else {

$pos  = strpos($searchLocation, ",");
$pos1 = strpos($searchLocation, " ");

if ($pos === false && $pos1 === false) {

$searchrecords =  exec("grep -i '|".$searchLocation."|' $ZIPCODE_FILE -m 1");    
$searchrecords_arr = explode('|', $searchrecords);
$zipcode = $searchrecords_arr[0];
$lat = $searchrecords_arr[3];
$lng = $searchrecords_arr[4];

} else {
$pos = strpos($searchLocation, ",");

if ($pos === false)
$searchLocation = array_map("trim", explode(" ", $searchLocation, 2));
else
$searchLocation = array_map("trim", explode(",", $searchLocation, 2));

if (strlen($searchLocation[1]) > 2) {

$searchrecords =  exec("grep -i '|".$searchLocation[0]."|'.*'|".$searchLocation[1]."|' $ZIPCODE_FILE -m 1");   
$searchrecords_arr = explode('|', $searchrecords);
$zipcode = $searchrecords_arr[0];
$lat = $searchrecords_arr[3];
$lng = $searchrecords_arr[4];

} else {

$searchrecords =  exec("grep -i '|".$searchLocation[0]."'.*'|".$searchLocation[1]."|' $ZIPCODE_FILE -m 1");
$searchrecords_arr = explode('|', $searchrecords);
$zipcode = $searchrecords_arr[0];
$lat = $searchrecords_arr[3];
$lng = $searchrecords_arr[4];

}

}

}


// $start_time  = microtime(true);

$CHANGECALUSE    = ' AND ';
$keywordHasHypen = strpos(trim($keyword), "/");
if ($keywordHasHypen !== false) {

$CHANGECALUSE = ' OR ';
$keyword      = preg_replace('#\/+#', ' ', $keyword);

}

function bar_get_nearby( $lat, $lng, $limit = 50, $distance, $unit = 'km' ) {

// radius of earth; @note: the earth is not perfectly spherical, but this is considered the 'mean radius'
if( $unit == 'km' ) { $radius = 6371.009; }
elseif ( $unit == 'mi' ) { $radius = 3958.761; }

// latitude boundaries
$maxLat = ( float ) $lat + rad2deg( $distance / $radius );
$minLat = ( float ) $lat - rad2deg( $distance / $radius );

// longitude boundaries (longitude gets smaller when latitude increases)
$maxLng = $lng - rad2deg($distance / $radius / cos(deg2rad($lat)));
$minLng = $lng + rad2deg($distance / $radius / cos(deg2rad($lat)));


/*
// longitude boundaries (longitude gets smaller when latitude increases)
$maxLng =  (( float ) $lng + rad2deg( $distance / $radius))  /  cos( deg2rad( ( float ) $lat ) );
$minLng =  (( float ) $lng - rad2deg( $distance / $radius))  /  cos( deg2rad( ( float ) $lat ) );
*/
$max_min_values = array(
'max_latitude' => $maxLat,
'min_latitude' => $minLat,
'max_longitude' => $maxLng,
'min_longitude' => $minLng
);
return $max_min_values;                                                       
}     

// $getlongitude=$objDBManager->exeQuery("select latitude,longitude from tbl_zipcodes where zip='$zipcode'");
// if($row=mysql_fetch_array($getlongitude))
// {
//   $lat = $row['latitude'];
//   $lng = $row['longitude'];
// }

if(isset($_GET['radius']) AND $_GET['radius'] != '') {
$distance = 1.6 * $_GET['radius'];
} else {
$distance = 48;
}
$getbound=bar_get_nearby( $lat, $lng, $limit = 50, $distance, $unit = 'km' );

$maxlat = $getbound['max_latitude'];
$minlat = $getbound['min_latitude'];
$maxlong = $getbound['max_longitude'];
$minlong = $getbound['min_longitude'];
$cloudstart = $start;
$radius = ($_GET['radius'] == '') ? '30' : $_GET['radius'];
//$cloudurl = file_get_contents("http://s2.oakjobalerts.com:8080/JavaAwsCloudSearchAPI/?keyword=".urlencode($keyword)."&start=".$start."&offset=15&upperLatitude=".$maxlat."&upperLongitude=".$maxlong."&lowerLatitude=".$minlat."&lowerLongitude=".$minlong);

// if($_SERVER['REMOTE_ADDR'] == '112.196.1.221' || $_SERVER['REMOTE_ADDR'] == '111.93.206.202') {

if(empty($location)){
$whereFields                 = array();
$whereFields['ip'] = $_SERVER['REMOTE_ADDR'];
$checkUser = $objDBManager->getRecord('ip_zip_match', $whereFields, false, '');
$location = (empty($checkUser)) ? '' : $checkUser[0]['zip'];

}
// }

if($_SERVER['HTTP_HOST']=='numberonejobsite.co.uk' || $_SERVER['HTTP_HOST']=='www.numberonejobsite.co.uk'){

if (strpos(strtolower($location), "england") === false){

$location=$location.',ENGLAND';
}
}

$isIndeed = '';
if(isset($_GET['isIndeedExpire']) && $_GET['isIndeedExpire'] == 'yes')
$isIndeed = '&isIndeed=yes';

$cloudurl = "http://s2.oakjobalerts.com:8080/JavaStaggingApi/?keyword=".urlencode(str_replace("&", "$$", $keyword))."&start=".$start."&offset=20".$sort."&radius=".$radius."&zipcode=".urlencode($location)."&domain=".DOMAIN."&ipaddress=".$_SERVER['REMOTE_ADDR'].$isIndeed;

$requestedAjaxUrl = 'http://'.$_SERVER['HTTP_HOST'].'/get_more_jobs.php?uri='.urlencode($cloudurl);

//$cloudurl = "http://s2.oakjobalerts.com:8080/JavaStaggingApi/?keyword=".urlencode($keyword)."&start=".$start."&offset=15".$sort."&upperLatitude=".$maxlat."&upperLongitude=".$maxlong."&lowerLatitude=".$minlat."&lowerLongitude=".$minlong."&domain=".DOMAIN;
//if(DOMAIN =='purejobalerts.com'){
//clearfitjobalerts.com
//$keyword = str_replace("&", "$$", $keyword);
//$cloudurl = "http://s2.oakjobalerts.com:8080/JavaStaggingApiForTesting/?keyword=".urlencode(str_replace("&", "$$", $keyword))."&start=".$start."&offset=15".$sort."&radius=".$radius."&zipcode=".urlencode($location)."&domain=".DOMAIN."&ipaddress=".$_SERVER['REMOTE_ADDR'];
//}

//$cloudurl = "http://s2.oakjobalerts.com:8080/JavaStaggingApi/?keyword=".urlencode($keyword)."&start=".$start."&offset=15".$sort."&upperLatitude=".$maxlat."&upperLongitude=".$maxlong."&lowerLatitude=".$minlat."&lowerLongitude=".$minlong."&domain=".DOMAIN;
// if(DOMAIN =='clearfitjobalerts.com'){

//   $keyword = str_replace("&", "$$", $keyword);
//   $cloudurl = "http://s2.oakjobalerts.com:8080/JavaStaggingApiForTesting/?keyword=".urlencode(str_replace("&", "$$", $keyword))."&start=".$start."&offset=15".$sort."&radius=".$radius."&zipcode=".urlencode($location)."&domain=".DOMAIN."&ipaddress=".$_SERVER['REMOTE_ADDR'];
//  }
// Get cURL resource
$curl = curl_init();
// Set some options - we are passing in a useragent too here
curl_setopt_array($curl, array(
CURLOPT_RETURNTRANSFER => 1,
CURLOPT_URL => $cloudurl
));
// Send the request & save response to $resp
$resp = curl_exec($curl);

// Close request to clear up some resources
curl_close($curl);

// $result_arr = json_decode($resp, true);
$result_arr = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $resp), true );
//echo "<pre>"; print_r($result_arr['title']); echo "</pre>";

$cloudtotal=$result_arr['total_found'];
// echo "<pre>"; print_r($result_arr); 
$cloudsearchapi = array();
$cloud = 0;
foreach($result_arr as $key => $val)
{

if(isset($_GET['sort']) && $_GET['sort'] == 'date') {
usort($val, function($a, $b) {
return strtotime($a['postingdate']) < strtotime($b['postingdate']);
});
}

foreach($val as $key => $value)
{

$cloudsearchapi[$cloud]['id'] = $value['id'];
$cloudsearchapi[$cloud]['title'] = (string)$value['title'];
$cloudsearchapi[$cloud]['employer'] = (string)$value['employer'];
$cloudsearchapi[$cloud]['postingdate'] = (string)$value['postingdate'];
$cloudsearchapi[$cloud]['e'] = "";
$cloudsearchapi[$cloud]['city'] = (string)$value['city'];
$cloudsearchapi[$cloud]['state'] = (string)$value['state'];
$cloudsearchapi[$cloud]['url'] = (string)$value['joburl'];
$cloudsearchapi[$cloud]['source'] =$value['source'];
$cloudsearchapi[$cloud]['_score'] =$value['_score'];
$cloudsearchapi[$cloud]['cpc'] =$value['cpc'];
$cloudsearchapi[$cloud]['gcpc'] =$value['gross_cpc'];
$cloudsearchapi[$cloud]['priority'] =$value['priority'];
$cloudsearchapi[$cloud]['sourcename'] =$value['sourcename'];
$cloudsearchapi[$cloud]['description'] =$value['description'];
$cloudsearchapi[$cloud]['displaysource'] =$value['displaysource'];
$cloudsearchapi[$cloud]['onmousedown'] =$value['onmousedown'];
$cloudsearchapi[$cloud]['cloudsearchsource'] =25;
$cloud++;

}
}

//if ($zipcode != '' && count($inArrayZips) > 0)
$response = $cloudsearchapi;



$totalResults = $cloudtotal;
$jobsResult   = $response;

$thispage     = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME'] . "?q=" . $_GET['q'] . "&l=" . $_GET['l'] . $sort."&radius=".$radius;
$num          = $totalResults; // number of items in list
$per_page     = 20; // Number of items to show per page
$showeachside = 5; //  Number of items to show either side of selected page
$showstart    = $start + 1;

if($showstart >= $num && $num < 20){
$showstart = '1';
}elseif($showstart >= $num){
$showstart = $num - 20;
}

$showend      = $showstart + ((count($jobsResult) > 20) ? 20 : count($jobsResult));
$max_pages    = ceil($num / $per_page); // Number of pages
$cur          = ceil($start / $per_page) + 1; // Current page number

if($cur > $max_pages){
$cur = $max_pages;
}

?>

<!--CHECK FOR WIDGET START HERE-->
<?php if(!widgetEnable()) { ?>

<!-- Navigation -->
<nav class="navbar navbar-default">
<div class="container">
<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header page-scroll">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="<?php echo $baseurl; ?>">
<img src="<?=IMAGE_BASE_URL ?>/logo1.png" title="<?= TITLE ?> Logo"> <?= HEADER_TITLE ?>
</a>
</div>
<div style="display:none;"><img src="img/loadingAnimation.gif" /></div>

<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

<ul class="nav navbar-nav navbar-right">

<li class="hidden">
<a href="#page-top"></a>
</li>
<ul class="mobile">
<li><a class="active" href="#">Search Jobs</a></li>
<?php if (!isset($_COOKIE['login_type'])) { ?>
<li><a href="sign_in.php">Manage Alerts</a></li>
<li><a href="sign_in.php">Saved Jobs</a></li>
<?php } else { ?>
<li><a href="alert_list.php">Manage Alerts</a></li>
<li><a href="savedjobs.php">Saved Jobs</a></li>
<li><a href="profile.php?id=<?php echo base64_encode($_COOKIE['id']); ?>">Manage Profile</a></li>
<?php } ?>
<li><a href="https://jobalerts.freshdesk.com/support/tickets/new" alt="Help/FAQ">Help/FAQ</a></li>
</ul>

<li class="page-scroll">
<?php if (!isset($_COOKIE['login_type'])) { ?>
<a href="sign_in.php" style="color: #fff;text-decoration: none;padding-right:0px;"><button type="submit" class="btn btn-signin"><i class="fa fa-lock" aria-hidden="true"></i>Sign in</button></a>
<?php } else { ?>
<a href="sign_in.php?logout=true" style="color: #fff;text-decoration: none;"><button type="submit" class="btn btn-signup">Sign out</button></a>
<?php } ?>
</li>
<li class="page-scroll">
<?php if(!isset($_COOKIE['login_type'])) {?>
<a href="sign_up.php" style="color: #fff;text-decoration: none;"><button type="submit" class="btn btn-signup">Sign Up</button></a>
<?php }?>
</li>
</ul>



</div>
<!-- /.navbar-collapse -->
</div>
<!-- /.container-fluid -->

<div class="secondnav web">
<ul class="container">
<li><a class="active" href="index.php">Search Jobs</a></li>
<?php if (!isset($_COOKIE['login_type'])) { ?>
<li><a href="sign_in.php">Manage Alerts</a></li>
<li><a href="sign_in.php">Saved Jobs</a></li>
<?php } else { ?>
<li><a href="alert_list.php">Manage Alerts</a></li>
<li><a href="savedjobs.php">Saved Jobs</a></li>
<li><a href="profile.php?id=<?php echo base64_encode($_COOKIE['id']); ?>">Manage Profile</a></li>
<?php } ?>
<li><a href="https://jobalerts.freshdesk.com/support/tickets/new" alt="Help/FAQ">Help/FAQ</a></li>
</ul>
</div>

</nav>

<!--CHECK FOR WIDGET END HERE-->    
<?php } ?>

<!-- section -->
<section>
<div class="container" style="padding-bottom:10px;">

<?php
if(DYNAMIC_TOP_AD ==''
// &&DOMAIN!='cylcon.com'
){


?>
<div class="panel-v1">
<!-- Small modal -->
<!-- Small modal -->
<div style="display:none;">
<button id="firstTimePopUp" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-sm">Popup</button>
</div>

<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<h4> We found <strong><?= $totalResults ?> "<?php echo ucwords($_GET['q']); ?>" jobs</strong> within <?= ($_GET['radius']) ? $_GET['radius'] : "10" ?> miles of <?= ($location) ? urldecode($location) : "" ?></h4>
<p>Where should we email your jobs? </p>

<form class="navbar-form" role="search" onsubmit="return getNewsLetterMail('<?php echo DOMAIN; ?>');" method="post">
<div class="form-group">
<input type="text" onfocus="$(this).css('border-color','#dce4ec');$(this).attr('placeholder','');" class="form-control input-lg" id="popupEmailAddress" name="EmailAddress" placeholder="Enter your email address">
<input type="hidden" class="form-control input-lg" name="Keyword" value="<?php if (isset($_GET['q'])) { echo $_GET['q']; } ?>" >
<input type="hidden" class="form-control input-lg" name="Location" value="<?php if (isset($_GET['l'])) { echo ($_GET['l']=='') ? $location :$_GET['l'] ; } ?>" >
<input type="hidden" class="form-control input-lg" name="Distance" value="<?php if (isset($_GET['radius'])) { echo $_GET['radius']; } ?>" >

</div><div id="createAlertbutton">
<button type="submit" class="btn btn-default" style="margin-left: -50px;">Create Alert</button>
<a class="close_btn" href="javascript:;" onclick="closePopUp();" style="position: absolute; margin-top: 8px; font-size: 14px;">Close</a>
</div>
</form>
<span>By clicking "create alert" I agree to the <?= TITLE ?>. <a href="<?= TERMSANDSERVICE ?>.php" target="_blank">Terms of Use</a></span>
</div>
</div>
</div>
<div class="row">
<div class="intro-text text-left">
<form name="jobsubmit" action="/jobs.php" class="navbar-form" role="search" onsubmit="return validateForm();">
<div class="form-group">
<input type="text" class="form-control input-lg" maxlength=512 size=31 id=what name=q autocomplete=off value="<?php echo $keyword; ?>">
</div>
<div class="form-group">
<input type="text" class="form-control input-lg" maxlength=64 size=27 id=where name=l autocomplete=off value="<?php echo urldecode($location); ?>" onkeyup="autocomplet()">
<ul id="country_list_id" style="display:none; margin-top:0; width: 22%;"></ul>
</div>
<?php if (isset($_COOKIE['login_type'])) { ?>
<div class="form-group">
<div style="width: 7%; float: left;">
<input type="checkbox" id="alert" name="alert" checked="checked" autocomplete=off value="<?php echo urldecode($location); ?>">
</div>
<div style="width: 60%">
<label>Create Job Alert</label>

</div>

</div>
<?php } ?>
<button type="submit" class="btn btn-default">Submit</button>
<!-- </form> -->
</div>

<div class="col-lg-12">
<?php echo $breadcrumbCompleteLinks; ?>

</div>
</div>
<div class="row text-left">

<div class="col-lg-3 filter">
<div class="filter-sec">
<label><i class="fa-filter"></i>Filter</label>
<select class="form-control input-lg" name="sort" id="sort" onchange="this.form.submit()">
<option value="">Sort By Relevance</option>
<option <?php if ($_GET['sort'] == 'relevance') { ?> selected <?php } ?> value="relevance">Relevance</option>
<option <?php if ($_GET['sort'] == 'date') { ?> selected <?php } ?> value='date'>Date</option>
</select>

<select class="form-control input-lg" name="radius" id="radius" onchange="this.form.submit()">
<option value="">Sort By Distance</option>
<option <?php if($_GET['radius'] == '5') { ?> selected <?php } ?> value="5">5 Miles</option>
<option <?php if($_GET['radius'] == '10') { ?> selected <?php } ?> value="10">10 Miles</option>
<option <?php if($_GET['radius'] == '30') { ?> selected <?php } ?> value="30">30 Miles</option>
<option <?php if($_GET['radius'] == '50') { ?> selected <?php } ?> value="50">50 Miles</option>
<option <?php if($_GET['radius'] == '75') { ?> selected <?php } ?> value="75">75 Miles</option>
<option <?php if($_GET['radius'] == '100') { ?> selected <?php } ?> value="100">100 Miles</option>
<option <?php if($_GET['radius'] == '125') { ?> selected <?php } ?> value="125">125 Miles</option>
<option <?php if($_GET['radius'] == '150') { ?> selected <?php } ?> value="150">150 Miles</option>
<option <?php if($_GET['radius'] == '175') { ?> selected <?php } ?> value="175">175 Miles</option>
<option <?php if($_GET['radius'] == '200') { ?> selected <?php } ?> value="200">200 Miles</option>
</select>
</div>
<!--  side of search results -->
<!-- <ins class="adsbygoogle"  
style="display:inline-block;width:250px;height:600px"
data-ad-client="ca-pub-1699099086165943"
data-ad-slot="4800529119"></ins> 
-->

<?php     
if($userAgent=="web"){
echo LEFT_RIGHT_ALIGN_AD;
}
?>


</div>

</form>


<div class="col-lg-9">

<div class="webadds">
<?php 
echo Leaderboard_A;    


?>
</div>
<h6 style="width:300px;float:left;"><?php echo _urldecode($jobSearchHeading); ?></h6><span id="AlertMessage" style="float:left;color:#96CE69"></span>
<div style="clear:both"></div>

<?php if (!isset($_COOKIE['login_type'])) { ?>
<div class="dailyalert clearfix">
<p><i class="fa-envelope"></i>Get the newest jobs like these daily:</p>
<form role="search" onsubmit="return getNewsLetterMail1('<?php echo DOMAIN; ?>');" method="post">
<div class="form-group">                           
<input type="text" class="form-control input-lg" onfocus="$(this).css('border-color','#dce4ec');$(this).attr('placeholder','');" id="EmailAddress" name="EmailAddress" placeholder="Enter your email address">
<input type="hidden" class="form-control input-lg" name="Keyword" value="<?php if (isset($_GET['q'])) { echo $_GET['q']; } ?>" >
<input type="hidden" class="form-control input-lg" name="Location" value="<?php if (isset($_GET['l'])) { echo $_GET['l']; } ?>" >
<input type="hidden" class="form-control input-lg" name="Distance" value="<?php if (isset($radius)) { echo $radius; } ?>" >

</div>
<div id="createAlert"><button type="submit" class="btn btn-default">Create Alert</button></div>
</form>
</div>
<?php } ?>



<?php  if ($totalResults > 0) { 
if($userAgent == "mobile") { 
echo '<div id="searchCount">Number of jobs found : '.$totalResults.'</div>';
} else {
echo '<div id="searchCount">Jobs '.$showstart.' to '.$showend.' of '.$totalResults.'</div>';
}
?>
<div id="job-results-wrapper">
<div id="jobs-page-num-0">
<?php
foreach ($jobsResult as $jobkeyid=>$jobs) {

//$jobDescriptionQry   = "SELECT description FROM tbl_all_jobs_description WHERE id = " . $jobs['id'] . " LIMIT 0,1";
//$descriptionResults  = $objDBManager->fetchRecord($jobDescriptionQry);
//$jobs['description'] = $descriptionResults[0]['description']; 

$queryStr = "url=".str_replace("&","##", $jobs['url'])."&sourcename=".urlencode($jobs['sourcename'])."&source=".urlencode($jobs['source'])."&city=".urlencode($jobs['city'])."&state=".urlencode($jobs['state'])."&employer=".urlencode($jobs['employer'])."&title=".urlencode($jobs['title'])."&cpc=".$jobs['cpc']."&gcpc=".$jobs['gcpc'];
$compressed = base64_encode($queryStr) ;
$onmouse = (strpos(strtolower($jobs['sourcename']), 'indeed1234') !== false) ? $jobs['onmousedown'] : '';

if(isset($_GET['isFailure']) && $_GET['isFailure'] == true)
{
$redirectURL = (strpos(strtolower($jobs['sourcename']), 'indeed1234') !== false) ?  $jobs['url'] : "RedirectWEB.php?q=".$compressed."&token=".$jobs['id']."&isFailure=true";
}
else
{
$redirectURL = (strpos(strtolower($jobs['sourcename']), 'indeed1234') !== false) ?  $jobs['url'] : "RedirectWEB.php?q=".$compressed."&token=".$jobs['id']."";
}


?>
<div class="joblists clearfix">

<?php
if($jobkeyid == 5)
{

echo Leaderboard_B;


}
if($jobkeyid==10 && $userAgent=="mobile"){

echo Leaderboard_mobile_top;

}
?> 
<div class="col-lg-10">

<?php

$logoUrl =  getLogoImageUrl($jobs['employer']);

?>

<span class="left-img-des">
<div class="left-img-des-wrap">
<?php if($logoUrl != '') { ?>
<img src='<?php echo $logoUrl; ?>' style="">
<?php } ?>
</div>
</span>
<span class="right-txt-des">



<?php
if(DOMAIN=='purejobalerts.com'){


?>
<a onclick="goog_report_conversion ('<?php echo $redirectURL ?>')"
<?php
}
else{

?>
<a

<?php
}
?>
onmousedown="<?php echo $onmouse ?>" href="<?php echo $redirectURL ?>" target="_blank"><?php echo highlightkeyword(utf8_decode($jobs['title']),$keyword); ?></a>
<p>
<strong><?php echo utf8_decode($jobs['employer']); ?>
<span class='location'> 
<?php if($jobs['displaysource'] != '' && DOMAIN !='resumetargetalerts.com') { $source_name = " - <b style='color:black;'>" . strtoupper($jobs['displaysource']) . "</b>"; } else { $source_name = ""; } ?>
<?php echo " - ". $jobs['city'] . ", " . $jobs['state']; // . $source_name; ?>
</span></strong>
</p>
<!-- <em>
Posted yesterday on ZipRecruiter - 
<a href="#">ZipApply</a>
</em> -->
<p class="p-descript description-web">
<?php $truncated = utf8_decode((strlen(strip_tags(html_entity_decode($jobs['description']))) > 200) ? substr(strip_tags(html_entity_decode($jobs['description'])), 0, 200) . '...' : strip_tags(html_entity_decode($jobs['description']))); echo utf8_decode($truncated); ?>
</p>
<p class="p-descript description-mob">
<?php $truncated = utf8_decode((strlen(strip_tags(html_entity_decode($jobs['description']))) > 70) ? substr(strip_tags(html_entity_decode($jobs['description'])), 0, 70) . '...' : strip_tags(html_entity_decode($jobs['description']))); echo utf8_decode($truncated); ?>
</p>
<?php
$appendString = '';
if(isset($jobs['postingdate']) && !empty($jobs['postingdate'])) {
if(DOMAIN =='purejobalerts.com') {
//        $appendString = " | Score : ".$jobs['_score']." | Priority : ".$jobs['priority'];
}
$now        = time();
$your_date  = strtotime($jobs['postingdate']);
$datediff   = $now - $your_date;
$dateAgo    = floor($datediff / (60 * 60 * 24));
//$beforedate = ($dateAgo > 0) ? "$dateAgo days ago" : "New";
$beforedate = ($dateAgo > 0) ? "$dateAgo days ago" : "<span style='color: #ce433a;'>New</span>";
} else {
$beforedate='';
}
?> 
<div class="job-date-desktop">
<span class=date><?php echo $beforedate.$appendString; ?> </span>
</div>

<?php
if(DOMAIN=='purejobalerts.com'){
?>
<a onclick="goog_report_conversion ('<?php echo $redirectURL ?>')"

<?php
}
else{
?> 
<div class="mobile-apply apply-btn pull-right"><a
<?php
}
?>


style="text-decoration:none; color:white; font-size: 15px; font-weight: normal;" href="<?php echo $redirectURL ?>" target="_blank">
<button type="button" class="btn btn-default apply">Apply</button></a>
<button type="button" id='<?= $jobs['id'] ?>' class="btn btn-default" <?php if (isset($_COOKIE['email'])) { ?>onclick="return savejobs('<?= $jobs['id'] ?>','<?php echo addslashes($jobs['title']); ?>','<?php echo addslashes($jobs['employer']); ?>','<?= $jobs['url'] ?>','<?php echo $jobs['city'] . ", " . $jobs['state']; ?>','<?php echo addslashes(strip_tags($jobs['description'])); ?>','<?php echo $jobs['postingdate']; ?>');"  <?php } ?> ><i class="fa fa-star-o"></i></button></div>

</div>

<div class="col-lg-2">
<div class="job-date-mobile pull-left">
<span class=date><?php echo $beforedate.$appendString; ?> </span>
</div>

<?php
if(DOMAIN=='purejobalerts.com'){
?>
<a onclick="goog_report_conversion ('<?php echo $redirectURL ?>')"

<?php
}
else{
?> 
<div class="desktop-apply apply-btn pull-right"><a
<?php
}
?>


style="text-decoration:none; color:white; font-size: 15px; font-weight: normal;" href="<?php echo $redirectURL ?>" target="_blank">
<button type="button" class="btn btn-default apply">Apply</button></a>
<button type="button" id='<?= $jobs['id'] ?>' class="btn btn-default" <?php if (isset($_COOKIE['email'])) { ?>onclick="return savejobs('<?= $jobs['id'] ?>','<?php echo addslashes($jobs['title']); ?>','<?php echo addslashes($jobs['employer']); ?>','<?= $jobs['url'] ?>','<?php echo $jobs['city'] . ", " . $jobs['state']; ?>','<?php echo addslashes(strip_tags($jobs['description'])); ?>','<?php echo $jobs['postingdate']; ?>');"  <?php } ?> ><i class="fa fa-star-o"></i></button></div>

</span>
</div>
</div>

<?php
}
?>
</div>
</div>
<?php if($userAgent == "mobile") { ?>
<div id="ajax-loader" class="text-center hide-loader">
<img src="img/ajax-loader.gif" />
<input type="hidden" id="jobs-next-page-number" value="1">
<input type="hidden" id="jobs-current-page-number" value="0">
<input type="hidden" id="jobs-total-pages" value="<?php echo ceil($totalResults/20)?>">
</div>
<script type="text/javascript">
$(window).data('ajaxready', true).scroll(function(e) {
if ($(window).data('ajaxready') == false) return;

if ($(window).scrollTop() >= ($(document).height() - $(window).height() - 1500)) {
var next_page_num = parseInt($('#jobs-next-page-number').val());
var current_page_num = parseInt($('#jobs-current-page-number').val());
var total_pages = parseInt($('#jobs-total-pages').val())-1;
if(current_page_num < total_pages) {
$('#ajax-loader').removeClass('hide-loader');
$(window).data('ajaxready', false);
var req_url = "<?php print($requestedAjaxUrl); ?>";
$.ajax({
cache: false,
url: req_url+'&page='+next_page_num,
success: function(html) {
if (html) {
$('#job-results-wrapper').append(html);
$('#ajax-loader').addClass('hide-loader');
$('#jobs-current-page-number').val(next_page_num);
next_page_num = parseInt(next_page_num)+1;

$('#jobs-next-page-number').val(next_page_num);
} else {
$('#job-results-wrapper').html();
}
$(window).data('ajaxready', true);
}
});
}
}
});
</script>
<?php 
}

} else {


if(DOMAIN =='cylcon.com'){

$loction = '';
if(isset($_GET['l'])){
$loction = 'at '.$_GET['l'];
}

$keyword = '';
if(isset($_GET['q'])){
$keyword = $_GET['q'];
}

echo '<br />';
echo '<br />';


echo '<div style="width: 100%; text-align: center;"><h4><b>No matching '.$keyword.' jobs found '.$loction.'</b></h4></div>';

echo '<br />';
echo '<br />';

$keywords = array("Accounting","General Business","Other","Admin Clerical","General Labor","Pharmaceutical","Automotive","Government","Professional Services","Banking","Grocery","Purchasing - Procurement","Biotech","Health Care","QA - Quality Control","Broadcast - Journalism","Hotel - Hospitality","Real Estate","Business Development","Human Resources","Research","Construction","Information Technology","Restaurant - Food Service","Consultant","Installation - Maint - Repair","Retail","Customer Service","Insurance","Sales","Design","Inventory","Science","Distribution - Shipping","Legal","Skilled Labor - Trades","Education - Teaching","Legal Admin","Strategy - Planning","Engineering","Management","Supply Chain","Entry Level - New Grad","Manufacturing","Telecommunications","Executive","Marketing","Training","Facilities","Media - Journalism - Newspaper","Transportation","Finance","Nonprofit - Social Services","Warehouse","Franchise","Nurse");

shuffle($keywords);

$loc = 'US';

echo '<h3>You may also be interested in these jobs:</h3>';

echo '<div style="width: 100%;">';
echo '<table style="width: 100%;" >';
echo '<tr>';
echo "<td> <a href ='http://".DOMAIN."/jobs.php?q=".$keywords[0]."&l=".$loc."&popup=no' target = '_blank'>".$keywords[0]."</td>";
echo "<td> <a href ='http://".DOMAIN."/jobs.php?q=".$keywords[1]."&l=".$loc."&popup=no' target = '_blank'>".$keywords[1]."</td>";
echo "<td> <a href ='http://".DOMAIN."/jobs.php?q=".$keywords[2]."&l=".$loc."&popup=no' target = '_blank'>".$keywords[2]."</td>";
echo '</tr>';
echo '<tr>';
echo "<td> <a href ='http://".DOMAIN."/jobs.php?q=".$keywords[3]."&l=".$loc."&popup=no' target = '_blank'>".$keywords[3]."</td>";
echo "<td> <a href ='http://".DOMAIN."/jobs.php?q=".$keywords[4]."&l=".$loc."&popup=no' target = '_blank'>".$keywords[4]."</td>";
echo "<td> <a href ='http://".DOMAIN."/jobs.php?q=".$keywords[5]."&l=".$loc."&popup=no' target = '_blank'>".$keywords[5]."</td>";
echo '</tr>';
echo '<tr>';
echo "<td> <a href ='http://".DOMAIN."/jobs.php?q=".$keywords[6]."&l=".$loc."&popup=no' target = '_blank'>".$keywords[6]."</td>";
echo "<td> <a href ='http://".DOMAIN."/jobs.php?q=".$keywords[7]."&l=".$loc."&popup=no' target = '_blank'>".$keywords[7]."</td>";
echo "<td> <a href ='http://".DOMAIN."/jobs.php?q=".$keywords[8]."&l=".$loc."&popup=no' target = '_blank'>".$keywords[8]."</td>";
echo '</tr>';
echo '</table>';

echo '</div>';

echo '<br />';

echo '<div style="width: 100%;">';
echo '<p><b>Search suggestions to find available jobs:</b></p>';
echo '<ul>';
echo '<li>Broaden your search with new keywords</li>';
echo '<li>Try to search with generic keywords</li>';
echo '<li>Must check your spelling before you hit submit button</li>';
echo '<li>Modify your search with other parameters</li>';
echo '</ul>';
echo '</div>';

}else{
echo '<div style="width: 100%; text-align: center;"><b>No matching job found.</b></div>';
}


}
?>

<?php if($userAgent == "web") { ?>
<div id="job-results-pagination">
<?php if ($totalResults > 0) { ?>
<div id="job-results-pagination">
<table width="400" border="0" align="center" cellpadding="0" cellspacing="0" class="PHPBODY tablewdth">
<tr>
<td width="99" align="center" valign="middle" >
<?php if (($start - $per_page) >= 0) { $next = $start - $per_page; ?>
<a href="<?php print("$thispage" . ($next > 0 ? ("&start=") . $next : "")); ?>">Prev</a> 
<?php } ?>
</td>
<td width="201" align="center" valign="middle" class="selected">
Page <?php print($cur); ?> of <?php print($max_pages); ?><br> ( <?php print($num); ?> records )
</td>
<td width="100" align="center" valign="middle" > 
<?php if ($start + $per_page < $num) { ?>
<a href="<?php print("$thispage&start=" . max(0, $start + $per_page)); ?>">Next</a> 
<?php } ?>
</td>
</tr>
<tr><td colspan="3" align="center" valign="middle">&nbsp;</td></tr>
<tr><td colspan="3" align="center" valign="middle" class="selected"> 
<?php $eitherside = ($showeachside * $per_page);
if ($start + 1 > $eitherside)
print(" .... ");
$pg = 1;
for ($y = 0; $y < $num; $y += $per_page) {
$class = ($y == $start) ? "pageselected" : "";
if (($y > ($start - $eitherside)) && ($y < ($start + $eitherside))) { ?>
&nbsp;<a class="<?php print($class); ?>" href="<?php print("$thispage" . ($y > 0 ? ("&start=") . $y : "")); ?>"><?php print($pg); ?></a>
&nbsp;  <?php } 
$pg++;
}
if (($start + $eitherside) < $num)
print(" .... ");
?>
</td>
</tr>
</table>
</div>
<?php
}
?>
</div>
<?php } ?>
<div class="mobadds">

<?php 

if($userAgent=='mobile'){
// echo "mobile ad";
echo Leaderboard_A;
}

?>

</div>


<!-- list 3 end-->

</div>


</div>

</div>
<!-- panel v1 end -->
<?php
}
else{
?>
<div class="panel-v2">
	<?php 
/*	 if($_SERVER['REMOTE_ADDR'] == '112.196.1.221' || $_SERVER['REMOTE_ADDR'] == '111.93.206.202') {
		
	 }*/ ?>
<div class="container addhidden" style="padding:0;">
<div class="col-md-9 rightp-top-add">
<?php
if( DYNAMIC_TOP_AD != 'test') {
		
	echo DYNAMIC_TOP_AD;
}
?>  


</div>
<div class="col-md-3 panlv2-md3">&nbsp;</div>
</div>
<!-- Small modal -->
<div style="display:none;">
<button id="firstTimePopUp" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-sm">Popup</button>
</div>

<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<h4> We found <strong><?= $totalResults ?> "<?php echo ucwords($_GET['q']); ?>" jobs</strong> within <?= ($_GET['radius']) ? $_GET['radius'] : "10" ?> miles of <?= ($location) ? urldecode($location) : "" ?></h4>
<p>Where should we email your jobs? </p>

<form class="navbar-form" role="search" onsubmit="return getNewsLetterMail('<?php echo DOMAIN; ?>');" method="post">
<div class="form-group">
<input type="text" onfocus="$(this).css('border-color','#dce4ec');$(this).attr('placeholder','');" class="form-control input-lg" id="popupEmailAddress" name="EmailAddress" placeholder="Enter your email address">
<input type="hidden" class="form-control input-lg" name="Keyword" value="<?php if (isset($_GET['q'])) { echo $_GET['q']; } ?>" >
<input type="hidden" class="form-control input-lg" name="Location" value="<?php if (isset($_GET['l'])) { echo ($_GET['l']=='') ? $location :$_GET['l'] ; } ?>" >
<input type="hidden" class="form-control input-lg" name="Distance" value="<?php if (isset($_GET['radius'])) { echo $_GET['radius']; } ?>" >

</div><div id="createAlertbutton">
<button type="submit" class="btn btn-default" style="margin-left: -50px;">Create Alert</button>
<a class="close_btn" href="javascript:;" onclick="closePopUp();" style="position: absolute; margin-top: 8px; font-size: 14px;">Close</a>
</div>
</form>
<span>By clicking "create alert" I agree to the <?= TITLE ?>. <a href="<?= TERMSANDSERVICE ?>.php" target="_blank">Terms of Use</a></span>
</div>
</div>
</div>

<div class="row">
<div class="intro-text text-left">
<form name="jobsubmit" action="" class="navbar-form" role="search" onsubmit="return validateForm();">
<div class="form-group">
<input type="text" class="form-control input-lg" maxlength=512 size=31 id=what name=q autocomplete=off value="<?php echo $keyword; ?>">
</div>
<div class="form-group">
<input type="text" class="form-control input-lg" maxlength=64 size=27 id=where name=l autocomplete=off value="<?php echo urldecode($location); ?>" onkeyup="autocomplet()">
<ul id="country_list_id" style="display:none; margin-top:0; width: 22%;"></ul>
</div>
<?php if (isset($_COOKIE['login_type'])) { ?>
<div class="form-group">
<div style="width: 7%; float: left;">
<input type="checkbox" id="alert" name="alert" checked="checked" autocomplete=off value="<?php echo urldecode($location); ?>">
</div>
<div style="width: 60%">
<label>Create Job Alert</label>

</div>

</div>
<?php } ?>
<button type="submit" class="btn btn-default">Submit</button>
<!-- </form> -->
</div>
<div class="text-left">
<div class="col-lg-9 text-left">
<?php echo $breadcrumbCompleteLinks; ?>

</div>



<div class="col-lg-3 filter pull-right">
<div class="filter-sec">
<label><i class="fa-filter"></i>Filter</label>
<select class="form-control input-lg" name="sort" id="sort" onchange="this.form.submit()">
<option value="">Sort By Relevance</option>
<option <?php if ($_GET[ 'sort']=='relevance' ) { ?> selected
<?php } ?> value="relevance">Relevance</option>
<option <?php if ($_GET[ 'sort']=='date' ) { ?> selected
<?php } ?> value='date'>Date</option>
</select>

<select class="form-control input-lg" name="radius" id="radius" onchange="this.form.submit()">
<option value="">Sort By Distance</option>
<option <?php if($_GET[ 'radius']=='5' ) { ?> selected
<?php } ?> value="5">5 Miles</option>
<option <?php if($_GET[ 'radius']=='10' ) { ?> selected
<?php } ?> value="10">10 Miles</option>
<option <?php if($_GET[ 'radius']=='30' ) { ?> selected
<?php } ?> value="30">30 Miles</option>
<option <?php if($_GET[ 'radius']=='50' ) { ?> selected
<?php } ?> value="50">50 Miles</option>
<option <?php if($_GET[ 'radius']=='75' ) { ?> selected
<?php } ?> value="75">75 Miles</option>
<option <?php if($_GET[ 'radius']=='100' ) { ?> selected
<?php } ?> value="100">100 Miles</option>
<option <?php if($_GET[ 'radius']=='125' ) { ?> selected
<?php } ?> value="125">125 Miles</option>
<option <?php if($_GET[ 'radius']=='150' ) { ?> selected
<?php } ?> value="150">150 Miles</option>
<option <?php if($_GET[ 'radius']=='175' ) { ?> selected
<?php } ?> value="175">175 Miles</option>
<option <?php if($_GET[ 'radius']=='200' ) { ?> selected
<?php } ?> value="200">200 Miles</option>
</select>
</div>
<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
<div class=" text-left">

</div>
</div>


<div class="col-lg-9 text-left">
<!-- /451298204/FBG_OakJobs_Job_Search_Responsive_Leaderboard_A -->
<div class="webadds">
<?php
if($isFor_MediaADS){
if($userAgent!="mobile"){
echo $medianet_desktop;  

}else{
echo $media_mobile_ads;
}  
}else{           

echo Leaderboard_A ;    
}
?>
</div>







<h6 style="width:300px;float:left;"><?php echo $jobSearchHeading; ?></h6><span id="AlertMessage" style="float:left;color:#96CE69"></span>
<div style="clear:both"></div>

<?php if (!isset($_COOKIE['login_type'])) { ?>
<div class="dailyalert clearfix">
<p><i class="fa-envelope"></i>Get the newest jobs like these daily:</p>
<form role="search" onsubmit="return getNewsLetterMail1('<?php echo DOMAIN; ?>');" method="post">
<div class="form-group">
<input type="text" class="form-control input-lg" onfocus="$(this).css('border-color','#dce4ec');$(this).attr('placeholder','');" id="EmailAddress" name="EmailAddress" placeholder="Enter your email address">
<input type="hidden" class="form-control input-lg" name="Keyword" value="<?php if (isset($_GET['q'])) { echo $_GET['q']; } ?>">
<input type="hidden" class="form-control input-lg" name="Location" value="<?php if (isset($_GET['l'])) { echo $_GET['l']; } ?>">
<input type="hidden" class="form-control input-lg" name="Distance" value="<?php if (isset($radius)) { echo $radius; } ?>">

</div>
<div id="createAlert">
<button type="submit" class="btn btn-default">Create Alert</button>
</div>
</form>
</div>
<?php } ?>



<?php  if ($totalResults > 0) { 
if($userAgent == "mobile") { 
echo '<div id="searchCount">Number of jobs found : '.$totalResults.'</div>';
} else {
echo '<div id="searchCount">Jobs '.$showstart.' to '.$showend.' of '.$totalResults.'</div>';
}
?>
<div id="job-results-wrapper">
<div id="jobs-page-num-0">
<?php


foreach ($jobsResult as $jobkeyid=>$jobs) {

	//$jobDescriptionQry   = "SELECT description FROM tbl_all_jobs_description WHERE id = " . $jobs['id'] . " LIMIT 0,1";
	//$descriptionResults  = $objDBManager->fetchRecord($jobDescriptionQry);
	//$jobs['description'] = $descriptionResults[0]['description'];


	$queryStr = "url=".str_replace("&","##", $jobs['url'])."&sourcename=".urlencode($jobs['sourcename'])."&source=".urlencode($jobs['source'])."&city=".urlencode($jobs['city'])."&state=".urlencode($jobs['state'])."&employer=".urlencode($jobs['employer'])."&title=".urlencode($jobs['title'])."&cpc=".$jobs['cpc']."&gcpc=".$jobs['gcpc'];
	$compressed = base64_encode($queryStr) ;

	$onmouse = (strpos(strtolower($jobs['sourcename']), 'indeed1234') !== false) ? $jobs['onmousedown'] : '';

	$forExtraValues= (isset($_GET['clicktype'])) ? '&clicktype='.$_GET['clicktype'].'' : ''; // for notification link
	$redirectURL = (strpos(strtolower($jobs['sourcename']), 'indeed1234') !== false) ?  $jobs['url'] : "RedirectWEB.php?q=".$compressed."&token=".$jobs['id'].$forExtraValues."";

	?>

	<div class="joblists clearfix row">
	<div class="middle-add-align">
	<?php
	if($isFor_MediaADS) {
		if($jobkeyid == 5)  {  	echo Leaderboard_A ;   }
		if($jobkeyid == 10) {   echo Leaderboard_B;    }
		if($jobkeyid == 15) {
			if($userAgent=="mobile" || DYNAMIC_TOP_AD != 'test') {
				echo Leaderboard_mobile_top;
			} else {
				$SJA_Keyword    = (isset($_GET['q']))?$_GET['q']:'';
				$SJA_Location   = (isset($_GET['l']))?$_GET['l']:'';
				$SJA_Domain     = DOMAIN;
				$curPageNum     = (isset($_GET['start']))? ((int)$_GET['start']/20)+1:1;
				if(($curPageNum % 2) != 0) {
				$smsAdCode = getSMSJobAd1($SJA_Keyword, $SJA_Location, $SJA_Domain, $mobileAdStyle);
				}  else {
				$smsAdCode = getSMSJobAd2($SJA_Keyword, $SJA_Location, $SJA_Domain, $mobileAdStyle);
				}
				echo $smsAdCode;
			}
		}
	}  else  {      //For those domains without media net ads
		if($jobkeyid==5){ echo Leaderboard_B; }
		if($jobkeyid==10 && $userAgent=="mobile")  {
			echo Leaderboard_mobile_top;
		} 
		if($jobkeyid==15)   {
			$SJA_Keyword    = (isset($_GET['q']))?$_GET['q']:'';
			$SJA_Location   = (isset($_GET['l']))?$_GET['l']:'';
			$SJA_Domain     = DOMAIN;
			$curPageNum     = (isset($_GET['start']))? ((int)$_GET['start']/20)+1:1;
			if(($curPageNum % 2) != 0) {
				$smsAdCode = getSMSJobAd1($SJA_Keyword, $SJA_Location, $SJA_Domain, $mobileAdStyle);
			}  else {
				$smsAdCode = getSMSJobAd2($SJA_Keyword, $SJA_Location, $SJA_Domain, $mobileAdStyle);
			}
			echo $smsAdCode;
		}
	}
	?>
	</div>

	<div class="col-lg-10 col-xs-12">

	<?php

	$logoUrl =  getLogoImageUrl($jobs['employer']);

	?>
	<span class="left-img-des">
	<div class="left-img-des-wrap">
	<?php if($logoUrl != '') { ?>
	<img src="<?php echo $logoUrl; ?>" style="">
	<?php  } ?>
	</div>
	</span>
	<span class="right-txt-des">
	<?php
	if(DOMAIN=='purejobalerts.com'){


	?>
	<a onclick="goog_report_conversion ('<?php echo $redirectURL ?>')"
	<?php
	}
	else{

	?>
	<a

	<?php
	}
	?>
	onmousedown="<?php echo $onmouse ?>" href="<?php echo $redirectURL ?>" target="_blank">
	<?php echo utf8_decode($jobs['title']); ?>
	</a>
	<p>
	<strong><?php echo utf8_decode($jobs['employer']); ?>
	<span class='location'> 
	<?php if($jobs['displaysource'] != '') { $source_name = " - <b style='color:black;'>" . strtoupper($jobs['displaysource']) . "</b>"; } else { $source_name = ""; } ?>
	<?php echo " - ". $jobs['city'] . ", " . $jobs['state'];// . $source_name; ?>
	</span></strong>
	</p>
	<!-- <em>
	Posted yesterday on ZipRecruiter - 
	<a href="#">ZipApply</a>
	</em> -->
	<p class="p-descript description-web">
	<?php $truncated = utf8_decode((strlen(strip_tags(html_entity_decode($jobs['description']))) > 200) ? substr(strip_tags(html_entity_decode($jobs['description'])), 0, 200) . '...' : strip_tags(html_entity_decode($jobs['description']))); echo utf8_decode($truncated); ?>
	</p>
	<p class="p-descript description-mob">
	<?php $truncated = utf8_decode((strlen(strip_tags(html_entity_decode($jobs['description']))) > 70) ? substr(strip_tags(html_entity_decode($jobs['description'])), 0, 70) . '...' : strip_tags(html_entity_decode($jobs['description']))); echo utf8_decode($truncated); ?>
	</p>
	<?php
	$appendString = '';
	if(isset($jobs['postingdate']) && !empty($jobs['postingdate'])) {
	if(DOMAIN =='purejobalerts.com') {
	//        $appendString = " | Score : ".$jobs['_score']." | Priority : ".$jobs['priority'];
	}
	$now        = time();
	$your_date  = strtotime($jobs['postingdate']);
	$datediff   = $now - $your_date;
	$dateAgo    = floor($datediff / (60 * 60 * 24));
	//$beforedate = ($dateAgo > 0) ? "$dateAgo days ago" : "New";
	$beforedate = ($dateAgo > 0) ? "$dateAgo days ago" : "<span style='color: #ce433a;'>New</span>";
	} else {
	$beforedate='';
	}  ?>
	<div class="job-date-desktop"><span class=date><?php echo $beforedate; ?></span></div>
	<div style="display: none;" class="mobile-apply apply-btn pull-right">
	<?php
	if(DOMAIN=='purejobalerts.com'){
	?>
	<a onclick="goog_report_conversion ('<?php echo $redirectURL ?>')"

	<?php
	}
	else{
	?> 
	<a
	<?php
	}
	?>
	style="text-decoration:none; color:white; font-size: 15px; font-weight: normal;" href="<?php echo $redirectURL ?>" target="_blank">



	<button type="button" class="btn btn-default apply">Apply</button>
	</a>
	<?php 
	if(isset($_COOKIE[ 'email'])) { 
		$savejobs_title 	= addslashes($jobs['title']);
		$savejobs_employer 	= addslashes($jobs['employer']);
		$savejobs_desc	 	= addslashes(strip_tags($jobs['description']));
		$onClickButton = "return savejobs('".$jobs['id']."','".$savejobs_title."','".$savejobs_employer."','".$jobs['joburl']."','".$jobs['city'].", ".$jobs['state']."','".$savejobs_desc."','".$jobs['postingdate']."');";
		// savejobs(id,title,employer,joburl,location,description,postedtime)
		// location => city, state 
	} else {
		$onClickButton = 'return saveJobsLoginAlert();';
	}
	?>
	<button type="button" id='<?= $jobs['id'] ?>' class="btn btn-default"  title="Save this Job" onclick="<?php echo $onClickButton; ?>" ><i class="fa fa-star-o"></i></button>
	
	
	</div>            


	</div>

	<div class="col-lg-2 col-xs-12 post-btn-set">
	<div class="job-date-mobile pull-left"><span class=date><?php echo $beforedate; ?></span></div>
	<div class="apply-btn pull-right">
	<a style="text-decoration:none; color:white; font-size: 15px; font-weight: normal;" href="<?php echo $redirectURL ?>" target="_blank">
	<button type="button" class="btn btn-default apply">Apply</button>
	</a>
	<?php 
	if(isset($_COOKIE[ 'email'])) { 
		$savejobs_title 	= addslashes($jobs['title']);
		$savejobs_employer 	= addslashes($jobs['employer']);
		$savejobs_desc	 	= addslashes(strip_tags($jobs['description']));
		$onClickButton = "return savejobs('".$jobs['id']."','".$savejobs_title."','".$savejobs_employer."','".$jobs['joburl']."','".$jobs['city'].", ".$jobs['state']."','".$savejobs_desc."','".$jobs['postingdate']."');";
		// savejobs(id,title,employer,joburl,location,description,postedtime)
		// location => city, state 
	} else {
		$onClickButton = 'return saveJobsLoginAlert();';
	}
	?>
	<button type="button" id='<?= $jobs['id'] ?>' class="btn btn-default"  title="Save this Job" onclick="<?php echo $onClickButton; ?>" ><i class="fa fa-star-o"></i></button>
	
		
	
	</div>
	</div>
	</span>
	</div>

<?php
}
?>
</div>
</div>
<?php if($userAgent == "mobile") { ?>
<div id="ajax-loader" class="text-center hide-loader">
<img src="img/ajax-loader.gif" />
<input type="hidden" id="jobs-next-page-number" value="1">
<input type="hidden" id="jobs-current-page-number" value="0">
<input type="hidden" id="jobs-total-pages" value="<?php echo ceil($totalResults/20)?>">
</div>
<script type="text/javascript">
$(window).data('ajaxready', true).scroll(function(e) {
if ($(window).data('ajaxready') == false) return;

if ($(window).scrollTop() >= ($(document).height() - $(window).height() - 1500)) {
var next_page_num = parseInt($('#jobs-next-page-number').val());
var current_page_num = parseInt($('#jobs-current-page-number').val());
var total_pages = parseInt($('#jobs-total-pages').val())-1;
if(current_page_num < total_pages) {
$('#ajax-loader').removeClass('hide-loader');
$(window).data('ajaxready', false);
var req_url = "<?php print($requestedAjaxUrl); ?>";
$.ajax({
cache: false,
url: req_url+'&page='+next_page_num,
success: function(html) {
if (html) {
$('#job-results-wrapper').append(html);
$('#ajax-loader').addClass('hide-loader');
$('#jobs-current-page-number').val(next_page_num);
next_page_num = parseInt(next_page_num)+1;

$('#jobs-next-page-number').val(next_page_num);
} else {
$('#job-results-wrapper').html();
}
$(window).data('ajaxready', true);
}
});
}
}
});
</script>
<?php
} 
} else {
echo '<div style="width: 100%; text-align: center;"><b>No matching job found.</b></div>';
}
?>
<?php if($userAgent == "web") { ?>
<div id="job-results-pagination">
<?php if ($totalResults > 0) { ?>

<table width="400" border="0" align="center" cellpadding="0" cellspacing="0" class="PHPBODY tablewdth">
<tr>
<td width="99" align="center" valign="middle">
<?php if (($start - $per_page) >= 0) { $next = $start - $per_page; ?>
<a href="<?php print(" $thispage " . ($next > 0 ? ("&start=") . $next : " ")); ?>">Prev</a>
<?php } ?>
</td>
<td width="201" align="center" valign="middle" class="selected">
Page
<?php print($cur); ?> of
<?php print($max_pages); ?>
<br> (
<?php print($num); ?> records )
</td>
<td width="100" align="center" valign="middle">
<?php if ($start + $per_page < $num) { ?>
<a href="<?php print(" $thispage&start=" . max(0, $start + $per_page)); ?>">Next</a>
<?php } ?>
</td>
</tr>
<tr>
<td colspan="3" align="center" valign="middle">&nbsp;</td>
</tr>
<tr>
<td colspan="3" align="center" valign="middle" class="selected">
<?php $eitherside = ($showeachside * $per_page);
if ($start + 1 > $eitherside)
print(" .... ");
$pg = 1;
for ($y = 0; $y < $num; $y += $per_page) {
$class = ($y == $start) ? "pageselected" : "";
if (($y > ($start - $eitherside)) && ($y < ($start + $eitherside))) { ?>
&nbsp;<a class="<?php print($class); ?>" href="<?php print("$thispage" . ($y > 0 ? ("&start=") . $y : "")); ?>"><?php print($pg); ?></a>
&nbsp;  <?php } 
$pg++;
}
if (($start + $eitherside) < $num)
print(" .... ");
?>
</td>
</tr>
</table>
<?php
}
?>
</div>
<?php } ?>
<div class="middle-add-align" style="">

<?php
// echo Leaderboard_C;
?>   
</div>



<!-- list 3 end-->
<div class="add-banner ">

</div>

</div>
<div class="col-lg-3 filter pull-right panel2filtermb">



<?php
echo LEFT_RIGHT_ALIGN_AD;
?>


</div>






</div>

</div>

</div>
<?php
}
?>
<!-- panel-v2 end -->  


</div>
</section>



<?php
include("include/footer.php");
?>

<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
<div class="scroll-top page-scroll visible-xs visble-sm">
<a class="btn btn-primary" href="#page-top">
<i class="fa fa-chevron-up"></i>
</a>
</div>


<script type="text/javascript">

<?php
if (($_SESSION['showPopUp'] == 'Yes' && $totalResults && !isset($_COOKIE['login_type'])) || SHOWALERTPOPUP || ( $_GET['popup']=='yes')) {

?>
autoClickPopUp();
function autoClickPopUp() {
$('#firstTimePopUp').click();
}
<?php
$_SESSION['showPopUp'] = 'No';
}
?>

function getNewsLetterMail(domain) {



if($('#popupEmailAddress').val()==''){
$('#popupEmailAddress').css('border-color', 'red');
$('#popupEmailAddress').val('');
$('#popupEmailAddress').attr('placeholder', 'The email format was not valid');

return false;            
}
var email;
email=$('#popupEmailAddress').val();
var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
if (filter.test(email)) {

}else{
//alert('email format is not correct');
$('#popupEmailAddress').css('border-color', 'red');
$('#popupEmailAddress').val('');
$('#popupEmailAddress').attr('placeholder', 'The email format is not valid');

return false;
}
$("#createAlertbutton").html("<img src='img/loader.gif' >");
$.post( "insertalert.php", 
{ 
Keyword:"<?php echo $_GET['q']; ?>",
Distance:"<?php echo $radius; ?>",
EmailAddress:email,
Location:"<?php echo $_GET['l']; ?>" 
}).done(function( data ) {            

setTimeout(function() {
$("#createAlertbutton").html("<span style='color:#7cc243;'>Job Alert added successfully</span>");
}, 2000);
//set time out
setTimeout(function() {               
$('.modal').modal('toggle');
}, 3000);

if(domain == 'purejobalerts.com'){
	createAlert_goog_report_conversion('Create Alert Button Clicked');
}

// window.location.reload();
window.location= "http://<?php echo $_SERVER['HTTP_HOST'] ?>/jobs.php?q=<?php echo $_GET['q'] ?>&l=<?php echo $_GET['l'] ?>&sort=relevance&radius=<?php echo $radius ?>&popup=no"

//window.location.href="jobs.php";
return false;
});


return false;
}

function getNewsLetterMail1(domain) {

// createAlert_goog_report_conversion('Create Alert Button Clicked');

if($('#EmailAddress').val()==''){
$('#EmailAddress').css('border-color', 'red');
$('#EmailAddress').val('');
$('#EmailAddress').attr('placeholder', 'The email format is not valid');

return false;

}
var email;
email=$('#EmailAddress').val();
var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
if (filter.test(email)) {

}else{
$('#EmailAddress').css('border-color', 'red');
$('#EmailAddress').val('');
$('#EmailAddress').attr('placeholder', 'The email format is not valid');

return false;
}
$("#createAlert").html("<img src='img/loader.gif' >");
$.post("insertalert.php", 
{ 
Keyword:"<?php echo $_GET['q']; ?>",
Distance:"<?php echo $radius; ?>",
EmailAddress:email,
Location:"<?php echo $_GET['l']; ?>" 
}).done(function( data ) {           

setTimeout(function() {
$('#EmailAddress').val('');
$('#EmailAddress').css('border', '');    
$('#AlertMessage').text('Job Alert added successfully');   
$("#createAlert").html('<button type="submit" class="btn btn-default">Create Alert</button>');
}, 2000);

if(domain == 'purejobalerts.com'){
	createAlert_goog_report_conversion('Create Alert Button Clicked');
}

window.location.reload();
//window.location.href="sign_in.php";            
return false;
});



return false;
}
function saveJobsLoginAlert() {
	
	bootbox.alert({
		message: "You will have to logged in for saving jobs.",
		callback: function () {
			window.location.href = 'sign_in.php';
		},
		buttons: {
			ok: {
				label: "Login",
			}
		}
	}); 
}

function savejobs(id,title,employer,joburl,location,description,postedtime) {
	
	$.get( "get_saved_jobs_list.php", 
	{ 
		title:title,
		employer:employer,
		location:location
	}).done(function(data) {
		var myAry = jQuery.parseJSON(data);
		if(myAry.status == '0') {
			// Insert Alert
			$.post( "insertalert.php", 
			{ 
				id:id,
				title:title,
				employer:employer,
				joburl:joburl,
				email_id:'<?php echo $_COOKIE['email']; ?>',
				location:location,
				description:description,
				postedtime:postedtime,
				operation:'save jobs' 
			}).done(function(data) {
				bootbox.alert("<h6>Message</h6>Job has been saved!", function() { });
				$('#job-results-wrapper .desktop-apply button#'+id).addClass('apply');
				$('#job-results-wrapper .desktop-apply button#'+id).prop('disabled', true);  
			});
		}	else 	{
			bootbox.alert("<h6>Message</h6>You have already saved this job!", function() { });
			$('#job-results-wrapper .desktop-apply button#'+id).addClass('apply');
			$('#job-results-wrapper .desktop-apply button#'+id).prop('disabled', true);  
		}
		
	});
	return false;
}

function validateForm(){
/*if($('#what').val() == ''){
$('#where').css('border-color','#ccc');
$('#where').css('border-bottom-color','#aaa');
$('#where').css('border-right-color','#aaa');
$('#what').css('border-color','red');
return false;
} else*/ if($('#where').val() == ''){
$('#what').css('border-color','#ccc');
$('#what').css('border-bottom-color','#aaa');
$('#what').css('border-right-color','#aaa');
$('#where').css('border-color','red');
return false;
}
}

function closePopUp() {
$('.modal').modal('toggle');
}
</script>

<script>
var headerHeight = $('.navbar-default').outerHeight() + 163; // true value, adds margins to the total height
var footerHeight = $('footer').outerHeight() + 10;
$('#account-overview-container').affix({
    offset: {
        top: headerHeight,
        bottom: footerHeight
    }
}).on('affix.bs.affix', function () { // before affix
    $(this).css({
        /*'top': headerHeight,*/    // for fixed height
        'width': $(this).outerWidth()  // variable widths
    });
});
</script>

</body>

</html>

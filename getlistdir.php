<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"> </script>
	<script type="text/javascript">
		$(function() {
			$("#directory_listing").change(function() {
				$("#dirform").submit();
			});
		});
	</script>
</head>
<body>
<?php
	if(isset($_GET['dir'])) {
		$comparedData 	=  getComparedDataFromBothServer($_GET['dir']);
	} else {
		$comparedData 	=  getComparedDataFromBothServer();
	}
	$comparedData		=	json_decode($comparedData);
	$oldServerFiles 		= $comparedData->old->files;
	$updatedOldServerFiles  = [];
	foreach($oldServerFiles as $key=>$val){
		$updatedOldServerFiles[$val] = $key;
	}
	$oldServerDirectories 	= $comparedData->old->dir;
	$oldServerDirectories = (array)$oldServerDirectories;
	$newServerFiles 		= $comparedData->newfiles;
	?>
<div class="container">
	<div class="row">
		<div class="col-md-12 text-center">
			<h1>NEW 27 SERVER FILES</h1>
		</div>
		<div class="col-md-12 text-center">
			<form method="get" id="dirform">
				<select name="dir" id="directory_listing" style="width:50%;">
					<option value="">- Select Directory of OLD Server-</option>
					<?php
					$selectedVal='';
					asort($oldServerDirectories);
					foreach($oldServerDirectories as $k=>$v) {
						if(isset($_GET['dir']) && ($_GET['dir'] == $v)) {
							$selectedVal = 'selected="selected"';
						} else {
							$selectedVal='';
						}
						echo '<option '.$selectedVal.' value="'.$v.'">'.$v.'</option>';
					}
					?>
				</select>
			</form>
		</div>
	</div>
	<div class="row">
	<table class="table" border="1">
	<tr>
		<th>
			<b>File Name</b>
		</th>
		<th>
			<b>Date - OLD 27 Server</b>
		</th>
		<th>
			<b>Date - NEW 27 Server</b>
		</th>
	</tr>
	<?php
    foreach($newServerFiles as $k=>$v) {   // $k is timestamp & $v is file name
		if(date('F d Y',$updatedOldServerFiles[$v]) == date('F d Y',$k)) {
			$className = 'bg-success text-white';
		} else {
			if(strtotime(date('F d Y',$updatedOldServerFiles[$v])) <= strtotime(date('F d Y',$k))) {
				$className = 'bg-warning text-blue';
			} else {
				$className = 'bg-danger text-white';
			}
		}
        echo'<tr class="'.$className.'">';
			echo '<td>'.$v.'</td>';
			if(isset($updatedOldServerFiles[$v])) {
				echo '<td>'.date('F d Y, H:i:s',$updatedOldServerFiles[$v]).'</td>';
				unset($updatedOldServerFiles[$v]);
			}	else {
				echo '<td> - </td>';
			}
			echo '<td>'.date('F d Y, H:i:s',$k).'</td>';
			
		echo '</tr>';
    }
    if(count($updatedOldServerFiles) > 0) {
		echo'<tr><td colspan="3" class="heading text-center"> Files not available on NEW 27 Server</td></tr>';
		foreach($updatedOldServerFiles as $k=>$v) {
			echo'<tr>';
				echo '<td>'.$k.'</td>';
				echo '<td>'.date('F d Y, H:i:s',$v).'</td>';
				echo '<td> - </td>';
			echo '</tr>';
		}
	}
		
    echo '</table>';
    
function getDataFromOld27Server($dirName='') {
	if(empty($dirName)) {
		$url = 'http://cylcon.com/getlistdir.php';
	} else {
		$url = 'http://cylcon.com/getlistdir.php?dir='.$dirName;
	}
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	$output = curl_exec($ch);
	curl_close($ch); 
	return $output;
}

function getComparedDataFromBothServer($dirName='.') {
	if($dirName == '') { $dirName = '.'; }
	$old27Files 				= getDataFromOld27Server($dirName);
	$old27FilesAry 				= json_decode($old27Files);
	$updatedOld27FilesAry 		= array();
	foreach($old27FilesAry->files as $k=>$v) {
		$updatedOld27FilesAry[$v] = $k;
	}

	$fileList = array();
	if ($handle = opendir($dirName)) {
		while (false !== ($entry = readdir($handle))) {
			if ($entry != "." && $entry != "..") {
				if(!is_dir($entry)) {
					$fileList[filemtime($entry)] = $entry;
				}
			}
		}
		closedir($handle);
	}
	krsort($fileList);
	$resAry = array('old' => json_decode($old27Files), 'newfiles' => $fileList);
	return json_encode($resAry);
}

?>
	</div>
</div>
</body>
</html>

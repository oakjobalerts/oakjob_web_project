
<?php

require_once('/var/www/vhosts/oakjob/public_html/include/DBManager.php');

$failure_case ="" ;   

class RedirectLandingPageValidations
{
     

    function redirectLandingPageValidationsMethod($getUrlResult)
    {
        
        
        global $failure_case ;

        if(strstr(strtolower($getUrlResult['source']), "banner") == true)
            return;

        $checksArray = array();
        
        $json        = file_get_contents('/var/nfs-93/redirect/mis_logs/masterSourceMapping/expireFeed.json');
        $resultArray = json_decode($json);
        foreach ($resultArray as $value) {
            $feedName = $value->feedName;
            if (strtolower($feedName) == strtolower($getUrlResult['source'])) {
                $checksArray = (array) $value;
                break;
            }
        }

        $memcache_obj_elastic = memcache_connect("oakelasticcache.25s6pq.0001.use1.cache.amazonaws.com", 11211);
        $value = memcache_get($memcache_obj_elastic, $_SERVER['REMOTE_ADDR']);

        if (!empty($checksArray)) {
            
            $botType = $checksArray['botType'];

            if (!empty($botType)) {

                $cbBot   = $this -> checkIpBotSeries("cb.csv");
                if($cbBot){
                    if(empty($value))
                         $value = "cb";
                     else
                        $value = $value . "|cb";
                }

                $leadBot = $this -> checkIpBotSeries("lead5ip.csv");
                if($leadBot){
                    if(empty($value))
                         $value = "lead";
                     else
                        $value = $value . "|lead";
                }


                if (!empty($value) && strstr(strtolower($value), strtolower($botType)) == true) {
                    $failure_case ="bot_click_" . $botType;
                    $getUrlResult['bot_feeds'] = $value;
                    $getUrlResult['job_invisible'] = "yes";
                    $this ->redirectToLandingPage($getUrlResult);
                } 
            }
              
                            

            if ($checksArray['rulesApplies'] == "1") {
   
                //if any bot exist
                if (!empty($value)) {
                    $failure_case ="bot_click";
                    $getUrlResult['bot_feeds'] = $value;
                    $this ->redirectToLandingPage($getUrlResult);
                }

                //check oak and Honeypot bot
                $key   = "oak_hp_";
                $value = memcache_get($memcache_obj_elastic, $key . $_SERVER['REMOTE_ADDR']);
                if (!empty($value)) {
                    $failure_case ="bot_click_oak_hp";
                    $this ->redirectToLandingPage($getUrlResult);
                }

                //CHECK FOR $) CLICKS IN @$ HOURS
                if (!empty($getUrlResult['email'])) {
                    $count = memcache_get($memcache_obj_elastic, $getUrlResult['email']);
                    if (empty($count))
                        $count = 0;
                    $count = $count + 1;
                    memcache_set($memcache_obj_elastic, $getUrlResult['email'], $count, false, time() + 43200);
                    if ($count > 40) {
                        $failure_case ="clicks_limit_exceeded";
                        $this ->redirectToLandingPage($getUrlResult);
                    }
                }
                
                //CHECK IS JOB EXPIRE
                $expireHour = $checksArray['expireHours'];

                if($expireHour != '0')
                {   
                    $now        = time(); // or your date as well
                    $your_date  = strtotime((isset($getUrlResult['date']) && $getUrlResult['date'] != '') ? $getUrlResult['date'] : date('Y-m-d H:i'));
                    
                    $datediff = $now - $your_date;
                    $hours    = floor($datediff / (60 * 60));
                    
                    if ($hours > $expireHour) {
                        $failure_case ="expire_job";
                        $this ->redirectToLandingPage($getUrlResult);
                    }
                }
                
                $minCpc = $checksArray['minCpc'];
                //CHECK CPC OF JOB
                if ($getUrlResult['cpc'] < $minCpc) {
                    $failure_case ="expire_job";
                    $this ->redirectToLandingPage($getUrlResult);
                }
                
                //IF ELITE-110 USERS
                if (strstr(strtolower($getUrlResult['provider']), "110-") == true && strstr(strtolower($getUrlResult['provider']), "cylcon") == false) {
                    $failure_case ="elite-110-users";
                    $this ->redirectToLandingPage($getUrlResult);
                }
            } 

            if ($feedDisabled == '1') {
                $getUrlResult['job_invisible'] = "yes";
                $failure_case ="feed_disabled";
                $this ->redirectToLandingPage($getUrlResult);
            }
        }
        
        //CHECK IS COUNTRY MISMATCH
        $ip                  = $_SERVER['REMOTE_ADDR'];
        $jobCountry          = $getUrlResult['job_country'];
        $redirectToActualJob = false;
        
        $file = fopen("/var/nfs-93/redirect/mis_logs/IpSeries/ip.txt", "r");
        
        
        while (!feof($file)) {
            $iparr = split(",", fgets($file));
            
            $min = trim($iparr[0]);
            $max = trim($iparr[1]);
            
            if (ip2long($min) <= ip2long($ip) && ip2long($ip) <= ip2long($max)) {
                $ipCountry = trim($iparr[2]);
                break;
            }
        }
        
        if (empty($getUrlResult['job_country']) && $ipCountry != '')
            $redirectToActualJob = true;
        else if (empty($getUrlResult['job_country']))
            $redirectToActualJob = false;
        else if (strtolower($ipCountry) == 'united states' && (strtolower($jobCountry) == 'usa' || strtolower($jobCountry) == 'us' || strtolower($jobCountry) == 'united states'))
            $redirectToActualJob = true;
        else if (strtolower($ipCountry) == 'united kingdom' && (strtolower($jobCountry) == 'gb' || strtolower($jobCountry) == 'uk' || strtolower($jobCountry) == 'united kingdom'))
            $redirectToActualJob = true;
        else if (strtolower($ipCountry) == 'australia' && (strtolower($jobCountry) == 'aus' || strtolower($jobCountry) == 'au' || strtolower($jobCountry) == 'australia'))
            $redirectToActualJob = true;
        else if (strtolower($ipCountry) == 'ireland' && (strtolower($jobCountry) == 'ie' || strtolower($jobCountry) == 'ireland'))
            $redirectToActualJob = true;
        
        fclose($file);
        
        if ($redirectToActualJob == false) {
            $failure_case ="country_mismatch";
            $this ->redirectToLandingPage($getUrlResult);
        }
    }
    
    function redirectToLandingPage($getUrlResult)
    {
        global $failure_case;

        if(isset($getUrlResult['failure_case']) && !empty($getUrlResult['failure_case'])){
             $getUrlResult['failure_case']     = $failure_case."_".$getUrlResult['failure_case'];
        }else{
            $getUrlResult['failure_case']     = $failure_case;
        }
        
        $getUrlResult['lp_click_type']     = "LP";

        if(!isset($getUrlResult['landing_page_type']) 
            || empty($getUrlResult['landing_page_type'])){
             $getUrlResult['landing_page_type'] = "2";
         }

        $this -> log_clicks_queue_java($getUrlResult);
        
        $session_key = $getUrlResult['job_position'] . '$$$' . $getUrlResult['source'] . '$$$' . $getUrlResult['jobtitle'] . '$$$' . $getUrlResult['email'];
        
        $session_key            = md5($session_key);
        $_SESSION[$session_key] = json_encode($getUrlResult);
        $urlRecirect            = "/jobs_landing_page.php?sk=" . $session_key;
        
        header("Location: " . $urlRecirect);
        die;
    }
    
    function log_clicks_queue_java($getUrlResult)
    {

        $logeventsObj = new LogEvents();
        $logeventsObj->logEventstoQueue($getUrlResult);
        // $logeventsObj->logEventstoDynamoQueue($getUrlResult);
    }
    
    
    function checkIpBotSeries($fileName)
    {

        $ip                        =  $_SERVER['REMOTE_ADDR'];
        $file                      = fopen("/var/nfs-93/redirect/mis_logs/IpSeries/" . $fileName, "r"); //cb.csv//lead5ip.csv
        $redirectToLandingPageFlag = false;
        while (!feof($file)) {
            $iparr = split(",", fgets($file));
            $min   = trim($iparr[0]);
            $max   = trim($iparr[1]);
            if (ip2long($min) <= ip2long($ip) && ip2long($ip) <= ip2long($max)) {
                $redirectToLandingPageFlag = true;
                break;
            }
        }
        fclose($file);
        return $redirectToLandingPageFlag;
    }
}
?>
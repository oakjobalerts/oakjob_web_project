<?php 
error_reporting(E_ALL); ini_set('display_errors', 1);
ob_start(); ?>
<!DOCTYPE html>
<html lang="en">
<?php 
    session_start(); 
    $_SESSION['showPopUp'] = "Yes";
    require_once('configuration.php');
?>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?=TITLE?></title>

    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/oakstyle.css" rel="stylesheet">

    <link rel="shortcut icon" href="<?=FAVICONPATH?>favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?=FAVICONPATH?>favicon.ico" type="image/x-icon">
    
    <!-- Custom Fonts -->
    <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- UserVoice JavaScript SDK (only needed once on a page) -->
<script>(function(){var uv=document.createElement('script');uv.type='text/javascript';uv.async=true;uv.src='//widget.uservoice.com/RUVxfxpYSZzV74j6P7cENw.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(uv,s)})()</script>

<!-- A link to launch the Classic Widget -->
			
</head>

<body id="page-top" class="index">
<?php include('common.php'); ?>
    <!-- Navigation -->
    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div style="height: 50px">&nbsp;&nbsp;&nbsp;</div>
                
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                
                <?php if(isset($_COOKIE['login_type']) && !empty($_COOKIE['login_type'])){ ?>
                
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <ul class="mobile">
                    <li><a class="active" href="#">Search Jobs</a></li>
                     <?php if(!isset($_COOKIE['login_type'])) { ?>
						 <li><a href="sign_in.php">Manage Alerts</a></li>
						 <li><a href="sign_in.php">Saved Jobs</a></li>
                     <?php } else { ?>
						  <li><a href="alert_list.php">Manage Alerts</a></li>
						  <li><a href="savedjobs.php">Saved Jobs</a></li>
						  <li><a href="profile.php?id=<?php echo base64_encode($_COOKIE['id']);?>">Manage Profile</a></li>
                      <?php } ?>
                    <li><a href="javascript:void(0)" data-uv-lightbox="classic_widget" data-uv-mode="full" data-uv-primary-color="#cc6d00" data-uv-link-color="#007dbf" data-uv-default-mode="support" data-uv-forum-id="271999">Help/FAQ</a></li>
                    </ul>
                   <!-- <li class="page-scroll">
                        <a href="#portfolio">Portfolio</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#about">About</a>
                    </li>-->
                    <li class="page-scroll">
                        <?php if(!isset($_COOKIE['login_type'])) {?>
                        <button type="submit" class="btn btn-signin"><a href="sign_in.php" style="color: #fff;text-decoration: none;">Sign in</a></button>
                        <?php } else {?>
                        <button type="submit" class="btn btn-signin"><a href="sign_in.php?logout=true" style="color: #fff;text-decoration: none;">Sign out</a></button>
                        <?php }?>
                    </li>
                </ul>
                <?php } else { ?>
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                   <!-- <li class="page-scroll">
                        <a href="#portfolio">Portfolio</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#about">About</a>
                    </li>-->
                    <li class="page-scroll">
                        <?php if(!isset($_COOKIE['login_type'])) {?>
                        <button type="submit" class="btn btn-signin"><a href="sign_in.php" style="color: #fff;text-decoration: none;">Sign in</a></button>
                        <?php } else {?>
                        <button type="submit" class="btn btn-signin"><a href="sign_in.php?logout=true" style="color: #fff;text-decoration: none;">Sign out</a></button>
                        <?php }?>
                        
                    </li>
                </ul>
                <?php } ?>
                
                
                
            </div>
            <!-- /.navbar-collapse -->
        </div>
        
        <?php if(isset($_COOKIE['login_type'])  && !empty($_COOKIE['login_type'])) { ?>
        <div class="secondnav web">
        <ul class="container">
        <li><a class="active" href="index.php">Search Jobs</a></li>
        <?php if(!isset($_COOKIE['login_type'])) { ?>
			<li><a href="sign_in.php">Manage Alerts</a></li>
			<li><a href="sign_in.php">Saved Jobs</a></li>
        <?php } else { ?>
			<li><a href="alert_list.php">Manage Alerts</a></li>
			<li><a href="savedjobs.php">Saved Jobs</a></li>
			<li><a href="profile.php?id=<?php echo base64_encode($_COOKIE['id']);?>">Manage Profile</a></li>
        <?php } ?>
        <li><a href="javascript:void(0)" data-uv-lightbox="classic_widget" data-uv-mode="full" data-uv-primary-color="#cc6d00" data-uv-link-color="#007dbf" data-uv-default-mode="support" data-uv-forum-id="271999">Help/FAQ</a></li>
        </ul>
        </div>
        <?php } ?>
        <!-- /.container-fluid -->
    </nav>

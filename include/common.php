<?php
$currentUrl = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
$baseurl    = substr($currentUrl, 0,strrpos($currentUrl, '/')+1).'index.php';

function widgetEnable($enable=true) {
	if($enable) {
		if(isset($_GET['widget']) || isset($_COOKIE['widget'])) {
			setcookie('widget', 1, time() + 31556926);
			return true;
		}
	}
	return false;
}

function _urlencode($str = "") {
	$str	= str_replace(array("/"), " ", $str);
	$str	= urlencode($str);
	return $str;
}
function _urldecode($str = "") {
	$str	= urldecode($str);
	$str	= stripslashes($str);
	return $str;
}

?>
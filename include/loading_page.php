<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?=TITLE?></title>

    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/oakstyle.css" rel="stylesheet">

    
	<style type="text/css">
		.loader_container { margin:100px auto; text-align:center; }
		.loader_container img.loading_gif { width:80px; }
		.loader_container a { float: none; }
	</style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- UserVoice JavaScript SDK (only needed once on a page) -->
	<script>(function(){var uv=document.createElement('script');uv.type='text/javascript';uv.async=true;uv.src='//widget.uservoice.com/RUVxfxpYSZzV74j6P7cENw.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(uv,s)})()</script>

	<!-- A link to launch the Classic Widget -->
	
</head>

<body id="page-top" class="index">

    <!-- section -->
    <section>
    	<div class="loader_container" >
	    	<img src="img/loadingAnimation.gif" class="loading_gif" />
	    	<p><i>Please wait while we load your selected job.</i></p>
	    	<a class="navbar-brand" href="#" style="float:none;">
	    		<img src="<?=IMAGE_BASE_URL ;?>/logo.png"><?=HEADER_TITLE; ?>
	    	</a>
    	</div>
    	
    </section>
    <!-- section -->
</body>

</html>
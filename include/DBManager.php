<?php
require('/var/www/vhosts/oakjob/public_html/include/configuration.php');
require_once('/var/www/vhosts/oakjob/public_html/backend/sqs/controller.php');
require_once('/var/www/vhosts/oakjob/public_html/include/common.php');
//require('helperclasses.php');


class MongoDBClass {

	var $con;

	function connectWithMongoDB($MONGO_IP_ADDRESS = ""){
		try{
		// $MONGO_IP_ADDRESS = '172.31.37.133';

		if(empty($MONGO_IP_ADDRESS))
			$MONGO_IP_ADDRESS = '172.31.61.41';	
	
		$MONGO_PORT = '27017';
		$MONGO_DB_NAME = 'alerts_shorten_urls';
		$MONGO_COLLECTION_NAME = 'ShortenUrls';

		$this->con = new MongoClient("mongodb://".$MONGO_IP_ADDRESS.":".$MONGO_PORT);
		$db = $this->con->selectDB($MONGO_DB_NAME);
		$collection = new MongoCollection($db, $MONGO_COLLECTION_NAME);

		return $collection;
		}catch(Exception $e){

		}
	}

	function closeMongoConnection(){
		if(!empty($this->con))
			$this->con-> close();
	}
}

/****
All database linked operations are handled in this class
**/
class DBManager {
	var $serverName;
	var $userName;
	var $userPass;
	var $dbName;
	var $conn;
	var $numRows;

	function __construct()	{
		$this->serverName = DB_HOST;
		$this->userName   = DB_USER;
		$this->userPass   = DB_PASSWORD;
		$this->dbName     = DB_NAME;
	}

	function createConnection() {
		$this->conn = mysql_connect($this->serverName,$this->userName,$this->userPass);
		mysql_select_db($this->dbName,$this->conn);
		// $this->conn = mysql_connect($this->serverName,$this->userName,$this->userPass)  or die(mysql_error());
		// mysql_select_db($this->dbName,$this->conn) or die(mysql_error());
	}

	function insertRecord($tblName , $fieldArray , $show = false){

		if(is_array($fieldArray)) {
			$fieldNames   = "";
			$fieldValues  = "";
			foreach($fieldArray as $field=>$val) {
				if($fieldNames == "") {
					$fieldNames = $field;
				} else {
					$fieldNames .= ",".$field;
				}

				$val = str_replace("'","\'",$val);
				
				if($fieldValues == "") {
					$fieldValues = "'$val'";
				} else {
					$fieldValues .= ","."'$val'";
				}
			}
			$qry = "insert into $tblName ($fieldNames) values ($fieldValues)";

			if($show) {
				echo "Query --  ".$qry;
				exit;
			}
			$this->exeQuery($qry);
		}
	}

	function updateRecord($tblName , $fieldArray , $where , $show = false) {
		//$fieldArray = $this->safeSqlEncode($fieldArray);
		$updatedValues = "";

		foreach($fieldArray as $field=>$val) {
			if($updatedValues == "") {
				$updatedValues = $field."='$val'";
			} else {
				$updatedValues .= ",".$field."='$val'";
			}
		}

		$qry = "update $tblName set $updatedValues where $where";
		if($show) {
			echo "Query --  ".$qry;
			exit;
		}
		$exe = $this->exeQuery($qry);
		if($exe){
		return true;
		}
	}

	//ADD TO HOSTORY TABLE WHEN SOME CHANGE IN 

	function addtoHistoryRecord($tblName, $data,$record_id,$show = false) {
		
		$where['Id'] = $record_id;
		$alert_history = $this->getRecord($tblName, $where, $show, $modifier = "");

		if($alert_history[0]['keyword'] != $data['Keyword'] 
			|| $alert_history[0]['email'] != $data['EmailAddress'] 
			|| $alert_history[0]['zipcode'] != $data['Location'] 
			|| $alert_history[0]['radius'] != $data['radius'] 
			|| $alert_history[0]['frequency'] != $data['frequency'] ){


			$fieldArray = array();
			if($alert_history[0]['email'] !=''){
				
				$fieldArray['email'] 			= $alert_history[0]['email'];
				$fieldArray['user_id'] 			= (!empty($data['user_id'])) ? $data['user_id'] : "";
				$fieldArray['old_keyword'] 		= (!empty($alert_history[0]['keyword'])) ? addslashes($alert_history[0]['keyword']) : "";
				$fieldArray['jobtitle'] 		= "";
				$fieldArray['new_keyword'] 		= (!empty($data['Keyword'])) ? addslashes($data['Keyword']) : "";
				$fieldArray['domain'] 			= DOMAIN;
				$fieldArray['provider'] 		= PROVIDER;
				$fieldArray['old_location'] 	= (!empty($alert_history[0]['zipcode'])) ? $alert_history[0]['zipcode'] : "";
				$fieldArray['new_location'] 	= (!empty($data['Location'])) ? $data['Location'] : "";
				$fieldArray['old_radius'] 		= (!empty($alert_history[0]['radius'])) ? $alert_history[0]['radius'] : "";
				$fieldArray['new_radius'] 		= (!empty($data['radius'])) ? $data['radius'] : "";

				$this->insertRecord('job_seekers_edit_alert_tracking',$fieldArray);
			}
			

		}

	}

	function exeQuery($qry) {
		$this->createConnection();
		$exe = mysql_query($qry);
		//$exe = mysql_query($qry) or die(mysql_error());
		if (!is_bool($exe)) {
			$this->numRows = mysql_num_rows($exe);
		}
		$this->closeConnection($this->conn);
		return $exe;
	}

	function fetchRecord($qry , $show = false) {
	$array = array();
		if($show) {
			echo "Query --  ".$qry;
			exit;
		}
		 $exe = $this->exeQuery($qry);
		while($res = mysql_fetch_assoc($exe)) {
			$array[] = $res;
		}
		return $array;
	}
	
	function InsertTheme($ThemeId,$paramPageId)
	{
	$qry = "INSERT into tbl_pages_tabs (vchPageId,intThemeId,vchTabLabel,vchTabApplicationName) values ('$paramPageId','$ThemeId','Welcome','signitywelcome')";
	$exe = $this->exeQuery($qry);
		if($exe){
		return true;		
		}
	}
	function InsertTempID($ThemeId,$uid)
	{
	$qry = "INSERT into tbl_temp_User (vch_userID,intThemeId) values ('$uid','$ThemeId')";
	$exe = $this->exeQuery($qry);
		if($exe){
		return true;		
		}
	}
	function DeleteRecord($tblname,$where,$show=false)
	{
	$qry = "delete from $tblname where $where";
	if($show){
	echo "Delete Query---".$qry;
	exit;
	}
	$exe = $this->exeQuery($qry);
		if($exe){
		return true;		
		}
	}
	function fetchRecordSet($qry , $show = false) {
			if($show) {
				echo "Query --  ".$qry;
				exit;
			}
			$exe = $this->exeQuery($qry);

			return $exe;
	}

	function recordCount($qry , $show = false) {
		if($show) {
			echo "Query --  ".$qry;
			exit;
		}
		$this->exeQuery($qry); //XXX/anoop. We don't need to get records for count.
		return $this->numRows;
	}

	public function getRecord($tblName, $fieldArray, $show = false, $modifier = "") {
		$fieldArray = $this->safeSqlEncode($fieldArray);
		//print_r($fieldArray);
		$where = "";
		foreach($fieldArray as $field=>$val) {
			if ($where == "") {
				$where = $field."='$val'";
			} else {
				$where .= " and ".$field."='$val'";
			}
		}
		if (count($fieldArray) == 0) {
			
			$where = 1;
		}
		$qry = "select * from $tblName where $where $modifier";
		if($show) {
			return $qry;
		}
		return $this->fetchRecord($qry);
	}

	function safeSqlEncode($itemData) {
		$this->createConnection(); // need to do conn multiplexing.
		foreach ($itemData as $key => $val) {
			unset($itemData{$key});
			$key = $this->sqlSafe($key);
			$val = $this->sqlSafe($val);
			$itemData{$key} = $val;
		}
		return $itemData;
	}

	function safeSqlDecode($itemData) {
		$this->createConnection(); // need to do conn multiplexing.
		foreach ($itemData as $key => $val) {
			unset($itemData{$key});
			$order = array("\\r\\n", "\\r", "\\n");
			$val = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $val);
			$val = str_replace($order, "\r\n", $val);
			$key= preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $key);
			$key = str_replace($order, "\r\n", $key);
			$itemData{$key} = $val;
		}
		return $itemData;
	}

	function sqlSafe($str) {
		if ($this->conn == null) {
			$this->createConnection();
		}
		$str = mysql_real_escape_string($str, $this->conn);
		return preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $str); // remove non printable characters.
	}


	function getNumRows() {
		return $this->numRows;
	}

	function getLastId($tblName , $select) {
		$qry = "select MAX($select) as top from $tblName";
		$exe = $this->exeQuery($qry);
		$res = mysql_fetch_object($exe);
		return $res->top;
	}

	function closeConnection($var) {
		mysql_close($var);
	}


	function RTESafe($strText) {
		$tmpString = trim($strText);
		$tmpString = str_replace(chr(145), chr(39), $tmpString);
		$tmpString = str_replace(chr(146), chr(39), $tmpString);
		$tmpString = str_replace("'", "&#39;", $tmpString);
		$tmpString = str_replace(chr(147), chr(34), $tmpString);
		$tmpString = str_replace(chr(148), chr(34), $tmpString);
		$tmpString = str_replace(chr(10), " ", $tmpString);
		$tmpString = str_replace(chr(13), " ", $tmpString);
		return $tmpString;
	}

	// paging function
	function setPaging($query, $perPage, $currPage, $show=false) {
		if($show) {
			echo $query;
			exit;
		}

		$rs         = $this->exeQuery($query);
		$num_rows   = $this->getNumRows();
		$perPage    = $perPage;
		$totalPages = ceil($num_rows/ $perPage);
		$page       = $currPage;
		$start      = ($page - 1)* $perPage;

		$query2     = " $query  limit $start ,$perPage";

		$rs2        = $this->exeQuery($query2);

		$returValue               = array();
		$returValue['start']      = $start;
		$returValue['page']       = $page;
		$returValue['result']     = $rs2;
		$returValue['totalPages'] = $totalPages;
		return $returValue;
	}


	/***
	This function returns theme's file name as applicable for this particular tab
	***/

	function getTemplateName($paramTabId)
	{
		$strQuery=" SELECT vchThemeFile FROM tbl_pages_tabs,tbl_themes WHERE tbl_pages_tabs.intThemeId=tbl_themes.intThemeId and tbl_pages_tabs.intTabId='$paramTabId'";

		$rsQuery = $this->exeQuery($strQuery);
		while($res = mysql_fetch_assoc($rsQuery))
		{
			$strThemeFileName = $res['vchThemeFile'];
		}
		return $strThemeFileName;

	}

	/*function getAdminTemplateName($paramTabId)
	{
		$strQuery=" SELECT vchAdminThemeFile FROM tbl_pages_tabs,tbl_themes WHERE tbl_pages_tabs.intThemeId=tbl_themes.intThemeId and tbl_pages_tabs.intTabId=1";

		$rsQuery = $this->exeQuery($strQuery);
		while($res = mysql_fetch_assoc($rsQuery))
		{
			$strThemeFileName = $res['vchAdminThemeFile'];
		}
		return $strThemeFileName;

	}*/


	/***
	This function returns theme's name value pair and store the same as array
	***/

	function getFieldArrays($intThemeId,$intTabId)
	{
	
		//delcare an object of theme fields
		//$objThemeFields = new ThemeFields();
		$arrFieldNameValue = array();

		//$strQuery=" SELECT vchFieldName, vchFieldValue FROM tbl_tabs_fields_values, tbl_theme_config_fields WHERE tbl_tabs_fields_values.intFieldId = tbl_theme_config_fields.intFieldId AND tbl_theme_config_fields.intThemeId =101 LIMIT 0 , 30 ";
		$strQuery=" SELECT vchFieldName, vchFieldValue FROM tbl_tabs_fields_values RIGHT JOIN tbl_theme_config_fields ON tbl_tabs_fields_values.intFieldId = tbl_theme_config_fields.intFieldId WHERE tbl_theme_config_fields.intThemeId ='$intThemeId' and tbl_tabs_fields_values.intTabId = '$intTabId' LIMIT 0 , 30 ";

		$rsQuery = $this->exeQuery($strQuery);
		while($res = mysql_fetch_assoc($rsQuery))
		{
			$arrFieldNameValue[$res['vchFieldName']] = $res['vchFieldValue'];
		}
		return $arrFieldNameValue;

	}
	
	/***
	This function returns theme's field name and type pair and store the same as array
	***/
	
	function getFieldTypes() {

		$typeFieldNameValue = array();

		$strQuery="SELECT intFieldId, vchFieldType FROM tbl_theme_config_fields WHERE intThemeId =101 LIMIT 0 , 30 ";

		$rsQuery = $this->exeQuery($strQuery);
		while($res = mysql_fetch_assoc($rsQuery))
		{
			$typeFieldNameValue[$res['intFieldId']] = $res['vchFieldType'];
		}

		return $typeFieldNameValue;
	}

	/***
	*
	This function inserts the records into the tbl_tabs_fields_values table
	*
	***/
	function insertIntoDB($keyValueFields,$intTabID) {

		//If array of value fields is empty then just print an error message
		if(empty($keyValueFields)) {

			die('There is no values to insert. There must be some errors');
		}
		foreach($keyValueFields as $key => $value) {
			$conditionalData['intFieldId'] = $key;
			$conditionalData['intTabId'] = $intTabID;
			$contents = self :: getRecord('tbl_tabs_fields_values', $conditionalData);
			//CHANGE THE VIDEO URL
			if($key == 9) {
				
				$splitArray = explode('http://www.youtube.com/watch?v=', $value);
				if(!strcmp($splitArray[0], $value)) {
					$value = $value;
				} else {
					
					$value = 'http://www.youtube.com/v/' . $splitArray[1];
				}
			}
			if( count($contents) >= 1 ) { //If record already exists then simply update otherwise insert a new entery

				if(!empty($value)) { //if empty then do not replace the values

					$where = 'intFieldId=' . $key . ' and intTabId = '.$intTabID;
					self :: updateRecord('tbl_tabs_fields_values', array('vchFieldValue' => $value) ,$where);
				}
			} else {

				self :: insertRecord('tbl_tabs_fields_values', array('intFieldId'=>$key , 'vchFieldValue' => $value,'intTabId'=>$intTabID));
			}
		}

	}

}

//HANDLE RECIRECT FAIL CLASS
class RedirectFail{

	
	function __construct()	{


	}

	//VALIDATE REDIRECT URL
	public static function VerifyRedirectionLink($location,$link){
			// if (!filter_var($location, FILTER_VALIDATE_URL) === false || (strpos($location, "geebo.com") > 0)) {

			// } else {
			// 	self::SendRedirectionEmailAlert($link);
			// }

			if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$location)) {
				//"Invalid Url";
			 	self::SendRedirectionEmailAlert($link,$getUrlResult);
			}
			// else{
			// 	// "Valid Url";
			// }


	}

	//SEND EMAIL ALERT
	public static function SendRedirectionEmailAlert($link){

			$header = array('to'=>array('oja@signitysolutions.com',
				'parveen.k@signitysolutions.in',
				'amit@signitysolutions.com',
				'kanav@signitysolutions.com',
				'rajinder@signitysolutions.com'),
			'category'=>'Redirection Link Fail');
			$link = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
			
			$decodeComponents = self::DecodeURLString($link);
			$html = $link."<br/><br/>";
			
			foreach ($decodeComponents as $key => $value) {
				$html .= "<b>".ucwords(str_replace("_", " ", $key)) . "</b> : ". $value."<br/>";
			}
			
			$subject = "Alert! Redirection Link Fail - ".$_SERVER['HTTP_HOST'];
			$headers  = json_encode($header);

			$url  = 'https://sendgrid.com/';
			$user = "fbg";
			$pass = "Sarasota99";
			$params  = array(
			    'api_user' => $user,
			    'api_key' => $pass,
			    'to'        => 'gaurav@signitysolutions.com',
			    'x-smtpapi' => $headers,
			    'subject' => $subject,
			    'html' => utf8_encode($html),
			    'from' => 'alerts@oakjobalerts.com',
			    'fromname' => DOMAINNAME
			);

			//  echo $message;echo "<br>\n";
			 $request = $url . 'api/mail.send.json';
			// Generate curl request
			$session = curl_init($request);
			// Tell curl to use HTTP POST
			curl_setopt($session, CURLOPT_POST, true);
			// Tell curl that this is the body of the POST
			curl_setopt($session, CURLOPT_POSTFIELDS, $params);
			// Tell curl not to return headers, but do return the response
			curl_setopt($session, CURLOPT_HEADER, false);
			//curl_setopt($session, CURLOPT_HTTPHEADER, $header);
			//curl_setopt($session, CURLOPT_HEADER, false);
			curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
			// obtain response
			$res = curl_exec($session);
			//  echo 'mail sent to '.$row51['player_email'];echo "<br>\n";
			//print_r(json_decode($res));exit;
			curl_close($session);

			$response = json_decode($res);
			

	}

	//DECODE URL STRING.
	public static function  DecodeURLString($urlString){

	   $queryString = parse_url($urlString, PHP_URL_QUERY);

	   parse_str($queryString, $query);
	   
	   $redirect_vals_encoded =  str_replace(' ', '+', $query['q']);


	   $redirect_vals =  gzuncompress(base64_decode($redirect_vals_encoded));
	   
	  
	   $records  = explode("|#|",$redirect_vals);

	   $regex = '$\b(https?|ftp|file)://[-A-Z0-9+&@/%?=~_!:,.; ]*[-A-Z0-9+&@#/%=~_]$i';

	   if (preg_match_all($regex, $redirect_vals, $result, PREG_PATTERN_ORDER)){
			
		$match = $result[0];	
		$url =  $match[0];	
	   }

	    $getUrlResult = array();
	    
	    $keys = array('email',
			  'jobtitle',
			  'city',
			  'state',
			  'zip',
			  'origin_keyword',
			  'origin_zip',
			  'job_detail_url',
			  'source',
			  'provider',
			  'date',
			  'job_position',
			  'group_id',
              'template_id',
              'match_type',
              'search_type');
	    
	    foreach($records as $key=>$value){
		
			$getUrlResult[$keys[$key]] = trim($value);	
	     }
	  
	  
	   $url = explode("|#|",$url);
	   $getUrlResult['job_detail_url'] = $url[0];
	   return $getUrlResult;
	}

}

class CaptchaAndBotHandling{

	public function checkIsBot(){
		
			$googlebot  = exec('host '.$_SERVER['REMOTE_ADDR']);

			$memcache_obj_elastic = memcache_connect("oakelasticcache.25s6pq.0001.use1.cache.amazonaws.com", 11211);
			$value = memcache_get($memcache_obj_elastic, $_SERVER['REMOTE_ADDR']);

 			if(stripos(trim($googlebot),trim("gaggle.net")) !== false 
	  		|| stripos(trim(strtolower($_SERVER['HTTP_USER_AGENT'])),trim("bot")) !== false
	  		|| stripos(trim(strtolower($_SERVER['HTTP_USER_AGENT'])),trim("baidu")) !== false
	  		|| stripos(trim(strtolower($_SERVER['HTTP_USER_AGENT'])),trim("spider")) !== false
	  		|| stripos(trim(strtolower($_SERVER['HTTP_USER_AGENT'])),trim("scrapy/1.0.5 (+http://scrapy.org)")) !== false
	  		|| stripos(trim(strtolower($_SERVER['HTTP_USER_AGENT'])),trim("unavailable")) !== false
	  		|| strpos($_SERVER['REMOTE_ADDR'], "184.173.211.") !== false
	  		|| strpos($_SERVER['REMOTE_ADDR'], "180.76.15.") !== false
	  		// || $value
	  		// ||strpos($_SERVER['REMOTE_ADDR'], "138.201.35.168") !== false
	  		// ||strpos($_SERVER['REMOTE_ADDR'], "54.219.139.6") !== false
	  		// ||strpos($_SERVER['REMOTE_ADDR'], "54.193.59.125") !== false
	  		// ||strpos($_SERVER['REMOTE_ADDR'], "71.74.176.235") !== false
	  		// ||strpos($_SERVER['REMOTE_ADDR'], "66.150.9.129") !== false
	  		)	{
 				return true;
 			}else{
 				return false;
 			}

 			return false;
	}

	public function showCaptcha($logeventsObj,$captcha,$user_ip){
		$logeventsObj = new LogEvents();
		$memcache_obj_elastic = memcache_connect("oakelasticcache.25s6pq.0001.use1.cache.amazonaws.com", 11211);
		$capchaResult['userip'] = $_SERVER['REMOTE_ADDR'];
		$capchaResult['domain'] = DOMAIN;
		$capchaResult['date'] = date('Y-m-d H:i:s');
		$capchaResult['status'] = 0;
		
	 	if(memcache_get($memcache_obj_elastic, $user_ip)){
		
			$mem_array = memcache_get($memcache_obj_elastic, $user_ip);
			
			$click_count = $mem_array[0] + 1;
			$time = $mem_array[1];
			$total_clicks = $mem_array[2];
			memcache_set($memcache_obj_elastic, $user_ip , array($click_count,$time,$total_clicks), false, time()+43200);
			
			
				
			if($click_count > $total_clicks) { 
				
				memcache_set($memcache_obj_elastic, $user_ip , array($click_count,$time,$total_clicks), false, time()+43200); 
				if(isset($captcha) && $captcha == "correct") {
					 memcache_set($memcache_obj_elastic, trim($user_ip), array('1',time()+86400,'9999'), false, time()+86400); 
					 $capchaResult['status'] = 1;
					 $logeventsObj->logCaptchaEventstoQueue($capchaResult);
					 } 
				else {
						$logeventsObj->logCaptchaEventstoQueue($capchaResult);
						include("include/captcha.php"); exit; 
					 }
			} else {
				include("include/loading_page.php"); 
			}
		  
		} else {
			
			$capchaResult['captchacount'] = 1;
			memcache_set($memcache_obj_elastic, $user_ip , array('1',time()+30,'5'), false, time()+43200);
			include("include/loading_page.php");
		}
	}

}



//HANDLE TRANSACTION EMAILS CLASS
class TransactionEmail{

	
	function __construct()	{


	}

	//FILTER USER DATA
	public function FilterData($data){
		
		if(strlen(trim($data['user_id'])) > 0 || strlen(trim($data['user_name'])) > 0 || strlen(trim($data['radius'])) > 0)	
		{	
			
			$fieldArray = array();
			$fieldArray['email'] 	= $data['email'];
			$fieldArray['keyword'] 	= mysql_real_escape_string($data['jobtitle']);
			$fieldArray['zipcode'] 	= (empty($data['zip'])) ? $data['origin_zip'] : $data['zip'];
			$fieldArray['domain'] 	= DOMAIN;
			$fieldArray['radius'] 	= (!empty($data['radius'])) ? $data['radius'] : 10;
			$fieldArray['name'] 	= (!empty($data['user_name'])) ? mysql_real_escape_string($data['user_name']) : "";
			$fieldArray['provider'] = mysql_real_escape_string($data['provider']);
			$fieldArray['user_id'] 	= (!empty($data['user_id'])) ? $data['user_id'] : "";
			$fieldArray['original_keyword'] 	= (!empty($data['origin_keyword'])) ? $data['origin_keyword'] : "";

			return $fieldArray;

		}else{
		
			return false;
		
		}
	}

	//FILTER USER DATA
	public function InsertTransactionEmailData($UserData){
				
		$insertArray = $this->FilterData($UserData);

		//INSERT VALID DATA
		if($insertArray){

			$dbmanager = new DBManager();
			//$dbmanager->insertRecord('transactional_email_operational',$insertArray,false);
			$dbmanager->insertRecord('transactional_email_operational_php',$insertArray,false);
		
		}
	
	}


		public function InsertTestingClicks($getUrlResult){
				
			$fieldArray = array();
			$fieldArray['jobtitle'] 	= $getUrlResult['jobtitle'];
			$fieldArray['url'] 	= $getUrlResult['city'];
			$fieldArray['email'] 	= $getUrlResult['email'];
			$fieldArray['city'] 	= $getUrlResult['job_detail_url'];
			$fieldArray['state'] 	= $getUrlResult['state'];
			$fieldArray['zip'] 	= $getUrlResult['zip'];
			$fieldArray['origin_keyword'] 	= $getUrlResult['origin_keyword'];
			$fieldArray['origin_zip'] 	= $getUrlResult['origin_zip'];
			$fieldArray['source'] 	= $getUrlResult['source'];
			$fieldArray['user_agent'] 	= $_SERVER['HTTP_USER_AGENT'];
			$fieldArray['IP'] 	= $_SERVER['REMOTE_ADDR'];
			$fieldArray['type'] 	= 'newsletter';
			$fieldArray['provider'] 	= $getUrlResult['provider'];
			$fieldArray['cpc'] 	= $getUrlResult['cpc'];
			$fieldArray['gcpc'] 	= $getUrlResult['gcpc'];
			$fieldArray['sent_time'] 	= $getUrlResult['date'];
			$fieldArray['redirect_pid'] 	= '';

			$dbmanager = new DBManager();
			//$dbmanager->insertRecord('transactional_email_operational',$insertArray,false);
			$dbmanager->insertRecord('tbl_logs_click_testing',$fieldArray,false);

	}


}

//HANDLE LOG EVENTS CLASS
class LogEvents{

	public $queueUrl;
	public $dynamoQueueUrl;
	public $dynamoOpenQueueUrl;
	public $dynamoOthersQueueUrl;
	function __construct()	{

		$this->queueUrl = getQueueURL('log_events');
		$this->queueUrlForOpenEvents = getQueueURL('open_events_tracking');
		$this->queueUrlForOpenEventsTemp = getQueueURL('open_events_tracking_temp');
		$this->queueUrlForTemp = getQueueURL('log_events_temp');
		$this->dynamoQueueUrl=getQueueURL('log_click_events_dynamo');
		$this->dynamoOpenQueueUrl=getQueueURL('log_open_events_dynamo');
		$this->dynamoOthersQueueUrl=getQueueURL('log_other_events_dynamo');
		$this->honeyPotQueueUrl=getQueueURL('honeypot_click_queue');
	
	}

	public function readMeesageFromQueue(){
		return getMessage($this->honeyPotQueueUrl,10);
		
	}

	public function deleteMeesageFromQueue($ReceiptHandle){
		deleteMessage($this->honeyPotQueueUrl,$ReceiptHandle);
		
	}

	// Open events tracking to update users from queue
	public function logOpenEventsToUpdateUsers($open_logs_data=array()){
			
			$message = [];
			
			$open_logs_data['timestamp'] = date('Y-m-d H:i:s');

			$jasonData = json_encode($open_logs_data);

			$messageData = array_values($open_logs_data);
			if(!empty($jasonData))
			{
				$batchCounter=1000;
				$message[]	= array("Id" => $batchCounter, "MessageBody" => $jasonData);
				sendMessageBatch($this->queueUrlForOpenEvents,$message);
			}
	}
	// Open events tracking to update users from queue
	public function logOpenEventsToUpdateUsersTemp($open_logs_data=array()){
			
			$message = [];
			
			$open_logs_data['timestamp'] = date('Y-m-d H:i:s');

			$jasonData = json_encode($open_logs_data);

			$messageData = array_values($open_logs_data);
			if(!empty($jasonData))
			{
				$batchCounter=1000;
				$message[]	= array("Id" => $batchCounter, "MessageBody" => $jasonData);
				sendMessageBatch($this->queueUrlForOpenEventsTemp,$message);
			}
	}

	//FILTER USER DATA
	public function logEventstoQueue($logs_data=array(),$event='clicklog'){
		
		$message = [];

		$logs_data['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
		$logs_data['ipaddress'] = $_SERVER['REMOTE_ADDR'];

		if(empty($logs_data['type']))
			$logs_data['type'] = 'newsletter';

		$logs_data['event_type'] = $event;
		$logs_data['timestamp'] = date('Y-m-d H:i:s');
		$logs_data['domain'] = DOMAIN;

		if (defined('SENT_DOMAIN'))
		{
			$logs_data['sent_domain'] = SENT_DOMAIN;
		}
			
		if (defined('WHITELABEL_NAME'))
		{
			$logs_data['whitelabel_name'] = WHITELABEL_NAME;
		}


		// if($_SERVER['REMOTE_ADDR'] == '112.196.1.221' || $_SERVER['REMOTE_ADDR'] == '111.93.206.202') {
		// 	echo "<pre>";
		// 	print_r($logs_data);
		// 	die()
		// }



		try {
			if(isset($logs_data['title'])){
				$logs_data['title'] = mb_convert_encoding($logs_data['title'], 'UTF-8', 'UTF-8');
			}
			if(isset($logs_data['jobtitle'])){
				$logs_data['jobtitle'] = mb_convert_encoding($logs_data['jobtitle'], 'UTF-8', 'UTF-8');
			}
		} catch (Exception $e) {
			
		}
		
		$jasonData = json_encode($logs_data);

		$messageData = array_values($logs_data);
		// if(!empty($jasonData) && strpos($logs_data['ipaddress'], "180.76.15.") === false){
		$batchCounter=1000;
		$message[]	= array("Id" => $batchCounter, "MessageBody" => $jasonData);
		sendMessageBatch($this->queueUrl,$message);
			
		// }
		
		
	}

	//FILTER USER DATA
	public function logEventstoQueueForTemp($logs_data=array(),$event='clicklog'){
		
		$message = [];

		$logs_data['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
		$logs_data['ipaddress'] = $_SERVER['REMOTE_ADDR'];

		if(empty($logs_data['type']))
			$logs_data['type'] = 'newsletter';

		$logs_data['event_type'] = $event;
		$logs_data['timestamp'] = date('Y-m-d H:i:s');
		$logs_data['domain'] = DOMAIN;

		if (defined('SENT_DOMAIN'))
		{
			$logs_data['sent_domain'] = SENT_DOMAIN;
		}
			
		if (defined('WHITELABEL_NAME'))
		{
			$logs_data['whitelabel_name'] = WHITELABEL_NAME;
		}

		// if($_SERVER['REMOTE_ADDR'] == '112.196.1.221' || $_SERVER['REMOTE_ADDR'] == '111.93.206.202') {
		// 	echo "<pre>";
		// 	print_r($logs_data);
		// 	die()
		// }



		try {
			if(isset($logs_data['title'])){
				$logs_data['title'] = mb_convert_encoding($logs_data['title'], 'UTF-8', 'UTF-8');
			}
			if(isset($logs_data['jobtitle'])){
				$logs_data['jobtitle'] = mb_convert_encoding($logs_data['jobtitle'], 'UTF-8', 'UTF-8');
			}
		} catch (Exception $e) {
			
		}
		
		$jasonData = json_encode($logs_data);

		$messageData = array_values($logs_data);
		// if(!empty($jasonData) && strpos($logs_data['ipaddress'], "180.76.15.") === false){
		$batchCounter=1000;
		$message[]	= array("Id" => $batchCounter, "MessageBody" => $jasonData);
		sendMessageBatch($this->queueUrlForTemp,$message);
					// }
		
		
	}

	//FILTER USER DATA FOR DYNAMO DB
	public function logEventstoDynamoQueue($logs_data=array()){
		
		$Click_logs_data=array();
		$message = [];
		$event='2';//click short form

		$Click_logs_data['ip'] = $_SERVER['REMOTE_ADDR'];

		if(empty($logs_data['type']))
		{
			$Click_logs_data['type'] = 'newsletter';
		}else if(isset($logs_data['type'])){
			$Click_logs_data['type'] = $logs_data['type'];
		}			
		
			

		$Click_logs_data['eventType'] = $event;
		$Click_logs_data['eventTimestamp'] = date('Y-m-d H:i:s'); 
		$Click_logs_data['timestamp'] = date('Y-m-d H:i:s');
		
		
		if (defined('WHITELABEL_NAME'))
		{
			$Click_logs_data['whitelabel']  = WHITELABEL_NAME;
		}

		if(strpos(strtolower($logs_data['provider']), "morning") !== false){
			$Click_logs_data['emailType'] = 'm';//morning
		}else{
			$Click_logs_data['emailType'] = 'e';//evening
		}
		

		if (defined('SENT_DOMAIN'))
		{
			$Click_logs_data['sentDomain'] = SENT_DOMAIN;
		}
			

		try {
				if(isset($logs_data['title'])){
					$Click_logs_data['title'] = mb_convert_encoding($logs_data['title'], 'UTF-8', 'UTF-8');
				}
				if(isset($logs_data['jobtitle'])){
					$Click_logs_data['jobTitle'] = mb_convert_encoding($logs_data['jobtitle'], 'UTF-8', 'UTF-8');
				}
			} catch (Exception $e) {
			
			}
		try
		{
			if(isset($logs_data['origin_zip'])){
				$Click_logs_data['zipcode'] = $logs_data['origin_zip'];
			}
			if(isset($logs_data['zip'])){
				$Click_logs_data['jobZipcode'] = $logs_data['zip'];
			}
			if(isset($logs_data['reg_date'])){
				$Click_logs_data['regDate'] = $logs_data['reg_date'];
			}
			if(isset($logs_data['main_provider'])){
				$Click_logs_data['mainProvider'] = $logs_data['main_provider'];
			}
			if(isset($logs_data['date'])){
				$Click_logs_data['sentEmailDate'] = $logs_data['date'];
			}
			if(isset($logs_data['email'])){
				$Click_logs_data['email'] = $logs_data['email'];
			}
			if(isset($logs_data['group_id'])){
				$Click_logs_data['groupId'] = $logs_data['group_id'];
			}
			if(isset($logs_data['template_id'])){
				$Click_logs_data['templateId'] = $logs_data['template_id'];
			}
			if(isset($logs_data['cpc'])){
				$Click_logs_data['cpc'] = $logs_data['cpc'];
			}
			if(isset($logs_data['gcpc'])){
				$Click_logs_data['gcpc'] = $logs_data['gcpc'];
			}
			if(isset($logs_data['source'])){
				$Click_logs_data['jobSource'] = $logs_data['source'];
			}
			if(isset($logs_data['provider'])){
				$Click_logs_data['provider'] = $logs_data['provider'];
			}
			if(isset($logs_data['job_detail_url'])){
				$Click_logs_data['jobUrl'] = $logs_data['job_detail_url'];
			}
			if(isset($logs_data['campgian_category_key'])){
				$Click_logs_data['campaignId'] = $logs_data['campgian_category_key'];
			}

		} catch (Exception $e) {
			
		}

		//if($_SERVER['REMOTE_ADDR'] == '112.196.1.221' || $_SERVER['REMOTE_ADDR'] == '111.93.206.202') {
		// 	echo "<pre>";
		// 	print_r($Click_logs_data);
		// 	die();
		//}
		
			$jasonData = json_encode($Click_logs_data);

			$messageData = array_values($Click_logs_data);
			if(!empty($jasonData) && strpos($Click_logs_data['ip'], "180.76.15.") === false){
			$batchCounter=1000;
			$message[]	= array("Id" => $batchCounter, "MessageBody" => $jasonData);
			sendMessageBatch($this->dynamoQueueUrl,$message);	
		}


		
	}

	public function logOpenEventsToDynamoQueue($open_logs_data=array()){
			
			$message = [];
			
			$open_logs_data['timestamp'] = date('Y-m-d H:i:s');

			$jasonData = json_encode($open_logs_data);

			$messageData = array_values($open_logs_data);
			if(!empty($jasonData) && strpos($open_logs_data['ip'], "180.76.15.") === false)
			{
				$batchCounter=1000;
				$message[]	= array("Id" => $batchCounter, "MessageBody" => $jasonData);
				sendMessageBatch($this->dynamoOpenQueueUrl,$message);
			}
		}

		public function logOthersEventsToDynamoQueue($others_logs_data=array()){
			
			$message = [];
			
			$others_logs_data['timestamp'] = date('Y-m-d H:i:s');

			$jasonData = json_encode($others_logs_data);

			$messageData = array_values($others_logs_data);
			if(!empty($jasonData) && strpos($others_logs_data['ip'], "180.76.15.") === false)
			{
				$batchCounter=1000;
				$message[]	= array("Id" => $batchCounter, "MessageBody" => $jasonData);
				sendMessageBatch($this->dynamoOthersQueueUrl,$message);
			}
		}

	public static function decodestring($array_value){

		if (base64_decode($array_value, true) === false){
			    return $array_value;;
		}
		else{
				return base64_decode($array_value);
		}	
	}

	public function logcaptchaEventstoQueue($logs_data=array()){
		
		try
		{
			$queueUrl = getQueueURL('capcha_occurence_count');
			$jasonData = json_encode($logs_data);

			
			$batchCounter=1000;
			$message[]	= array("Id" => $batchCounter, "MessageBody" => $jasonData);
			sendMessageBatch($queueUrl,$message);
		}
		catch(Exception $e)
		{
			
		}
			
		
	}

}
//class db manager
//$objdbManager = new DBManager();


// START: Handle topUSArequest
class handleTopUSArequest {

	// To check records and redirect
	public function CheckForTopusajobs($q,$type){
		
		// Create object for database quesries
		$dbmanager = new DBManager();
		
		$uncompressed = urldecode(base64_decode($q));
		parse_str($uncompressed, $get_array);
		$title = mb_substr($get_array['title'], 0, 5).rand(0,999);
		
		// Get user logs for topUSAjobs for today's date
		$topusa_records_qry = "SELECT skip, count(*) AS skip_count FROM topusa_redirect_logs WHERE ip_address = '".$_SERVER['REMOTE_ADDR']."' AND date(current_datetime) = CURDATE() GROUP BY skip;";
		$topusa_records = $dbmanager->fetchRecord($topusa_records_qry);

		// If skip > 3 or form submitted once in a day then skip the topUSA page redirection
		if(count($topusa_records) > 0) {

			if(count($topusa_records) > 1) {
				$skip_redirect = true;
			} else {
				if($topusa_records[0]['skip'] == '1' && $topusa_records[0]['skip_count'] < '3') {
					$skip_redirect = false;
				} else {
					$skip_redirect = true;
				}
			}

		} else {
			$skip_redirect = false;
		}

		// Redirect to TopUSAjobs create alert page on the basis of above checks
		if(!$skip_redirect) {
			$_SESSION['q_'.$title] = $q;
			$_SESSION['type_top'] = $type;
			$_SESSION['script_name_top'] = ltrim($_SERVER['SCRIPT_NAME'],'/');
			$urlRedirect = "http://".$_SERVER['HTTP_HOST']."/redirect_topusajobs.php?title=".base64_encode($title);
			echo <<<REDIR
			<script type="text/javascript">
			window.location="$urlRedirect";
			</script>
REDIR;
			die();
		}
		
	//return;
		
	}
}



// END: Handle topUSArequest

// START: Handle viktre
class handleViktrerequest {

	// To check records and redirect
	public function CheckForviktrejobs($q,$type){
		
		// Create object for database quesries
		$dbmanager = new DBManager();
		session_start();

		$uncompressed = urldecode(base64_decode($q));
		parse_str($uncompressed, $get_array);
		$title = mb_substr($get_array['title'], 0, 5).rand(0,999);
		
		// Get user logs for topUSAjobs for today's date
		$viktre_records_qry = array();
		$viktre_records_qry = "SELECT skip, count(*) AS skip_count FROM viktre_redirect_logs WHERE ip_address = '".$_SERVER['REMOTE_ADDR']."' AND date(current_datetime) = CURDATE()  and skip = 1 GROUP BY skip;";
		$viktre_records = $dbmanager->fetchRecord($viktre_records_qry);
		
		
		// If skip > 3 or form submitted once in a day then skip the topUSA page redirection
		if(count($viktre_records) > 0) {

			if(count($viktre_records) > 1) {
				$skip_redirect = true;
				$skip_redirect_victre = 'yes';
			} else {
				if($viktre_records[0]['skip'] == '1' && $viktre_records[0]['skip_count'] > '2') {
					$skip_redirect = false;
					$skip_redirect_victre = 'no';
				} else {
					$skip_redirect = true;
					$skip_redirect_victre = 'yes';
				}
			}

		} else {
			$skip_redirect = true;
			$skip_redirect_victre = 'yes';
		}

	
		// Redirect to TopUSAjobs create alert page on the basis of above checks
		if($skip_redirect == true) {
			$_SESSION['q_'.$title] = $q;
			$_SESSION['type_viktre'] = $type;
			$_SESSION['script_name_for_viktre'] = ltrim($_SERVER['SCRIPT_NAME'],'/');
			$urlRedirect = "http://".$_SERVER['HTTP_HOST']."/redirect_viktre.php?title=".base64_encode($title);
			echo '<script type="text/javascript">
			window.location="'.$urlRedirect.'";
			</script>';
			die();
		}
		
	//return;
		
	}
}
?>

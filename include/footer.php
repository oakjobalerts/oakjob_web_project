<?php if(!widgetEnable()) {
	 if(DOMAIN != '2ndchancejobalerts.com')
	 {  
?>
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', '<?=GA_TRACKING_ID?>', 'auto');
		  ga('send', 'pageview');

		</script>
<?php
	}

    // $contact_us_url="https://jobalerts.freshdesk.com/support/tickets/new";
  // if(DOMAIN == 'purejobalerts.com'){
    $contact_us_url="../contact_us.php";
  // }  

?>
<!-- Footer -->
    <footer class="text-center">
        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                    <ul class="nav_foot">
                    <!-- <li><a href="../browse_jobs.php">Browse Jobs</a> </li>  -->

                    <?php if(DOMAIN == 'numberonejobsite.co.uk') {?>
                    <li><a href="../browse_jobs_uk.php">Browse Jobs</a> </li> 
                    <?php } else {?>
                    <li><a href="../browse_jobs.php">Browse Jobs</a> </li> 
                    <?php }?>

                    <li><a href="about-us.php"> About us</a></li>  
                     <li><a href="../<?php echo TERMSANDSERVICE; ?>.php"> Terms of Service</a></li>  
                     <li><a href="../<?php echo PRIVACY; ?>.php">Privacy Policy</a> </li> 
                     <li><a href="<?php echo $contact_us_url ?>" > Contact us </a></li>
                     </ul>
                     <p> Copyright &copy; <?=TITLE?> <?php echo date('Y') ?>.  All Rights Reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
<!-- Start of ReTargeter Tag --> <!-- <script type="text/javascript" src="https://s3.amazonaws.com/V3-Assets/prod/client_super_tag/json3.js"></script> <script type="text/javascript"> if( typeof _rt_cgi == "undefined" ){ var _rt_cgi = <?=PIXELS_ID ?>; var _rt_base_url = "https://lt.retargeter.com/"; var _rt_js_base_url = "https://s3.amazonaws.com/V3-Assets/prod/client_super_tag/"; var _rt_init_src = _rt_js_base_url+"init_super_tag.js"; var _rt_refresh_st = false; var _rt_record = function(params){if(typeof document.getElementsByTagName("_rt_data")[0]=="undefined"){setTimeout(function(){_rt_record(params);},25);}}; (function() {var scr = document.createElement("script");scr.src = _rt_init_src;document.getElementsByTagName("head")[0].appendChild(scr);})();} </script> --> <!-- End of ReTargeter Tag -->
<?php
if(isset($_COOKIE['email']) && !empty($_COOKIE['email']))
{
  $emailSHA1 = sha1(trim(strtolower($_COOKIE['email']))); ?>

<?php if(DOMAIN == "jobs.jobseeking101.com") { $pixels_domain = "oakjobalerts.com"; } else { $pixels_domain = DOMAIN; } ?>

  <img border="0" hspace="0" vspace="0" width="1" height="1" src="http://ei.rlcdn.com/<?=LIVERAMP_WEB?>.gif?s=<?=$emailSHA1?>&n=1"/>
  <img border="0" hspace="0" vspace="0" width="1" height="1" src="http://ei.rlcdn.com/<?=LIVERAMP_WEB?>.gif?s=<?=$emailSHA1?>&n=2"/>
  <img border="0" hspace="0" vspace="0" width="1" height="1" src="http://ei.rlcdn.com/<?=LIVERAMP_WEB?>.gif?s=<?=$emailSHA1?>&n=3"/>
  <img border="0" hspace="0" vspace="0" width="1" height="1" src="http://ei.rlcdn.com/<?=LIVERAMP_WEB?>.gif?s=<?=$emailSHA1?>&n=4"/>
  <img border="0" hspace="0" vspace="0" width="1" height="1" src="http://ei.rlcdn.com/<?=LIVERAMP_WEB?>.gif?s=<?=$emailSHA1?>&n=5"/>

  <!-- Start Conversant Tag -->

  <script type="text/javascript">
    // var dtmSrc = window.location.protocol + "//login.dotomi.com/ucm/UCMController?"+"dtm_com=28&dtm_fid=101&dtm_cid=<?=CONVERSANT?>&dtm_cmagic=<?=CONVERSANT_CMAGIC?>&dtm_format=5";
    // var dtmTag = new Array();
    // dtmTag.cli_promo_id = "1";
    // dtmTag.dtm_email_hash = "[MD5 hash of the lower case of the customer’s email address]";
    // dtmTag.dtm_user_token = "";
    // dtmTag.dtmc_ref = document.referrer;
    // dtmTag.dtmc_loc = document.location.href;
    // function readCookieDotomi() { var name = "dtm_token"; var nameEQ = name + "="; var ca = document.cookie.split(';'); for(var i = 0; i < ca.length; i++) { var c = ca[i]; while(c.charAt(0) == ' ') c = c.substring(1, c.length); if(c.indexOf(nameEQ) == 0) { var d = c.substring(nameEQ.length, c.length); dtmTag.dtm_user_token = d; } } }
    // readCookieDotomi();
    // for (var item in dtmTag){ if(typeof dtmTag[item] != "function" && typeof dtmTag[item] != "object") }
    // setTimeout('timeOutDotomi()',2000);
    // document.write('<div id="dtmdiv" style="display:none;">' +
    // '<iframe name="response_frame" src="' + dtmSrc + '"></iframe></div>');
    // function timeOutDotomi() { document.getElementById("dtmdiv").innerHTML = "";}
  </script>

  <!-- End Conversant Tag --> 

<?php } 

if(DOMAIN == 'Paperrosejobs.com')   { 
 ?>

<script type="text/javascript" src="http://assets.freshdesk.com/widget/freshwidget.js"></script>
<script type="text/javascript">
FreshWidget.init("", {"queryString": "&widgetType=popup", "utf8": "✓", "widgetType": "popup", "buttonType": "text", "buttonText": "JOB ALERT SUPPORT", "buttonColor": "white", "buttonBg": "#000000", "alignment": "4", "offset": "235px", "formHeight": "500px", "url": "https://jobalerts.freshdesk.com"} );
</script>

<?php } } ?>



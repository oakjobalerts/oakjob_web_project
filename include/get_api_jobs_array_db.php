<?php

function juju_jobs($value,$source){
	
		$response = xml2array($value);
		$matchRecordsResults = array();
		
		if(!key_exists("rss",$response)){
			
		     $matchRecordsResults['total_result'] = 0;
		     return $matchRecordsResults;
		}
		
		$resultCount =  $response['rss']['channel']['totalresults'];;
		$resultCount = ($resultCount) ? $resultCount : 0;
		$id = 1;	
		$resultArray = $response['rss']['channel']['item'];
		
		if($resultCount > 0){
			if($resultCount == 1){
					$id++;	
					$RecordsResults['title'] =  strip_tags(addslashes($resultArray['title']));
					$RecordsResults['employer'] =  addslashes($resultArray['company']);
					$RecordsResults['city'] =  addslashes($resultArray['city']);
					$RecordsResults['state'] =  addslashes($resultArray['state']);
					$RecordsResults['joburl'] =  strip_tags(trim($resultArray['link']));
					$RecordsResults['zipcode'] =  addslashes(trim($resultArray['city'].", ".$resultArray['state']));
					$RecordsResults['postingdate'] =  $resultArray['postdate'];
					$RecordsResults['Id'] =  $id;
					$RecordsResults['source'] =  $source;
					$matchRecordsResults['jobs'][] = $RecordsResults;
					
						
				
			}else {
				foreach($resultArray as $keyValue=>$dataValue){
					
					$id++;	
					$RecordsResults['title'] =  strip_tags(addslashes($dataValue['title']));
					$RecordsResults['employer'] =  addslashes($dataValue['company']);
					$RecordsResults['city'] =  addslashes($dataValue['city']);
					$RecordsResults['state'] =  addslashes($dataValue['state']);
					$RecordsResults['joburl'] =  strip_tags(trim($dataValue['link']));
					$RecordsResults['zipcode'] =  addslashes(trim($dataValue['city'].", ".$dataValue['state']));
					$RecordsResults['postingdate'] =  $dataValue['postdate'];
					$RecordsResults['Id'] =  $id;
					$RecordsResults['source'] = $source;
					$matchRecordsResults['jobs'][] = $RecordsResults;
					
						
				}
				
			}
			
			$matchRecordsResults['total_result'] = $resultCount;
			return $matchRecordsResults;
		}else{
			
			$matchRecordsResults['total_result'] = 0;
			return $matchRecordsResults;
		}
	
} 



function cb_jobs($value){
	
		$response = xml2array($value);
		$matchRecordsResults = array();
		
		if(!key_exists("ResponseJobSearch",$response)){
		    
		    $matchRecordsResults['total_result'] = 0;
		    return $matchRecordsResults;
		}
		
		$resultCount =  $response['ResponseJobSearch']['TotalCount'];
		$resultCount = ($resultCount) ? $resultCount : 0;
		$id = 1;
		$resultArray = $response['ResponseJobSearch']['Results']['JobSearchResult'];
		
		if($resultCount > 0){
			if($resultCount == 1){
					
					$id++;	
					$RecordsResults['title'] =  strip_tags(addslashes($resultArray['JobTitle']));
					$RecordsResults['employer'] =  addslashes($resultArray['Company']);
					$RecordsResults['city'] =  addslashes($resultArray['City']);
					$RecordsResults['state'] =  addslashes($resultArray['State']);
					$RecordsResults['joburl'] =  strip_tags(trim($resultArray['JobDetailsURL']))."&utm_source=oakjobalerts.com&utm_medium=aggregator&utm_campaign=api-jobs";
					$RecordsResults['zipcode'] =  addslashes(trim($resultArray['City'].", ".$resultArray['State']));
					$RecordsResults['postingdate'] =  $resultArray['PostedDate'];
					$RecordsResults['Id'] =  $id;
					$RecordsResults['source'] =  '4';
					$matchRecordsResults['jobs'][] = $RecordsResults;
					
						
				
			}else{
				
				foreach($resultArray as $keyValue=>$dataValue){
					
					
					$id++;	
					$RecordsResults['title'] =  strip_tags(addslashes($dataValue['JobTitle']));
					$RecordsResults['employer'] =  addslashes($dataValue['Company']);
					$RecordsResults['city'] =  addslashes($dataValue['City']);
					$RecordsResults['state'] =  addslashes($dataValue['State']);
					$RecordsResults['joburl'] =  strip_tags(trim($dataValue['JobDetailsURL']))."&utm_source=oakjobalerts.com&utm_medium=aggregator&utm_campaign=api-jobs";
					$RecordsResults['zipcode'] =  addslashes(trim($dataValue['City'].", ".$dataValue['State']));
					$RecordsResults['postingdate'] =  $dataValue['PostedDate'];
					$RecordsResults['Id'] =  $id;
					$RecordsResults['source'] =  '4';
					$matchRecordsResults['jobs'][] = $RecordsResults;
					
						
				}	
				
			}
			$matchRecordsResults['total_result'] = $resultCount;
			return $matchRecordsResults;
		}else{
			$matchRecordsResults['total_result'] = 0;
			return $matchRecordsResults;
		}
	
	
} 

function sh_jobs($value,$searchLocation,$searchKeyword,$source){
		
		$value = str_replace("sh_cb(","",$value);
		$value = str_replace(");","",$value);

		$response = json_decode($value, true);
		// echo "<pre>";
		// print_r($response);

		//exit;

		$matchRecordsResults = array();
		if(!key_exists("rs",$response)){
		    $matchRecordsResults['total_result'] = 0;
		    return $matchRecordsResults;
		}
		
		$resultCount =  $response['rq']['tv'];
		$resultCount = ($resultCount) ? $resultCount : 0;
		$resultArray = $response['rs'];
		
		$id = 1;
		if($resultCount > 0){
			
			if($resultCount == 1){
				
				if(is_array($resultArray))
				{
					$id++;	
					$RecordsResults['title'] =  strip_tags(addslashes((string)$resultArray['jt']));
					$RecordsResults['employer'] =  addslashes((string)$resultArray['cn']);
					$RecordsResults['city'] =  $resultArray['cty'];
					$RecordsResults['state'] =  $resultArray['st'];
					  try {
						$RecordsResults['joburl'] =  "http://104.130.9.242/RedirectSH.php?jk=".$resultArray['jk']."&q=".$searchKeyword."&l=".$searchLocation;
					    } catch (Exception $e) {
						echo 'Caught exception: ',  $e->getMessage(), "\n";
					    }
					$RecordsResults['zipcode'] =  addslashes(trim((string)$resultArray['postal']));
					$RecordsResults['postingdate'] =  (string)$resultArray['dp'];
					$RecordsResults['Id'] =  $id;
					$RecordsResults['source'] =  $source;
					$matchRecordsResults['jobs'][] = $RecordsResults;
				}
				
			}else{
				
				foreach($resultArray as $keyValue=>$dataValue){
					
					
					if(is_array($dataValue)){
						$id++;	
						$RecordsResults['title'] =  strip_tags(addslashes((string)$dataValue['jt']));
						$RecordsResults['employer'] =  addslashes((string)$dataValue['cn']);
						$RecordsResults['city'] =  $dataValue['cty'];
						$RecordsResults['state'] =  $dataValue['st'];
						  try {
							$RecordsResults['joburl'] =  "http://104.130.9.242/RedirectSH.php?jk=".$dataValue['jk']."&q=".$searchKeyword."&l=".$searchLocation;
						    } catch (Exception $e) {
							echo 'Caught exception: ',  $e->getMessage(), "\n";
						    }
						$RecordsResults['zipcode'] =  addslashes(trim((string)$dataValue['postal']));
						$RecordsResults['postingdate'] =  (string)$dataValue['dp'];
						$RecordsResults['Id'] =  $id;
						$RecordsResults['source'] = $source;
						$matchRecordsResults['jobs'][] = $RecordsResults;
					}
						
				}
				
			}
			
			$matchRecordsResults['total_result'] = $resultCount;
			return $matchRecordsResults;
			
		}else{
			$matchRecordsResults['total_result'] = 0;
			return $matchRecordsResults;
			
		}
	
} 


function sh_jobs_old($value){
	
		$response = xml2array($value);
		$matchRecordsResults = array();
		if(!key_exists("shrs",$response)){
		    $matchRecordsResults['total_result'] = 0;
		    return $matchRecordsResults;
		}
		
		$resultCount =  $response['shrs']['rq']['tv'];
		$resultCount = ($resultCount) ? $resultCount : 0;
		$resultArray = $response['shrs']['rs']['r'];
		
		$id = 1;
		if($resultCount > 0){
			
			if($resultCount == 1){
				
					if(is_array($resultArray)){
					$id++;	
					$RecordsResults['title'] =  strip_tags(addslashes((string)$resultArray['jt']));
					$RecordsResults['employer'] =  addslashes((string)$resultArray['cn']);
					$RecordsResults['city'] =  '';
					$RecordsResults['state'] =  '';
					  try {
						$RecordsResults['joburl'] =  strip_tags(trim((string)$resultArray['src_attr']['url']));
					    } catch (Exception $e) {
						echo 'Caught exception: ',  $e->getMessage(), "\n";
					    }
					$RecordsResults['zipcode'] =  addslashes(trim((string)$resultArray['loc']));
					$RecordsResults['postingdate'] =  (string)$resultArray['dp'];
					$RecordsResults['Id'] =  $id;
					$RecordsResults['source'] =  '1';
					$matchRecordsResults['jobs'][] = $RecordsResults;
					}
					
					
						
				
			}else{
				
				foreach($resultArray as $keyValue=>$dataValue){
					
					
					if(is_array($dataValue)){
					$id++;	
					$RecordsResults['title'] =  strip_tags(addslashes((string)$dataValue['jt']));
					$RecordsResults['employer'] =  addslashes((string)$dataValue['cn']);
					$RecordsResults['city'] =  '';
					$RecordsResults['state'] =  '';
					  try {
						$RecordsResults['joburl'] =  strip_tags(trim((string)$dataValue['src_attr']['url']));
					    } catch (Exception $e) {
						echo 'Caught exception: ',  $e->getMessage(), "\n";
					    }
					$RecordsResults['zipcode'] =  addslashes(trim((string)$dataValue['loc']));
					$RecordsResults['postingdate'] =  (string)$dataValue['dp'];
					$RecordsResults['Id'] =  $id;
					$RecordsResults['source'] =  '1';
					$matchRecordsResults['jobs'][] = $RecordsResults;
					}
						
				}
				
			}
			
			$matchRecordsResults['total_result'] = $resultCount;
			return $matchRecordsResults;
			
		}else{
			$matchRecordsResults['total_result'] = 0;
			return $matchRecordsResults;
			
		}
	
} 

function j2c_jobs($value,$source){
	
		$response = json_decode($value);
		$matchRecordsResults = array();
		
		$totalResults = $response->total;
		$jobsResult = $response->jobs;
		
		$resultCount = $totalResults;
		
		$id = 1;
		if($resultCount > 0){
			
			foreach($jobsResult as $jobs) {
				
				$id++;
					$RecordsResults['title'] =  $jobs->title;
					$RecordsResults['employer'] =  $jobs->company;
					$RecordsResults['city'] =  '';
					$RecordsResults['state'] =  '';
					$RecordsResults['joburl'] =  $jobs->url."&t2=954JS";
					$RecordsResults['postingdate'] =  date('Y-m-d',strtotime($jobs->date));
					$RecordsResults['Id'] =  $id;
					$RecordsResults['source'] =  $source;
					$RecordsResults['zipcode'] = $jobs->city[0];
					$matchRecordsResults['jobs'][] = $RecordsResults;
					
			}
		
			$matchRecordsResults['total_result'] = $totalResults;
			return $matchRecordsResults;
		
		}else{
			$matchRecordsResults['total_result'] = 0;
			return $matchRecordsResults;
		
		}
	
} 
function my_jobs($value,$source){
	
		$response = json_decode($value);
		$matchRecordsResults = array();
		
		$totalResults = $response->queryResult->totalResults;
		$jobsResult = $response->queryResult->jobs;
		
		$resultCount = $totalResults;
		
		$id = 1;
		if($resultCount > 0){
			
			foreach($jobsResult as $jobs) {
				
				
				if($jobs->expired != 'N')
						continue;
				$id++;
					$RecordsResults['title'] =  $jobs->title;
					$RecordsResults['employer'] =  $jobs->company;
					$RecordsResults['city'] =  '';
					$RecordsResults['state'] =  '';
					$RecordsResults['joburl'] =  $jobs->url;
					$RecordsResults['postingdate'] =  date('Y-m-d',strtotime($jobs->postDate));
					$RecordsResults['Id'] =  $id;
					$RecordsResults['source'] =  $source;
					$RecordsResults['zipcode'] = $jobs->city.", ".$jobs->state;
					$matchRecordsResults['jobs'][] = $RecordsResults;
					
			}
		
			$matchRecordsResults['total_result'] = $totalResults;
			return $matchRecordsResults;
		
		}else{
			$matchRecordsResults['total_result'] = 0;
			return $matchRecordsResults;
		
		}
	
}

function jobapproved_jobs($value,$source=''){
	
		$response = json_decode($value);
		$matchRecordsResults = array();
		
		$totalResults = $response[0]->response->totalresults;
		$jobsResult = $response[0]->response->results;
		
		$resultCount = $totalResults;
		
		$id = 1;
		if($resultCount > 0){
			
			foreach($jobsResult as $jobs) {
				
				
				$id++;
					$RecordsResults['title'] =  $jobs->jobtitle;
					$RecordsResults['employer'] =  $jobs->company;
					$RecordsResults['city'] =  '';
					$RecordsResults['state'] =  '';
					$RecordsResults['joburl'] =  $jobs->url;
					$RecordsResults['postingdate'] =  date('Y-m-d',strtotime($jobs->date));
					$RecordsResults['Id'] =  $id;
					$RecordsResults['source'] =  $source;
					$RecordsResults['zipcode'] = $jobs->city.", ".$jobs->state;
					$matchRecordsResults['jobs'][] = $RecordsResults;
					
			}
		
			$matchRecordsResults['total_result'] = $totalResults;
			return $matchRecordsResults;
		
		}else{
			$matchRecordsResults['total_result'] = 0;
			return $matchRecordsResults;
		
		}
	
} 

function ziprecruiter_jobs($value,$source){
		
		$zr_api_resp = json_decode($value,1);
		$matchRecordsResults = array();
		
		if(count($zr_api_resp['jobs']) == 0){
			
		     $matchRecordsResults['total_result'] = 0;
		     return $matchRecordsResults;
		}
		
		 $resultCount =  count($zr_api_resp['jobs']);
		// $resultCount = ($resultCount) ? $resultCount : 0;
		$id = 1;
		$resultArray = $zr_api_resp['jobs'];
		
		if($resultCount > 0){
			if($resultCount == 1){
					
					if(strip_tags(addslashes($resultArray['title'])) == ''){
						
						$matchRecordsResults['total_result'] = 0;
						return $matchRecordsResults;
				        }
				        
					$id++;
					$RecordsResults['title'] =  strip_tags(addslashes($resultArray['title']));
					$RecordsResults['employer'] =  addslashes($resultArray['org_name']);
					$RecordsResults['city'] =  addslashes($resultArray['city']);
					$RecordsResults['state'] =  addslashes($resultArray['state']);
					$RecordsResults['joburl'] =  strip_tags(trim($resultArray['url']));
					$RecordsResults['zipcode'] =  addslashes(trim($resultArray['city'].", ".$resultArray['state']));
					$RecordsResults['postingdate'] =  "";
					$RecordsResults['Id'] =  $id;
					$RecordsResults['source'] =  $source;
					$matchRecordsResults['jobs'][] = $RecordsResults;
	
				
			}else {
	
				foreach($resultArray as $keyValue=>$dataValue){
					
					if(strip_tags(addslashes($dataValue['title'])) == '')
					continue;
				
					$id++;	
					$RecordsResults['title'] =  strip_tags(addslashes($dataValue['title']));
					$RecordsResults['employer'] =  addslashes($dataValue['org_name']);
					$RecordsResults['city'] =  addslashes($dataValue['city']);
					$RecordsResults['state'] =  addslashes($dataValue['state']);
					$RecordsResults['joburl'] =  strip_tags(trim($dataValue['url']));
					$RecordsResults['zipcode'] =  addslashes(trim($dataValue['city'].", ".$dataValue['state']));
					$RecordsResults['postingdate'] =  "";
					$RecordsResults['Id'] =  $id;
					$RecordsResults['source'] =  $source;
					$matchRecordsResults['jobs'][] = $RecordsResults;
					
						
				}
				
			}
			
			$matchRecordsResults['total_result'] = $resultCount;
			return $matchRecordsResults;
		}else{
			
			$matchRecordsResults['total_result'] = 0;
			return $matchRecordsResults;
		}
	
}

function glassdoor_jobs($value,$source)
{    
	$obj=json_decode($value);
	$matchRecordsResults = array();
	$totalResults=$obj->response->totalRecordCount;
	$resultCount = $totalResults;
		
		
		if($resultCount > 1)
		{
				$response=$obj->response->jobListings;
				foreach($response as $val)
				{
					
					        $location=$val->location;
						$location=explode(",",$location);
						
						$city= $location[0];
						$state= $location[1];
						
						$employer=$val->employer->name;
						
						$RecordsResults['title']=$val->jobTitle;
						$RecordsResults['employer'] =  addslashes($val->employer->name);
						$RecordsResults['state']=addslashes($state);
						$RecordsResults['city']=addslashes($city);
						$RecordsResults['joburl'] =  strip_tags(trim($val->jobViewUrl));
						$RecordsResults['zipcode'] =  addslashes(trim($city.", ".$state));
						$RecordsResults['postingdate'] =  $val->date;
						$RecordsResults['Id'] =  $val->jobListingId;
						$RecordsResults['source'] =  $source;
						$matchRecordsResults['jobs'][] = $RecordsResults;
					
				}
				$matchRecordsResults['total_result'] = $totalResults;
			    return $matchRecordsResults;
	 	}
	 	else
	 	{
			$matchRecordsResults['total_result'] = 0;
			return $matchRecordsResults;
		}	
	
}

function cloud_search_jobs($value)
{    
	$obj = json_decode($value, true);
	$matchRecordsResults = array();
	$totalResults = $obj['total_found'];
	$resultCount = $totalResults;
		
		if($resultCount > 1)
		{
				$response=$obj['Jobs'];
				foreach($response as $val)
				{
						
						$RecordsResults['title']=$val['title'];
						$RecordsResults['employer'] =  addslashes($val['employer']);
						$RecordsResults['state']=addslashes($val['state']);
						$RecordsResults['city']=addslashes($val['city']);
						$RecordsResults['joburl'] =  strip_tags(trim($val['joburl']));
						$RecordsResults['zipcode'] =  addslashes(trim($val['city'].", ".$val['state']));
						$RecordsResults['postingdate'] =  $val['postingdate'];
						$RecordsResults['Id'] =  $val['id'];
						$RecordsResults['source'] =  $val['source'];
						$matchRecordsResults['jobs'][] = $RecordsResults;
					
				}
				$matchRecordsResults['total_result'] = $totalResults;
			    return $matchRecordsResults;
	 	}
	 	else
	 	{
			$matchRecordsResults['total_result'] = 0;
			return $matchRecordsResults;
		}	
	
}

function college_db_jobs($searchString,$zipcode) {
	global $objDBManager;

	$BEYOND_XML_TABLE_NAME = 'tbl_collagerecruiter';
	$ZIP_RADIOUS_TABLE = 'tbl_zipcodes_test';
  //$sourceMatchModifier = " AND (j.source >'0')";
   $sourceMatchModifier = '';
   //$havingSourceMatchModifier = "(j2.source >'0')";
   $havingSourceMatchModifier = '';
   $caseModifierForRecentJob = "j2.postingdate >= (CASE WHEN (j2.source = '9') THEN CURDATE() - INTERVAL 1 DAY WHEN (j2.source = '2' OR j2.source = '3' OR j2.source = '7') THEN CURDATE() - INTERVAL 2 DAY  ELSE NOW() - INTERVAL 1 DAY END)";
   
     //$startZiptime = microtime(true);
     $keywordHasSpace = strpos($searchString, " ");
     if($keywordHasSpace === false){
            
              

			     $matchRecordsSql = "SELECT j.id AS referencenumber,  j.source, j.employer AS company,j.city AS city,j.state AS state,j.zipcode AS zip,j.joburl AS url,j.postingdate AS updated,j.title 
									FROM $BEYOND_XML_TABLE_NAME AS j
									INNER JOIN $ZIP_RADIOUS_TABLE AS z ON j.zipcode = z.zipcode
									WHERE j.title LIKE '%".$searchString."%'
									AND z.zip =  '".mysql_escape_string($zipcode)."'  $sourceMatchModifier order by source,postingdate DESC
									LIMIT 0 , 500 ";
		   
           }else{
            
	           
					  $keywordSplit = array_map("trim",explode(" ",$searchString));
					  $fullKeywordQry = "SELECT j.id AS referencenumber,  j.source, j.employer AS company,j.city AS city,j.state AS state,j.zipcode AS zip,j.joburl AS url,j.postingdate AS updated,j.title,'1' AS priority 
											      FROM $BEYOND_XML_TABLE_NAME AS j
											      INNER JOIN $ZIP_RADIOUS_TABLE AS z ON j.zipcode = z.zipcode
											      WHERE j.title LIKE '%".$searchString."%'
											      AND z.zip =  '".mysql_escape_string($zipcode)."'  $sourceMatchModifier ";
											      
										       
					  $matchRecordsSql = "select referencenumber, source, company, city, state, zip, url, updated,title,priority from( ";
					  $matchRecordsSql .= $fullKeywordQry;
					
					  $LIKE = '';
					  foreach($keywordSplit as $sliptkeyword){
					      
					      
					      if($LIKE == '') 
					       $LIKE  = "(j.title LIKE '%".mysql_escape_string($sliptkeyword)."%'";
					     else
					       $LIKE .= " AND j.title LIKE '%".mysql_escape_string($sliptkeyword)."%'";
					      
					      
					     
					      
					  }
					  $LIKE .=")";
					  $LIKE1 = str_replace("j.title","j2.title",$LIKE);
					 $matchRecordsSql .= "UNION ALL ";
					 $matchRecordsSql .= "SELECT j.id AS referencenumber, j.source, j.employer AS company,j.city AS city,j.state AS state,j.zipcode AS zip,j.joburl AS url,j.postingdate AS updated,j.title,'2' AS priority  
											      FROM $BEYOND_XML_TABLE_NAME AS j
											      INNER JOIN $ZIP_RADIOUS_TABLE AS z ON j.zipcode = z.zipcode
											      WHERE $LIKE
											      AND z.zip =  '".mysql_escape_string($zipcode)."'  $sourceMatchModifier ";
											      
											      
					
							$LIKE2 = '';
							$LIKE1 = '';
						       foreach($relationKeywords as $relationKeyword){
							     
							     if(trim(strtolower($relationKeyword['keyword'])) == strtolower(trim($searchString)))
								       continue;
							     
							     $matchRecordsSql .=" UNION ALL ";
							     
							      $LIKE1  = "j.title LIKE '%".mysql_escape_string(trim($relationKeyword['keyword']))."%'";
							      $LIKE2 = "j2.title LIKE '%".mysql_escape_string(trim($relationKeyword['keyword']))."%'";
							      
							      $matchRecordsSql .= "SELECT j.id AS referencenumber,  j.source, j.employer AS company,j.city AS city,j.state AS state,j.zipcode AS zip,j.joburl AS url,j.postingdate AS updated,j.title,'3' AS priority   
													     FROM $BEYOND_XML_TABLE_NAME AS j
													     INNER JOIN $ZIP_RADIOUS_TABLE AS z ON j.zipcode = z.zipcode
													     WHERE $LIKE1
													     AND z.zip =  '".mysql_escape_string($zipcode)."'  $sourceMatchModifier ";
							     
							 }
			  								      
											      
							  $matchRecordsSql .= " ) as jobs ORDER BY priority,source,updated DESC LIMIT 0 , 500";  
	
           }
          // echo $matchRecordsSql;
          // exit;
          // echo "<br><br><br><br>";
           
	   $matchRecordsRecords = $objDBManager->fetchRecord($matchRecordsSql);
	   foreach ($matchRecordsRecords as $key => $matchRecordsResults) {

		    $RecordsResults['title']=$matchRecordsResults['title'];
			$RecordsResults['employer'] =$matchRecordsResults['company'];
			$RecordsResults['state']=$matchRecordsResults['state'];
			$RecordsResults['city']=$matchRecordsResults['city'];
			$RecordsResults['joburl'] =$matchRecordsResults['url'];
			$RecordsResults['zipcode'] =  $matchRecordsResults['zip'];
			$RecordsResults['postingdate'] =  $matchRecordsResults['updated'];
			$RecordsResults['Id'] =  $matchRecordsResults['referencenumber'];
			$RecordsResults['source'] =  '1';
			$Results['jobs'][] = $RecordsResults;
		}
	   $totalCountRecords = count($Results['jobs']);
	   $Results['total_result'] = $totalCountRecords;

	   return $Results;
}

function get_lat_long($location) {

	global $objDBManager;

	$searchLocation = mysql_escape_string(trim($location));
	
	if (is_numeric($searchLocation)) {
	    
	    $zipcode             = $searchLocation;
	    
	} else {
	    
	    $pos  = strpos($searchLocation, ",");
	    $pos1 = strpos($searchLocation, " ");
	    
	    if ($pos === false && $pos1 === false) {
	        
	        // echo 'gaurav';
	        $matchZipcode        = "SELECT zip FROM tbl_zipcodes WHERE primary_city = '" . $searchLocation . "' OR state='" . $searchLocation . "' OR state in(SELECT abbreviation as state FROM state WHERE name = '" . $searchLocation . "')  ORDER BY estimated_population DESC LIMIT 0,1";
	        $matchZipcodeResults = $objDBManager->fetchRecord($matchZipcode);
	        $zipcode             = $matchZipcodeResults[0]['zip'];
	        
	    } else {

	        $pos = strpos($searchLocation, ",");
	        
	        if ($pos === false)
	            $searchLocation = array_map("trim", explode(" ", $searchLocation, 2));
	        else
	            $searchLocation = array_map("trim", explode(",", $searchLocation, 2));
	        
	        if (strlen($searchLocation[1]) > 2) {
            
	            $matchZipcode        = "SELECT zip FROM tbl_zipcodes WHERE primary_city = '" . $searchLocation[0] . "' AND state='" . $abbreviation . "'  ORDER BY estimated_population DESC LIMIT 0,1";
	            $matchZipcodeResults = $objDBManager->fetchRecord($matchZipcode);
	            $zipcode             = $matchZipcodeResults[0]['zip'];
	            
	        } else {
	            
	            $matchZipcode        = "SELECT zip,state FROM tbl_zipcodes WHERE primary_city = '" . $searchLocation[0] . "' AND state='" . $searchLocation[1] . "'  ORDER BY estimated_population DESC LIMIT 0,1";
	            $matchZipcodeResults = $objDBManager->fetchRecord($matchZipcode);
	            $zipcode             = $matchZipcodeResults[0]['zip'];
	            
	        }
	        
	    }
	    
	}

	$getlongitude_qry = "SELECT latitude,longitude FROM tbl_zipcodes WHERE zip = '$zipcode'";

	$getlongitude=$objDBManager->exeQuery($getlongitude_qry);
	if($row=mysql_fetch_array($getlongitude))
	{
	  $lat = $row['latitude'];
	  $lng = $row['longitude'];
	}

	// if( $unit == 'km' ) { $radius = 6371.009; }
 	// elseif ( $unit == 'mi' ) { $radius = 3958.761; }
	if(isset($lat) && isset($lng)) {
		$radius = 3958.761;
	    $distance = 34;

	    // latitude boundaries
	    $maxLat = ( float ) $lat + rad2deg( $distance / $radius );
	    $minLat = ( float ) $lat - rad2deg( $distance / $radius );

	    // longitude boundaries (longitude gets smaller when latitude increases)
	    $maxLng = $lng - rad2deg($distance / $radius / cos(deg2rad($lat)));
	    $minLng = $lng + rad2deg($distance / $radius / cos(deg2rad($lat)));

	    $max_min_values = array(
	    'max_latitude' => $maxLat,
	    'min_latitude' => $minLat,
	    'max_longitude' => $maxLng,
	    'min_longitude' => $minLng
	    );

	    //print_r($max_min_values); exit;

	} else {
		$max_min_values = "";

	}
    
    return $max_min_values;

}   

function xml2array($contents, $get_attributes=1, $priority = 'tag') {
		if(!$contents) return array();
	
		if(!function_exists('xml_parser_create')) {
			//print "'xml_parser_create()' function not found!";
			return array();
		}
	
		//Get the XML parser of PHP - PHP must have this module for the parser to work
		$parser = xml_parser_create('');
		xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8"); # http://minutillo.com/steve/weblog/2004/6/17/php-xml-and-character-encodings-a-tale-of-sadness-rage-and-data-loss
		xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
		xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
		xml_parse_into_struct($parser, trim($contents), $xml_values);
		xml_parser_free($parser);
	
		if(!$xml_values) return;//Hmm...
	
		//Initializations
		$xml_array = array();
		$parents = array();
		$opened_tags = array();
		$arr = array();
	
		$current = &$xml_array; //Refference
	
		//Go through the tags.
		$repeated_tag_index = array();//Multiple tags with same name will be turned into an array
		foreach($xml_values as $data) {
			unset($attributes,$value);//Remove existing values, or there will be trouble
	
			//This command will extract these variables into the foreach scope
			// tag(string), type(string), level(int), attributes(array).
			extract($data);//We could use the array by itself, but this cooler.
	
			$result = array();
			$attributes_data = array();
			
			if(isset($value)) {
				if($priority == 'tag') $result = $value;
				else $result['value'] = $value; //Put the value in a assoc array if we are in the 'Attribute' mode
			}
	
			//Set the attributes too.
			if(isset($attributes) and $get_attributes) {
				foreach($attributes as $attr => $val) {
					if($priority == 'tag') $attributes_data[$attr] = $val;
					else $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
				}
			}
	
			//See tag status and do the needed.
			if($type == "open") {//The starting of the tag '<tag>'
				$parent[$level-1] = &$current;
				if(!is_array($current) or (!in_array($tag, array_keys($current)))) { //Insert New tag
					if($result == 'undefined'){
						
						$result = array();
					
					}
					$current[$tag] = $result;
					if($attributes_data) $current[$tag. '_attr'] = $attributes_data;
					$repeated_tag_index[$tag.'_'.$level] = 1;
	
					$current = &$current[$tag];
	
				} else { //There was another element with the same tag name
	
					if(isset($current[$tag][0])) {//If there is a 0th element it is already an array
						$current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;
						$repeated_tag_index[$tag.'_'.$level]++;
					} else {//This section will make the value an array if multiple tags with the same name appear together
						$current[$tag] = array($current[$tag],$result);//This will combine the existing item and the new item together to make an array
						$repeated_tag_index[$tag.'_'.$level] = 2;
						
						if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well
							$current[$tag]['0_attr'] = $current[$tag.'_attr'];
							unset($current[$tag.'_attr']);
						}
	
					}
					$last_item_index = $repeated_tag_index[$tag.'_'.$level]-1;
					$current = &$current[$tag][$last_item_index];
				}
	
			} elseif($type == "complete") { //Tags that ends in 1 line '<tag />'
				//See if the key is already taken.
				if(!isset($current[$tag])) { //New Key
					$current[$tag] = $result;
					$repeated_tag_index[$tag.'_'.$level] = 1;
					if($priority == 'tag' and $attributes_data) $current[$tag. '_attr'] = $attributes_data;
	
	
				} else { //If taken, put all things inside a list(array)
					if(isset($current[$tag][0]) and is_array($current[$tag])) {//If it is already an array...
	
						// ...push the new element into that array.
						$current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;
						
						if($priority == 'tag' and $get_attributes and $attributes_data) {
							$current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
						}
						$repeated_tag_index[$tag.'_'.$level]++;
	
					} else { //If it is not an array...
						$current[$tag] = array($current[$tag],$result); //...Make it an array using using the existing value and the new value
						$repeated_tag_index[$tag.'_'.$level] = 1;
						if($priority == 'tag' and $get_attributes) {
							if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well
								
								$current[$tag]['0_attr'] = $current[$tag.'_attr'];
								unset($current[$tag.'_attr']);
							}
							
							if($attributes_data) {
								$current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
							}
						}
						$repeated_tag_index[$tag.'_'.$level]++; //0 and 1 index is already taken
					}
				}
	
			} elseif($type == 'close') { //End of tag '</tag>'
				$current = &$parent[$level-1];
			}
		}
		
		return($xml_array);
}
?>
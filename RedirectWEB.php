<?php
ob_start();
// error_reporting(E_ALL);
// ini_set('display_errors', 1);
require_once('include/configuration.php');
require_once('include/sources_config.php');
require_once('include/DBManager.php');

$objDBManager = new DBManager(); //initialize db connection
$objDBManager->createConnection();

// $logeventsObj = new LogEvents();
$isNotBotClick = TRUE;

	$catcha_secret_key = "6LcYWQcTAAAAADxYGTrYArU95_ks9-GKau5tBrzq";

	if(defined('CAPTCHA_SECRET_KEY') && !empty(CAPTCHA_SECRET_KEY)){
		$catcha_secret_key=CAPTCHA_SECRET_KEY;
	}

//if($_SERVER['REMOTE_ADDR'] == '112.196.1.221') { echo "Hello.."; }
//include("include/loading_page.php");

/**
 * Redirect to another page using Javascript. Set optional (bool)$dontwait to false to force manual redirection (make sure a message has been read by user)
 *
 */

//MEMCACHE OBJ
//$memcache_obj = memcache_connect("localhost", 11211);
$memcache_obj_elastic = memcache_connect("oakelasticcache.25s6pq.0001.use1.cache.amazonaws.com", 11211);
//$memcache_obj->flush();
session_start();
$user_ip = $_SERVER['REMOTE_ADDR']."_".DOMAIN;
//memcache_set($memcache_obj, $user_ip, array(), false, 0);

if(isset($_GET['captchaSubmit'])){

	$captcha_val = $_GET['g-recaptcha-response'];	
	if(!$captcha_val){
	  echo '<h2>Please check the the captcha form.</h2>';
	  exit;
	}
	
	$response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$catcha_secret_key."&response=".$captcha_val."&remoteip=".$_SERVER['REMOTE_ADDR']), true);

	if($response['success'] == false)
	{
	  $captcha="incorrect";
	} 
	else 
	{
	  $captcha="correct";
	}
	
}


if(isset($_GET['q']) && !empty($_GET['q'])){
    
    $token = $_GET['token'];
    $tablename = 'tbl_all_jobs';
    $q=$_GET['q'];
	$isFailure = (isset($_GET['isFailure']) && $_GET['isFailure'] == true) ? 'TRUE' : 'FALSE';


		
	
		$captchaAndBotHandlingObject = new CaptchaAndBotHandling();
	
   		$isBot = $captchaAndBotHandlingObject->checkIsBot();
   		if($isBot){
			$isNotBotClick = FALSE;
			// $logeventsObj->logEventstoQueue($queueLogsArray,'bot_click');
		}
		// else{

			// if($_SERVER['HTTP_HOST'] == 'paperrosejobs.com' || $_SERVER['HTTP_HOST'] == 'www.paperrosejobs.com'){

			// 	$logeventsObj = new LogEvents();
			// 	$captchaAndBotHandlingObject->showCaptcha($logeventsObj,$captcha,$user_ip);

			// // Start: Captcha condition for bots
			// 	if(!memcache_get($memcache_obj_elastic, $user_ip)) {
			// 		$check_bot = mysql_query("SELECT ip FROM bot_ips WHERE ip = '".$_SERVER['REMOTE_ADDR']."'");
			// 		if(mysql_num_rows($check_bot) > 0) {
			// 			if(isset($captcha) && $captcha == "correct") { 
			// 				memcache_set($memcache_obj_elastic, trim($user_ip), array('1',time()+86400,'9999'), false, time()+86400); 
			// 			} else { 
			// 				include("include/captcha.php"); exit; 
			// 			}
			// 		}
			// 	}
			// 	// End: Captcha condition for bots
			// 	if(memcache_get($memcache_obj_elastic, $user_ip)){
					
			// 		$mem_array = memcache_get($memcache_obj_elastic, $user_ip);
					
			// 		$click_count = $mem_array[0] + 1;
			// 		$time = $mem_array[1];
			// 		$total_clicks = $mem_array[2];
			// 		memcache_set($memcache_obj_elastic, $user_ip , array($click_count,$time,$total_clicks), false, time()+43200);
					
					
			// 		if($click_count > $total_clicks) { 
						
			// 			memcache_set($memcache_obj_elastic, $user_ip , array($click_count,$time,$total_clicks), false, time()+43200); 
			// 			if(isset($captcha) && $captcha == "correct") { memcache_set($memcache_obj_elastic, trim($user_ip), array('1',time()+86400,'9999'), false, time()+86400); } 
			// 			else { include("include/captcha.php"); exit; }
			// 		} else {
			// 			include("include/loading_page.php"); 
			// 		}
				  
			// 	} else {
			// 		memcache_set($memcache_obj_elastic, $user_ip , array('1',time()+30,'5'), false, time()+43200);
			// 		include("include/loading_page.php");
			// 	}

		// }
	// }


    yourls_redirect($token,$tablename,$q);
}
else{
    
    header( "Location: http://cylcon.com" );
    die;
    
}


function yourls_redirect_javascript( $location, $dontwait = true ) {
		$message =  'if you are not redirected after 10 seconds, please <a href="'.$location.'">click here</a>' ;
		echo <<<REDIR
		<script type="text/javascript">
		window.location="$location";
		</script>
		<small>($message)</small>
REDIR;
    }
/**
 * Redirect to another page
 *
 */
function yourls_redirect( $token,$tablename,$q) {
	
	global $sources;
	global $sourcesInsert;
	global $objDBManager;
	global $isNotBotClick;

        $LOGS_TABLE_NAME = 'tbl_logs';
        
       $uncompressed = urldecode(base64_decode($q));
		parse_str($uncompressed, $get_array);
		
		$getUrlResult['source'] = $get_array['source'];
		$getUrlResult['sourcename'] = $get_array['sourcename'];
		$getUrlResult['joburl'] = str_replace("##","&",$get_array['url']);
		$getUrlResult['title'] = $get_array['title'];
		$getUrlResult['city'] = $get_array['city'];
		$getUrlResult['state'] = $get_array['state'];
		$getUrlResult['email'] 		= ($get_array['email']) ? $get_array['email'] :'';
		$getUrlResult['cpc'] 		= ($get_array['cpc']) ? $get_array['cpc'] : '';
		$getUrlResult['gcpc'] 		= ($get_array['gcpc']) ? $get_array['gcpc'] : '';
		$getUrlResult['zipcode'] 	= '';
		$getUrlResult['employer'] 	= $get_array['employer'];
		$getUrlResult['provider'] 	= urldecode($get_array['provider']);

		if(isset($get_array['lp_click_type']))
		{
			$getUrlResult['lp_click_type'] 	= $get_array['lp_click_type'];
			$getUrlResult['landing_page_type'] 	= $get_array['landing_page_type'];
			$getUrlResult['failure_case'] 	= urldecode($get_array['failure_case']);
			
		}

      //  $getUrlQuery = "SELECT employer,title,postingdate,joburl,zipcode,city,state,source FROM $tablename WHERE id = $token";
      //  $getUrlResult = mysql_fetch_assoc(mysql_query($getUrlQuery));
        
	
	$subid = '';
	
	if(strpos(strtolower($getUrlResult['sourcename']), 'jobs2careers') !== false){
		
	  $subid = SUBID_JOBS2CAREERS;
			
		
	}if(strpos(strtolower($getUrlResult['sourcename']), 'topusajobs') !== false){
        
		// START: Redirect to topUSAjobs create alert page before redirect to job
		//if($_SERVER['REMOTE_ADDR'] == '112.196.1.221' || $_SERVER['REMOTE_ADDR'] == '111.93.206.202') {
			if(!isset($_GET['topusa_registered']) && strpos(strtolower(DOMAIN), 'topusa') === false) {
				$objTopUSA = new handleTopUSArequest(); //initialize db connection
				$objTopUSA->CheckForTopusajobs($q,'web');
			}
		//}
		// END: Redirect to topUSAjobs create alert page before redirect to job


        $subid = SUBID_TOPUSAJOBS;    
        $getUrlResult['joburl'] = str_replace("&f=fbg", "", $getUrlResult['joburl']);	

	}


// VIKTRE Redirect popup
	if(strpos(strtolower($getUrlResult['sourcename']), 'viktre') !== false){
			if(!isset($_GET['viktre_registered'])) {
				$_SESSION['type'] = 'web';
				$objViktre = new handleViktrerequest();
				$objViktre->CheckForviktrejobs($q,'web');
			}
        $subid = SUBID_VICTRE;    
        $getUrlResult['joburl'] = str_replace("&f=fbg", "", $getUrlResult['joburl']);	

	}



	if(strpos(strtolower($getUrlResult['sourcename']), 'juju') !== false){
		
		
	    $getUrlResult['joburl'] = str_replace("&amp;channel=staticfile", "", $getUrlResult['joburl']);
		$subid = SUBID_JUJU;
		$getUrlResult['joburl'] = str_replace("&amp;", "&", $getUrlResult['joburl']);	
	}
	if(strpos(strtolower($getUrlResult['source']),'lead') !== false && strpos(strtolower($getUrlResult['source']),'api') === false ){
		
		$getUrlResult['job_detail_url'] = str_replace("&CHID=", "", $getUrlResult['job_detail_url']);	
		$subid = SUBID_LEAD;
	   	$getUrlResult['job_detail_url'] = str_replace("&amp;", "&", $getUrlResult['job_detail_url']);	
	}

		$type = 'web';

		if(isset($getUrlResult['provider']) && !empty($getUrlResult['provider'])){
			echo "";
		}else if(isset($_COOKIE['source']) && !empty($_COOKIE['source'])) {
			$type = $_COOKIE['source'];
	    }else if(PROVIDER != '') {
	    	$refereurl = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
			if($refereurl == 'a.viktrejobalerts.com'){
					$type = 'viktre';
				} else {
					$type = PROVIDER;
				}
	    } else if(SOURCE != '') {
			$type = base64_decode(SOURCE);
	    }
	    $type = (array_key_exists('provider', $get_array))?$get_array['provider']:$type;

	    if(isset($getUrlResult['email']) && !empty($getUrlResult['email'])){
	     	$_COOKIE['email'] = $getUrlResult['email'];
	    }



	 //FOR LENSA
	  $randnumber =  rand(0,1);
	if(strtolower($getUrlResult['sourcename']) == 'lensa' && $randnumber % 2 == 0){
		
		 $encoded_lensa_data = base64_encode($getUrlResult['email'].'|'.$getUrlResult['title'].'|'.$getUrlResult['city'].'|'.$getUrlResult['state'].'|'.$getUrlResult['zipcode'].'|||'.$getUrlResult['joburl'].'|'.$getUrlResult['sourcename'].'|'.PROVIDER.'||||||||||'.$getUrlResult['gcpc'].'|'.$getUrlResult['cpc'].'|');
		 $location = 'http://'.$_SERVER['HTTP_HOST'].'/lensa_redirect.php?q='.$encoded_lensa_data;

		 if($isNotBotClick == FALSE){
		 	log_clicks_queue_java($getUrlResult,$getUrlResult['joburl'],$type);
		 }


		 header( "Location: $location" );
		 // if($_SERVER['REMOTE_ADDR'] == '49.200.119.31') {
		 // echo $location;exit;
		 // }
		
		die();
		
	}
	
	 //FOR Beyond	
	// if(strtolower($getUrlResult['sourcename']) == 'beyond'){
		
	// 	 $encoded_beyond_data = base64_encode($getUrlResult['email'].'|'.$getUrlResult['title'].'|'.$getUrlResult['city'].'|'.$getUrlResult['state'].'|'.$getUrlResult['zipcode'].'|||'.$getUrlResult['joburl'].'|'.$getUrlResult['sourcename'].'|web||||||||||'.$getUrlResult['gcpc'].'|'.$getUrlResult['cpc'].'|');
	// 	 $location = 'http://'.$_SERVER['HTTP_HOST'].'/beyond_redirect.php?q='.$encoded_beyond_data;
		
	// 	 	if($isNotBotClick == FALSE)
	// 		{
	// 		 log_clicks_queue_java($getUrlResult,$getUrlResult['joburl'],$type);
	// 		}

	// 	 header( "Location: $location" );
	// 	 // if($_SERVER['REMOTE_ADDR'] == '49.200.119.31') {
	// 	 // echo $location;exit;
	// 	 // }
	// 	die();
	// }
	
	$location =  $getUrlResult['joburl'].$subid;
	
	
	/*if($getUrlResult['source'] == '11'){
		
		 $location = str_replace("&amp;", "&", $location);	
		 $location = str_replace("##siteid##", "sep_cb012&utm_source=oakjobalerts.com&utm_medium=aggregator&utm_campaign=all-jobs", $location);
		
	}*/
		if(isset($_GET['showURL']))
		{ echo $location; exit; }

        if(!empty($location)){

	    	if(strtolower(trim($getUrlResult['sourcename'])) == 'bayard') {
			
				$searchBayardRecord =  exec("grep -c $location /var/nfs-93/bayard.xml");

				if(trim($searchBayardRecord) == 0) {
					$urlRecirect = "http://".DOMAIN."/jobs.php?q=".$getUrlResult['origin_keyword']."&l=".$getUrlResult['origin_zip']."&expired=1";
					header( "Location: ".$urlRecirect );
					die;
					
				}
							
			}
	
	
			
	
	/*	
	    $type = 'web';
	    if(isset($_COOKIE['source']) && !empty($_COOKIE['source'])){
        
		  $type = $_COOKIE['source'];
        
	     }	
	    if(SOURCE != '')
		$type = base64_decode(SOURCE);
      */  
		//sleep(1);
		   //LOGS BOTS IN ANOTHER TABLE
		   
		   
	    // if(stripos(trim(strtolower($_SERVER['HTTP_USER_AGENT'])),trim("bot")) === false)
	   	// {
	  //  		$queueLogsArray = $getUrlResult;
	  //  		$queueLogsArray['provider'] 		=				$type ;
			// $queueLogsArray['source'] 			=				$getUrlResult['sourcename'] ;
			// $queueLogsArray['email'] 			=				($_COOKIE['email']) ? $_COOKIE['email'] : '';
			// $queueLogsArray['jobtitle'] 		=				$getUrlResult['title'] 		;
			// $queueLogsArray['city'] 			=				$getUrlResult['city'] 		;
			// $queueLogsArray['state'] 			=				$getUrlResult['state'] 		;
			// $queueLogsArray['zip'] 				=				$getUrlResult['zipcode'] 	;
   //  		$queueLogsArray['job_detail_url'] 	= 				$location;
   //  		$queueLogsArray['campgian_category_key'] 	= 		'web';
   //  		$logeventsObj = new LogEvents();

	        // $insertURL = "INSERT INTO $LOGS_TABLE_NAME (email,url,redirect_pid, jobtitle, city, state, zip, origin_keyword, origin_city, origin_state, origin_zip,source,user_agent,IP,type,provider,cpc,gcpc)
			// VALUES ('".$_COOKIE['email']."','".$location."','N/A', '".mysql_escape_string($getUrlResult['title'])."', '".$getUrlResult['city']."', '".$getUrlResult['state']."', '".$getUrlResult['zipcode']."', 'NA', 'NA', 'NA', 'NA', '".$getUrlResult['sourcename']."','".$_SERVER['HTTP_USER_AGENT']."','".$_SERVER['REMOTE_ADDR']."','web','".$type."','".$getUrlResult['cpc']."','".$getUrlResult['gcpc']."')";	

		    // $googlebot  = exec('host '.$_SERVER['REMOTE_ADDR']);
		    // if(stripos(trim($googlebot),trim("gaggle.net")) === false)	
		    // {
		    	log_clicks_queue_java($getUrlResult,$location,$type);
		    	// $insertURLQry = mysql_query($insertURL);
		    	// $logeventsObj->logEventstoQueue($queueLogsArray);
		    // }
		    // else{
		    // 	$logeventsObj->logEventstoQueue($queueLogsArray,'bot_click');

		    // }
		    
		     // Insert into tbl_logs_mobius if user came from advertisement
		   if(isset($_COOKIE['aff_query_elem']) && (strpos(strtolower($_COOKIE['source']),'mobius-') !== false)) {

				$aff_query_elem = json_decode(urldecode($_COOKIE['aff_query_elem']),true);
				$insertMobiusLogs = "INSERT INTO tbl_logs_mobius (
					                                affiliate_id,
					                                c1,
					                                c2,
					                                c3,
					                                c4,
					                                query_string
				                                ) VALUES (
					                                '".$aff_query_elem['affid']."',
					                                '".$aff_query_elem['c1']."',
					                                '".$aff_query_elem['c2']."',
					                                '".$aff_query_elem['c3']."',
					                                '".$aff_query_elem['c4']."',
					                                '".urldecode($_COOKIE['aff_query_elem'])."'
				                                )";
				$insertMobiusLogsQry = mysql_query($insertMobiusLogs);

		   }
		    
		    
		// }else{


		// 	$insertURL = "INSERT INTO tbl_bot_click_logs (
	 //                                                url,
	 //                                                email,
	 //                                                jobtitle,
	 //                                                city,
	 //                                                state,
	 //                                                zip,
	 //                                                source,
	 //                                                user_agent,
	 //                                                IP,
	 //                                                type,
	 //                                                provider
	                                                
	 //                                                ) VALUES (
	                                                
	 //                                                '".$location."',
	 //                                                '".$_COOKIE['email']."',
	 //                                                '".mysql_escape_string($getUrlResult['title'])."',
	 //                                                '".$getUrlResult['city']."',
	 //                                                '".$getUrlResult['state']."',
	 //                                                '".$getUrlResult['zipcode']."',
	 //                                                '".$getUrlResult['sourcename']."',
	 //                                                '".$_SERVER['HTTP_USER_AGENT']."',
	 //                                                '".$_SERVER['REMOTE_ADDR']."',
	 //                                                'web',
	 //                                                '".$type."'
		// 											)";
		// 	$insertURLQry = mysql_query($insertURL);
		// }
        
		// $log_qry_vals = "'".$_COOKIE['email']."','".$location."','N/A', '".mysql_escape_string($getUrlResult['title'])."', '".$getUrlResult['city']."', '".$getUrlResult['state']."', '".$getUrlResult['zipcode']."', 'NA', 'NA', 'NA', 'NA', '".$getUrlResult['sourcename']."','".$_SERVER['HTTP_USER_AGENT']."','".$_SERVER['REMOTE_ADDR']."','web','".$type."'";
		// file_put_contents( CLICKLOGS_FILE_PATH . date('Y-m-d')."_logs.txt", $log_qry_vals."\n", FILE_APPEND );

           $location = htmlspecialcharsdecode($location);
            // if( !headers_sent() ) {
            	
            // 	header( "Location: $location" );
            // } else {
            	yourls_redirect_javascript( $location );
            //}
        
            }else{
            
                header( "Location: http://cylcon.com" );
            
            }
	die();
}


function log_clicks_queue_java($getUrlResult,$location,$type){

    // global $getUrlResult;
    // global $location;
    // global $redirect_source;
     global $isNotBotClick;

   			$queueLogsArray = $getUrlResult;
	   		$queueLogsArray['provider'] 		=				$type ;
			$queueLogsArray['source'] 			=				$getUrlResult['sourcename'] ;
			$queueLogsArray['email'] 			=				($_COOKIE['email']) ? $_COOKIE['email'] : '';
			$queueLogsArray['jobtitle'] 		=				$getUrlResult['title'] 		;
			$queueLogsArray['city'] 			=				$getUrlResult['city'] 		;
			$queueLogsArray['state'] 			=				$getUrlResult['state'] 		;
			$queueLogsArray['zip'] 				=				$getUrlResult['zipcode'] 	;
    		$queueLogsArray['job_detail_url'] 	= 				$location;
    		$queueLogsArray['campgian_category_key'] 	= 		'web';
    		
    		if(strpos(strtolower($queueLogsArray['provider']), 'jobexport') !== false 
    			&& $getUrlResult['lp_click_type'] == 'FLP'){
    			$queueLogsArray['type'] 	= 		'jobExport';
    		}else if(isset($getUrlResult['lp_click_type']))
    			$queueLogsArray['type'] 	= 		'newsletter';
    		else
    			$queueLogsArray['type'] 	= 		'web';
			
			$queueLogsArray['isFailure'] 	= 	(isset($_GET['isFailure']) && $_GET['isFailure'] == true) ? TRUE : FALSE;


			// if($_SERVER['REMOTE_ADDR'] == '112.196.1.221'||$_SERVER['REMOTE_ADDR'] == '111.93.206.202') { echo "Hello.."; 

			// echo"<pre>";
			// print_r($queueLogsArray);
			// die();
			// }

			
    		$logeventsObj = new LogEvents();



		$log_qry_vals = "'".$_COOKIE['email']."','".$location."','N/A', '".mysql_escape_string($getUrlResult['title'])."', '".$getUrlResult['city']."', '".$getUrlResult['state']."', '".$getUrlResult['zipcode']."', 'NA', 'NA', 'NA', 'NA', '".$getUrlResult['sourcename']."','".$_SERVER['HTTP_USER_AGENT']."','".$_SERVER['REMOTE_ADDR']."','web','".$type."'";
		
		writesLogs($log_qry_vals);
	
	//LOGS BOTS IN ANOTHER TABLE
    if($isNotBotClick)
   	{

   		$logeventsObj->logEventstoQueue($queueLogsArray);
		writeslowLogs();
   	}
   	else{
   		$logeventsObj->logEventstoQueue($queueLogsArray,'bot_click');
   	}
   	// else{
   	// 	$logeventsObj->logEventstoQueue($queueLogsArray,'bot_click');
   	// 	return;
   	// }

	

}

function writesLogs($string){
     		
    file_put_contents( CLICKLOGS_FILE_PATH . date('Y-m-d')."_logs.txt", $string."\n", FILE_APPEND );	
}

function writeslowLogs(){
     		
    global $startTime;
    global $startlogtime;
			
    $endTime = date("Y-m-d H:i:s");
    $endlogtime	= microtime(true);
    $logsString = 'Start-Time : '.$startTime."|".' End-Time : '.$endTime."|".' IP : '.$_SERVER['REMOTE_ADDR']."| Total : ".number_format($endlogtime-$startlogtime, 4);
    file_put_contents( CLICKLOGS_FILE_PATH . date('Y-m-d')."_time_logs.txt", $logsString."\n", FILE_APPEND );
	
}

function htmlspecialcharsdecode($string,$style=ENT_QUOTES) {
	
	      $translation = array_flip(get_html_translation_table(HTML_SPECIALCHARS,$style));
	      if($style === ENT_QUOTES){ $translation['&#39;'] = '\''; }
	      if($style === ENT_QUOTES){ $translation['&#039;'] = '\''; }
	      return strtr($string,$translation);
}
?>

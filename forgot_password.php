<?php
// error_reporting(E_ALL);
// ini_set('display_errors', 1);
require_once('include/DBManager.php');

//Added for sparkpostmail through smtp
require_once('smtp/class.phpmailer.php');
include("smtp/class.smtp.php");

$objDBManager = new DBManager();//initialize db connection
$objDBManager->createConnection();
$message = '';
$message1 = '';

require '/var/www/vhosts/oakjob/public_html/mailgun/vendor/autoload.php';
use Mailgun\Mailgun;

if(!empty($_POST)) {
    
    
    $whereFields = array();
    $whereFields['email'] =  $_POST['email'];
    
    if(PROVIDER == '')
      $provider = (isset($_COOKIE['source']))       ?       $_COOKIE['source']      :       'web';
      else
      $provider = PROVIDER;
  
    if(PROVIDER != '')
        $whereFields['provider'] =  $provider;
    else
      $whereFields['provider'] =  $provider;

    $valid_user = true;
    $checkUser = $objDBManager->getRecord('tbl_signup_users',$whereFields,false,'');

    if(count($checkUser) == 0) {

        $whereFieldsSeekers['email'] =  $_POST['email'];

        $checkUserInCylcon = $objDBManager->getRecord('tbl_jobserious_cylcon',$whereFieldsSeekers,false,'');
        $checkUserInLempop = $objDBManager->getRecord('tbl_job_seekers_lempop',$whereFieldsSeekers,false,'');
        $checkUserInAlumni = $objDBManager->getRecord('tbl_job_seekers_alumni_2',$whereFieldsSeekers,false,'');
        $checkUserInJobvitals = $objDBManager->getRecord('tbl_jobvital_seeker',$whereFieldsSeekers,false,'');
        $checkUserInEducate = $objDBManager->getRecord('tbl_job_seekers_educatetocareer',$whereFieldsSeekers,false,'');
        $checkUserInCollege = $objDBManager->getRecord('tbl_job_seekers_college',$whereFieldsSeekers,false,'');

        if((count($checkUserInCylcon) > 0) || (count($checkUserInLempop) > 0) || (count($checkUserInAlumni) > 0) || (count($checkUserInJobvitals) > 0) || (count($checkUserInEducate) > 0) || (count($checkUserInCollege) > 0)) {

            $insertFieldArray = array('email' => $_POST['email'], 'provider' => $provider,'unique_id' => $_POST['email'],'login_type' => 'normal','email_alert' => '1');
            $objDBManager->insertRecord('tbl_signup_users',$insertFieldArray);

            $checkUser = $objDBManager->getRecord('tbl_signup_users',$whereFields,false,'');

        } else {

            $valid_user = false;

        }

    }

    if($valid_user) {
        
        $uniquestr = time().generateRandomString();


		$to = $checkUser[0]['email'];

		$subject = 'Password Reset';

		$message1 = "Hi ".$checkUser[0]['firstname']." ".$checkUser[0]['lastname'].",";
		$message1.= "<br/><br/>";
		$message1.= "We received a request for a password change on your ".DOMAINNAME." account. You can reset your password <a href='http://".DOMAIN."/changepassword.php?token=$uniquestr' >here</a>";
		$message1.= "<br/><br/>";
		$message1.= "If you don't want to reset it, simply disregard this email.   If you need more help or believe this email was sent in error, feel free to contact us.";
		$message1.= "<br/><br/>";
		$message1.= "Thank you,";
		$message1.= "<br/>";
		$message1.= "<b>The ".DOMAINNAME." Team</b>";
                $message1.= "<br/><br/><img src='http://".DOMAIN."/".IMAGE_BASE_URL."/logo.png' />";

		$headers = 'From: Oak Job Alerts <alerts@oakjobalerts.com>' . "\r\n";
		$headers .= 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

        if(EMAIL_SENDER == 'SENDGRID') {
			
            $header = array('category'=>'Forgot Email');
            
            $header  = json_encode($header);
            $url  = 'https://sendgrid.com/';
            $user    = EMAIL_SENDER_USER;
            $pass    = EMAIL_SENDER_PSW;
            $params  = array(
                'api_user' => $user,
                'api_key' => $pass,
                //'to'        => 'gaurav@signitysolutions.com',
                'to' => $to,
                //'cc'        =>$mailTesterEmail,
                'x-smtpapi' => $header,
                'subject' => $subject,
                'html' => utf8_encode($message1),
                //'text'      => '',
                'from' => 'alerts@'.DOMAIN,
                //'replyto'   => 'tickets@numberonejobsite.uservoice.com',
                'fromname' => DOMAINNAME
            );
            //  echo $message;echo "<br>\n";
            $request = $url . 'api/mail.send.json';
            //$header = "X-SMTPAPI:".$header."";
            // Generate curl request
            $session = curl_init($request);
            // Tell curl to use HTTP POST
            curl_setopt($session, CURLOPT_POST, true);
            // Tell curl that this is the body of the POST
            curl_setopt($session, CURLOPT_POSTFIELDS, $params);
            // Tell curl not to return headers, but do return the response
            curl_setopt($session, CURLOPT_HEADER, false);
            //curl_setopt($session, CURLOPT_HTTPHEADER, $header);
            //curl_setopt($session, CURLOPT_HEADER, false);
            curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
            // obtain response
            $res = curl_exec($session);
            //  echo 'mail sent to '.$row51['player_email'];echo "<br>\n";
            //print_r(json_decode($res));exit;
            curl_close($session);
            
            
            $response = json_decode($res);
           

        } else if(EMAIL_SENDER == 'MAILGUN') {

            $mgClient = new Mailgun(EMAIL_SENDER_KEY);
            $domain = EMAIL_SENDER_DOMAIN;

            # Make the call to the client.
            $result = $mgClient->sendMessage($domain, array(
                'from'    => DOMAINNAME.'<alerts@'.DOMAIN.'>',
                'to'      => $to,
                'subject' => $subject,
                'html'    => utf8_encode($message1)
            ));

        } else if(EMAIL_SENDER == 'SES') {
                
            include('/var/www/vhosts/oakjob/public_html/ses/sendMail.php');
            
            $to = array($to);
            $from = 'alerts@lempop.com';
            $fromname = DOMAINNAME;

            sendEmail($to,$bcc=array(),$from,$fromname,$subject,utf8_encode($message1));
            
        }else if(EMAIL_SENDER == 'SPARKPOST'){
					$mail             = new PHPMailer();
					$mail->IsSMTP(); // telling the class to use SMTP
					$mail->Host       = "smtp.sparkpostmail.com"; // SMTP server
					$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
															   // 1 = errors and messages
															   // 2 = messages only
					$mail->SMTPAuth   = true;                  // enable SMTP authentication
					$mail->Host       = "smtp.sparkpostmail.com"; // sets the SMTP server
					$mail->Port       = 587;                    // set the SMTP port for the GMAIL server
					
					/////// Email id Credentials
					$mail->Username   = EMAIL_SENDER_USER; // SMTP account username
					$mail->Password   = EMAIL_SENDER_PSW;        // SMTP account password
					$mail->SetFrom('info@e.'.DOMAINNAME, 'moreljobs');
					$mail->AddReplyTo('support@e.'.DOMAINNAME, 'moreljobs');

					$mail->Subject    = $subject;
					$mail->MsgHTML(utf8_encode($message1));

					//// Email address to which email need to sent
					$address = $to;
					$mail->AddAddress($address);
					$mail->Send();							

			   }
        
        
	
        $message = '<div style="color: #7cc243;">Reset password link has been sent to your email address.</div>';
        $where = 'email = "'.$_POST['email'].'"';
        $fieldArray['f_id'] = $uniquestr;
        $objDBManager->updateRecord('tbl_signup_users',$fieldArray,$where,false,'');
                    
                
   
   
   }else{
    
        $message = '<div style="color: red;">This email address does not exists. Please sign up <a href="sign_up.php" style="color:red; text-decoration:underline;">Here</a></div>';
        // $message = '<div style="color: red;">This email address is invaild.</div>';
    
   }
}
function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?=TITLE?></title>

    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <link rel="shortcut icon" href="<?=FAVICONPATH?>favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?=FAVICONPATH?>favicon.ico" type="image/x-icon">
    
    <!-- Custom CSS -->
    <link href="css/oakstyle.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">


<?php
if(DOMAIN == '2ndchancejobalerts.com')
{ 
    ?>
         	<!-- Google Tag Manager -->
			
			<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','GTM-5TGHSS');</script>
			<!-- End Google Tag Manager -->
<?php
}
?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="">
    

<?php
if(DOMAIN == '2ndchancejobalerts.com')
{
	?>
		  <!-- Google Tag Manager -->
            <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5TGHSS"
            height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<?php
}

?>
    <!-- Navigation -->
    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php"><img src="<?=IMAGE_BASE_URL?>/logo1.png"> <?=HEADER_TITLE?></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                   <!-- <li class="page-scroll">
                        <a href="#portfolio">Portfolio</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#about">About</a>
                    </li>-->
                    <li class="page-scroll">
                         <button class="btn btn-signin" type="submit"><a style="color: #fff;text-decoration: none;" href="sign_in.php">Sign in</a></button>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- section -->
    <section>
        <div class="container login">
            <div class="row">
                <div class="col-lg-12">
                    <?php echo $message; ?>
                    <div class="intro-text text-center">
                       <form class="navbar-form" action="" method="post" onsubmit="return formValidation();" role="search">
                        <div class="form-group text-center">
                        <h4>Forgot Password</h4>
                       <p>Enter the email address associated with your <?=TITLE?> account below and we will email you a link to reset your password.</p>
                                                   
                        <input type="text" id="email" name="email" onfocus="$(this).css('border-color','#dce4ec');$(this).attr('placeholder','Your Email Address');" class="form-control input-lg" placeholder="Your Email Address">
                        
                         <button type="submit" class="btn btn-default">Send Email</button>
                         <div class="row">
                         <p><a class="text-left" href="sign_in.php"><i class="fa-chevron-left" style="font-size: 14px;"></i>Go to Sign in</a></p>
                         
                         </div>
                        </div>
                     
                      </form>
                      
                     
                    </div>
                </div>
            </div>
        </div>
    </section>

   

    <?php include("include/footer.php"); ?>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top page-scroll visible-xs visble-sm">
        <a class="btn btn-primary" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>

   <!-- jQuery Version 1.11.0 -->
    <script src="js/jquery-1.11.0.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    

    <!-- Custom Theme JavaScript -->
    <script src="js/oak.js"></script> 
    
    <script type="text/javascript">
    
    function formValidation(){
        
        var email           = $('#email').val();
        if(email == ''){
                
                $('#email').css('border-color','red');
                $('#email').attr('placeholder','Email is mandatory.');
                return false;
                
            }   
                
    }   
    </script>
    
    
    
    

</body>

</html>

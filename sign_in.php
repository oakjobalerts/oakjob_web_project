<?php
//error_reporting(-1); 
//ini_set('display_errors', 'On');
ob_start();
require_once('include/DBManager.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?=TITLE?></title>

    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/oakstyle.css" rel="stylesheet">

    <link rel="shortcut icon" href="<?=FAVICONPATH?>favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?=FAVICONPATH?>favicon.ico" type="image/x-icon">

    <!-- Custom Fonts -->
    <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

          <?php
          if(DOMAIN == '2ndchancejobalerts.com')
          { 
              ?>
                  	<!-- Google Tag Manager -->
			
				<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
				new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
				j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
				'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
				})(window,document,'script','dataLayer','GTM-5TGHSS');</script>
			<!-- End Google Tag Manager -->
          <?php
          }
          ?>

<script>(function(){var uv=document.createElement('script');uv.type='text/javascript';uv.async=true;uv.src='//widget.uservoice.com/RUVxfxpYSZzV74j6P7cENw.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(uv,s)})()</script>
</head>
<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
session_start();
$objDBManager = new DBManager();//initialize db connection
$objDBManager->createConnection();


if(!empty($_POST)) {
  
if(PROVIDER == '')
  $provider_where = "AND (provider = 'web' OR provider = 'jobserious' OR provider = 'topusajobs')";
else
  $provider_where = "AND provider = '".PROVIDER."'";
   
  
  //Array of list.  
  $query ="select * from  tbl_signup_users where email='".$_POST['email']."' ".$provider_where;
  // echo $query;
  // exit;
  $checkUser=$objDBManager->fetchRecord($query);    

   // $whereFields = array();
   // $whereFields['email'] =  $_POST['email'];
   // $whereFields['password'] =  md5($_POST['password']);
   // $whereFields['provider']=$provider;
   
   // $checkUser = $objDBManager->getRecord('tbl_signup_users',$whereFields,true,'');
    
   if(count($checkUser) > 0) {
                
                if(md5($_POST['password']) == $checkUser[0]['password']){


                setcookie('id',$checkUser[0]['id'],time()+31556926);
                setcookie('first_name',$checkUser[0]['firstname'],time()+31556926);
                setcookie('last_name',$checkUser[0]['lastname'],time()+31556926);
                setcookie('email',$checkUser[0]['email'],time()+31556926);
                setcookie('zip_code',$checkUser[0]['zipcode'],time()+31556926);
                setcookie('phone',$checkUser[0]['phonenumber'],time()+31556926);
                setcookie('login_type',$checkUser[0]['login_type'],time()+31556926);
                if(SOURCE == '')
                setcookie('source',$checkUser[0]['provider'],time()+31556926);
                else
                setcookie('source',base64_decode(SOURCE),time()+31556926);
    
                 // echo "<pre>";
                // print_r($_COOKIE['first_name']);
                // echo 'exists'; exit;

                  header('Location:index.php');
          }else
                header('Location:sign_in.php?login=failed');

    
   }else{
    
                header('Location:sign_in.php?login=failed');
   }
    
}



if(isset($_GET['logout'])){

	for($i=0;$i<5;$i++){
		unset($_COOKIE['first_name']);
		unset($_COOKIE['last_name']);
		unset($_COOKIE['email']);
		unset($_COOKIE['zip_code']);
		unset($_COOKIE['phone']);
		unset($_COOKIE['login_type']);
    unset($_COOKIE['source']);
    setcookie('id', '',time()-3600);
		setcookie('first_name', '',time()-3600);
		setcookie('last_name', '',time()-3600);
		setcookie('email', '',time()-3600);
		setcookie('zip_code', '',time()-3600);
		setcookie('phone', '',time()-3600);
		setcookie('login_type', '',time()-3600);
                setcookie('source', '',time()-3600);
	}
	header('Location:sign_in.php');
}

?>
<body id="page-top" class="">

<?php
if(DOMAIN == '2ndchancejobalerts.com')
{
	?>
		  <!-- Google Tag Manager -->
            <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5TGHSS"
            height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<?php
}

?>
    <!-- Navigation -->
    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php"><img src="<?=IMAGE_BASE_URL?>/logo1.png" title="<?= TITLE ?> Logo"> <?=HEADER_TITLE?></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                   <!-- <li class="page-scroll">
                        <a href="#portfolio">Portfolio</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#about">About</a>
                    </li>-->
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- section -->
    <section>
        <div class="container login">
            <div class="row">
                <div class="col-lg-12">
                        <?php if(isset($_GET['login']) && $_GET['login'] == 'notmatched') { ?>
                            <label style="color: red;">This account is not signed up yet.</label>
                        <?php } elseif(isset($_GET['login']) && $_GET['login'] == 'failed') { ?>
                            <label style="color: red;">Wrong password. Please try again.</label>
                        <?php }if(isset($_GET['signup']) && !empty($_GET['signup'])) { 
							
							  if(DOMAIN == 'purejobalerts.com')
							  {
								   require_once('adwords/signup.php');
							  }
							
							?>
                            <label style="color: #7cc243;">You have been signed up successfully.</label>
                        <?php } ?>
                    <div class="intro-text text-center">
                       <form class="navbar-form" method="post" role="search" action="" onsubmit="return formValidation();">
                        <div class="form-group text-center">
                        <h4>Login </h4>
                        
                          <h5 class="">Enter Your Email Id and Password</h5>
                          <input type="text" name="email" id="email" onfocus="$(this).css('border-color','#dce4ec');$(this).attr('placeholder','');" class="form-control input-lg" placeholder="Email">
                          
                        <input type="password" name="password" onfocus="$(this).css('border-color','#dce4ec');$(this).attr('placeholder','');" id="password" class="form-control input-lg" placeholder="Password">
                        
                         <button type="submit" class="btn btn-default">Sign in</button>
                         <div>
                        <p class="col-lg-5 text-left"><a class="text-left" href="forgot_password.php">Forgot Password?</a></p>
                         <p class="col-lg-7 text-right">Don't have an account? <a class="text-left text-warning" href="sign_up.php">Sign Up</a></p>
                         </div></div>
                    
                     
                      </form>
                      
                     
                    </div>
                </div>
            </div>
        </div>
    </section>

   

    <?php include("include/footer.php"); ?>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top page-scroll visible-xs visble-sm">
        <a class="btn btn-primary" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>

   <!-- jQuery Version 1.11.0 -->
    <script src="js/jquery-1.11.0.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    

    <!-- Custom Theme JavaScript -->
    <script src="js/oak.js"></script> 
  <script type="text/javascript">
    
    function formValidation(){
        
            var email           = $('#email').val();
            var password        = $('#password').val();
        if(email == ''){
                
                $('#email').css('border-color','red');
                $('#email').attr('placeholder','Email is mandatory.');
                return false;
                
            }else if(!isValidEmailAddress(email)){
                
                $('#email').css('border-color','red');
                $('#email').attr('placeholder','Email is invalid.');
                return false;
                
            }else if(password == ''){
                $('#password').css('border-color','red');
                $('#password').attr('placeholder','Password is mandatory.');
                return false;
                
            }  
                
                
}
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
};
    function getData(email,unique_value) {
    var params = {"operation" : "operation", "email" : email, "unique_value" : unique_value};
            
    $.ajax({
        type: "POST",
        url: "pfblogin.php",
        dataType:"json",
        data: params,
        cache: false,
        success:function(data) {
        	//alert(data.success);
        	if(data.success == "true") {
        		// window.location.href = "http://www.numberonejobsite.com/staging/search.php";
        		window.location.href = "index.php";
        	}else {
        		// window.location.href = "http://www.numberonejobsite.com/staging/login.php";
        		window.location.href = "sign_in.php?login=failed";
        	}
        },
        error:function() {
                    alert('There is some error. Please try again.');
        }
    });
    }
    function login(){
           
           FB.login(function(response) {
                statusChangeCallback(response);
          }, {scope: 'email'});
        
  }
  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      testAPI();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into Facebook.';
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '609959692445734',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.1' // use version 2.1
  });

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
        
                getData(response.email,response.id);
                 
    });
  }
</script> 
    
    
    
    

</body>

</html>

<?php        
        require_once('include/DBManager.php');
        $objDBManager = new DBManager();
        $user_ip_address = $_GET['ip'] ;
        $whereFields['ip'] = $_GET['ip'];
        
        $zipcode_res = $objDBManager->getRecord('tbl_ip_zip_match', $whereFields, false, '');
        
        $zipCode = '';

        if(count($zipcode_res) > 0)
        {
             $zipCode = $zipcode_res[0]['zipcode'];
        }
        else
        {
             $info_from_ipinfodb = file_get_contents('http://api.ipinfodb.com/v3/ip-city/?key=f82cdd68d64ff37394ea8c0909ae21413aac4098ec1a8e23a22ab0c6d7706721&ip='.$user_ip_address.'&format=json');
             $info_from_ipinfodb = json_decode($info_from_ipinfodb,true);
             
              $zipCode = $info_from_ipinfodb['zipCode'];
             
        }

        $array['zipcode'] = $zipCode;


        echo json_encode($array);

?>
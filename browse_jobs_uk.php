<?php
// error_reporting(E_ALL); ini_set('display_errors', 1);
ob_start();
session_start();
include('include/common.php');
require_once('include/DBManager.php');
require_once('include/configuration.php');
require_once('include/sources_config.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>

     <?php 

      $country = 'USA';
     if(defined('COUNTRY') && COUNTRY !== ''){
      $country = COUNTRY;
     }

    $title = "Browse available jobs"." | ".TITLE;
    $meta_description = "Search & apply for jobs. Apply online and get hired today!";
    if(isset($_GET['browse']) && trim($_GET['browse']) != "") {

      if($_GET['browse'] == 'company') {

        $title = $country." Jobs by Company: ".$_GET['alph']." | ".TITLE; 
          $meta_description = "Browse ".$country." jobs by company name: ".$_GET['alph']; 

      } else if($_GET['browse'] == 'state') {

      $title = "Available jobs in "._urldecode($_GET['state']).", ".$country." | ".TITLE;
          $meta_description = "Search available jobs in "._urldecode($_GET['state']).", ".$country.". Apply online & get hired today!";

      } else if($_GET['browse'] == 'category') {

      $title = _urldecode($_GET['category'])." jobs ".$country." | ".TITLE;
          $meta_description = "Search & apply for "._urldecode($_GET['category'])." jobs.";

      }
        
    } ?>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $meta_description; ?>">
    <meta name="author" content="">

    <title><?= $title ?></title>

    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/oakstyle.css?v=2" rel="stylesheet">
    
    <link rel="shortcut icon" href="<?= FAVICONPATH ?>favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?= FAVICONPATH ?>favicon.ico" type="image/x-icon">

    <!-- Custom Fonts -->
    <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- UserVoice JavaScript SDK (only needed once on a page) -->
<script>(function(){var uv=document.createElement('script');uv.type='text/javascript';uv.async=true;uv.src='//widget.uservoice.com/RUVxfxpYSZzV74j6P7cENw.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(uv,s)})()</script>

<!-- A link to launch the Classic Widget -->

 <!-- jQuery Version 1.11.0 -->
<script src="js/jquery-1.11.0.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script type="text/javascript" src="http://www.indeed.com/ads/apiresults.js"></script>
<script src="js/classie.js"></script>
<script src="js/cbpAnimatedHeader.js"></script>

<!-- Contact Form JavaScript -->
<script src="js/jqBootstrapValidation.js"></script>


<!-- Custom Theme JavaScript -->
<script src="js/oak.js"></script>
<script src="js/bootbox.js"></script>

<style type="text/css">
    .browse_jobs_container { margin-top: 15px; }
    .browse_jobs_container a { text-decoration: underline; color: #00c; }
    .section_heading { font-weight: bold; font-size: 17px; margin-bottom: 5px; }
    .browse_jobs_section { margin-bottom: 15px; }
</style>


<?php
if(DOMAIN == '2ndchancejobalerts.com')
{ 
    ?>
         	<!-- Google Tag Manager -->
			
			<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','GTM-5TGHSS');</script>
			<!-- End Google Tag Manager -->
<?php
}
?>

</head>

<body id="page-top" class="listing">


<?php
if(DOMAIN == '2ndchancejobalerts.com')
{
	?>
		  <!-- Google Tag Manager -->
            <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5TGHSS"
            height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<?php
}


$objDBManager = new DBManager(); //initialize db connection
$objDBManager->createConnection();

// CHECK FOR WIDGET START HERE
if(!widgetEnable()) { ?>

    <!-- Navigation -->
    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo $baseurl; ?>">
                  <img src="<?= IMAGE_BASE_URL ?>/logo1.png" title="<?= TITLE ?> Logo"> <?= HEADER_TITLE ?>
                </a>
            </div>
            <div style="display:none;"><img src="img/loadingAnimation.gif" /></div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
           
                <ul class="nav navbar-nav navbar-right">
                 
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <ul class="mobile">
                    <li><a class="active" href="#">Search Jobs</a></li>
                     <?php if (!isset($_COOKIE['login_type'])) { ?>
                       <li><a href="sign_in.php">Manage Alerts</a></li>
                       <li><a href="sign_in.php">Saved Jobs</a></li>
                     <?php } else { ?>
                        <li><a href="alert_list.php">Manage Alerts</a></li>
                        <li><a href="savedjobs.php">Saved Jobs</a></li>
                        <li><a href="profile.php?id=<?php echo base64_encode($_COOKIE['id']); ?>">Manage Profile</a></li>
                      <?php } ?>
                    <li><a href="https://jobalerts.freshdesk.com/support/tickets/new" alt="Help/FAQ">Help/FAQ</a></li>
                    </ul>

                    <li class="page-scroll">
                        <?php if (!isset($_COOKIE['login_type'])) { ?>
                          <button type="submit" class="btn btn-signin"><a href="sign_in.php" style="color: #fff;text-decoration: none;">Sign in</a></button>
                        <?php } else { ?>
                          <button type="submit" class="btn btn-signin"><a href="sign_in.php?logout=true" style="color: #fff;text-decoration: none;">Sign out</a></button>
                        <?php } ?>
                    </li>
                </ul>
                  
                  
              
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
        
        <div class="secondnav web">
        <ul class="container">
        <li><a class="active" href="index.php">Search Jobs</a></li>
        <?php if (!isset($_COOKIE['login_type'])) { ?>
          <li><a href="sign_in.php">Manage Alerts</a></li>
          <li><a href="sign_in.php">Saved Jobs</a></li>
        <?php } else { ?>
          <li><a href="alert_list.php">Manage Alerts</a></li>
          <li><a href="savedjobs.php">Saved Jobs</a></li>
          <li><a href="profile.php?id=<?php echo base64_encode($_COOKIE['id']); ?>">Manage Profile</a></li>
        <?php } ?>
        <li><a href="https://jobalerts.freshdesk.com/support/tickets/new" alt="Help/FAQ">Help/FAQ</a></li>
        </ul>
        </div>
        
    </nav>

<!--CHECK FOR WIDGET END HERE-->    
<?php } ?>

    <!-- section -->
    <section>
        <div class="container">
         <!-- Small modal -->
        <div style="display:none;">
        <button id="firstTimePopUp" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-sm">Popup</button>
        </div>

        <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
            <h4> We found <strong><?= $totalResults ?> "<?php echo ucwords($_GET['q']); ?>" jobs</strong> within <?= ($_GET['radius']) ? $_GET['radius'] : "10" ?> miles of <?= ($location) ? urldecode($location) : "" ?></h4>
            <p>Where should we email your jobs? </p>
             
                       <form class="navbar-form" role="search" onsubmit="return getNewsLetterMail();" method="post">
                         <div class="form-group">
                           <input type="text" onfocus="$(this).css('border-color','#dce4ec');$(this).attr('placeholder','');" class="form-control input-lg" id="popupEmailAddress" name="EmailAddress" placeholder="Enter your email address">
                           <input type="hidden" class="form-control input-lg" name="Keyword" value="<?php if (isset($_GET['q'])) { echo $_GET['q']; } ?>" >
                           <input type="hidden" class="form-control input-lg" name="Location" value="<?php if (isset($_GET['l'])) { echo $_GET['l']; } ?>" >
                           <input type="hidden" class="form-control input-lg" name="Distance" value="<?php if (isset($_GET['radius'])) { echo $_GET['radius']; } ?>" >

                        </div><div id="createAlertbutton">
                        <button type="submit" class="btn btn-default" style="margin-left: -50px;">Create Alert</button>
                        <a class="close_btn" href="javascript:;" onclick="closePopUp();" style="position: absolute; margin-top: 8px; font-size: 14px;">Close</a>
                      </div>
                      </form>
                <span>By clicking "create alert" I agree to the <?= TITLE ?>. <a href="<?= TERMSANDSERVICE ?>.php" target="_blank">Terms of Use</a></span>
            </div>
          </div>
        </div>
            <div class="row" style="border-bottom: 1px solid #ccc;">
                    <div class="intro-text text-left">
                       <form name="jobsubmit" action="jobs.php" class="navbar-form" role="search" onsubmit="return validateForm();">
                        <div class="form-group">
                          <input type="text" class="form-control input-lg" maxlength=512 size=31 id=what name=q autocomplete=off placeholder="job title, keywords or company name" value="<?php echo $keyword; ?>">
                         </div>
                         <div class="form-group">
                           <input type="text" class="form-control input-lg" maxlength=64 size=27 id=where name=l autocomplete=off placeholder="city, state or zipcode" value="<?php echo urldecode($location); ?>" onkeyup="autocomplet()">
                           <ul id="country_list_id" style="display:none; margin-top:0; width: 22%;"></ul>
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                      <!-- </form> -->
                    </div>
             
            </div>

            <!-- START: Browse Jobs Section -->
            <div class="row text-left browse_jobs_container">
                <?php if(isset($_GET['browse'])) { ?>

                  <!-- IF ANY OPTION SELECTED -->
                  <!-- If Company Alphabet Selected -->
                  <?php if($_GET['browse'] == 'company') {

						echo '<div class="browse_jobs_section company_section col-md-12 row">
						          <p class="section_heading">Jobs By Company</p>
						          <div class="col-md-12">';
						            foreach (range('A', 'Z') as $char) {
										// ROMMY - 20170510 - SEO FRIENDLY URLS //
						                // echo '<a href="?browse=company&alph='.$char.'">'.$char.'</a>&nbsp;&nbsp;';
						                echo '<a href="/company-'.$char.'-jobs.html">'.$char.'</a>&nbsp;&nbsp;';
						            }
						echo '</div></div>';

						$company_alphabet = $_GET['alph'];

						// Get companies starting with selected alphabet
						$companies = $objDBManager->fetchRecord("SELECT * FROM tbl_job_companies WHERE company_name LIKE '".$company_alphabet."%' group by company_name");

						echo '<div class="col-md-12 row">';
						echo "<p class='section_heading'>Companies With Alphabet '".$company_alphabet."'</p>";
						foreach ($companies as $company) {
							// ROMMY - 20170510 - SEO FRIENDLY URLS //
							// echo '<div class="col-md-4"><a href="jobs.php?q='.urlencode($company['company_name']).'&l=">'.$company['company_name'].'</a></div>';
							echo '<div class="col-md-4"><a href="jobs-q-'._urlencode($company['company_name']).'-jobs.html">'.$company['company_name'].'</a></div>';
						}
						echo '</div>';

                    } else if($_GET['browse'] == 'state') {

	                    // $abbr = _urldecode($_GET['abbr']);
	                    $state = _urldecode($_GET['state']);	// ROMMY - 20170510 - SEO FRIENDLY URLS //

	                    // Get companies starting with selected alphabet
                      $cities = $objDBManager->fetchRecord("SELECT DISTINCT primary_city FROM tbl_zipcodes_uk WHERE state = '".$state."'");

	                    echo '<div class="col-md-12 row">';
	                    echo "<p class='section_heading'><strong>Search jobs in top Cities of ".$state."</strong></p>";
	                    if(count($cities) > 0) {
	                    	foreach ($cities as $city) {
								// ROMMY - 20170510 - SEO FRIENDLY URLS //
		                      	// echo '<div class="col-md-3"><a href="jobs.php?q=&l='.urlencode($city['primary_city']).'">Jobs in '.$city['primary_city'].'</a></div>';
		                      	echo '<div class="col-md-3"><a href="jobs-l-'._urlencode($city['primary_city']).'-jobs.html">Jobs in '.$city['primary_city'].'</a></div>';
		                    }	
	                    } else {
							// ROMMY - 20170510 - SEO FRIENDLY URLS //
	                    	// echo '<div class="col-md-3"><a href="jobs.php?q=&l='.urlencode($state).'">Jobs in '.$state.'</a></div>';
	                    	echo '<div class="col-md-3"><a href="jobs-l-'._urlencode($state).'-jobs.html">Jobs in '.$state.'</a></div>';
	                    }
	                    
	                    echo '</div>';

                    } else if($_GET['browse'] == 'region') {

                      $world_region = _urldecode($_GET['world_region']);
 
                       echo '<div class="col-md-12 row">';
                      echo "<p class='section_heading'><strong>Jobs By ".$world_region." State</strong></p>";
                     


                      $query = "select state from tbl_zipcodes_uk where world_region = '".$world_region."' group by 1";
     
                        // Get States
                        
                        $states = $objDBManager->fetchRecord($query);


                        // $states = $objDBManager->getRecord('state', $whereFields, false, '');

                        foreach ($states as $state) {
							// ROMMY - 20170510 - SEO FRIENDLY URLS //
							// echo '<div class="col-md-3"><a href="?browse=state&abbr='.$state['state'].'&state='.$state['state'].'">'.$state['state'].'</a></div>';

                          //'-'._urlencode($state['state']). removed by parveen from below url 20170511
							echo '<div class="col-md-3"><a href="region-state-'._urlencode($state['state']).'-jobs.html">'.$state['state'].'</a></div>';
                        }

                      echo '</div>';

                    } else if($_GET['browse'] == 'category') {

	                    $category = _urldecode($_GET['category']);	// ROMMY - 20170510 - SEO FRIENDLY URLS //

	                    // Get companies starting with selected alphabet
	                    $sub_categories = $objDBManager->fetchRecord("SELECT * FROM tbl_job_categories WHERE job_category = '".$category."'");
	                    
	                    echo '<div class="col-md-12 row">';
	                    echo "<p class='section_heading'><strong>".$category." Jobs</strong></p>";
	                    foreach ($sub_categories as $sub_category) {
	                    	$sub_category = ($sub_category['job_subcategory'] == "") ? $sub_category['job_category'] : $sub_category['job_subcategory'];
							// ROMMY - 20170510 - SEO FRIENDLY URLS //
	                      	// echo '<div class="col-md-3"><a href="jobs.php?q='.urlencode($sub_category).'&l=">'.$sub_category.'</a></div>';
	                      	echo '<div class="col-md-3"><a href="jobs-q-'._urlencode($sub_category).'-jobs.html">'.$sub_category.'</a></div>';
	                    }
	                    echo '</div>';

                    } ?>

                <?php } else { ?>

                  <!-- IF NO OPTION SELECTED -->

                    <!-- Jobs By Company -->
                    <div class="browse_jobs_section company_section col-md-12 row">
                        <p class="section_heading">Jobs By Company</p>
                        <?php
                        echo "<div class='col-md-12'>";
                        foreach (range('A', 'Z') as $char) {
							// ROMMY - 20170510 - SEO FRIENDLY URLS //
                            // echo '<a href="?browse=company&alph='.$char.'">'.$char.'</a>&nbsp;&nbsp;';
                            echo '<a href="/company-'.$char.'-jobs.html">'.$char.'</a>&nbsp;&nbsp;';
                        }
                        echo "</div>"; ?>
                    </div>

                    <!-- Jobs By State -->
                    <div class="browse_jobs_section state_section col-md-12 row">
                        <p class="section_heading">Jobs By Region</p>
                        <?php
                        // Get States
                        $whereFields = array();
            			$states = $objDBManager->fetchRecord("select distinct world_region from tbl_zipcodes_uk");

                        // $states = $objDBManager->getRecord('state', $whereFields, false, '');

                       
                        foreach ($states as $state) {
							// ROMMY - 20170510 - SEO FRIENDLY URLS //
							// echo '<div class="col-md-3"><a href="?browse=region&world_region='.$state['world_region'].'">'.$state['world_region'].'</a></div>';
							echo '<div class="col-md-3"><a href="/region-'._urlencode($state['world_region']).'-jobs.html">'.$state['world_region'].'</a></div>';
                        }

                         ?>
                    </div>

                    <!-- Jobs By Category -->
                    <div class="browse_jobs_section category_section">
                        <p class="section_heading">Jobs By Category</p>
                        <?php
                        // Get Category
                        $whereFields = array();
                        $categories = $objDBManager->getRecord('tbl_job_categories', $whereFields, false, 'GROUP BY job_category');

                        foreach ($categories as $category) {
							// ROMMY - 20170510 - SEO FRIENDLY URLS //
							// echo '<div class="col-md-3"><a href="?browse=category&category='.$category['job_category'].'">'.$category['job_category'].'</a></div>';
							echo '<div class="col-md-3"><a href="/category-'._urlencode($category['job_category']).'-jobs.html">'.$category['job_category'].'</a></div>';
                        }

                         ?>
                    </div>

                     <div class="browse_jobs_section state_section col-md-12 row">
                        <p class="section_heading" style="padding-top: 20px;margin-bottom: -25px;"><a href ='browse_jobs.php' target="_blank">Browse Jobs in USA</a></p>
                     </div>

                <?php } ?>

            </div>
            <!-- END: Browse Jobs Section -->
                
        </div>
    </section>

   

    <?php
include("include/footer.php");
?>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top page-scroll visible-xs visble-sm">
        <a class="btn btn-primary" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>
   

    <script type="text/javascript">
 
        function validateForm(){
             if($('#where').val() == ''){
                $('#what').css('border-color','#ccc');
                $('#what').css('border-bottom-color','#aaa');
                $('#what').css('border-right-color','#aaa');
                $('#where').css('border-color','red');
                return false;
            }
        }

    </script>

</body>

</html>

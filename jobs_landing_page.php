<?php
//error_reporting(E_ALL); ini_set('display_errors', 1);
ob_start();
session_start();

require_once('include/configuration.php');
require_once('include/common.php');
include('include-landing/functionsbox.php'); 

/* ~ ~ ~ ~ ~ ~ A D S E N S E   C O D E ~ ~ ~ ~ ~ ~ */
$userAgent='web';
$http_user_agent_value=$_SERVER['HTTP_USER_AGENT'];
if ((strpos($http_user_agent_value, 'Mobile') !== false) || (strpos($http_user_agent_value, 'Nexus') !== false)) {
	$userAgent="mobile";
}
/* ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ */

/* ~ ~ ~ ~ ~ ~ C A L C U L A T I O N S ~ ~ ~ ~ ~ ~ */
// Initiate Memcache
$memcache_obj_elastic = memcache_connect("oakelasticcache.25s6pq.0001.use1.cache.amazonaws.com", 11211);
if(isset($_GET['sk']) && !empty($_GET['sk'])) {

	if((!empty($_SESSION[$_GET['sk']]) && isset($_SESSION[$_GET['sk']])) || (!empty(memcache_get($memcache_obj_elastic, $_GET['sk'])))) {
		
		if(isset($_SESSION[$_GET['sk']])) {
			// GET Data from Session & Set in Cookie for next 1 hr& unset Session Key
			$jsnData = $_SESSION[$_GET['sk']]; 
			// SET Cookie for 01 hrs.
			memcache_set($memcache_obj_elastic,$_GET['sk'] , $jsnData, false, time()+3600);
			//setcookie($_GET['sk'], $jsnData, time()+3600);
			unset($_SESSION[$_GET['sk']]);
		}	else	{
			// GET Data from Cookie
			//$jsnData = $_COOKIE[$_GET['sk']]; 
			$jsnData = memcache_get($memcache_obj_elastic, $_GET['sk']);
		}
		$getData = json_decode($jsnData); 
		$getData = (array)$getData;


		$jobtitle = $getData['jobtitle'];
		$city = $getData['city'];
		$state = $getData['state'];
		$loction = $getData['zip'];
		
		if(!empty($city) && !empty($state)){
			$loction = $city.', '.$state;
		} else if(!empty($city)){
			$loction = $city;	
		}else if(!empty($state)){
			$loction = $state;	
		}

		if(!empty($jobtitle) && !empty($loction)){
			$title = "Search & Apply "._urldecode($jobtitle)." jobs in "._urldecode($loction)." | ".$_SERVER['HTTP_HOST']; 
			$meta_description = "Search & apply for ".$jobtitle." jobs in ".$loction.". Apply online and get hired today!";
		}else{
			$title = "Search & Apply  jobs"." | ".$_SERVER['HTTP_HOST']; 
			$meta_description = "Search & apply for jobs. Apply online and get hired today!";
		}

		/*if($_SERVER['REMOTE_ADDR'] == '112.196.1.221' || $_SERVER['REMOTE_ADDR'] == '111.93.206.202') {
			
			echo "<pre>"; print_r($getData); echo "</pre>";
		}*/
		if($getData['landing_page_type'] == '1') {
			$sidebarAdUnit = '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
				<ins class="adsbygoogle"
				style="display:inline-block;width:300px;height:600px"
				data-ad-client="ca-pub-1699099086165943"
				data-ad-slot="9351320961"></ins>
				<script type="text/javascript">(adsbygoogle = window.adsbygoogle || []).push({});</script>';
			$topAdUnit = '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
				 <ins class="adsbygoogle"
				 style="display:inline-block;width:100%;height:90px"
				 data-ad-client="ca-pub-1699099086165943"
				 data-ad-slot="9303219508"></ins>
				 <script type="text/javascript">(adsbygoogle = window.adsbygoogle || []).push({});</script>';
		} else if($getData['landing_page_type'] == '2') {
			$sidebarAdUnit = '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
				 <ins class="adsbygoogle"
				 style="display:inline-block;width:300px;height:600px"
				 data-ad-client="ca-pub-1699099086165943"
				 data-ad-slot="7067047756"></ins>
				 <script type="text/javascript">(adsbygoogle = window.adsbygoogle || []).push({});</script>';
			$topAdUnit = '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
				 <ins class="adsbygoogle"
				 style="display:inline-block;width:728px;height:90px"
				 data-ad-client="ca-pub-1699099086165943"
				 data-ad-slot="3619112557"></ins>
				 <script type="text/javascript">(adsbygoogle = window.adsbygoogle || []).push({});</script>';
		} else if($getData['landing_page_type'] == '3') {
			$sidebarAdUnit = '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
				 <ins class="adsbygoogle"
				 style="display:inline-block;width:300px;height:600px"
				 data-ad-client="ca-pub-1699099086165943"
				 data-ad-slot="8349351595"></ins>
				 <script type="text/javascript">(adsbygoogle = window.adsbygoogle || []).push({});</script>';
			$topAdUnit = '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
				 <ins class="adsbygoogle"
				 style="display:inline-block;width:728px;height:90px"
				 data-ad-client="ca-pub-1699099086165943"
				 data-ad-slot="8931805521"></ins>
				 <script type="text/javascript">(adsbygoogle = window.adsbygoogle || []).push({});</script>';
		} else {
			// ELSE Case is SAME as 1st Case 
			$sidebarAdUnit = '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
				<ins class="adsbygoogle"
				style="display:inline-block;width:300px;height:600px"
				data-ad-client="ca-pub-1699099086165943"
				data-ad-slot="9351320961"></ins>
				<script type="text/javascript">(adsbygoogle = window.adsbygoogle || []).push({});</script>';
			$topAdUnit = '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
				 <ins class="adsbygoogle"
				 style="display:inline-block;width:100%;height:90px"
				 data-ad-client="ca-pub-1699099086165943"
				 data-ad-slot="9303219508"></ins>
				 <script type="text/javascript">(adsbygoogle = window.adsbygoogle || []).push({});</script>';
		}
		
		// Create object of cylconLanding class
		$obj 	= new cylconLanding();
		// LOG data in DB & redirect to job url
		if((isset($_GET['isSFLP'])) && ($_GET['isSFLP'] == '1'))  {
			$obj->log_clicks_queue_java($getData);
			header('Location: '.$getData['job_detail_url']);
			die;
		}
		$cloudURLKeys 					= array();
		$cloudURLKeys['cloudurl']		= 'http://s2.oakjobalerts.com:8080/JavaStaggingApi/?';
		if(isset($getData['search_keyword'])) {
			$cloudURLKeys['keyword']	= str_replace(" ","+", 'keyword='.$getData['search_keyword']);
			$sKeyword					= $getData['search_keyword'];
		} else {

			//Changed with jobtitle instead of origin_keyword;

			// $cloudURLKeys['keyword']	= str_replace(" ","+", 'keyword='.$getData['origin_keyword']);
			// $sKeyword					= $getData['origin_keyword'];

			$cloudURLKeys['keyword']	= str_replace(" ","+", 'keyword='.$getData['jobtitle']);
			$sKeyword					= $getData['jobtitle'];


		}
		$cloudURLKeys['start']			= '&start=0';
		$cloudURLKeys['offset']			= '&offset=20';
		$cloudURLKeys['sort']			= '&sort=';
		$cloudURLKeys['radius']			= '&radius=30';
		if(isset($getData['search_zipcode'])) {
			$cloudURLKeys['zipcode']	= '&zipcode='.$getData['search_zipcode'];
			$sZipcode					= $getData['search_zipcode'];
		} else {
			$cloudURLKeys['zipcode']	= '&zipcode='.$getData['origin_zip'];
			$sZipcode					= $getData['origin_zip'];
		}
		$cloudURLKeys['domain']			= '&domain='.DOMAIN;
		$cloudURLKeys['botFeeds']		= '&botFeeds='.$getData['bot_feeds'];
		$cloudURLKeys['ipaddress']		= '&ipaddress='.$_SERVER['REMOTE_ADDR'];
		
		/*if($_SERVER['REMOTE_ADDR'] == '112.196.1.221' || $_SERVER['REMOTE_ADDR'] == '111.93.206.202') {
			echo "<pre>"; print_r($getData); echo "</pre>"; die;
		}*/
		
		// Create Object & call Methods
		if(isset($getData['job_invisible']) && ($getData['job_invisible'] == 'yes')) {
			$featuredJobHTML 	= '';
		}	else 	{
			$featuredJobHTML    = '<div class="featured_jobs">';
			$featuredJobHTML   .= $obj->get_featured_job($getData);
			$featuredJobHTML   .= '</div>';
		}
		$getJobs			= $obj->get_suggested_jobs($cloudURLKeys,0,$getData['email'], $getData['landing_page_type'],$getData['failure_case'],$getData['provider']); 
		/*if($_SERVER['REMOTE_ADDR'] == '112.196.1.221' || $_SERVER['REMOTE_ADDR'] == '111.93.206.202') {
			
			echo "<pre>"; print_r($getJobs); echo "</pre>";
		}*/
		$suggestedJobsHTML 	= $getJobs['renderData'];
		$totalSuggestedJobs = $getJobs['totalJobs'];

		// MEDIA.Net Ad Code
		$getMediaNetAdCode	= $obj->getMediaDotNetAd($getData['origin_keyword'], $getData['origin_zip'], $getData['landing_page_type']);
		$topAdUnit			= $getMediaNetAdCode;
		$mediaNetFlag		= true;
		$medianetclass			= 'medianet';
		
		
		// Set & Generate Default Values
		$offset 			= 20;
		$currentPageNum 	= 0;
		if($totalSuggestedJobs <= $offset) {
			$totalPages 	= 1;
			$nextPageNum 	= 0;
		} else {
			$totalPages 	= ceil($totalSuggestedJobs/$offset);
			$nextPageNum 	= $currentPageNum + 1;
		}
		
	// Set AJAX hit URL
		$ajaxURL 				= implode('###',$cloudURLKeys);
		$suggestedJobsAjaxUrl	= $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/include-landing/get_more_suggested_jobs.php?uri='.urlencode($ajaxURL);
	}  else  {
		header('Location: '.$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST']);
	}
} else {
	header('Location: '.$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST']);
}

/* ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=<?php echo $meta_description; ?>>
    <meta name="author" content="">
    <meta name="google-site-verification" content="IL9bKPkej9goiu6Cm0LN1dpoA3nmeBEgHtwdMF-Kkok" />

    <title><?php echo $title; ?></title>

    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/oakstyle_beyond_landingpage.css?v=2" rel="stylesheet">
    
    <!-- Favicon Path -->
    <link rel="shortcut icon" href="<?php echo IMAGE_BASE_URL; ?>/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo IMAGE_BASE_URL; ?>/favicon.ico" type="image/x-icon">

    <!-- Custom Fonts -->
    <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- UserVoice JavaScript SDK (only needed once on a page) -->
	<script>(function(){var uv=document.createElement('script');uv.type='text/javascript';uv.async=true;uv.src='//widget.uservoice.com/RUVxfxpYSZzV74j6P7cENw.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(uv,s)})()</script>
	
	<!-- Advertisement -->
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
        (adsbygoogle = window.adsbygoogle || []).push({});
        (adsbygoogle = window.adsbygoogle || []).push({});
        (adsbygoogle = window.adsbygoogle || []).push({});
    
    </script>

    <!--pushnnami script for senza by pawan-->
<?php
if($_SERVER['HTTP_HOST'] == 'senzajobs.com')
{
?>
<script src="https://api.pushnami.com/scripts/v1/push/59c3e8b9e3f49f552259a6d5"></script>
<link rel="manifest" href="manifest.json">
<?php
}
?>
<?php
if($_SERVER['HTTP_HOST'] == 'senzadaily.com')
{
?>
<script src="https://api.pushnami.com/scripts/v1/push/59c3eaa38089573fd96a484f"></script>
<link rel="manifest" href="/manifest.json">
<?php
}
?>
<?php
if($_SERVER['HTTP_HOST'] == 'senzajobalerts.com')
{
?>
<script src="https://api.pushnami.com/scripts/v1/push/59c3eacefe835554ee9553dc"></script>
<link rel="manifest" href="/manifest.json">
<?php
}
?>
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<!-- A link to launch the Classic Widget -->

	<!-- impelemting arbor pixel-->
	<!-- jQuery Version 1.11.0 -->
	<script src="js/jquery-1.11.0.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.min.js"></script>

	<!-- Plugin JavaScript -->
	<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
	<script type="text/javascript" src="http://www.indeed.com/ads/apiresults.js"></script>
	<script src="js/classie.js"></script>
	<script src="js/cbpAnimatedHeader.js"></script>

	<!-- Contact Form JavaScript -->
	<script src="js/jqBootstrapValidation.js"></script>

	<!-- Custom Theme JavaScript -->
	<script src="js/oak.js"></script>
	<script src="js/bootbox.js"></script>
	<style type="text/css">
		.hide-loader{display:none;}
	</style>
</head>
<body>
	<div class="headerbox">
		<?php include('include-landing/headerbox.php'); ?>
	</div>
	<div class="contentbox">
		<section>
			<div style="padding-bottom:10px;" class="container">
				<div class="panel-v2">
					<div class="row">
					   <div class="col-lg-9 text-left">
							<?php echo $featuredJobHTML; ?>
							<?php if($mediaNetFlag == false ) { ?>
								<div class="webadds text-center"> 
									<!-- landing_page_top_ad -->
									<?php echo $topAdUnit; ?>
								</div>
							<?php } ?>
							  <div class="suggested_jobs_search">
								  <div class="dailyalert <?php echo $medianetclass; ?>">
									<form name="jobsubmit" class="navbar-form1" method="post" role="search" action="jobsnew_search.php">
										<div class="content">
										<div class="row">
											<div class="col-lg-2 col-xs-12 hidden-xs col-sm-2 p0">
												<h4>Search Jobs</h4>
											</div>
											<div class="col-lg-4 col-xs-6 col-sm-4 pr5">
												<div class="form-group">
													<input type="text" class="form-control input-lg" maxlength="512" size="31" id="what" name="q" autocomplete="off" value="<?php echo $sKeyword; ?>" /> 
												</div>
											</div>
											<div class="col-lg-4 col-xs-6 col-sm-4 pl5">
												 <div class="form-group">
													<!--<label for="where">Where</label>-->
													<input type="text" class="form-control input-lg" maxlength="64" size="27" required=true id="where" name="l" autocomplete="off" value="<?php echo $sZipcode; ?>" />
													<ul id="country_list_id" style="display:none; margin-top:0; width: 22%;"></ul>
												 </div>
											 </div>
											<div class="col-lg-2 col-xs-12 col-sm-2">
												<div class="submit text-center">
													<input type="hidden" value="<?php echo urlencode(json_encode($getData)); ?>" id="sd" name="sd" />
													<input type="hidden" name="sk" id="sk" value="<?php echo $_GET['sk']; ?>" />
													<input style="margin-top:15px;" type="submit" value="Submit" class="btn btn-default" />
													
												</div>
											 </div>
										</div>
										</div>
									</form>
								  </div>
							  </div>
							<?php if($mediaNetFlag == true ) { ?>
								<div class="webadds text-center"> 
									<!-- landing_page_top_ad -->
									<?php echo $topAdUnit; ?>
								</div>
							<?php } ?>
							  <div class="similar_jobs">
								  <div class="suggested_jobs_search_subtitle">
									<span class="text-left"><?php echo $totalSuggestedJobs; ?> Similar Jobs</span>
									<div class="hidden_data">
										<input type="hidden" id="origin_user_email_id" value="<?php echo $getData['email']; ?>" />
										  <input type="hidden" value="<?php echo $totalPages; ?>" id="suggested_pages_total" />
										  <input type="hidden" value="<?php echo $currentPageNum; ?>" id="suggested_page_current" />
										  <input type="hidden" value="<?php echo $nextPageNum; ?>" id="suggested_page_next" />
										  <input type="hidden" value="<?php echo $totalSuggestedJobs; ?>" id="suggested_jobs_total" />
										  <input type="hidden" value="<?php echo $suggestedJobsAjaxUrl; ?>" id="suggested_jobs_ajax_url" />
										  <input type="hidden" value="<?php echo $getData['landing_page_type']; ?>" id="landing_page_type" />
										  <input type="hidden" value="<?php echo $getData['failure_case']; ?>" id="failure_case" />
										  <input type="hidden" value="<?php echo $getData['provider']; ?>" id="provider" />
									</div>
								  </div>
								  <div class="suggested_jobs_list" id="suggested_jobs_list_content">
									  <?php echo $suggestedJobsHTML; ?>
								  </div>
								</div>
								<div id="ajax-loader" class="text-center hide-loader">
										<img src="/include-landing/img/ajax-loader.gif" />
								</div>
						  </div> 
						  <!-- list 3 end-->
					   <div class="add-banner col-lg-3 hidden-xs">
						<!-- landing_page_side_bar -->
						<?php echo $sidebarAdUnit; ?>
					  </div>
					</div>
				</div>
			</div>
		</section>
	</div>
	<div class="footerbox">
		<?php include("include/footer.php"); ?>
	</div>
	<script src="/include-landing/js/landing-custom.js"></script>
</body>
</html>

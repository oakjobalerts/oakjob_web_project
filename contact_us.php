<?php
    require_once('include/configuration.php');

?>
<!DOCTYPE html>
<html>
<head>


 
 
	<title>Contact Us | <?=TITLE?></title>
<!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/oakstyle.css" rel="stylesheet">

	<script type="text/javascript" src="http://assets.freshdesk.com/widget/freshwidget.js"></script><style type="text/css" media="screen, projection">
@import url(http://assets.freshdesk.com/widget/freshwidget.css);
</style>
    <link rel="shortcut icon" href="<?=FAVICONPATH?>favicon.ico" type="image/x-icon">
    
    
   <!-- A link to launch the Classic Widget -->
<style>
    p
    {
        font-size: 17px !important;
    }
	
	.mid-form{ padding:0px 0px 40px 0px; }
    
</style> 
    
    
    
    
    
    
    

</head>




<body id="page-top" class="index">

<?php
    include('include/common.php');
?>


<!-- Navigation -->
    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo $baseurl; ?>"><img src="<?=IMAGE_BASE_URL?>/logo1.png"><?=HEADER_TITLE?></a>
               
                
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                
                
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                   <!-- <li class="page-scroll">
                        <a href="#portfolio">Portfolio</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#about">About</a>
                    </li>-->
                    <li class="page-scroll">
                       
                    </li>
                </ul>

                
                
                
            </div>
            <!-- /.navbar-collapse -->
        </div>
        
        
        <!-- /.container-fluid -->
    </nav>

<div class="container">
<div class="row">
<div class="col-md-12 text-center">


<div class="mid-form">

 <h3 style="text-align:center; margin-bottom: !important;">Contact us</h3>

 <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- contact_us_feedback_form_top -->
<ins class="adsbygoogle"
     style="display:inline-block;width:728px;height:90px"
     data-ad-client="ca-pub-1699099086165943"
     data-ad-slot="6049074975"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
 
<iframe title="Feedback Form" class="freshwidget-embedded-form" id="freshwidget-embedded-form" src="https://jobalerts.freshdesk.com/widgets/feedback_widget/new?&widgetType=embedded&screenshot=no&attachFile=no&searchArea=no&captcha=yes" scrolling="no" height="500px" width="100%" frameborder="0" >
</iframe>

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- contact_us_feedback_form_bottom -->
<ins class="adsbygoogle"
     style="display:inline-block;width:728px;height:90px"
     data-ad-client="ca-pub-1699099086165943"
     data-ad-slot="1218408367"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>


</div>
</div>
</div>
</div>

 <?php include("include/footer.php"); ?>
 
 
     <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top page-scroll visible-xs visble-sm">
        <a class="btn btn-primary" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>
    
    

</body>
</html>
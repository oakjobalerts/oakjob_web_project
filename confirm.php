<!DOCTYPE html>
<html lang="en">
<?php

error_reporting(2);
        $flag = false;
        include('include/common.php');
  
        require_once('include/DBManager.php');
        $objDBManager = new DBManager();//initialize db connection
        $objDBManager->createConnection();

    if(isset($_GET['id']) && !empty($_GET['id'])){
	      
        $rec    = "SELECT * FROM tbl_signup_users WHERE md5(email) = '".$_GET['id']."'";
        $data = $objDBManager->fetchRecord($rec);
               
        if(empty($data[0]['email'])) {
             $flag = false;   
        }
        else
        {
            $insert_jobAlerts=array();
            
            $confirm_data['confirmed']           =      1;
            
            $objDBManager->updateRecord('tbl_signup_users',$confirm_data," md5(email) = '".$_GET['id']."'");
            $flag = true;
        }
    }
        ?>        
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?=TITLE?></title>

    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/oakstyle.css" rel="stylesheet">

    <link rel="shortcut icon" href="<?=FAVICONPATH?>favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?=FAVICONPATH?>favicon.ico" type="image/x-icon">

    <!-- Custom Fonts -->
    <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">

    <!-- Navigation -->
    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php"><img src="<?=IMAGE_BASE_URL?>/logo1.png"><?=HEADER_TITLE?></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                   <!-- <li class="page-scroll">
                        <a href="#portfolio">Portfolio</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#about">About</a>
                    </li>-->
                    <li class="page-scroll">
                        <a href="#contact">Sign in</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- section -->
    <section style="text-align:left;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                <?php if($flag == true) { ?>
                    <!--img class="img-responsive" src="img/tick.png" alt="" width="100" height="100"-->
            
                    <div class="intro-text">
                        <p style="padding-top:0px;font-size:16px;">
                        Hi,
                        <br/>
                        <br/>
                        Thanks for confirming your account.  You account is now fully active and you are on your way to find a new job.  Click here to <a href="index.php" >learn more</a>.
                        <br/>
                        <br/>
                        Thank you,<br/>
                        The <?=DOMAINNAME?> Team
                        </p>
                        
                        <img class="img-responsive" src="<?=IMAGE_BASE_URL?>/logo.png" style="margin:0px;">
                    </div>

                    <?php } else 
                    { ?>
                    <div class="intro-text">
                    
                        <h3 class="text-center">Your email id does not match the records.</h3>
                    
                        <br/><br/><br/><br/>
                    </div>                       
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>

   

    <?php include("include/footer.php"); ?>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top page-scroll visible-xs visble-sm">
        <a class="btn btn-primary" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>

   <!-- jQuery Version 1.11.0 -->
    <script src="js/jquery-1.11.0.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    

    <!-- Custom Theme JavaScript -->
    <script src="js/oak.js"></script> 
    
    
    
    
    
    

</body>

</html>

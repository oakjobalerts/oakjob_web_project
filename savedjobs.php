<?php
ob_start();
 require_once('include/DBManager.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?=TITLE?></title>

    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <link rel="shortcut icon" href="<?=FAVICONPATH?>favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?=FAVICONPATH?>favicon.ico" type="image/x-icon">
    <!-- Custom CSS -->
    <link href="css/oakstyle.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

        <?php
        if(DOMAIN == '2ndchancejobalerts.com')
        { 
            ?>
                	<!-- Google Tag Manager -->
			
				<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
				new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
				j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
				'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
				})(window,document,'script','dataLayer','GTM-5TGHSS');</script>
				<!-- End Google Tag Manager -->
        <?php
        }
        ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- arbor pixel implementation-->

    <?php
     if(isset($_COOKIE['login_type'])) {
         $pixel = '17028';
         $md5_email = md5($_COOKIE['email']);
         $sha1_email = hash('sha1', $_COOKIE['email']);
         $sha256_email =  hash('sha256', $_COOKIE['email']);
         ?>
            
            <script src="//pippio.com/api/sync?pid=<?=ARBOR_PIXEL?>&it=4&iv=<?php echo  $md5_email; ?>&it=4&iv=<?php echo  $sha1_email; ?>&it=4&iv=<?php echo  $sha256_email; ?>" async></script>
            
         <?php
     }
    ?>

</head>

<body id="page-top" class="listing">

<?php
if(DOMAIN == '2ndchancejobalerts.com')
{
	?>
		  <!-- Google Tag Manager -->
            <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5TGHSS"
            height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<?php
}

?>	
<?php
//require_once('include/DBManager.php');

  $dbMgr = new DBManager();
  
  //
  $msg='';
  if(isset($_GET['jid']) && $_GET['jid']!=''){
    
      $dbMgr->deleteRecord('tbl_saved_jobs','id='.$_GET['jid']);
      $msg='Saved Job Deleted';

  }

   if(PROVIDER == '')
      $provider = (isset($_COOKIE['source']))       ?       $_COOKIE['source']      :       'web';
      else
      $provider = PROVIDER;
   if(PROVIDER != '')
        $appendWhere =  ' AND provider ="'.$provider.'"';
   else
      $appendWhere =  ' AND provider ="'.$provider.'"';
      
  //Array of list.  
  $query ='select * from  tbl_saved_jobs where email_id="'.$_COOKIE['email'].'"'.$appendWhere;
  $SaveLiist=$dbMgr->fetchRecord($query);
    
 ?>

    <!-- Navigation -->
    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php"><img src="<?=IMAGE_BASE_URL?>/logo1.png" title="<?= TITLE ?> Logo"> <?=HEADER_TITLE?></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
           
                <ul class="nav navbar-nav navbar-right">
                 
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <ul class="mobile">
                    <li><a  href="index.php">Search Jobs</a></li>
                    <?php if(!isset($_COOKIE['login_type'])) {?>
                        <li><a href="sign_in.php">Manage Alerts</a></li>
                        <li><a  class="active" href="sign_in.php">Saved Jobs</a></li>
                        <?php } else { ?>
                        <li><a  href="alert_list.php">Manage Alerts</a></li>
                        <li><a class="active" href="savedjobs.php">Saved Jobs</a></li>
                        <li><a href="profile.php?id=<?php echo base64_encode($_COOKIE['id']);?>">Manage Profile</a></li>
                        <?php } ?>
                    <li><a href="https://jobalerts.freshdesk.com/support/tickets/new" alt="Help/FAQ">Help/FAQ</a></li>
                    </ul>
                   <!-- <li class="page-scroll">
                        <a href="#portfolio">Portfolio</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#about">About</a>
                    </li>-->
                    <li class="page-scroll">
                        <a href="sign_in.php?logout=true" style="color: #fff;text-decoration: none;"><button type="submit" class="btn btn-signup">Sign out</button></a>
                    </li>
                </ul>
                  
                  
              
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
        
        <div class="secondnav web">
        <ul class="container">
        <li><a href="index.php">Search Jobs</a></li>
        <?php if(!isset($_COOKIE['login_type'])) {?>
                        <li><a  href="sign_in.php">Manage Alerts</a></li>
                        <li><a class="active" href="sign_in.php">Saved Jobs</a></li>
                        <?php } else { ?>
                        <li><a href="alert_list.php">Manage Alerts</a></li>
                        <li><a class="active" href="savedjobs.php">Saved Jobs</a></li>
                        <li><a href="profile.php?id=<?php echo base64_encode($_COOKIE['id']);?>">Manage Profile</a></li>
                        <?php } ?>
        <li><a href="https://jobalerts.freshdesk.com/support/tickets/new" alt="Help/FAQ">Help/FAQ</a></li>
        </ul>
        </div>
        
    </nav>
    

    <!-- section -->
    <section>
        <div class="container">

            <h4 class="heading text-left" style="width:88.5%">List of Saved Jobs</h4>
            <?php if(count($SaveLiist) == 0) { ?>
            
                <div class="joblists clearfix"> <div class="row"><div class="col-lg-9 col-md-9 text-left" style="text-align: center; width: 100% !important;"><b>No saved jobs found.</b></div> </div></div>
            <?php } ?>
             <?php foreach($SaveLiist as $list) { ?>
                   <div class="joblists clearfix">
                  
                   <div class="row">

                     <div class="col-lg-9 col-md-9 text-left">
                     <a href="<?php echo $list['joburl'] ?>"><?php echo $list['title'] ?></a>
                     <p><b><?php echo $list['employer'].'-'.$list['location'] ;?></b></p>
                      <p><?php echo $truncated = (strlen( strip_tags($list['description'])) > 200) ? substr(strip_tags($list['description']), 0, 200) . '...' : utf8_decode(strip_tags(html_entity_decode($list['description']))); ?><p>
                      <p><?php  preg_match_all('!\d+!', $list['postingdate'], $matches); 
                      if (strpos($list['postingdate'],'days')==true) {
                         $date1= date('Y-m-d', strtotime('-'.$matches[0][0]. 'day',strtotime($list['timestamp'])));
                         $date2= date('Y-m-d', strtotime($list['timestamp']));
                         $datetime1 = new DateTime($date1);
                          $datetime2 = new DateTime($date2);
                          $interval = $datetime1->diff($datetime2);
                          echo $interval->format('%d days');
                          echo $days.' ago';
                        }else echo 'Now';
                        ?>
                        <p>
                   </div>
                   
                   
                   <div class="col-lg-3 col-md-3 text-right">
                    <a href="javascript:;" onclick="abc('<?=$_GET['id']?>','<?=$list['id']?>')"><i class="fa fa-trash-o"></i></a>
                   </div>

                  
                  <!-- list 1 end-->
                  
                 
                  
                   <!-- list 2 end-->
                   
                   
                    
                  
                   <!-- list 3 end-->
                   
                  
                  
               </div> 
                 
            </div>
            <?php } ?>
        </div>
    </section>



   

   <?php include("include/footer.php"); ?>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top page-scroll visible-xs visble-sm">
        <a class="btn btn-primary" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>
       

    <!-- jQuery Version 1.11.0 -->
    <script src="js/jquery-1.11.0.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    

    <!-- Custom Theme JavaScript -->
    
    <script src="js/oak.js"></script>
    <script src="js/bootbox.js"></script>
    <script>
        
        function abc(id,jid) {
        var resultDelete = false;
        bootbox.confirm('Do you want to delete this job?', function(result) {
            if(result){
                window.top.location = 'savedjobs.php?id='+id+'&jid='+jid;
            }
        });
        return resultDelete;
        }
        
    </script>
</body>

</html>

<?php
ob_start();
require('logs_other_clicks.php');
require_once('include/DBManager.php');

//Added for sparkpostmail through smtp
require_once('smtp/class.phpmailer.php');
include("smtp/class.smtp.php");
// if($_SERVER['REMOTE_ADDR'] == '112.196.1.221' || $_SERVER['REMOTE_ADDR'] == '111.93.206.202'){
// error_reporting(E_ALL);
// ini_set('display_errors', 1);
// }
require '/var/www/vhosts/oakjob/public_html/mailgun/vendor/autoload.php';
use Mailgun\Mailgun;
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title><?=TITLE?></title>

<!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
<link href="css/bootstrap.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="css/oakstyle.css" rel="stylesheet">

<link rel="shortcut icon" href="<?=FAVICONPATH?>favicon.ico" type="image/x-icon">
<link rel="icon" href="<?=FAVICONPATH?>favicon.ico" type="image/x-icon">

<!-- Custom Fonts -->
<link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
<meta name="google-signin-clientid" content="984802659276-pg7bs72pgq5hjm7pgemnrfv5ttg03qns.apps.googleusercontent.com" />
<meta name="google-signin-scope" content="https://www.googleapis.com/auth/plus.login" />
<meta name="google-signin-requestvisibleactions" content="http://schema.org/AddAction" />
<meta name="google-signin-cookiepolicy" content="single_host_origin" />
<!-- jQuery Version 1.11.0 --> 
<script src="js/jquery-1.11.0.js"></script> 

<!-- Bootstrap Core JavaScript --> 
<script src="js/bootstrap.min.js"></script> 

<!-- Plugin JavaScript --> 
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script> 
<script src="js/classie.js"></script> 
<script src="js/cbpAnimatedHeader.js"></script> 

<!-- Contact Form JavaScript --> 
<script src="js/jqBootstrapValidation.js"></script> 

<!-- Custom Theme JavaScript --> 
<script src="js/oak.js"></script>

<script>(function(){var uv=document.createElement('script');uv.type='text/javascript';uv.async=true;uv.src='//widget.uservoice.com/RUVxfxpYSZzV74j6P7cENw.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(uv,s)})()</script>


<?php
if(DOMAIN == '2ndchancejobalerts.com')
{ 
    ?>
        	<!-- Google Tag Manager -->
			
			<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','GTM-5TGHSS');</script>
			<!-- End Google Tag Manager -->
<?php
}
?>

</head>

<body id="page-top" class="listing">

<?php
if(DOMAIN == 'purejobalerts.com')
{
    ?>
 <!-- Google Code for Add to Cart Conversion Page
    In your html page, add the snippet and call goog_report_conversion
    when someone clicks on the chosen link or button. -->
    <script type="text/javascript">
     /* <![CDATA[ */
      goog_snippet_vars = function() {
      var w = window;
          w.google_conversion_id = 868380543;
      w.google_conversion_label = "peY5COORuGwQ_96JngM";
      w.google_conversion_language = "en";
      w.google_conversion_format = "3";
      w.google_conversion_color = "ffffff";
      // w.google_conversion_value = 13.00;
      // w.google_conversion_currency = "USD";
      w.google_remarketing_only = false;
    }
    // DO NOT CHANGE THE CODE BELOW.
    goog_report_conversion = function(url) {
      goog_snippet_vars();
      window.google_conversion_format = "3";
      var opt = new Object();
      opt.onload_callback = function() {
      if (typeof(url) != 'undefined') {
        // window.location = url;
      }
    }
    var conv_handler = window['google_trackConversion'];
    if (typeof(conv_handler) == 'function') {
      conv_handler(opt);
      }
        }
    /* ]]> */
    </script>
    <script type="text/javascript"
    src="//www.googleadservices.com/pagead/conversion_async.js">
    </script>
    <?php
        }
    ?>
<?php
if(DOMAIN == '2ndchancejobalerts.com')
{
	?>
		  <!-- Google Tag Manager -->
            <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5TGHSS"
            height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<?php
}

?>	
	
<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

$objDBManager = new DBManager();//initialize db connection
$objDBManager->createConnection();

$firstname      = '';
$lastname       = '';
$email        = '';
$zip_code       = '';
$phone        = '';
$other_fields     = '';
$education_level  = '';
$education_year   = '';
$citizenship        = '';
$check_age      = '';
$password       = '';
$alert_check    = '';
$unique_value     = '';
$login_type     = '';
$provider                       ='';

if(isset($_GET['source']) && !empty($_GET['source'])){
        
     $insert_clickAlerts=array();
     $event=    'Sign Up';
     insertOtherClick($event,$_GET['source']);
     
        setcookie('source',base64_decode($_GET['source']),time()+31556926);
        
}
if(PROVIDER == '')
  $provider = (isset($_COOKIE['source'])) ? $_COOKIE['source'] : "web";
else
  $provider = PROVIDER;


if(!empty($_POST)) {

  $firstname    = $_POST['firstname'];
  $lastname     = $_POST['lastname'];
    $zip_code             = $_POST['zipcode'];
    $email      = $_POST['email'];
  $phone      = $_POST['phonenumber'];
  $password     = md5($_POST['password']);
  $alert_check  = 'Y';
  $unique_value   = $_POST['unique_value'] == '' ? $_POST['email'] : $_POST['unique_value'];
  $login_type = $_POST['unique_value'] == '' ? 'normal' : $_POST['login_type'];

  if(isset($_POST['emailalert'])){
    $alert_check = ($_POST['emailalert'])      ?   '1'   :   '0';
  }
  
       if(PROVIDER == '')
          $provider_where = "AND (provider = 'web' OR provider = 'jobserious' OR provider = 'topusajobs')";
        else
          $provider_where = "AND provider = '".PROVIDER."'";
           
          
          //Array of list.  
          $query ="select * from  tbl_signup_users where email='".$_POST['email']."' ".$provider_where;
          // echo $query;
          // exit;
          $checkUser=$objDBManager->fetchRecord($query);    

        // $whereFields = array();
        // $whereFields['unique_id'] =  $unique_value;
        // $whereFields['login_type'] =  $login_type;
        
        // if(PROVIDER != '')
        // $whereFields['provider'] =  PROVIDER;

        // $checkUser = $objDBManager->getRecord('tbl_signup_users',$whereFields,false,'');
  if(count($checkUser) == 0) {
    

                
    $fieldArray = array();
    $fieldArray['firstname'] = addslashes($firstname);
    $fieldArray['lastname'] = addslashes($lastname);
    $fieldArray['email']      = addslashes($email);
    $fieldArray['zipcode'] = addslashes($zip_code);
    $fieldArray['phonenumber']    = addslashes($phone);
    $fieldArray['password']    = $password;
    $fieldArray['unique_id']  = $unique_value;
    $fieldArray['email_alert']  = $alert_check;
    $fieldArray['login_type'] = $login_type;
    $fieldArray['provider'] = $provider;
    $objDBManager->insertRecord('tbl_signup_users',$fieldArray,false);


    
    /*setcookie('first_name',$firstname,time()+31556926);
    setcookie('last_name',$lastname,time()+31556926);
    setcookie('email',$email,time()+31556926);
    setcookie('zip_code',$zip_code,time()+31556926);
    setcookie('phone',$phone,time()+31556926);
    setcookie('session_id',$sessionID,time()+31556926);
    setcookie('login_type',$login_type,time()+31556926);*/


        
                if(empty($firstname) && !isset($firstname))
                {
                  $firstname = "User";
                }
                
                    $message = "
            
                Dear " . $firstname . ",
                <br/><br/>
                Thanks for signing up with ".DOMAINNAME."! Your account is nearly ready. To confirm your account, click the link below. 
                <br/>
                <br/>
                <a href='[LINK]' >[LINK]</a>
                <br/>
                <br/>
                Thank you,
                <br/><br/>
                The ".DOMAINNAME." Team
                <br/>
                <br/><br/>
                <img src='http://".DOMAIN."/".IMAGE_BASE_URL."/logo.png' />
                <br/><br/><br/><br/>
            <img border='0' src='".$tracker."' width='1' height='1' />
                ";
              $key = $email;  
              if(DOMAIN == "jobs.jobseeking101.com") { $pixels_domain = "oakjobalerts.com"; } else { $pixels_domain = DOMAIN; } 
                 $message .= '<img border="0" hspace="0" vspace="0" width="1" height="1" src="http://images.'.$pixels_domain.'/'.LIVERAMP_REG.'.gif?s='.sha1(trim(strtolower($key))).'&n=1"/>
            <img border="0" hspace="0" vspace="0" width="1" height="1" src="http://images.'.$pixels_domain.'/'.LIVERAMP_REG.'.gif?s='.sha1(trim(strtolower($key))).'&n=2"/>
            <img border="0" hspace="0" vspace="0" width="1" height="1" src="http://images.'.$pixels_domain.'/'.LIVERAMP_REG.'.gif?s='.sha1(trim(strtolower($key))).'&n=3"/>
            <img border="0" hspace="0" vspace="0" width="1" height="1" src="http://images.'.$pixels_domain.'/'.LIVERAMP_REG.'.gif?s='.sha1(trim(strtolower($key))).'&n=4"/>
            <img border="0" hspace="0" vspace="0" width="1" height="1" src="http://images.'.$pixels_domain.'/'.LIVERAMP_REG.'.gif?s='.sha1(trim(strtolower($key))).'&n=5"/>
            <img src="http://images2.'.$pixels_domain.'/seg?add='.RETARGETER_EMAIL.'&t=2" width="1" height="1" />
            <img src="http://images3.'.$pixels_domain.'/e/v1/pixel/networks/1/partners/'.MILENIAL_REG.'/users/'.sha1(trim(strtolower($key))).'?jtxe='.sha1(trim(strtolower($key))).'" width="1" height="1" border="0"/>

            <img src="http://images4.'.$pixels_domain.'/ucm/UCMController?dtm_com=2&dtm_cid='.CONVERSANT.'&dtm_cmagic='.CONVERSANT_CMAGIC.'&dtm_fid=101&dtm_format=6&cli_promo_id=99&dtm_email_hash='.md5(trim(strtolower($key))).'&dtmc_drop_id='.time().'" width="1" height="1" border="0">
            <img src="http://images5.'.$pixels_domain.'/cookieredir/'.CONVERSANT.'/pub1.php?a'.md5(trim(strtolower($key))).'=1" width="1" height="1" border="0">
            <img src="http://images5.'.$pixels_domain.'/cookieredir/'.CONVERSANT.'/pub2.php?a'.md5(trim(strtolower($key))).'=1" width="1" height="1" border="0">
            <img src="http://images5.'.$pixels_domain.'/cookieredir/'.CONVERSANT.'/pub3.php?a'.md5(trim(strtolower($key))).'=1" width="1" height="1" border="0">
            <img src="http://images5.'.$pixels_domain.'/cookieredir/'.CONVERSANT.'/pub4.php?a'.md5(trim(strtolower($key))).'=1" width="1" height="1" border="0">
            <img src="http://images5.'.$pixels_domain.'/cookieredir/'.CONVERSANT.'/pub5.php?a'.md5(trim(strtolower($key))).'=1" width="1" height="1" border="0">
            <img src="http://images5.'.$pixels_domain.'/cookieredir/'.CONVERSANT.'/pub6.php?a'.md5(trim(strtolower($key))).'=1" width="1" height="1" border="0">
            <img src="http://images5.'.$pixels_domain.'/cookieredir/'.CONVERSANT.'/pub7.php?a'.md5(trim(strtolower($key))).'=1" width="1" height="1" border="0">
            <img src="http://images5.'.$pixels_domain.'/cookieredir/'.CONVERSANT.'/pub8.php?a'.md5(trim(strtolower($key))).'=1" width="1" height="1" border="0">
            <img src="http://images5.'.$pixels_domain.'/cookieredir/'.CONVERSANT.'/pub9.php?a'.md5(trim(strtolower($key))).'=1" width="1" height="1" border="0">
            <img src="http://images5.'.$pixels_domain.'/cookieredir/'.CONVERSANT.'/pub10.php?a'.md5(trim(strtolower($key))).'=1" width="1" height="1" border="0">';

                    $message = str_replace("[LINK]", "http://" . DOMAINNAME . "/confirm.php?id=" . md5($email), $message);
                    
                    $mailTesterEmail = "oakalerts$emailIncreement@mail-tester.com";
                    
           
             

                if(EMAIL_SENDER == 'SENDGRID') {
                 
                    //$header = array('to'=>array('gaurav@signitysolutions.com','jason@jasonstevens.ca','amit@signitysolutions.com','david@firebrickgroup.com'),'category'=>'OAK JOB ALERTS');  
                    $sendgridCategory = 'Registration Confirmation';
                    
                    $header = array(
                        'category' => $sendgridCategory
                    );
                    
                    $header  = json_encode($header);
                    $url     = 'https://api.sendgrid.com/';
                    $user    = EMAIL_SENDER_USER;
                    $pass    = EMAIL_SENDER_PSW;
                    $params  = array(
                        'api_user' => $user,
                        'api_key' => $pass,
                        //'to'        => 'gaurav@signitysolutions.com',
                        'to' => $email,
                        //'cc'        =>$mailTesterEmail,
                        'x-smtpapi' => $header,
                        'subject' => 'Registration Confirmation',
                        'html' => utf8_encode($message),
                        //'text'      => '',
                        'from' => 'alerts@'.DOMAIN,
                        //'replyto'   => 'tickets@numberonejobsite.uservoice.com',
                        'fromname' => DOMAINNAME
                    );
                    //  echo $message;echo "<br>\n";
                    $request = $url . 'api/mail.send.json';
                    //$header = "X-SMTPAPI:".$header."";
                    // Generate curl request
                    $session = curl_init($request);
                    // Tell curl to use HTTP POST
                    curl_setopt($session, CURLOPT_POST, true);
                    // Tell curl that this is the body of the POST
                    curl_setopt($session, CURLOPT_POSTFIELDS, $params);
                    // Tell curl not to return headers, but do return the response
                    curl_setopt($session, CURLOPT_HEADER, false);
                    //curl_setopt($session, CURLOPT_HTTPHEADER, $header);
                    //curl_setopt($session, CURLOPT_HEADER, false);
                    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                    // obtain response
                    $res = curl_exec($session);
                    //  echo 'mail sent to '.$row51['player_email'];echo "<br>\n";
                    //print_r(json_decode($res));exit;
                    curl_close($session);
                    
                    
                    $response = json_decode($res);

              //        if($_SERVER['REMOTE_ADDR'] == '112.196.1.221' || $_SERVER['REMOTE_ADDR'] == '111.93.206.202') {
              // echo "in mail sent EMAIL_SENDER=".$response;
              //    die();
              
              //   }

                    header("Location: sign_in.php?signup=success");
                } else if(EMAIL_SENDER == 'MAILGUN') {


            
                    try{
                    $mgClient = new Mailgun(EMAIL_SENDER_KEY);
                    $domain = EMAIL_SENDER_DOMAIN;

                    # Make the call to the client.
                    $result = $mgClient->sendMessage($domain, array(
                        'from'    => DOMAINNAME.'<alerts@'.DOMAIN.'>',
                        'to'      => $email,
                        'subject' => 'Registration Confirmation',
                        'html'    => utf8_encode($message)
                    ));
                }catch (Exception $e) {
                    // echo 'Caught exception: ',  $e->getMessage(), "\n";
                }
                    
                    header("Location: sign_in.php?signup=success");
                } else if(EMAIL_SENDER == 'SES') {
                    include('/var/www/vhosts/oakjob/public_html/ses/sendMail.php');
                    
                    $to = array($email);
                    $from = 'alerts@lempop.com';
                    $fromname = DOMAINNAME;
                    $subject = 'Registration Confirmation';

                    sendEmail($to,$bcc=array(),$from,$fromname,$subject,utf8_encode($message),$client_region_east);
                    header("Location: sign_in.php?signup=success");
               }else if(EMAIL_SENDER == 'SPARKPOST'){
					$mail             = new PHPMailer();
					$mail->IsSMTP(); // telling the class to use SMTP
					$mail->Host       = "smtp.sparkpostmail.com"; // SMTP server
					$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
															   // 1 = errors and messages
															   // 2 = messages only
					$mail->SMTPAuth   = true;                  // enable SMTP authentication
					$mail->Host       = "smtp.sparkpostmail.com"; // sets the SMTP server
					$mail->Port       = 587;                    // set the SMTP port for the GMAIL server
					
					/////// Email id Credentials
					$mail->Username   = EMAIL_SENDER_USER; // SMTP account username
					$mail->Password   = EMAIL_SENDER_PSW;        // SMTP account password
					$mail->SetFrom('info@e.'.DOMAINNAME, 'moreljobs');
					$mail->AddReplyTo('support@e.'.DOMAINNAME, 'moreljobs');

					$mail->Subject    = "Registration Confirmation";
					$mail->MsgHTML(utf8_encode($message));

					//// Email address to which email need to sent
					$address = $email;
					$mail->AddAddress($address);
					$mail->Send();
					header("Location: sign_in.php?signup=success");				

			   }
        
        
          header('Location:sign_in.php?signup=success');
  } else {
   
    header('Location:sign_up.php?already=exist');
  }
        
}



?>
<!-- Navigation -->
<nav class="navbar navbar-default">
  <div class="container"> 
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header page-scroll">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand" href="index.php"><img src="<?=IMAGE_BASE_URL?>/logo1.png" title="<?= TITLE ?> Logo"> <?=HEADER_TITLE?></a> </div>
    
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li class="hidden"> <a href="#page-top"></a> </li>
        
        <!-- <li class="page-scroll">
                        <a href="#portfolio">Portfolio</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#about">About</a>
                    </li>-->
      </ul>
    </div>
    <!-- /.navbar-collapse --> 
  </div>
  <!-- /.container-fluid --> 
  
</nav>

<!-- section -->
<section>
  <div class="container text-left">
    <h4 class="heading">Create Your Account</h4>
   <?php if(isset($_GET['already']) && !empty($_GET['already'])) {?>
    <label class="has-error" style="color: red;">This email address is already registered.</label>
    <?php } ?>
    <div class="row">
      <div class="col-lg-8 sign_left">
        <form role="form" action="" method="post" onsubmit="return formValidation();">
          <div class="row">
            <div class="col-xs-6">
              <label>First Name</label>
              <input type="text" name="firstname" id="firstname" onfocus="$(this).css('border-color','#dce4ec');$(this).attr('placeholder','');" value="<?php echo $firstname;?>" class="form-control input-lg" placeholder="">
            </div>
            <div class="col-xs-6">
              <label>Last Name</label>
              <input type="text" name="lastname" id="lastname" onfocus="$(this).css('border-color','#dce4ec');$(this).attr('placeholder','');"  value="<?php echo $lastname;?>" class="form-control input-lg" placeholder="">
            </div>
          </div>
          <div class="row">
            <div class="col-xs-6">
              <label>Email Address</label>
              <input type="text" id="email" required name="email" onfocus="$(this).css('border-color','#dce4ec');$(this).attr('placeholder','');"  value="<?php echo $email;?>" class="form-control input-lg" placeholder="">
            </div>
            <div class="col-xs-6">
              <label>Zip Code</label>
              <input type="text" name="zipcode" id="zipcode" onfocus="$(this).css('border-color','#dce4ec');$(this).attr('placeholder','');"  class="form-control input-lg" placeholder="">
            </div>
          </div>
          
          <div class="row">
            <div class="col-xs-6">
              <label>Mobile Number</label>
              <input type="text" name="phonenumber" id="phonenumber" onfocus="$(this).css('border-color','#dce4ec');$(this).attr('placeholder','');"  value="<?=$phone?>" class="error form-control input-lg" placeholder="">
            </div>
          </div>
          <div class="row">
            <div class="col-xs-6">
              <label>Password</label>
              <input type="password" name="password" required id="password" onfocus="$(this).css('border-color','#dce4ec');$(this).attr('placeholder','');"  class="form-control input-lg" placeholder="">
            </div>
          </div>
          <div class="row">
            <div class="col-xs-6">
              <label>Confirm password</label>
              <input type="password" name="repassword" required id="repassword" onfocus="$(this).css('border-color','#dce4ec');$(this).attr('placeholder','');"  class="form-control input-lg" placeholder="">
            </div>
            </div>
            <div class="checkbox">
                <label>
                  <input type="checkbox"  name="emailalert" checked="checked" value="yes">I would like to receive job alerts from <?=DOMAINNAME?>
                </label>
                
                <label>
                  <input type="checkbox" id="terms" checked="checked" name="terms" value="yes"> <span id="termstxt">I agree to the <a href="<?php echo TERMSANDSERVICE; ?>.php">Terms of Service</a> and <a href="<?php echo PRIVACY; ?>.php">Privacy Policy</a> of this site </span>
                </label>
                
       </div>
            <input type="hidden" class="form-control" name="unique_value" id="unique_value" value="<?php echo $unique_value; ?>"/>
           <input type="hidden" class="form-control" name="login_type" id="login_type" value="<?php echo $login_type; ?>"/>
        
          <button type="submit" class="btn btn-default">Register</button>
        </form>
      </div>
      <div class="col-lg-4">
        <h5 style="visibility: hidden;">Or Register with</h5>
         <div class="sign_right">
        <div class="social_icon text-center"> 
        <p>Already registered?<a href="sign_in.php"> Click here </a>to login</p>
        </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php include("include/footer.php"); ?>

<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
<div class="scroll-top page-scroll visible-xs visble-sm"> <a class="btn btn-primary" href="#page-top"> <i class="fa fa-chevron-up"></i> </a> </div>


<script>
 $( document ).ready(function() {
    $("#terms").click(function() {
    // this function will get executed every time the #home element is clicked (or tab-spacebar changed)
    if($(this).is(":checked")) // "this" refers to the element that fired the event
    {
        $('#termstxt').css('color','#333');
    }
});
});       

        
        
        function formValidation(){

                <?php
                if(DOMAIN == 'purejobalerts.com')
                {
                    ?>
                goog_report_conversion('Signup Button Clicked');
                 <?php
                }
                            ?>

            var firstname       = $('#firstname').val();
            var lastname        = $('#lastname').val();
            var email           = $('#email').val();
            var zipcode         = $('#zipcode').val();
            var phonenumber     = $('#phonenumber').val();
            var password        = $('#password').val();
            var repassword      = $('#repassword').val();
            var emailalert      =$('#emailalert').val();
            
         
            //alert($(this).prop("checked"));
            if(firstname == ''){
                $('#firstname').css('border-color','red');
                $('#firstname').attr('placeholder','Firstname is mandatory.');
                return false;
                
            }else if(lastname == ''){
                
                $('#lastname').css('border-color','red');
                $('#lastname').attr('placeholder','Lastname is mandatory.');
                return false;
                
            } else if(email == ''){
                
                $('#email').css('border-color','red');
                $('#email').attr('placeholder','Email is mandatory.');
                return false;
                
            }else if(!isValidEmailAddress(email)){
                
                $('#email').css('border-color','red');
                $('#email').attr('placeholder','Email is invalid.');
                return false;
                
            } else if(zipcode == ''){
                $('#zipcode').css('border-color','red');
                $('#zipcode').attr('placeholder','Zipcode is mandatory.');
                return false;
                
            } else if(phonenumber == ''){
                $('#phonenumber').css('border-color','red');
                $('#phonenumber').attr('placeholder','Phonenumber is mandatory.');
                return false;
                
            } else if(password == ''){
                $('#password').css('border-color','red');
                $('#password').attr('placeholder','Password is mandatory.');
                return false;
                
            } else if(repassword != password){
                $('#repassword').css('border-color','red');
                $('#repassword').val('');
                $('#repassword').attr('placeholder','Password and repeat password not matched.');
                return false;
                
            }  else if(!$('#terms').is(':checked')){
                
                $('#termstxt').css('color','red');
                return false;
            }  
                
                
}
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
};
  function login(){
           
           FB.login(function(response) {
                statusChangeCallback(response);
          }, {scope: 'email'});
        
  }
  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      testAPI();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into Facebook.';
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '609959692445734',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.1' // use version 2.1
  });

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
                
              $('#firstname').val(response.first_name);
              $('#lastname').val(response.last_name);
              $('#email').val(response.email);
              $('#unique_value').val(response.id);
              $('#login_type').val('facebook');
              
    });
  }
</script>

</body>
</html>

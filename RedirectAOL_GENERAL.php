<?php
// error_reporting(E_ALL);
// ini_set('display_errors', 1);

require_once('include/configuration.php');
require_once('include/DBManager.php');
require_once('include/landing_page_conditions.php');

// $objDBManager = new DBManager(); //initialize db connection
// $objDBManager->createConnection();

$logeventsObj = new LogEvents();
$isNotBotClick = TRUE;

	$catcha_secret_key = "6LcYWQcTAAAAADxYGTrYArU95_ks9-GKau5tBrzq";

	if(defined('CAPTCHA_SECRET_KEY') && !empty(CAPTCHA_SECRET_KEY)){
		$catcha_secret_key=CAPTCHA_SECRET_KEY;
	}

/**
 * Redirect to another page using Javascript. Set optional (bool)$dontwait to false to force manual redirection (make sure a message has been read by user)
 *
 */

//MEMCACHE OBJ
//$memcache_obj = memcache_connect("localhost", 11211);
$memcache_obj_elastic = memcache_connect("oakelasticcache.25s6pq.0001.use1.cache.amazonaws.com", 11211);

session_start();

//memcache_set($memcache_obj, $user_ip, array(), false, 0);

if(isset($_GET['captchaSubmit'])){

	$captcha_val = $_GET['g-recaptcha-response'];	
	if(!$captcha_val){
	  echo '<h2>Please check the the captcha form.</h2>';
	  exit;
	}
	$response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$catcha_secret_key."&response=".$captcha_val."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
	if($response['success'] == false)
	{
	  $captcha="incorrect";
	} 
	else 
	{
	  $captcha="correct";
	}
	
}

// Start: Captcha condition for bots
// if(!memcache_get($memcache_obj_elastic, $user_ip)) {
// 	$check_bot = mysql_query("SELECT ip FROM bot_ips WHERE ip = '".$_SERVER['REMOTE_ADDR']."'");
// 	if(mysql_num_rows($check_bot) > 0) {
// 		if(isset($captcha) && $captcha == "correct") { 
// 			memcache_set($memcache_obj_elastic, trim($user_ip), array('1',time()+86400,'9999'), false, time()+86400); 
// 		} else { 
// 			include("include/captcha.php"); exit; 
// 		}
// 	}
// }
// End: Captcha condition for bots


if(isset($_GET['q']) && !empty($_GET['q'])){
    
    $redirect_vals_encoded =  str_replace(' ', '+', $_GET['q']);

    //echo "Encoded val: ".$redirect_vals_encoded;

   $redirect_vals =  gzuncompress(base64_decode($redirect_vals_encoded));
    
   //echo "<br><br>Decoded val: ".$redirect_vals;

   $records  = explode("|#|",$redirect_vals);
   $records[7] = LogEvents::decodestring($records[7]); 
   $redirect_vals = implode('|#|', $records);  
     
   $regex = '$\b(https?|ftp|file)://[-A-Z0-9+&@/%?=~_!:,.; ]*[-A-Z0-9+&@#/%=~_]$i';

   if (preg_match_all($regex, $redirect_vals, $result, PREG_PATTERN_ORDER)){
		
	$match = $result[0];	
	$url =  $match[0];	
   }
	
     
    $getUrlResult = array();
    
    $keys = array('email',
		  'jobtitle',
		  'city',
		  'state',
		  'zip',
		  'origin_keyword',
		  'origin_zip',
		  'job_detail_url',
		  'source',
		  'provider',
		  'date',
		  'job_position',
		  'group_id',
          'template_id',
          'match_type',
          'search_type',
          'user_id',
          'radius',
          'user_name',
          'gcpc',
          'cpc',
          'campgian_category_key',
          'reg_date',
		  'job_id',
		  'job_country',
		  'channel');
    
    foreach($records as $key=>$value){
	
		$getUrlResult[$keys[$key]] = trim($value);	
     }

     		
 	if(strstr(strtolower($getUrlResult['job_detail_url']), "&sl=signity") == true)
   		{

   		$getUrlResult['job_detail_url'] = str_replace("&sl=signity", "", $getUrlResult['job_detail_url']);

   		$sl = $getUrlResult['job_detail_url'];

   		$flag = true;
   
   		try {
   		
			$mongoDBClassObject = new MongoDBClass();
			
			if($getUrlResult['date'] < '2017-09-08 00:00'){
				//OLD MONGO
				$collection =  $mongoDBClassObject->connectWithMongoDB("172.31.61.41");
			}else{
				//NEW MONGO
				$collection =  $mongoDBClassObject->connectWithMongoDB("172.31.50.38");
			}
			
			if(!empty($collection)){
				$id = array('_id' => $sl);
				$cursor = $collection->find($id);
				$longurl = '';
				foreach ($cursor as $doc) { 
					$longurl = $doc['long_url'];
				}
			}

			$flag = false;

			$mongoDBClassObject->closeMongoConnection();

   		} catch (Exception $e) {
   			
   		}
		
		if(empty($longurl) || $flag){

			$data = date('Y-m-d H:i:s')."==>".$sl."==>".$_GET['q'];

			file_put_contents("/var/www/vhosts/oakjob/public_html/oakconsole/sl_failure_logs.txt", '===='.$data."\n", FILE_APPEND );

			$urlRecirect = "http://".$_SERVER['HTTP_HOST']."/jobs.php?q=".$getUrlResult['origin_keyword']."&l=".$getUrlResult['origin_zip'];
			header( "Location: ".$urlRecirect );
			die;

		}			
		else{		
			$getUrlResult['job_detail_url'] = $longurl;
		}

   }
   
   $url = explode("|#|",$url);
   if(!empty($url[0]))
   		$getUrlResult['job_detail_url'] = $url[0];

  
   // $url = explode("|#|",$url);
   // $getUrlResult['job_detail_url'] = $url[0];



   RedirectFail::VerifyRedirectionLink($getUrlResult['job_detail_url'],$redirect_vals);

   		if(strtolower(trim($getUrlResult['source'])) == 'indeedapi') {
				

   				$now = time(); // or your date as well
				$your_date = strtotime((isset($getUrlResult['date']) && $getUrlResult['date'] != '') ? $getUrlResult['date'] : date('Y-m-d H:i'));

				$datediff = $now - $your_date;
				$mints = floor($datediff / (60 * 60 ));

				if($mints > 48) {

					$getUrlResult['isIndeedExpire'] = 'yes';
					$logeventsObj->logEventstoQueue($getUrlResult);

					$urlRecirect = "http://".$_SERVER['HTTP_HOST']."/jobs.php?q=".$getUrlResult['origin_keyword']."&l=".$getUrlResult['origin_zip'].'&isIndeedExpire=yes';
					header( "Location: ".$urlRecirect );
					die;
				}
						
			}

			if((strstr(strtolower($getUrlResult['source']), "jobs2careers") == true 
			 	|| strstr(strtolower($getUrlResult['source']), "j2c") == true)
		    	&& (strstr($getUrlResult['job_detail_url'], "&rdr=") == false)) {

		   		$rdrUrl = 'http://'.$_SERVER['HTTP_HOST'].'/jobs-fl-j2c-q-'.urlencode($getUrlResult['origin_keyword']).
		   		'-l-'.urlencode($getUrlResult['origin_zip']).'-jobs.html';

		   		$rdrUrl = '&rdr='.$rdrUrl;
		   		$getUrlResult['job_detail_url'] = $getUrlResult['job_detail_url'].$rdrUrl;
		   }

		   if(strstr(strtolower($getUrlResult['source']), "appcast") == true){
		   		
		   		$rdrUrl = 'http://'.$_SERVER['HTTP_HOST'].'/jobs-fl-appcast-q-'.urlencode($getUrlResult['origin_keyword']).
		   		'-l-'.urlencode($getUrlResult['origin_zip']).'-jobs.html';


		   		$rdrUrl = '&rdr='.$rdrUrl;
		   		$getUrlResult['job_detail_url'] = $getUrlResult['job_detail_url'].$rdrUrl;
		   }			
  
		$captchaAndBotHandlingObject = new CaptchaAndBotHandling();
	
   		$isBot = $captchaAndBotHandlingObject->checkIsBot();
   		if($isBot){	
	   		$isNotBotClick = FALSE;
	 		$logeventsObj->logEventstoQueue($getUrlResult,'bot_click');
   		}
   		else{

   			if($_SERVER['HTTP_HOST'] == 'paperrosejobs.com' || $_SERVER['HTTP_HOST'] == 'www.paperrosejobs.com'){
	   			$key = '';
				if(trim($getUrlResult['email']) !=''){
					$key = $getUrlResult['email'];
				}

	   			$user_ip = $_SERVER['REMOTE_ADDR']."_".DOMAIN."_".$key;
				$captchaAndBotHandlingObject->showCaptcha($logeventsObj,$captcha,$user_ip);
			}
		// if(memcache_get($memcache_obj_elastic, $user_ip)){
			
		// 	$mem_array = memcache_get($memcache_obj_elastic, $user_ip);
			
		// 	$click_count = $mem_array[0] + 1;
		// 	$time = $mem_array[1];
		// 	$total_clicks = $mem_array[2];
		// 	memcache_set($memcache_obj_elastic, $user_ip , array($click_count,$time,$total_clicks), false, time()+43200);
			
			
		// 	if($click_count > $total_clicks) { 
				
		// 		memcache_set($memcache_obj_elastic, $user_ip , array($click_count,$time,$total_clicks), false, time()+43200); 
		// 		if(isset($captcha) && $captcha == "correct") { memcache_set($memcache_obj_elastic, trim($user_ip), array('1',time()+86400,'9999'), false, time()+86400); } 
		// 		else { include("include/captcha.php"); exit; }
		// 	} else {
		// 		//include("include/loading_page.php"); 
		// 	}
		  
		// } else {
		// 	memcache_set($memcache_obj_elastic, $user_ip , array('1',time()+30,'5'), false, time()+43200);
		// 	include("include/loading_page.php");
		// }
   		}

    yourls_redirect($token,$getUrlResult,$_GET['q']);
}
else{
    
    header( "Location: http://www.".DOMAIN );
    die;
    
}


function yourls_redirect_javascript( $location, $dontwait = true ) {
		$message =  'if you are not redirected after 10 seconds, please <a href="'.$location.'">click here</a>' ;
		echo <<<REDIR
		<script type="text/javascript">
		window.location="$location";
		</script>
		<small>($message)</small>
REDIR;
    }
/**
 * Redirect to another page
 *
 */
function yourls_redirect( $token,$getUrlResult,$q='') {
		
	// global $objDBManager;
	global $redirect_source;
	global $location;
	global $CLICKS_TABLE_NAME;
	global $LOGS_TABLE_NAME;
	global $getUrlResult;
	
	
    $LOGS_TABLE_NAME = 'tbl_logs';
    $CLICKS_TABLE_NAME = 'tbl_click_logs';

	$subid = '';
	if((strpos(strtolower($getUrlResult['source']),'jobs2careers') !== false) || (strpos(strtolower($getUrlResult['source']),'j2cde xml') !== false)) {
	 
	  $getUrlResult['job_detail_url'] = str_replace("&t2=954JS", "", $getUrlResult['job_detail_url']);	
	  $channelHas = strpos($getUrlResult['job_detail_url'], "&t2=");
        if($channelHas === false)
			$subid = SUBID_JOBS2CAREERS;
	    else
			$subid = '';	
	   //$subid = SUBID_JOBS2CAREERS;
			
	}
	if(strpos(strtolower($getUrlResult['source']),'j2c apide') !== false){
	  
	  $linkHas = strpos($getUrlResult['job_detail_url'], "&t2=");
        if($linkHas === false)
			$subid = SUBID_JOBS2CAREERS;
	    else
			$subid = '';	
			
	}
	if(strpos(strtolower($getUrlResult['source']),'j2ccpa') !== false){
	  
	  $linkHas = strpos($getUrlResult['job_detail_url'], "&t2=");
        if($linkHas === false)
			$subid = SUBID_JOBS2CAREERS;
	    else
			$subid = '';	
			
	}
	if($getUrlResult['source'] == 'TopUSAJobs Direct' || $getUrlResult['source'] == 'TopUSAJobs High' || $getUrlResult['source'] == 'TopUSAJobs Low'){
            

       	//REDIRECT TO EDIT ALERT PAGE WHEN SOURCE IS TOPUSA BUT DOMAIN NOT.
        if(!isset($_GET['topusa_registered']) && strpos(strtolower(DOMAIN), 'topusa') === false) {
				$objTopUSA = new handleTopUSArequest(); //initialize db connection
				$objTopUSA->CheckForTopusajobs(str_replace(' ', '+', $_GET['q']),'newsletter');
		}    
		$subid = SUBID_TOPUSAJOBS;
	    
	    $getUrlResult['job_detail_url'] = str_replace("&f=fbg", "", $getUrlResult['job_detail_url']);			
	
	}


	// VIKTRE Redirect popup
	if(strpos(strtolower($getUrlResult['source']), 'viktre') !== false){
  
			if(!isset($_GET['viktre_registered'])) {
				$_SESSION['type'] = 'newsletter';
				$objViktre = new handleViktrerequest();
				$objViktre->CheckForviktrejobs(str_replace(' ', '+', $_GET['q']),'newsletter');
			}
       $subid = '';    
       $getUrlResult['job_detail_url'] = $getUrlResult['job_detail_url'];	
	}



	if(strpos(strtolower($getUrlResult['source']),'juju') !== false && strtolower($getUrlResult['source']) != 'juju api'){
		
		$getUrlResult['job_detail_url'] = str_replace("&amp;channel=staticfile", "", $getUrlResult['job_detail_url']);	
		
	    $channelHas = strpos($getUrlResult['job_detail_url'], "channel=");
        if($channelHas === false)
			$subid = SUBID_JUJU;
	    else
			$subid = '';	
			
		$getUrlResult['job_detail_url'] = str_replace("&amp;", "&", $getUrlResult['job_detail_url']);	
	}
	if(strpos(strtolower($getUrlResult['source']),'lead') !== false 
		// && strpos(strtolower($getUrlResult['source']),'api') === false 
		){
		
		$getUrlResult['job_detail_url'] = str_replace("&CHID=", "", $getUrlResult['job_detail_url']);	
		if(!empty($getUrlResult['channel']))
			$subid = "&CHID=".$getUrlResult['channel'];
		else
			$subid = SUBID_LEAD;

	   	$getUrlResult['job_detail_url'] = str_replace("&amp;", "&", $getUrlResult['job_detail_url']);	
	}
	if(strpos(strtolower($getUrlResult['source']),'athena health') !== false){
	 
	  $getUrlResult['job_detail_url'] = str_replace("&CODES=JAPJAP", "", $getUrlResult['job_detail_url']);	
	   $subid = SUBID_ATHENA;
			
	}
	if(strpos(strtolower($getUrlResult['source']),'mobiquity') !== false){
	 
	  $getUrlResult['job_detail_url'] = str_replace("&s=PureJobAlerts", "", $getUrlResult['job_detail_url']);	
	   $subid = SUBID_MOBIQUITY;
			
	}
	if(strcasecmp(trim($getUrlResult['source']), "Wells Fargo") == 0){

		$subid = '&utm_source=oakjobalerts.com&utm_medium=ppc&utm_campaign=FY15CB-PPC';

	}

				if((strstr(strtolower($getUrlResult['source']), "jobs2careers") == true 
					|| strstr(strtolower($getUrlResult['source']), "j2c") == true)
					&& (strstr($getUrlResult['job_detail_url'], "&t2=") == false)
					&& (!empty($getUrlResult['channel']))) {
					$subid = "&t2=".$getUrlResult['channel'];
				}

       			 $location =  $getUrlResult['job_detail_url'].$subid;

        		$captchaAndBotHandlingObject = new CaptchaAndBotHandling();
		   		$isBot = $captchaAndBotHandlingObject->checkIsBot();
		   		if($isBot){	
		   			echo "";
		   		}
		   		else{


						$originalJobUrl =  $getUrlResult['job_detail_url'];
	                 	$getUrlResult['job_detail_url'] = $location;

	                 	$redirectLandingPageValidationsObject =  new RedirectLandingPageValidations();
	                 	$redirectLandingPageValidationsObject -> redirectLandingPageValidationsMethod($getUrlResult);

	                	$getUrlResult['job_detail_url'] = $originalJobUrl;

	        		

	    //     		$json = file_get_contents('/var/nfs-93/redirect/mis_logs/masterSourceMapping/expireFeed.json');
					// $resultArray = json_decode($json);

					

					// foreach ($resultArray as $key => $value) {

					// 	$feedName = $resultArray[$key]->feedName;
					// 	$rulesApplies = $resultArray[$key]->rulesApplies;
											
					// 	if(strtolower(trim($feedName)) == strtolower(trim($getUrlResult['source']))){

					// 		if($rulesApplies == "1"){

					// 			$memcache_obj_elastic = memcache_connect("oakelasticcache.25s6pq.0001.use1.cache.amazonaws.com", 11211);
					// 			$value = memcache_get($memcache_obj_elastic, $_SERVER['REMOTE_ADDR']);

					// 	   		if($value == '1'){
					// 	   			$getUrlResult['failure_case'] = "bot_click";
					// 	   			redirectToLandingPage($getUrlResult);
					// 	   		}														

					// 	   		if(!empty($getUrlResult['email']))
					// 			{	
					// 				$count = memcache_get($memcache_obj_elastic, $getUrlResult['email']);

					// 				if(empty($count))
					// 					$count = 0;

					// 				$count = $count +1;
					// 				memcache_set($memcache_obj_elastic, $getUrlResult['email'] , $count, false, time()+43200); 
									
					// 				if($count > 40){
					// 					$getUrlResult['failure_case'] = "clicks_limit_exceeded";
					// 	   				redirectToLandingPage($getUrlResult);
					// 				}
					// 			}

					// 			if(	strstr(strtolower($getUrlResult['provider']), "110-") == true
					// 					&& strstr(strtolower($getUrlResult['provider']), "cylcon") == false
					// 					){
					// 					$getUrlResult['failure_case'] = "elite-110-users";
					// 	   				redirectToLandingPage($getUrlResult);
					// 				}
								
					// 				$ip=$_SERVER['REMOTE_ADDR'];
					// 				$jobCountry=$getUrlResult['job_country'];
					// 				$redirectToActualJob=false;

					// 				$file = fopen("/var/nfs-93/redirect/mis_logs/IpSeries/ip.txt","r");


					// 				while(! feof($file))
					// 				{
					// 					$iparr = split (",", fgets($file)); 

					// 					$min=trim($iparr[0]);
					// 					$max=trim($iparr[1]);

					// 					if(ip2long($min) <= ip2long($ip) && ip2long($ip) <= ip2long($max)){
					// 						$ipCountry=trim($iparr[2]);
					// 						break;
					// 					}
					// 				}

					// 				if(empty($getUrlResult['job_country']) &&  $ipCountry!='')
					// 					$redirectToActualJob=true;
					// 				else if(empty($getUrlResult['job_country']))
					// 					$redirectToActualJob=false;
					// 				else if (strtolower($ipCountry)=='united states' && (strtolower($jobCountry)=='usa' || strtolower($jobCountry)=='us' || strtolower($jobCountry)=='united states') )
					// 					$redirectToActualJob=true;

					// 				else if (strtolower($ipCountry)=='united kingdom' && (strtolower($jobCountry)=='gb' || strtolower($jobCountry)=='uk' || strtolower($jobCountry)=='united kingdom') )
					// 					$redirectToActualJob=true;

					// 				else if (strtolower($ipCountry)=='australia' && (strtolower($jobCountry)=='aus' || strtolower($jobCountry)=='au' || strtolower($jobCountry)=='australia') )
					// 					$redirectToActualJob=true;

					// 				else if (strtolower($ipCountry)=='ireland' && (strtolower($jobCountry)=='ie' || strtolower($jobCountry)=='ireland') )
					// 					$redirectToActualJob=true;


					// 				fclose($file);

					// 				if($redirectToActualJob == false){
					// 					$getUrlResult['failure_case'] = "country_mismatch";
					// 		   			redirectToLandingPage($getUrlResult);
					// 				}	


					// 			}

					// 		}
					// 	}
					}

	
	
	if($getUrlResult['source'] == 'CareerBuilder'){
	   //echo $location;	
	   	   $location = str_replace("&amp;", "&", $location);			
           $location = str_replace("##siteid##", "sep_cb012&utm_source=oakjobalerts.com&utm_medium=aggregator&utm_campaign=all-jobs", $location);
		
	}

	if(trim($getUrlResult['email']) !=''){
		
		$key = $getUrlResult['email'];
		
		echo '<img border="0" hspace="0" vspace="0" width="1" height="1" src="http://ei.rlcdn.com/'.LIVERAMP_REG.'.gif?s='.sha1(trim(strtolower($key))).'&n=1"/>
		<img border="0" hspace="0" vspace="0" width="1" height="1" src="http://ei.rlcdn.com/'.LIVERAMP_REG.'.gif?s='.sha1(trim(strtolower($key))).'&n=2"/>
		<img border="0" hspace="0" vspace="0" width="1" height="1" src="http://ei.rlcdn.com/'.LIVERAMP_REG.'.gif?s='.sha1(trim(strtolower($key))).'&n=3"/>
		<img border="0" hspace="0" vspace="0" width="1" height="1" src="http://ei.rlcdn.com/'.LIVERAMP_REG.'.gif?s='.sha1(trim(strtolower($key))).'&n=4"/>
		<img border="0" hspace="0" vspace="0" width="1" height="1" src="http://ei.rlcdn.com/'.LIVERAMP_REG.'.gif?s='.sha1(trim(strtolower($key))).'&n=5"/>';
	
	}
	/*<img src="http://pixel.jumptap.com/e/v1/pixel/networks/1/partners/54868/users/'.sha1(trim(strtolower($key))).'?jtxe='.sha1(trim(strtolower($key))).'"/>*/
	
	//sleep(1);
	
	//if($_SERVER['REMOTE_ADDR'] == '112.196.1.221') { echo $getUrlResult['source']; exit; }
        if(!empty($location)){
		
	
		//CHECK BAYARD EXPIRED JOB AND REDIRECT TO WEBSITE IN CASE.
		if(strtolower(trim($getUrlResult['source'])) == 'bayard') {
				
				$searchBayardRecord =  exec("grep -c $location /var/nfs-93/bayard.xml");
				
				if(trim($searchBayardRecord) == 0) {
					if(DOMAIN =='handpickedjobalerts.com')
					$urlRecirect = "http://".DOMAIN."/search/?location=".$getUrlResult['origin_zip']."&keyword=".$getUrlResult['origin_keyword']."&source=handpickedjobalerts";
					else	
					$urlRecirect = "http://".DOMAIN."/jobs.php?q=".$getUrlResult['origin_keyword']."&l=".$getUrlResult['origin_zip']."&expired=1";
					header( "Location: ".$urlRecirect );
					die;

					
					
				}
							
		}
		
		if(strtolower($getUrlResult['source']) == 'simplyhired api'){
			
			 $qry = parse_url($location);
			 parse_str($qry['query'], $output);
			 $location =  get_redirect_url($output['jk'],$output['q'],$output['l']);
			
		         // $googlebot  = exec('host '.$_SERVER['REMOTE_ADDR']);
	    
	  		 // if(stripos(trim($googlebot),trim("gaggle.net")) === false)	{		
				  
				  // //INSERT LOGS IN tbl_logs	
				  // // log_clicks();
				  // $lastID = mysql_insert_id();
				  
				  //INSERT LOGS IN tbl_clicks_logs
				  // log_clickedJob();

				  log_clicks_queue_java();
			 
			 // }
			 
			 // writeslowLogs();
			  
			 $location = 'http://www.'.DOMAIN.'/job-details.php?jk='.base64_encode($output['jk'])."&e=".base64_encode($getUrlResult['email'])."&q=".base64_encode($output['q'])."&l=".base64_encode($output['l'])."&last=".base64_encode($lastID);
			 
			 // if($_SERVER['REMOTE_ADDR'] == '49.200.119.31') {
			 // echo $location;exit;
			 // }
			 
			 if( !headers_sent() ) {
			    
				  header( "Location: $location" );
			
			 } else {
				   yourls_redirect_javascript( $location );
		         }
			die();
			
		}
        
          //FOR LENSA
           $randnumber =  rand(0,1);
        if(strtolower($getUrlResult['source']) == 'lensa' && $randnumber % 2 == 0){
			
			//echo "<pre>";print_r($getUrlResult);die;
			  $encoded_lensa_data = base64_encode($getUrlResult['email'].'|'.$getUrlResult['jobtitle'].'|'.$getUrlResult['city'].'|'.$getUrlResult['state'].'|'.$getUrlResult['zip'].'|'.$getUrlResult['origin_keyword'].'|'.$getUrlResult['origin_zip'].'|'.$getUrlResult['job_detail_url'].'|'.$getUrlResult['source'].'|'.$getUrlResult['provider'].'|'.$getUrlResult['date'].'|'.$getUrlResult['job_position'].'|'.$getUrlResult['group_id'].'|'.$getUrlResult['template_id'].'|'.$getUrlResult['match_type'].'|'.$getUrlResult['search_type'].'|'.$getUrlResult['user_id'].'|'.$getUrlResult['radius'].'|'.$getUrlResult['user_name'].'|'.$getUrlResult['gcpc'].'|'.$getUrlResult['cpc'].'|'.$getUrlResult['campgian_category_key']);
			 $location = 'http://'.$_SERVER['HTTP_HOST'].'/lensa_redirect.php?q='.$encoded_lensa_data;
			 header( "Location: $location" );
			 // if($_SERVER['REMOTE_ADDR'] == '49.200.119.31') {
			 // echo $location;exit;
			 // }
			
			die();
			
		}
	  
        // if(strtolower($getUrlResult['source']) == 'beyond'){
		// 	  $encoded_beyond_data = base64_encode($getUrlResult['email'].'|'.$getUrlResult['jobtitle'].'|'.$getUrlResult['city'].'|'.$getUrlResult['state'].'|'.$getUrlResult['zip'].'|'.$getUrlResult['origin_keyword'].'|'.$getUrlResult['origin_zip'].'|'.$getUrlResult['job_detail_url'].'|'.$getUrlResult['source'].'|'.$getUrlResult['provider'].'|'.$getUrlResult['date'].'|'.$getUrlResult['job_position'].'|'.$getUrlResult['group_id'].'|'.$getUrlResult['template_id'].'|'.$getUrlResult['match_type'].'|'.$getUrlResult['search_type'].'|'.$getUrlResult['user_id'].'|'.$getUrlResult['radius'].'|'.$getUrlResult['user_name'].'|'.$getUrlResult['gcpc'].'|'.$getUrlResult['cpc'].'|'.$getUrlResult['campgian_category_key']);
		// 	 $location = 'http://'.$_SERVER['HTTP_HOST'].'/beyond_redirect.php?q='.$encoded_beyond_data;
		// 	 header( "Location: $location" );
		// 	 // if($_SERVER['REMOTE_ADDR'] == '49.200.119.31') {
		// 	 // echo $location;exit;
		// 	 // }
		// 	die();
		// }


	   // $googlebot  = exec('host '.$_SERVER['REMOTE_ADDR']);
	  
	   // if(stripos(trim($googlebot),trim("gaggle.net")) === false){		
		
		//INSERT LOGS IN tbl_logs	
		// log_clicks();
				  
		//INSERT LOGS IN tbl_clicks_logs
		// log_clickedJob();

		log_clicks_queue_java();
	   // }
	   
	   // writeslowLogs();
	  
           $location = htmlspecialcharsdecode($location);
            if( !headers_sent() ) {
            	
            	header( "Location: $location" );
            } else {
            	yourls_redirect_javascript( $location );
            }
        
            }else{
            
                header( "Location: http://www.".DOMAIN );
            
            }
	die();
}


function redirectToLandingPage() {

		global $getUrlResult;	

		global $location;
		$getUrlResult['job_detail_url'] =  $location;
		
		$getUrlResult['lp_click_type'] = "LP";
		$getUrlResult['landing_page_type'] = "2";

		// if($value == '1' || stripos(trim(strtolower($_SERVER['HTTP_USER_AGENT'])),trim("unavailable")) !== false)
		log_clicks_queue_java();
								
		$session_key = $getUrlResult['job_position'].'$$$'.$getUrlResult['source'].'$$$'.$getUrlResult['jobtitle'].'$$$'.$getUrlResult['email'];
								
		$session_key = md5($session_key);
		$_SESSION[$session_key] = json_encode($getUrlResult);
		$urlRecirect = "/jobs_landing_page.php?sk=".$session_key ;

		header( "Location: ".$urlRecirect );
		die;
		     
       
}

function get_redirect_url($jk,$k,$l) {
		
		if($_SERVER['REMOTE_ADDR'] != ""){
			$user_ip_address = $_SERVER['REMOTE_ADDR'];
		} elseif ($_SERVER['HTTP_X_FORWARDED_FOR'] != "") {
			$user_ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}  elseif ($_SERVER['HTTP_FORWARDED_FOR'] != "") {
			$user_ip_address = $_SERVER['HTTP_FORWARDED_FOR'];
		} else {
			$user_ip_address = "";
		}

		return $api_url = 'http://api.simplyhired.com/api-test/jobs/v1/get/'.urlencode($jk).'/?&clip='.$user_ip_address.'&pshid=87357&auth=d95d2194671b646fba1996ef33cbfec790e5c745.87357';
       
       
}

function log_clickedJob(){
    
    // global $objDBManager;
    global $CLICKS_TABLE_NAME;
    global $getUrlResult;
    global $location;
    global $redirect_source;
    
    //LOGS BOTS IN ANOTHER TABLE
    if(stripos(trim(strtolower($_SERVER['HTTP_USER_AGENT'])),trim("bot")) === false)
   	{}else{
   		$CLICKS_TABLE_NAME = 'tbl_bot_click_logs';
   	}

    $insertURL = "INSERT INTO $CLICKS_TABLE_NAME (
                                                url,
                                                email,
                                                jobtitle,
                                                city,
                                                state,
                                                zip,
                                                origin_keyword,
                                                origin_location,
                                                source,
                                                user_agent,
                                                IP,
                                                type,
                                                provider,
                                                job_position,
                                                group_id,
                                                template_id,
                                                match_type,
                                                search_type,
                                                cpc,
                        						gcpc,
                        						sent_time
                                                
                                                ) VALUES (
                                                
                                                '".$location."',
                                                '".$getUrlResult['email']."',
                                                '".mysql_escape_string($getUrlResult['jobtitle'])."',
                                                '".$getUrlResult['city']."',
                                                '".$getUrlResult['state']."',
                                                '".$getUrlResult['zip']."',
                                                '".mysql_escape_string($getUrlResult['origin_keyword'])."',
                                                '".$getUrlResult['origin_zip']."',
                                                '".$getUrlResult['source']."',
                                                '".$_SERVER['HTTP_USER_AGENT']."',
                                                '".$_SERVER['REMOTE_ADDR']."',
                                                'newsletter',
                                                '".$getUrlResult['provider']."',
						'".$getUrlResult['job_position']."',
                                                '".$getUrlResult['group_id']."',
                                                '".$getUrlResult['template_id']."',
                                                '".$getUrlResult['match_type']."',
                                                '".$getUrlResult['search_type']."',
                                                '".$getUrlResult['cpc']."',
                                                '".$getUrlResult['gcpc']."',
                                                '".$getUrlResult['date']."')";	
		
    

    //$insertURLQry = mysql_query($insertURL);
    // $objDBManager->exeQuery($insertURL);
    
}

function log_clicks_queue_java(){

    global $getUrlResult;
    global $location;
    global $redirect_source;
     global $isNotBotClick;
    
    $queueLogsArray = $getUrlResult;
    $queueLogsArray['job_detail_url'] = $location;



    $log_qry_vals = "'".$getUrlResult['email']."','".$getUrlResult['id']."','".$location."', '".mysql_escape_string($getUrlResult['jobtitle'])."', '".$getUrlResult['city']."', '".$getUrlResult['state']."', '".$getUrlResult['zip']."', '".mysql_escape_string($getUrlResult['origin_keyword'])."', '".$getUrlResult['origin_city']."', '".$getUrlResult['origin_state']."', '".$getUrlResult['origin_zip']."', '".$getUrlResult['source']."','".$_SERVER['HTTP_USER_AGENT']."','".$_SERVER['REMOTE_ADDR']."','newsletter','".$getUrlResult['provider']."'";
    writesLogs($log_qry_vals);	

    $logeventsObj = new LogEvents();
  
	//LOGS BOTS IN ANOTHER TABLE
    if($isNotBotClick)
   	{

   		$logeventsObj->logEventstoQueue($queueLogsArray);
   		$logeventsObj->logEventstoDynamoQueue($queueLogsArray);

   	}
   	// else{
   	// 	$logeventsObj->logEventstoQueue($queueLogsArray,'bot_click');
   	// 	return;
   	// }

	writeslowLogs();

}

function log_clicks(){
    
    // global $objDBManager;
    global $LOGS_TABLE_NAME;
    global $getUrlResult;
    global $location;
    global $redirect_source;
    
    $queueLogsArray = $getUrlResult;
    $queueLogsArray['job_detail_url'] = $location;
    $logeventsObj = new LogEvents();


    //LOGS BOTS IN ANOTHER TABLE
    if(stripos(trim(strtolower($_SERVER['HTTP_USER_AGENT'])),trim("bot")) === false)
   	{
   		$logeventsObj->logEventstoQueue($queueLogsArray);
   	}else{
   		$logeventsObj->logEventstoQueue($queueLogsArray,'bot_click');
   		return;
   	}
    
    $insertURL = "INSERT INTO $LOGS_TABLE_NAME (email,
						redirect_pid,url,
						jobtitle,
						city,
						state,
						zip,
						origin_keyword,
						origin_city,
						origin_state,
						origin_zip,
						source,
						user_agent,
						IP,
						type,
						provider,
						cpc,
                        gcpc,
                        sent_time
						) VALUES (
						'".$getUrlResult['email']."',
						'".$getUrlResult['id']."',
						'".$location."',
						'".mysql_escape_string($getUrlResult['jobtitle'])."',
						'".$getUrlResult['city']."',
						'".$getUrlResult['state']."',
						'".$getUrlResult['zip']."',
						'".mysql_escape_string($getUrlResult['origin_keyword'])."',
						'".$getUrlResult['origin_city']."',
						'".$getUrlResult['origin_state']."',
						'".$getUrlResult['origin_zip']."',
						'".$getUrlResult['source']."',
						'".$_SERVER['HTTP_USER_AGENT']."',
						'".$_SERVER['REMOTE_ADDR']."',
						'newsletter',
						'".$getUrlResult['provider']."',
						'".$getUrlResult['cpc']."',
                        '".$getUrlResult['gcpc']."',
                        '".$getUrlResult['date']."'
						)";
		

    //$insertURLQry = mysql_query($insertURL);
    // $objDBManager->exeQuery($insertURL);
    $log_qry_vals = "'".$getUrlResult['email']."','".$getUrlResult['id']."','".$location."', '".mysql_escape_string($getUrlResult['jobtitle'])."', '".$getUrlResult['city']."', '".$getUrlResult['state']."', '".$getUrlResult['zip']."', '".mysql_escape_string($getUrlResult['origin_keyword'])."', '".$getUrlResult['origin_city']."', '".$getUrlResult['origin_state']."', '".$getUrlResult['origin_zip']."', '".$getUrlResult['source']."','".$_SERVER['HTTP_USER_AGENT']."','".$_SERVER['REMOTE_ADDR']."','newsletter','".$getUrlResult['provider']."'";
    writesLogs($log_qry_vals);	   
	   
    
    
}

function htmlspecialcharsdecode($string,$style=ENT_QUOTES) {
	
	      $translation = array_flip(get_html_translation_table(HTML_SPECIALCHARS,$style));
	      if($style === ENT_QUOTES){ $translation['&#39;'] = '\''; }
	      if($style === ENT_QUOTES){ $translation['&#039;'] = '\''; }
	      return strtr($string,$translation);
}

function writeslowLogs(){
     		
    global $startTime;
    global $startlogtime;
			
    $endTime = date("Y-m-d H:i:s");
    $endlogtime	= microtime(true);
    $logsString = 'Start-Time : '.$startTime."|".' End-Time : '.$endTime."|".' IP : '.$_SERVER['REMOTE_ADDR']."| Total : ".number_format($endlogtime-$startlogtime, 4);
    file_put_contents( CLICKLOGS_FILE_PATH . date('Y-m-d')."_time_logs.txt", $logsString."\n", FILE_APPEND );
	
}
function writesLogs($string){
     		
    file_put_contents( CLICKLOGS_FILE_PATH . date('Y-m-d')."_logs.txt", $string."\n", FILE_APPEND );	
}


?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-57859585-1', 'auto');
  ga('send', 'pageview');
</script>
?>

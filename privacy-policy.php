<?php ob_start();
require_once('include/configuration.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="We respect the privacy of our users and have developed this Privacy Policy to demonstrate our commitment to protecting your privacy.">
    <meta name="author" content="">

    <title>Privacy Policy | <?=TITLE?></title>

    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/oakstyle.css" rel="stylesheet">
     <link rel="shortcut icon" href="<?=FAVICONPATH?>favicon.ico" type="image/x-icon">
    <!-- Custom Fonts -->
    <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- UserVoice JavaScript SDK (only needed once on a page) -->
<script>(function(){var uv=document.createElement('script');uv.type='text/javascript';uv.async=true;uv.src='//widget.uservoice.com/RUVxfxpYSZzV74j6P7cENw.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(uv,s)})()</script>

<!-- A link to launch the Classic Widget -->
<style>
    p
    {
        font-size: 17px !important;
    }
    
</style>

<?php
if(DOMAIN == '2ndchancejobalerts.com')
{ 
    ?>
        	<!-- Google Tag Manager -->
			
			<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','GTM-5TGHSS');</script>
			<!-- End Google Tag Manager -->
<?php
}
?>

</head>

<body id="page-top" class="index">
	

<?php
if(DOMAIN == '2ndchancejobalerts.com')
{
	?>
		  <!-- Google Tag Manager -->
            <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5TGHSS"
            height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<?php
}

?>	
<?php
    include('include/common.php');
?>
    <!-- Navigation -->
    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo $baseurl; ?>"><img src="<?=IMAGE_BASE_URL?>/logo1.png"><?=HEADER_TITLE?></a>
               
                
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                
                
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                   <!-- <li class="page-scroll">
                        <a href="#portfolio">Portfolio</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#about">About</a>
                    </li>-->
                    <li class="page-scroll">
                       
                    </li>
                </ul>

                
                
                
            </div>
            <!-- /.navbar-collapse -->
        </div>
        
        
        <!-- /.container-fluid -->
    </nav>

    <!-- section -->
    <section>
        <h3 style="text-align:center; margin-bottom: 0px !important;">Privacy Policy</h3>
        <div class="container" style="text-align: left;">
            
            <div class="row">
                <h4>About this Privacy Policy</h4>
                <p>We respect the privacy of our users and have developed this Privacy Policy to demonstrate our commitment to protecting your privacy. This Privacy Policy is intended to describe information that we collect from you as part of the normal operation of our service and how that information may be used, with whom it may be shared, and your choices about such uses and disclosures. If you have questions about our privacy practices, please refer to the end of this Privacy Policy for information on how to <a href="https://jobalerts.freshdesk.com/support/tickets/new">Contact Us</a>.</p>
                <h4>Application of this Privacy Policy</h4>
                <p>This Privacy Policy applies to all websites ("Website(s)" or "Site(s)") owned, operated, and controlled by Fire Brick Group Ltd. ("<?=TITLE?>", "we", "our" or "us"), including, but not limited to, <?=DOMAINNAME?>. Please note that this Privacy Policy applies only to Sites owned, operated and controlled by us and not to websites maintained by other companies to which we may link.</p>
                <h4>Links to Third Parties</h4>
                <p>For your convenience and to improve the usage of the website we may insert links to third parties on this site. This Privacy Policy does not apply to such third party websites. These links take you outside of our service and off our Site and are beyond our control. This includes links from partners that may use <?=TITLE?> logos as part of a co-branding agreement. The sites that such links may direct you to have their own separate privacy policy. <?=TITLE?> is not liable for the content and activities of those sites. Your visits and access to such websites is at your own risk. Please note that third parties' sites may utilize their own cookies with users, collect users' data, and/or solicit users' personal information.</p>
                <h4>Agreement to Terms of Privacy Policy and Terms of Use</h4>
                <p>All activities in which you may engage on this website are voluntary. You are not required to provide any personal information to us unless you choose to access features on this site which require the information. If you do not agree with the terms of this policy or other terms of use on this website, then you should immediately exit this website and discontinue using the Site. If you do not agree with the terms of our Privacy Policy and our terms of use, please do not provide us with personal information, and leave this Website. The Terms of Service of this Site are expressly incorporated herein by reference and made a part of this policy. By using the Website, you signify that you agree to the terms of this Privacy Policy as well as to our Terms of Service.</p>
                <h4>No Use of This Site by Persons Under 18 Permitted</h4>
                <p>This Site is not directed at persons under the age of 18. Persons under the age of 18 must not send any personal information to <?=TITLE?> under any circumstances, including but not limited to name, address or email address.</p>
                <h4>Your Privacy is Important to Us</h4>
                <p>Our Privacy Policy explains what information we obtain on the Site and how we use the information you disclose to us, including how we may provide your information to our third party partners who have expressed a willingness to provide you with the products and services associated with this Site.</p>
                <h4>Who We Are</h4>
                <p>We are an online consumer service that introduces jobseekers to employment and other opportunities to participating third party partners such as other job boards, hiring companies, staffing agencies, business opportunity providers and other third parties who have relevant offers to our users.</p>
                <h4>Information We Collect</h4>
                <p>You may be required to provide "personal information" to access or use certain parts of our Website or features of the Site. If you do not provide the requested personal information, you may not be able to access or use the features of the Site where such information is requested. We may ask you for personal information, which includes information that pertains to your identity, that we or certain of our third party partners consider necessary for performing an evaluation of your qualifications for their employment opportunity or products or services offers. Such information may include, but is not limited to, items such as: your name; address; email address; telephone number; fax number; date of birth; age; gender; information about your interests in and use of various products, programs, and services; education level; educational interests; income; and the like and other information that does not identify you. We may also use your personal information to obtain additional information about you such as your credit score and/or your credit report to improve our service for you. In addition, we may collect certain information regarding the number and type of products and/or services you have responded to and completed.</p>
                <h4>Cookies</h4>
                <p>Cookies are small pieces of data that are sent to your browser from a web server and stored on your hard drive and/or your mobile device. In general, we use cookies and similar technologies to help us better serve you and improve your experience on our Site. As you browse areas of the Site, cookies may be placed on your computer's hard drive and/or your mobile device which may allow us to collect information, such as your internet protocol address and mobile device identifier. Cookies allow us to track your account so you don't need to retype the information each time you visit our Site. Cookies also allow us to provide certain services and features to you. You may refuse cookies in your browser, but doing so may result in certain services and features not functioning properly.</p>
                <p>We use third-party advertising companies to serve ads and/or collect certain information when you visit our Site. These advertisers may also serve cookies when you click on advertisements on the Site, or use pixel tags, web beacons, or similar technologies to collect non-personally identifiable aggregated information when users visit the Site in order to provide advertisements on our Site, other sites, and other forms of media about goods and services that may be of interest to you. This common practice is known as "online behavioral advertising" or "interest based advertising". To learn more about this practice or to opt-out of this use of your anonymous information, please visit: <a href="http://www.networkadvertising.org/managing/opt_out.asp" style="color: black">http://www.networkadvertising.org/managing/opt_out.asp</a>. In addition, if you want to prevent a third-party advertiser from collecting data, you may visit each ad network's web site individually and opt-out. We do not have access to personal information, if any, gathered by advertisers.</p>
                <h4>Third Party Cookies </h4>
                <p>Notwithstanding anything else in this policy, we or a data provider we have engaged may place or recognize a unique cookie on your browser (including on your mobile device) to enable you to receive customized ads or content, including through online behavioral advertising. These cookies contain no personally identifiable information. The cookies may reflect de-identified demographic or other data linked to data you voluntarily have submitted to us, e.g., your email address that we may share with a data provider solely in hashed, non-human readable form. To opt-out of these cookies, please go to <a href="https://jobalerts.freshdesk.com/support/tickets/new">Contact Us </a>.</p>
                <h4>Log Files</h4>
                <p>When you visit our Website, we track internet protocol (IP) addresses, browser type, internet service provider (ISP), referring/exit pages, platform type, date/time stamp and number of clicks to analyze trends and administer the Site, track user's movement in the aggregate and gather broad demographic information for aggregate use. We use your IP address at times to help identify you as you browse and to gather broad technical and demographic information on who uses the Site.</p>
                <h4>Web Beacons</h4>
                <p>We may use standard internet technology, called Web Beacons (also called clear gifs, pixel tags or web bugs) that are tiny graphics with a unique identifier that are used to track online movements. Unlike cookies which are stored on the user's computer, a web beacon is embedded invisibly on a webpage (or email). Web beacons help us better manage content through the service by informing us what content and which campaigns are effective. This allows us and our third party partners to gauge the effectiveness of marketing campaigns by showing how many emails the recipients have opened or by sending/receiving an anonymous identification number when you travel to a site from an online advertisement. We do not tie clear gifs to personally identifiable information.</p>
                <h4>How We Use Your Information</h4>
                <h4>General: In general we may use information we collect about you to:</h4>
                <p>Provide the products and services that you have requested;<br/>
                Perform research and analysis about your use of, or interest in our products, services, content, or the products, services, or content offered by third parties;<br/>
                Communicate with you via email, postal mail, telephone and/or mobile devices about products or services that may be of interest to you either from us or third parties;<br/>
                Develop and display content and advertisement tailored to your interests on our site and other sites;<br/>
                                Verify your eligibility to receive products and/or services;<br/>
                Enforce our Terms of Service;<br/>
                Manage our business; and<br/>
                Perform functions as otherwise described to you at the time of collection.</p>
                <p>For example, we may compare and review your personal information for errors, omissions, and accuracy based upon past usage and response; or compare your information against other publicly available files; or validate some of the personal information to verify that the information you submitted has a high probability of being valid information.</p>
                <p>Based upon your affirmative consent, as evidenced by your clicking on the applicable "submit" button, acknowledgement box, and/or your registration on a Site (i.e., your submission of the requested information, including your personally identifiable information), we may use the information you provide to attempt to match you with providers of products and services (for example, using your zip code to check for service providers whose service area covers your location) and/or we may forward the information that you provided directly to certain of our third party partners.</p>
                <p>Use for Research: We reserve the right to collect information about you and store it in connection with other online or offline information we may possess or obtain about you to create a general user profile. This profile may be created by combining your information with other sources of information such as information obtained from public databases, or from your browsing habits surrounding your visit to this Site.</p>
                <p>Email and Telephone Calls: From time to time, we may contact you via e-mail advertising, telephone marketing, direct mail marketing, or other means to inform you of additional opportunities to receive special offers for other services and/or products that we believe may be of interest to you. By providing your contact information when using our service, you are entering into a business relationship with <?=TITLE?> and are further requesting, and expressly consenting to, being contacted by <?=TITLE?> via telephone, fax, email, postal mail, or any other means, at any of your contact numbers or addresses, even if you are listed on any federal, state or other applicable "Do Not Call" list, in order that <?=TITLE?> may provide the services set forth on the Site or for other purposes reasonably related to your service request and the business of <?=TITLE?>, including marketing-related emails. You are under no obligation to accept any service or product offered through these communications. If you no longer wish to receive any e-mails from us, or if you want to modify your personal data or want your personal data removed from our database, you may cancel your registration at anytime and you may OPT OUT by following the opt out instructions provided herein.</p>
                <h4>Sharing your Information</h4>
                <p>Third parties</p>
                <p>WE MAY USE INFORMATION THAT WE HAVE COLLECTED FOR ANY LEGALLY PERMISSIBLE PURPOSE, INCLUDING SELLING OR TRANSFERRING SUCH INFORMATION AT ANY TIME TO THIRD PARTIES FOR ANY LEGALLY PERMISSIBLE PURPOSE. For example, we may share your personal information with third party partners so that they can contact you and offer you products and/or services that we and they believe might be of interest to you. This may include, but is not limited to, email advertising, telephone marketing, and direct mail marketing. You are under no obligation to accept any service or product offered by any of our third party partners through these communications. By providing personal information when using the Site, you are entering into a business relationship with <?=TITLE?>'s third party partners and are requesting, and expressly consenting to, being contacted by our third party partners via telephone, fax, email, postal mail or any other means, at any of your contact numbers or addresses, even if you are listed on any federal, state or other applicable "Do Not Call" list, in order that our third party partners may provide the services set forth on the Site or for other purposes reasonably related to your Service and/or product request to which you have expressed interest and the business of our third party partners, including marketing-related emails.</p>
                <p>PLEASE NOTE: submission of YOUR telephone number(s) and a best time to be called is YOUR authorization and request to be called at the telephone number(s) YOU provide.</p>
                <p>We may disclose your personally identifiable information for the purpose of the transfer or sale of personally identifiable information pursuant to the sale of our business or assets. If we transfer ownership of the Site to another company, we will notify you by general notice on our Site. We may also transfer or share non-individualized information, such as summary or aggregated anonymous information about all persons or sub-groups of persons visiting this Site. In addition, we may maintain separate email, mailing or phone lists for different purposes based on the information that you submitted and that we maintain.</p>
                <p>Service Providers</p>
                <p>In order to provide the products or services you have requested, we may disclose the information we collect to companies that facilitate or provide elements of our service on our behalf, including without limitation, email management firms, and call center providers.</p>
                <p>Other Purposes</p>
                <p>We may also disclose your personally identifiable information when required by law, such as in response to a subpoena, court order, or other legal process, or to prevent imminent harm to any person or entity. We will fully cooperate with law enforcement agencies in identifying those who use our services for illegal purposes.</p>
                <h4>How We Protect Your Privacy</h4>
                <p>Security Measures</p>
                <p>In order to protect both the personal information and the general information that we receive from you through our website, we have implemented various security measures.  In addition to our security measures, our third party partners are required to operate in accordance with federal and state laws and regulations, including regulations relating to CAN-SPAM and rules promulgated by the FTC. If we learn that any of our third party partners is breaching our agreement, we will take corrective actions including, if necessary, terminate our relationship with such third party partner.</p>
                <p>Risk of Interception</p>
                <p>We follow generally accepted industry standards to protect the personal information submitted to us. However, no method of transmission over the Internet, or method of electronic storage, is 100% secure. While we strive to use commercially acceptable means to protect your personal information, we cannot guarantee the security of any information you disclose online. For example, third parties may unlawfully intercept personal data or access our servers and obtain personal data. By using this site, you expressly acknowledge and agree that we do not guarantee the security of any data provided to or received by us through this site and that any personal information, general information, or other data or information received from you through the site is provided to us at your own risk, which you expressly assume. If you have any questions about security on our Web site, you can mail us at the address indicated below in the <a href="https://jobalerts.freshdesk.com/support/tickets/new">Contact Us</a> section.</p>
                <p>Opting Out</p>
                <p>If you wish to opt-out from receiving additional email offers, advertisements and promotions relating to this Site, please click on this <a href="https://jobalerts.freshdesk.com/support/tickets/new">Contact Us </a> to "opt-out".</p>
                <p>If you choose to opt-out, your email address will be added to the suppression files associated with this Site within ten (10) business days of your request. Thereafter, you should not receive any further email offers, advertisements and promotions from this Site. However you may still receive promotional communications from our third party partners with whom we have shared your information. To stop receiving further communications from these third party partners you will need to contact them directly in order to opt-out. Please note that after we process your request to opt-out from receiving additional email offers, advertisements and promotions from this Site, we will no longer make your information available to our third party partners relating to this Site.</p>
                <p>Effective Date and Notification of Changes to Privacy Policy</p>
                <p>This Privacy Policy is effective as of the Effective Date stated at the beginning of this Privacy Policy and will remain in effect except with respect to any of its provisions that are changed in the future, in which case the changes will become effective on the date they are posted on the website or we otherwise notify you of the changes. We reserve the right to change this Privacy Policy at any time. You should check this policy periodically as its terms may change from time to time. Your continued use of the Site after we post any such modifications will constitute your acknowledgment of the modified policy and your agreement to abide and be bound by the modified policy. We will also revise the "last updated" date found at the beginning of this Privacy Policy when we post changes to it.</p>
                <p>Contact Us</p>
                <p>If you have any questions or comments relating to our website or Privacy Policy, or if you would like to request information about our disclosure of personal information to third parties, please  <a href="https://jobalerts.freshdesk.com/support/tickets/new">Contact Us </a>  by email or by postal mail to P.O Box 86, Old Westbury, NY 11568</p>
                <p>You may also send your questions or comments to our address:</p>
                <p>Special Notification for California Residents</p>
                <p>Individual customers who reside in California and have provided their personal information may request information regarding disclosures of this information to third parties for direct marketing purposes. Such requests must be submitted through our <a href="https://jobalerts.freshdesk.com/support/tickets/new">Contact Us</a> instructions.</p>
                <p>Notification of Intellectual Property Rights Infringement</p>
                <p>We are committed to uphold and respect intellectual property rights. If you are an intellectual property owner and believe that your rights have been infringed by materials on this webpage, please <a href="https://jobalerts.freshdesk.com/support/tickets/new">Contact Us </a>.</p>
                <p>Please provide the following information in your correspondence to us: your name and contact information, identification of the copyrighted work claimed to be infringed, identification of the material that is claimed to be infringing, and sufficient information for us to locate the material. Please include with your correspondence, (i) a statement that you have a good faith belief that use of the material in the manner complained of is not authorized by you (copyright owner), your agent, or the law; (ii) a statement that the information you provided is accurate, and under penalty of perjury, that you are authorized to act on behalf of the owner of the exclusive right that is allegedly infringed.</p>
            
            </div>
    </section>

   

    <?php include("include/footer.php"); ?>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top page-scroll visible-xs visble-sm">
        <a class="btn btn-primary" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>

   <!-- jQuery Version 1.11.0 -->
    <script src="js/jquery-1.11.0.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    

    <!-- Custom Theme JavaScript -->
    <script src="js/oak.js"></script> 
    
    
    
    <script type="text/javascript">
        function validateForm(){
            if($('#what').val() == ''){
                $('#where').css('border-color','#ccc');
                $('#where').css('border-bottom-color','#aaa');
                $('#where').css('border-right-color','#aaa');
                $('#what').css('border-color','red');
                return false;
            } else if($('#where').val() == ''){
                $('#what').css('border-color','#ccc');
                $('#what').css('border-bottom-color','#aaa');
                $('#what').css('border-right-color','#aaa');
                $('#where').css('border-color','red');
                return false;
            }
        }

        $('#where').keyup(function(event) {
            if(event.keyCode == 37 || event.keyCode == 39) {
            } else {
                var tags = $(this).val().split(',');
                $(this).val(function(index, value) {
                    //alert(value);
                    if (tags.length > 2) {
                        value = value.replace(/,\s*$/, ''); // remove commas from existing input
                        return value; // add commas back in
                    } else {
                        return value; // add commas back in
                    }
                });
            }
       });
    </script>
    

</body>

</html>

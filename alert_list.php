<?php
ob_start();
  require_once('include/DBManager.php');
  require_once('include/configuration.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?=TITLE?></title>

    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/oakstyle.css" rel="stylesheet">

    <link rel="shortcut icon" href="<?=FAVICONPATH?>favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?=FAVICONPATH?>favicon.ico" type="image/x-icon">

    <!-- Custom Fonts -->
    <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- UserVoice JavaScript SDK (only needed once on a page) -->
    <script>(function(){var uv=document.createElement('script');uv.type='text/javascript';uv.async=true;uv.src='//widget.uservoice.com/RUVxfxpYSZzV74j6P7cENw.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(uv,s)})()</script>

<!-- A link to launch the Classic Widget -->

<?php
if(DOMAIN == '2ndchancejobalerts.com')
{ 
    ?>
         	<!-- Google Tag Manager -->
			
			<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','GTM-5TGHSS');</script>
			<!-- End Google Tag Manager -->
<?php
}
?>

<?php
     if(isset($_COOKIE['login_type'])) {
         $pixel = '17028';
         $md5_email = md5($_COOKIE['email']);
         $sha1_email = hash('sha1', $_COOKIE['email']);
         $sha256_email =  hash('sha256', $_COOKIE['email']);
         ?>
            
            <script src="//pippio.com/api/sync?pid=<?=ARBOR_PIXEL?>&it=4&iv=<?php echo  $md5_email; ?>&it=4&iv=<?php echo  $sha1_email; ?>&it=4&iv=<?php echo  $sha256_email; ?>" async></script>
            
         <?php
     }
    ?>

</head>

<body id="page-top" class="listing">
<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
  
    $objDBManager = new DBManager();//initialize db connection
    $objDBManager->createConnection();


    $tableName = "tbl_jobAlerts_whitelabels";

    // echo WHITELABEL_NAME;
    if(defined('WHITELABEL_NAME') &&  WHITELABEL_NAME == 'oakjob')
      $tableName = "tbl_jobAlerts";



  
  //
  $msg='';
  if(isset($_GET['jid']) && $_GET['jid']!=''){
    
      $where = 'Id='.$_GET['jid'];
      $objDBManager->DeleteRecord($tableName,$where,false);
      $msg='Alert Deleted';

  }
  
  if(PROVIDER == '')
      $provider = (isset($_COOKIE['source']))       ?       $_COOKIE['source']      :       'web';
      else
      $provider = PROVIDER;
   if(PROVIDER != '')
        $appendWhere =  ' AND provider ="'.$provider.'"';
   else
      $appendWhere =  ' AND provider ="'.$provider.'"';
  
  //Array of list.  
  $query ='select * from  '.$tableName.' where EmailAddress="'.$_COOKIE['email'].'"'.$appendWhere;
  
  $alertLiist=$objDBManager->fetchRecord($query);
  
  $loginfailed = false;
  if($_COOKIE['email'] == ''){

    $alertLiist= array();
    $loginfailed = true;
  } 



  
 ?>

    <!-- Navigation -->
    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php"><img src="<?=IMAGE_BASE_URL?>/logo1.png" title="<?= TITLE ?> Logo"> <?=HEADER_TITLE?></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
           
                <ul class="nav navbar-nav navbar-right">
                 
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <ul class="mobile">
                    <li><a  href="index.php">Search Jobs</a></li>
                    <?php if(!isset($_COOKIE['login_type'])) {?>
                        <li><a class="active" href="sign_in.php">Manage Alerts</a></li>
                        <li><a href="sign_in.php">Saved Jobs</a></li>
                        <?php } else { ?>
                        <li><a class="active" href="alert_list.php">Manage Alerts</a></li>
                        <li><a href="savedjobs.php">Saved Jobs</a></li>
                        <li><a href="profile.php?id=<?php echo base64_encode($_COOKIE['id']);?>">Manage Profile</a></li>
                        <?php } ?>
                    <li><a href="https://jobalerts.freshdesk.com/support/tickets/new" alt="Help/FAQ">Help/FAQ</a>
                    </li>
                    </ul>
                   <!-- <li class="page-scroll">
                        <a href="#portfolio">Portfolio</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#about">About</a>
                    </li>-->
                    <li class="page-scroll">
                      <?php if(!isset($_COOKIE['login_type'])) {?>
                        <a href="sign_in.php" style="color: #fff;text-decoration: none;"><button type="submit" class="btn btn-signin">Sign In</button></a>
                       <?php }else { ?>
                          <a href="sign_in.php?logout=true" style="color: #fff;text-decoration: none;"><button type="submit" class="btn btn-signup">Sign out</button></a>
                        <?php 
                        } 
                        ?>
                    </li>
                </ul>
                  
                  
              
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
        
        <div class="secondnav web">
        <ul class="container">
        <li><a href="index.php">Search Jobs</a></li>
        <?php if(!isset($_COOKIE['login_type'])) {?>
                        <li><a class="active" href="sign_in.php">Manage Alerts</a></li>
                        <li><a href="sign_in.php">Saved Jobs</a></li>
                        <?php } else { ?>
                        <li><a class="active" href="alert_list.php">Manage Alerts</a></li>
                        <li><a href="savedjobs.php">Saved Jobs</a></li>
                        <li><a href="profile.php?id=<?php echo base64_encode($_COOKIE['id']);?>">Manage Profile</a></li>
                        <?php } ?>
        <li><a href="https://jobalerts.freshdesk.com/support/tickets/new" alt="Help/FAQ">Help/FAQ</a></li>
        </ul>
        </div>
        
    </nav>
    

    <!-- section -->
    <section>
        <div class="container">

             <!-- New job added alert -->
            <?php if(isset($_GET['added']) && $_GET['added'] == 'true' ) { ?>
              <div style="text-align: center; color: #7CC243">Job Alert has been created.</div>
            <?php } ?>
            <!-- End of new job added alert -->
        
            <h4 class="heading text-left" style="Float:left;width:88.5%">List of Jobs</h4>
            <a style="color: #fff;" href="create_alert.php"> 
            <button class="btn btn-signin" type="button">
              Create Job Alert
          </button>
          </a>
           <?php if(count($alertLiist) == 0 && $loginfailed == false) { ?>
            
                <div class="joblists clearfix"> <div class="row"><div class="col-lg-9 col-md-9 text-left" style="text-align: center; width: 100% !important;"><b>No Job alerts found.</b></div> </div></div>
            <?php } ?>
            <?php if($loginfailed) { ?>
            
                <div class="joblists clearfix"> <div class="row"><div class="col-lg-9 col-md-9 text-left" style="text-align: center; width: 100% !important;"><b>Please login to see your Alerts</b></div> </div></div>
            <?php } ?>
          
           
             <?php foreach($alertLiist as $list) { ?>
                   <div class="joblists clearfix">
                  
                   <div class="row">

                     <div class="col-lg-9 col-md-9 text-left">
                     <a href="jobs.php?q=<?=$list['Keyword']; ?>&l=<?=$list['Location']; ?>"><?php echo $list['Keyword'] ?></a>
                     <p></p>
                      <em><?php echo $list['Location'] ?></em>
                   </div>
                   
                   
                   <div class="col-lg-3 col-md-3 text-right"><a href="update_alert.php?id=<?php echo base64_encode($list['Id']); ?>">
                   <i class="fa fa-pencil-square-o"></i></a>
                   <a href="javascript:;" onclick="abc('<?=$list['Id'];?>')"><i class="fa fa-trash-o"></i></a>
                   
                   
                   </div>

                   
                   
                   
                 
                  <!-- list 1 end-->
                  
                 
                  
                   <!-- list 2 end-->
                   
                   
                    
                  
                   <!-- list 3 end-->
                   
                  
                  
               </div> 
                 
            </div>
            <?php } ?>
        </div>
    </section>



   

    <?php include("include/footer.php"); ?>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top page-scroll visible-xs visble-sm">
        <a class="btn btn-primary" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>
       

    <!-- jQuery Version 1.11.0 -->
    <script src="js/jquery-1.11.0.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    
<script src="js/bootbox.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="js/oak.js"></script>
    <script>
         function abc(jid) {
        var resultDelete = false;
        bootbox.confirm('Do you want to delete this alert?', function(result) {
            if(result){
                window.top.location = 'alert_list.php?jid='+jid;
            }
        });
        return resultDelete;
        }
        
    </script>

</body>

</html>

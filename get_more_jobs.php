<?php 
//error_reporting(E_ALL); ini_set('display_errors', 1);
// error_reporting(E_ALL);
// ini_set('display_errors', 1);
// include('../include/common.php');
// require_once('../include/DBManager.php');
// require_once('../include/sources_config.php');
/*
 * Script for rendering json data into HTML format of job listing
 * @Author : Himanshu Batra
 */

	$logosArray = array("logo1.png", "logo2.png", "logo3.png", "logo4.png", "logo5.png");
	$logoCounter = 0;

	$memcache_obj_elastic = memcache_connect("oakelasticcache.25s6pq.0001.use1.cache.amazonaws.com", 11211);



					// function getLogoFromClearbitAndStore($company_name){

     //                    global $objDBManager;
     //                    global $memcache_obj_elastic;

     //                    $url="https://autocomplete.clearbit.com/v1/companies/suggest?query=".$company_name;
     //                    $result = file_get_contents($url);
     //                    $vars = json_decode($result, true);

     //                    foreach ($vars as &$innerArray) {
     //                      $logo =  $innerArray['logo'];
     //                      $name = $innerArray['name'];

     //                      $name = str_replace(" ","_",$name);
     //                      $name = str_replace(".","_",$name);
     //                      $name = str_replace("&","_",$name);
     //                      $name = str_replace(",","_",$name);
     //                      $name = str_replace("'","_",$name);

     //                      $content = file_get_contents($logo);
     //                      file_put_contents('/var/www/vhosts/oakjob/public_html/company_logos/'.$name.'.png', $content);
     //                      memcache_set($memcache_obj_elastic, $name, $name.'.png', false, time()+86400);

     //                      $insertRecords = array();
     //                      $insertRecords['company_name'] = $company_name;
     //                      $insertRecords['company_key'] = $name;
     //                      $insertRecords['image_name'] = $name.'.png';

     //                      $objDBManager->insertRecord('companies_logos',$insertRecords);

     //                    }

     //                      $key = str_replace(" ","_",$company_name);
     //                      $key = str_replace(".","_",$key);
     //                      $key = str_replace("&","_",$key);
     //                      $key = str_replace(",","_",$key);

     //                      $value  = memcache_get($memcache_obj_elastic, $key);
     //                      if(empty($value)){
     //                        memcache_set($memcache_obj_elastic, $key, 'logo not found', false, time()+86400);
     //                        $insertRecords = array();
     //                        $insertRecords['company_name'] = $company_name;
     //                        $insertRecords['company_key'] = $key;
     //                        $insertRecords['image_name'] = 'logo not found';
     //                        $objDBManager->insertRecord('companies_logos',$insertRecords);
     //                      }
     //                  }

                      function getLogoImageUrl($name){
                      	$logoUrl = "";
                      	try{
                      	 global $memcache_obj_elastic;
                      	 global $logoCounter;
                      	 global $logosArray;

                      
                      $key = str_replace(" ","_",$name);
                      $key = str_replace(".","_",$key);
                      $key = str_replace("&","_",$key);
                      $key = str_replace(",","_",$key);
                      $key = str_replace("'","_",$key);
                      
                
                      $value  = memcache_get($memcache_obj_elastic, $key);

                      if(empty($value) || $value == 'logo not found'){
                        $logoUrl = '';
                      }else{
                       $logoUrl = $value;
                      }

                    if($logoUrl != ''){
                      $logoUrl = 'http://s2.oakjobalerts.com/company_logos/'.$logoUrl;

                        $data = file_get_contents($logoUrl);
						if(empty($data)){
							$logoUrl = 'http://s2.oakjobalerts.com/company_logos_genric/'.$logosArray[$logoCounter];
						}
                    }
                    else{
                    $logoUrl = 'http://s2.oakjobalerts.com/company_logos_genric/'.$logosArray[$logoCounter];

                    if($logoCounter>=4)
                      $logoCounter = 0;
                    else
                      $logoCounter++;
                      }
                  }catch(Exception $e1){}

                      return $logoUrl;
                  }


$mobileAdStyle='';

if(strstr(strtolower($_SERVER['HTTP_USER_AGENT']),'mobile') == true){
	$mobileAdStyle="style='width: 100%;'";
}

if(isset($_GET['uri'])) {
	$requestedAjaxUrl = urldecode($_GET['uri']);
	$temp = explode('?',$requestedAjaxUrl);
	$uriAry = array();
	$uriAry['domain'] = $temp[0];
	$temp2 = explode('&',$temp[1]);
	$keyword='';
	$location='';
	$radius='';
	$domain='';
	$second_slot="false";
	
	foreach($temp2 as $k=>$v) {
		if (strpos($v, 'start=') !== false) {
			if(isset($_GET['page']) && !empty($_GET['page'])) {
				$page_start = (int)$_GET['page']*20;
				$temp2[$k] = 'start='.$page_start;
				if($page_start==20){
					$second_slot="true";
				}
			} else {
				$temp2[$k] = $v;
			}
		}
		if (strpos($v, 'keyword=') !== false) {
			$keyword=str_replace("keyword=","",$v);
			$keyword=str_replace(" ","+",$keyword);
			$temp2[$k] ="keyword=".$keyword;
		}
		if (strpos($v, 'zipcode=') !== false) {
			$location=str_replace("zipcode=","",$v);
			$temp2[$k] = 'zipcode='.urlencode($location);
		}
		if (strpos($v, 'radius=') !== false) {
			$radius=str_replace("radius=","",$v);
		}
		if (strpos($v, 'domain=') !== false) {
			$domain=str_replace("domain=","",$v);
		}
	 }
	$updatedURL = $temp[0].'?'.implode('&',$temp2);

	try {
		
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $updatedURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);  
        
        $resultAry = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $output), true );
        // $resultAry = json_decode($output); 
        echo '<div id="jobs-page-num-'.$_GET['page'].'">';
        foreach($resultAry['jobs'] as $key=>$val) 
        {
			$val = (array)$val;
			////////////////// CALCULATIONS ///////////////////
				$queryStr = "url=".str_replace("&","##", $val['joburl']).
				"&sourcename=".urlencode($val['sourcename']).
				"&source=".urlencode($val['source']).
				"&city=".urlencode($val['city']).
				"&state=".urlencode($val['state']).
				"&employer=".urlencode($val['employer']).
				"&title=".urlencode($val['title']).
				"&cpc=".$val['cpc'].
				"&gcpc=".$val['gross_cpc'];
				$compressed = base64_encode($queryStr) ;

				$onmouse = (strpos(strtolower($val['sourcename']), 'indeed1234') !== false) ? (isset($val['onmousedown']))?$val['onmousedown']:'' : '';
				
				$forExtraValues= (isset($_GET['clicktype'])) ? '&clicktype='.$_GET['clicktype'].'' : ''; // for notification link
				$redirectURL = (strpos(strtolower($val['sourcename']), 'indeed1234') !== false) ?  $val['joburl'] : "RedirectWEB.php?q=".$compressed."&token=".$val['id'].$forExtraValues."";
			//////////////////////////////////////////////////
			?>
			<div class="joblists clearfix row">
			    <div class="col-lg-12 col-xs-12">
				<?php	
				if($key == 5 || $key == 15 )	{
					$redirectRecords = "";
					$name = "smsjob_banner_ad.jpg";
					$Smsjobinput = $keyword.'|#|'.$location .'|#||#||#|'.$domain.'_1'.'|#|smsjob|#||#||#||#||#|';
					 $redirectRecords = base64_encode($Smsjobinput);
					$url = "http://smsjob.us?token=" . $redirectRecords;
					$queryStr = "url=".$url."&sourcename=smsjob_web_banner&source=smsjob_web_banner&city=".$location."&state=".$location;
					$compressed = urlencode(base64_encode($queryStr)) ;
					$redirecturl = "http://" . $domain . "/RedirectWEB.php?q=" . $compressed;
					echo "<div id='div-gpt-ad-1' class='text-center add-space'>
					<a href='". $redirecturl . "' target='_blank'>
					<img class='img-responsive' ".$mobileAdStyle." alt='' src='http://". $domain."/topresume_add_img/".$name . "' />
					</a></div>";	   
				}
				if($key == 10 || $key == 20  ) {
					$redirectRecords = "";
					$name = "smsjob_banner_ad_new.jpg";
					$Smsjobinput = $keyword.'|#|'.$location .'|#||#||#|'.$domain.'_2'.'|#|smsjob|#||#||#||#||#|';
					$redirectRecords = base64_encode($Smsjobinput);
					$url = "http://smsjob.us?token=" . $redirectRecords;
					$queryStr = "url=".$url."&sourcename=smsjob_web_banner_1&source=smsjob_web_banner_1&city=".$location."&state=".$location;
					$compressed = urlencode(base64_encode($queryStr)) ;
					$redirecturl = "http://" . $domain . "/RedirectWEB.php?q=" . $compressed;
					echo "<div id='div-gpt-ad-1' class='text-center add-space' style='text-align:center;'>
					<a href='". $redirecturl . "' target='_blank'>".
					"<img class='img-responsive' ".$mobileAdStyle." alt='' src='http://". $domain."/topresume_add_img/".$name . "' />" 
					."</a></div>";
				}
				   if(isset($val['employer'])){
				   		$logoUrl = getLogoImageUrl($val['employer']);
				   }
				   	else{

				   	// 	global $logosArray;

				   	// 	$logoUrl = 'http://s2.oakjobalerts.com/company_logos_genric/'.$logosArray[$logoCounter];

	       //              if($logoCounter>=4)
	       //                $logoCounter = 0;
	       //              else
	       //                $logoCounter++;
	       //                }
				   	}
                ?>
                </div>
				<div class="col-lg-10 col-xs-12">
						<span class="left-img-des">
				          <div class="left-img-des-wrap">
				          <?php if($logoUrl != '') { ?>
				          <img src='<?php echo $logoUrl; ?>' style="">
				          <?php } ?>
				          </div>
				      </span>
			          <span class="right-txt-des">
					<a target="_blank" href="<?php echo $redirectURL; ?>" onmousedown=""><?php echo $val['title']; ?></a>
					<p>
					 <strong>
						 <?php echo $val['employer']; ?>
						<span class="location"><?php echo ' - '.$val['city'].', '.$val['state']; ?> 
						<!-- - <b style="color:black;"><?php echo $val['displaysource']; ?></b> -->
						</span>
					 </strong>
					</p>
					<p class="post-description-text description-web">
						<?php 
						 $truncated = utf8_decode((strlen(strip_tags(html_entity_decode($val['description']))) > 200) ? substr(strip_tags(html_entity_decode($val['description'])), 0, 200) . '...' : strip_tags(html_entity_decode($val['description']))); 
						 echo utf8_decode($truncated);
						?>
					</p>
					<p class="post-description-text description-mob">
						<?php 
						 $truncated = utf8_decode((strlen(strip_tags(html_entity_decode($val['description']))) > 70) ? substr(strip_tags(html_entity_decode($val['description'])), 0, 70) . '...' : strip_tags(html_entity_decode($val['description']))); 
						 echo utf8_decode($truncated);
						?>
					</p>
					<div class="job-date-desktop">
						<?php
                          $now        = time();
                          $your_date  = strtotime($val['postingdate']);
                          $datediff   = $now - $your_date;
                          $dateAgo    = floor($datediff / (60 * 60 * 24));
                          $beforedate = ($dateAgo > 0) ? "$dateAgo days ago" : "New"; ?> <span class=date><?php echo $beforedate; ?></span>
					</div>
					</span>
				</div>
				<div class="col-lg-2 col-xs-12 post-btn-set">
					<div class="job-date-mobile pull-left">
						 <?php
                          $now        = time();
                          $your_date  = strtotime($val['postingdate']);
                          $datediff   = $now - $your_date;
                          $dateAgo    = floor($datediff / (60 * 60 * 24));
                          $beforedate = ($dateAgo > 0) ? "$dateAgo days ago" : "New"; ?> <span class=date><?php echo $beforedate; ?></span>
					</div>
					<div class="apply-btn pull-right">
						<a target="_blank" href="<?php echo $redirectURL; ?>">
						<button class="btn btn-default apply" type="button">Apply</button></a>
                        <button type="button" id='<?= $val['id'] ?>' class="btn btn-default" <?php 
                        if (isset($_COOKIE['email'])) { ?> 
							onclick="return savejobs('<?= $val['id'] ?>','<?php echo addslashes($val['title']); ?>','<?php echo addslashes($val['employer']); ?>','<?= $val['joburl'] ?>','<?php echo $val['city'] . ", " . $val['state']; ?>','<?php echo addslashes(strip_tags($val['description'])); ?>','<?php echo $val['postingdate']; ?>');"  <?php } ?> ><i class="fa fa-star-o"></i></button>
						
					</div>
				</div>
			</div>
			<?php
		}
		echo '</div>';
     }
    catch(Exception $e) {
	  echo 'Message: ' .$e->getMessage();
	 }
} else {
	echo 'Invalid URL requested.';
}
?>


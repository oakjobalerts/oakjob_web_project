/*!
 * Start Bootstrap - Freelancer Bootstrap Theme (http://startbootstrap.com)
 * Code licensed under the Apache License v2.0.
 * For details, see http://www.apache.org/licenses/LICENSE-2.0.
 */

// jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $('.page-scroll a').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});

// Floating label headings for the contact form
$(function() {
    $("body").on("input propertychange", ".floating-label-form-group", function(e) {
        $(this).toggleClass("floating-label-form-group-with-value", !! $(e.target).val());
    }).on("focus", ".floating-label-form-group", function() {
        $(this).addClass("floating-label-form-group-with-focus");
    }).on("blur", ".floating-label-form-group", function() {
        $(this).removeClass("floating-label-form-group-with-focus");
    });
});

// Highlight the top nav as scrolling occurs
$('body').scrollspy({
    target: '.navbar-fixed-top'
})

// Closes the Responsive Menu on Menu Item Click
$('.navbar-collapse ul li a').click(function() {
    $('.navbar-toggle:visible').click();
});

// Functions for auto fill
function autocomplet() 
{
    if(typeof keyUpAjax != 'undefined'){
     keyUpAjax.abort();
   };
    
    var min_length = 2; // min caracters to display the autocomplete
    var keyword = $('#where').val();
    if (keyword.length >= min_length) {
        keyUpAjax = $.ajax({
                      url: '/ajax_autofill_new.php',
                      dataType: "jsonp",
                      jsonp : "callback",
                      jsonpCallback: "callback_new",
                      crossDomain: true,
                      data: {keyword:keyword},
                      headers: { 'Access-Control-Allow-Origin' : '*' },
                      error: function(xhr, status, error) {
                                // console.log(error);
                      }
                  });
    } else {
        $('#country_list_id').hide();
    }
}

function callback_new(response){
    //alert(response);
    $('#country_list_id').show();
    $('#country_list_id').html(response);
}

function set_item(item) {
    // change input value
    $('#where').val(item);
    // hide proposition list
    $('#country_list_id').hide();
}

// Functions for auto fill end

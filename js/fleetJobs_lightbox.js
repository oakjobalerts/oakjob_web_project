      $( document ).ready(function() {
	    
	    var q= getParameterByName('q');
	    var l= getParameterByName('l');
	    var e= getParameterByName('e');

	    var html = '<section><div class="container"> <div class="row"> <div class="col-lg-12"> <button style="display: none;" type="button" class="" id="popupbtn" data-toggle="modal" data-target="#myModal"> </button> <div class="modal fade popup" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true"></span><span class="sr-only">Close</span></button> <h4 class="modal-title" id="myModalLabel">Create Job Alert</h4> </div> <div class="modal-body"> <form role="form" action="" method="post" onsubmit="return validate();"> <div class="form-group" style="text-align: left;"> <label for="keyword">Title</label> <input type="text" class="form-control" onfocus="$(this).css("border-color","#dce4ec");$(this).attr("placeholder","Job title, keywords or company name");" id="title" value="'+q+'" placeholder="Job title, keywords or company name"> </div> <div class="form-group" style="text-align: left;"> <label for="description">Location</label> <input type="text" class="form-control" onfocus="$(this).css("border-color","#dce4ec");$(this).attr("placeholder","City, state or zipcode");" id="location" value="'+l+'" placeholder="City, state or zipcode" onkeyup="autocomplet()"> <ul id="country_list_id" style="display:none;width: 90.5%;margin-top: 0;"></ul>  </div> <div class="form-group" style="text-align: left;"> <label for="email">Email</label> <input type="text" class="form-control" onfocus="$(this).css("border-color","#dce4ec");$(this).attr("placeholder","Email Address");" id="email" value="'+e+'" placeholder="Email Address"> </div> </form> <span>By clicking "create alert" I agree to the FleetJobs.com. <a href="#">Terms of Use</a></span></div> <div class="modal-footer text-center" style="text-align: center;"> <button class="btn btn-default" onclick="validate();" style="background-color:#2e358e !important; border-color:#2e358e !important;" type="submit">Create Alert</button> <a data-dismiss="modal" href="javascript:;" id="close" class="close_btn">Close</a> </div> </div> </div> </div> </div> </div> </div> </section>';
	    $("body").append(html);
	    $("#popupbtn").click();
            
           
	});
	
	function getParameterByName(name) {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
	
	function validate(){
	    
	 if($('#title').val()==''){
            $('#title').css('border-color', 'red');
            $('#title').val('');
            $('#title').attr('placeholder', 'Title is required field.');
            
            return false;            
          }
	  if($('#location').val()==''){
            $('#location').css('border-color', 'red');
            $('#location').val('');
            $('#location').attr('placeholder', 'Location is required field.');
            
            return false;            
          }
          if($('#email').val()==''){
            $('#email').css('border-color', 'red');
            $('#email').val('');
            $('#email').attr('placeholder', 'Email is required field.');
            
            return false;            
          }
          if( !isValidEmailAddress( $('#email').val() ) ) {
            
            $('#email').css('border-color', 'red');
            $('#email').val('');
            $('#email').attr('placeholder', 'Email is invalid.');
            
            return false; 
            
          }
	  var title = $('#title').val();
	  var location = $('#location').val();
	  var email = $('#email').val();
          
        
	 
          
          var postData = {
            
                'email' : email,
                'location' : location,
                'title' : title,
                'source' : 'FleetJobs'
          };
           $.ajax({
                    //type: "POST",
                    url: "http://oakjobalerts.com/alertWrite.php",
                    dataType: "jsonp",
                    jsonp : "callback",
                    jsonpCallback: "callback",
                    crossDomain: true,
                    data: postData , //assign the var here
                    headers: { 'Access-Control-Allow-Origin' : '*' },
                    error: function(xhr, status, error) {
                              //alert(error);
                    },
                    //success: 'jsonpcallback'
                      
        });
      
     
    
          
     
	  
}
function closePopup(){
      
      $("#close").click();
      
}
function callback(response){
      
                   // alert(response);
                   
                  $('.modal-title').text('Message'); 
                  $('.modal-body').html('<div><p></p><p>Your job alert has been created! <a href="javascript:;" onclick="closePopup();"> Click here </a> to close this window.</p><p></p><div>');  
                  $('.modal-footer').hide();
}
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
};

// Functions for auto fill
function autocomplet() 
{
    if(typeof keyUpAjax != 'undefined'){
     keyUpAjax.abort();
   };
    
    var min_length = 2; // min caracters to display the autocomplete
    var keyword = $('#location').val();
    if (keyword.length >= min_length) {
        keyUpAjax = $.ajax({
                      url: 'http://oakjobalerts.com/ajax_autofill_new.php',
                      dataType: "jsonp",
                      jsonp : "callback",
                      jsonpCallback: "callback_new",
                      crossDomain: true,
                      data: {keyword:keyword},
                      headers: { 'Access-Control-Allow-Origin' : '*' },
                      error: function(xhr, status, error) {
                                // console.log(error);
                      }
                  });
    } else {
        $('#country_list_id').hide();
    }
}

function callback_new(response){
    //alert(response);
    $('#country_list_id').show();
    $('#country_list_id').html(response);
}

function set_item(item) {
    // change input value
    $('#location').val(item);
    // hide proposition list
    $('#country_list_id').hide();
}

// Functions for auto fill end
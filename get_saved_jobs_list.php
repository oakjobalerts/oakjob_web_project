<?php
ob_start();
include('include/common.php');
require_once('include/DBManager.php');
require_once('include/sources_config.php');
session_start();
$objDBManager = new DBManager();//initialize db connection
//	Fetch Saved Jobs of Logged In Users
if(isset($_COOKIE[ 'email'])) { 
	if(PROVIDER == '')	{
		$provider = (isset($_COOKIE['source']))?$_COOKIE['source']:'web';
	}	else 	{
		$provider = PROVIDER;
	}
	$appendCond 	= ' AND provider ="'.$provider.'"';
	$appendCond 	.= ' AND title ="'.$_GET['title'].'"';
	$appendCond 	.= ' AND employer ="'.$_GET['employer'].'"';
	$appendCond 	.= ' AND location ="'.$_GET['location'].'"';
	$query 			= 'Select * from  tbl_saved_jobs where email_id="'.$_COOKIE['email'].'"'.$appendCond;
	$savedJobList	= $objDBManager->fetchRecord($query);
	if(count($savedJobList) > 0) {
		echo json_encode(['status'=>1]);
	} else {
		echo json_encode(['status'=>0]);
	}
}  else  {
	$savedJobList	= [];
	echo json_encode(['status'=>0]);
}
?>

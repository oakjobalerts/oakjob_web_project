<?php
require_once('include/DBManager.php');
$objDBManager = new DBManager();//initialize db connection
$objDBManager->createConnection();

if(!empty($_POST)) {
   $appendWhere = '';
  
    if(PROVIDER == '')
      $provider = (isset($_COOKIE['source']))       ?       $_COOKIE['source']      :       'web';
      else
      $provider = PROVIDER;
       
    $appendWhere =  ' AND provider ="'.$provider.'"';
        
   $whereFields  = 'f_id="'.$_POST['f_id'].'"'.$appendWhere;
   $fieldArray['password'] = md5($_POST['password']);
   $fieldArray['f_id'] = '';
   
   $objDBManager->updateRecord('tbl_signup_users',$fieldArray,$whereFields,false,'');
    header('location:changepassword.php?success=password');                
   
}


if(isset($_GET['token']) && !empty($_GET['token'])) {
    
    
   $whereFields = array();
   $whereFields['f_id'] =  $_GET['token'];
   $checkUser = $objDBManager->getRecord('tbl_signup_users',$whereFields,false,'');
   
}
function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?=TITLE?></title>

    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <link rel="shortcut icon" href="<?=FAVICONPATH?>favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?=FAVICONPATH?>favicon.ico" type="image/x-icon">
    <!-- Custom CSS -->
    <link href="css/oakstyle.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="">
    

    <!-- Navigation -->
    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php"><img src="<?=IMAGE_BASE_URL?>/logo1.png"> <?=HEADER_TITLE?></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                   <!-- <li class="page-scroll">
                        <a href="#portfolio">Portfolio</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#about">About</a>
                    </li>-->
                    <li class="page-scroll">
                    
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- section -->
    <section>
        <div class="container login">
            <div class="row">
                <div class="col-lg-12">
                    
                    
                    <div class="intro-text text-center">
                        
                       <form class="navbar-form" action="changepassword.php" method="post" onsubmit="return formValidation();" role="search">
                        <div class="form-group text-center">
                        <?php
                        if(isset($_GET['success']) && !empty($_GET['success'])) {
                    
                            echo "<h5 style='color:#7cc243;'>Your password has been reset. <a href='sign_in.php' style='text-decoration:underline;color:black;'>click here</a> to login.</h5>";
                          
                          }else if(count($checkUser) == 0) {
                    
                            echo "<h5 style='color:red;'>This is invalid request.</h5>";
                          
                          }else{                    
                          ?>    
                            
                        <h4>Change Password</h4>
                                                   
                        <input type="password" id="password" name="password" onfocus="$(this).css('border-color','#dce4ec');$(this).attr('placeholder','New Password');" class="form-control input-lg" placeholder="New Password">
                        <input type="password" id="repassword" name="repassword" onfocus="$(this).css('border-color','#dce4ec');$(this).attr('placeholder','Confirm Password');" class="form-control input-lg" placeholder="Confirm Password">
                          <input type="hidden" id="f_id" name="f_id" value="<?=$_GET['token']?>">
                         <button type="submit" class="btn btn-default">Save</button>
                         <div class="row">
                         <p><a class="text-left" href="sign_in.php"><i class="fa-chevron-left" style="font-size: 14px;"></i>Go to Sign in</a></p>
                         
                         </div>
                        </div>
                        <?php } ?>
                     
                      </form>
                      
                     
                    </div>
                </div>
            </div>
        </div>
    </section>

   

    <?php include("include/footer.php"); ?>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top page-scroll visible-xs visble-sm">
        <a class="btn btn-primary" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>

   <!-- jQuery Version 1.11.0 -->
    <script src="js/jquery-1.11.0.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    

    <!-- Custom Theme JavaScript -->
    <script src="js/oak.js"></script> 
    
    <script type="text/javascript">
    
    function formValidation(){
        
        var password           = $('#password').val();
        var repassword           = $('#repassword').val();
            
            if(password == ''){
                
                $('#password').css('border-color','red');
                $('#password').attr('placeholder','Field is mandatory.');
                return false;
                
            }else if(password != repassword){
                
                $('#repassword').css('border-color','red');
                $('#repassword').attr('placeholder','Password and confirm password not matched.');
                return false;
                
            }   
                
    }   
    </script>
    
    
    
    

</body>

</html>

